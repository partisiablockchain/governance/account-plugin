package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.util.ListUtil;

/**
 * Used to track how much of a particular external coin has been converted to gas, and how much gas
 * the conversion has created.
 */
@Immutable
public final class ConvertedExternalCoin implements StateSerializable {

  /** Utility for updating a list of converted external coins. */
  static final ListUtil<ConvertedExternalCoin> COINS_LIST =
      new ListUtil<>(ConvertedExternalCoin::create, ConvertedExternalCoin::isEmpty);

  /** How much of the coin that has been converted to gas in total. */
  public final Unsigned256 convertedCoins;

  /** How much gas the conversion has created in total. */
  public final Unsigned256 convertedGasSum;

  /** Default constructor. */
  @SuppressWarnings("unused")
  public ConvertedExternalCoin() {
    convertedCoins = Unsigned256.ZERO;
    convertedGasSum = Unsigned256.ZERO;
  }

  /**
   * Creates a new converted external coin.
   *
   * @param convertedCoins The amount of coin that has been converted.
   * @param convertedGasSum The amount of gas this conversion has created.
   */
  public ConvertedExternalCoin(Unsigned256 convertedCoins, Unsigned256 convertedGasSum) {
    this.convertedCoins = convertedCoins;
    this.convertedGasSum = convertedGasSum;
  }

  /**
   * Create a new converted external coin object from its state.
   *
   * @param accessor The state to initialize the object from.
   * @return A new converted external coin with the state of the accessor
   */
  public static ConvertedExternalCoin createFromStateAccessor(StateAccessor accessor) {
    return new ConvertedExternalCoin(
        accessor.get("convertedCoins").cast(Unsigned256.class),
        accessor.get("convertedGasSum").cast(Unsigned256.class));
  }

  /**
   * Get the total amount of this particular coin that has been converted.
   *
   * @return the amount of converted coin
   */
  public Unsigned256 getConvertedCoins() {
    return convertedCoins;
  }

  /**
   * Get the total amount of gas this particular coin has been converted into.
   *
   * @return the total amount of gas for this coin.
   */
  public Unsigned256 getConvertedGasSum() {
    return convertedGasSum;
  }

  /**
   * Create a new converted coin initialized with an empty state.
   *
   * @return a new external converted coin.
   */
  public static ConvertedExternalCoin create() {
    return new ConvertedExternalCoin(Unsigned256.ZERO, Unsigned256.ZERO);
  }

  /**
   * Add an amount of this coin that has been converted, and the corresponding amount of gas the
   * conversion created.
   *
   * @param convertedCoinsDiff The amount of coin that was converted
   * @param correspondingGas The amount of gas the conversion created
   * @return The new state with the new conversion added
   */
  public ConvertedExternalCoin addConvertedCoins(
      Unsigned256 convertedCoinsDiff, Unsigned256 correspondingGas) {
    return new ConvertedExternalCoin(
        convertedCoins.add(convertedCoinsDiff), convertedGasSum.add(correspondingGas));
  }

  boolean isEmpty() {
    return getConvertedCoins().equals(Unsigned256.ZERO)
        && getConvertedGasSum().equals(Unsigned256.ZERO);
  }
}
