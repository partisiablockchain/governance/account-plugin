package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;

/**
 * A vesting account contains information about the total amount of vested tokens together with the
 * amount of tokens currently released. Each vesting account is for a specific vesting schedule and
 * an account on the blockchain can have multiple vesting accounts.
 */
@Immutable
final class InitialVestingAccount implements StateSerializableInline {

  public final long tokens;
  public final long releasedTokens;
  public final long tokenGenerationEvent;
  public final long releaseDuration;
  public final long releaseInterval;

  /** Token generation event for all vesting schedules - corresponds to 2022-08-31. */
  static final long TGE = 1661904000000L;

  /** Start time of pausing the release of vested presale tokens - corresponds to 2023-05-20. */
  static final long PAUSE_RELEASE_START_TIME = 1684540800000L;

  /** End time of pausing the release of vested presale tokens - corresponds to 2024-03-20. */
  static final long PAUSE_RELEASE_END_TIME = 1710892800000L;

  /** Release duration for public token sale. */
  static final long PUBLIC_SALE_RELEASE_DURATION = 62899200000L;

  /** Release interval for public token sale. */
  static final long PUBLIC_SALE_RELEASE_INTERVAL = 7862400000L;

  InitialVestingAccount() {
    this.tokens = 0;
    this.releasedTokens = 0;
    this.tokenGenerationEvent = 0;
    this.releaseDuration = 0;
    this.releaseInterval = 0;
  }

  private InitialVestingAccount(
      long tokens,
      long releasedTokens,
      long tokenGenerationEvent,
      long releaseDuration,
      long releaseInterval) {
    this.tokens = tokens;
    this.releasedTokens = releasedTokens;
    this.tokenGenerationEvent = tokenGenerationEvent;
    this.releaseDuration = releaseDuration;
    this.releaseInterval = releaseInterval;
  }

  public static InitialVestingAccount createFromStateAccessor(StateAccessor accessor) {
    return new InitialVestingAccount(
        accessor.get("tokens").longValue(),
        accessor.get("releasedTokens").longValue(),
        accessor.get("tokenGenerationEvent").longValue(),
        accessor.get("releaseDuration").longValue(),
        accessor.get("releaseInterval").longValue());
  }

  /**
   * Creates a new InitialVestingAccount with a vesting schedule.
   *
   * @param tokens the amount of tokens who are to be vested for a given tier
   * @return the created initial vesting account with released tokens initially set to 0
   */
  public static InitialVestingAccount create(
      long tokens, long tokenGenerationEvent, long releaseDuration, long releaseInterval) {
    return new InitialVestingAccount(
        tokens, 0, tokenGenerationEvent, releaseDuration, releaseInterval);
  }

  /**
   * Calculates the amount of vested tokens ready for release.
   *
   * <p>The release of vested presale tokens are paused in the run-up to a potential exchange
   * launch. Token buyers during public sale at TGE are not affected by this.
   *
   * @param blockProductionTime block production time
   * @return amount of vested tokens ready for release
   */
  long getAmountToRelease(long blockProductionTime) {
    if (!isPublicSale()) {
      blockProductionTime = subtractPause(blockProductionTime);
    }
    if (blockProductionTime >= tokenGenerationEvent + releaseDuration) {
      return tokens - releasedTokens;
    } else {
      long amountToRelease = tokens / (releaseDuration / releaseInterval);
      long batches = Math.max(0, (blockProductionTime - tokenGenerationEvent) / releaseInterval);
      long totalToRelease = amountToRelease * batches;

      return totalToRelease - releasedTokens;
    }
  }

  /**
   * Checks if the vesting schedule is from a public sale.
   *
   * @return true if vesting schedule is from a public sale otherwise false
   */
  private boolean isPublicSale() {
    return releaseDuration == PUBLIC_SALE_RELEASE_DURATION
        && releaseInterval == PUBLIC_SALE_RELEASE_INTERVAL;
  }

  /**
   * Subtracts the pause from the block production time to avoid including it when calculating the
   * amount of vested tokens to release.
   *
   * <p>Depending on if the current block production time is before, during, or after the pause
   * duration the time is adjusted as follows:
   *
   * <ul>
   *   <li>If blockProductionTime < {@link InitialVestingAccount#PAUSE_RELEASE_START_TIME} return
   *       blockProductionTime
   *   <li>If blockProductionTime < {@link InitialVestingAccount#PAUSE_RELEASE_END_TIME} return
   *       {@link InitialVestingAccount#PAUSE_RELEASE_START_TIME}
   *   <li>If blockProductionTime > {@link InitialVestingAccount#PAUSE_RELEASE_END_TIME} return
   *       blockProductionTime - ({@link InitialVestingAccount#PAUSE_RELEASE_END_TIME} - {@link
   *       InitialVestingAccount#PAUSE_RELEASE_START_TIME})
   * </ul>
   *
   * @param blockProductionTime block production time
   * @return block production time adjusted for pause duration
   */
  static long subtractPause(long blockProductionTime) {
    return Math.max(
        Math.min(PAUSE_RELEASE_START_TIME, blockProductionTime),
        blockProductionTime - (PAUSE_RELEASE_END_TIME - PAUSE_RELEASE_START_TIME));
  }

  public InitialVestingAccount releaseTokens(long amount) {
    return new InitialVestingAccount(
        tokens, releasedTokens + amount, tokenGenerationEvent, releaseDuration, releaseInterval);
  }

  public long leftToRelease() {
    return tokens - releasedTokens;
  }
}
