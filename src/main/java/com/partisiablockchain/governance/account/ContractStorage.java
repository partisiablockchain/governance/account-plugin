package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.Objects;

/** Keeps track of a contract's state. */
@Immutable
public final class ContractStorage implements StateSerializable {
  /** The latest time, as a Unix timestamp, at which this contract paid a storage fee. */
  public final Long latestStorageFeeTime;

  /** The gas balance of this contract. */
  public final Signed256 balance;

  /**
   * The time, as a Unix timestamp, at which this contract was iced, or null if it is not iced. If a
   * contract is iced, it is not immediately deleted if it runs out of gas. Instead, the following
   * rules apply:
   *
   * <pre>
   * 1. If the contract was iced less than a week ago, it is not deleted.
   * 2. If the contract was iced less than a month ago,
   *    AND it has a positive amount of ice stakes associated, then it is not deleted.
   * 3. If the contract was iced more than a month ago,
   *    then it needs to have enough ice staked to it to cover the debt.
   * 4. In all other cases, the contract is deleted.
   * </pre>
   */
  public final Long icedTime;

  /**
   * The BYOC that this contract holds. The index of a coin corresponds to a coin in the global
   * state, see {@link AccountStateGlobal#coins}
   */
  public final FixedList<AccountCoin> accountCoins;

  /**
   * Pending transfers in this contract that are yet to be committed/aborted in the two phase
   * protocol. The pending transfer is identified by a hash, which is the identifier of the
   * transaction that initiated the transfer.
   */
  public final AvlTree<Hash, TransferInformation> storedPendingTransfers;

  /** The amount an account has ice staked to this contract. See also {@link IceStake}. */
  public final AvlTree<BlockchainAddress, Long> iceStakes;

  /**
   * Pending ice stakes, that are yet to be committed/aborted in the two phase protocol. The pending
   * ice stake is identified by a hash, which is the identifier of the transaction that initiated
   * the ice staking.
   */
  public final AvlTree<Hash, IceStake> storedPendingIceStakes;

  /** The amount of MPC tokens this contract holds. */
  public final long mpcTokens;

  ContractStorage() {
    this.latestStorageFeeTime = null;
    this.balance = Signed256.create();
    this.icedTime = null;
    this.accountCoins = FixedList.create();
    this.storedPendingTransfers = AvlTree.create();
    this.iceStakes = AvlTree.create();
    this.storedPendingIceStakes = AvlTree.create();
    this.mpcTokens = 0;
  }

  ContractStorage(
      Long latestStorageFeeTime,
      Signed256 balance,
      Long icedTime,
      FixedList<AccountCoin> accountCoins,
      AvlTree<Hash, TransferInformation> storedPendingTransfers,
      AvlTree<BlockchainAddress, Long> iceStakes,
      AvlTree<Hash, IceStake> storedPendingIceStakes,
      long mpcTokens) {
    this.latestStorageFeeTime = latestStorageFeeTime;
    this.balance = balance;
    this.icedTime = icedTime;
    this.accountCoins = accountCoins;
    this.storedPendingTransfers = storedPendingTransfers;
    this.iceStakes = iceStakes;
    this.storedPendingIceStakes = storedPendingIceStakes;
    this.mpcTokens = mpcTokens;
  }

  /**
   * Create a new contract storage object with a provided time for latest storage fee time.
   *
   * @param latestStorageFeeTime the latest storage fee time
   * @return a new contract storage object.
   */
  static ContractStorage create(Long latestStorageFeeTime) {
    return new ContractStorage(
        latestStorageFeeTime,
        Signed256.create(),
        null,
        FixedList.create(),
        AvlTree.create(),
        AvlTree.create(),
        AvlTree.create(),
        0);
  }

  /**
   * Create a storage object from a state accessor.
   *
   * @param accessor the state accessor
   * @return a storage object read from the state accessor.
   */
  static ContractStorage createFromStateAccessor(StateAccessor accessor) {
    Long latestStorageFeeTime = accessor.get("latestStorageFeeTime").cast(Long.class);

    FixedList<AccountCoin> accountCoins =
        FixedList.create(
            accessor.get("accountCoins").getListElements().stream()
                .map(AccountCoin::createFromStateAccessor)
                .toList());

    AvlTree<Hash, TransferInformation> storedPendingTransfers =
        StoredPendingTransfers.createFromStateAccessor(accessor.get("storedPendingTransfers"));

    AvlTree<BlockchainAddress, Long> iceStakes =
        accessor.get("iceStakes").typedAvlTree(BlockchainAddress.class, Long.class);
    AvlTree<Hash, IceStake> pendingIceStakes =
        IceStake.createPendingIceStakes(accessor.get("storedPendingIceStakes"));
    Long icedTime = accessor.get("icedTime").cast(Long.class);

    Signed256 balance = Signed256.createFromStateAccessor(accessor.get("balance"));
    long mpcTokenBalance = accessor.get("mpcTokens").longValue();

    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        accountCoins,
        storedPendingTransfers,
        iceStakes,
        pendingIceStakes,
        mpcTokenBalance);
  }

  Long latestStorageFeeTime() {
    return latestStorageFeeTime;
  }

  ContractStorage updateLatestStorageFeeTime(long latestStorageFeeTime) {
    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        accountCoins,
        storedPendingTransfers,
        iceStakes,
        storedPendingIceStakes,
        mpcTokens);
  }

  ContractStorage addBalance(Unsigned256 diff) {
    Signed256 updatedBalance = balance.add(diff);
    Long updatedIceTime = updatedBalance.isPositive() ? null : icedTime;
    return new ContractStorage(
        latestStorageFeeTime,
        balance.add(diff),
        updatedIceTime,
        accountCoins,
        storedPendingTransfers,
        iceStakes,
        storedPendingIceStakes,
        mpcTokens);
  }

  ContractStorage deductBalance(Unsigned256 diff, Long currentTime) {
    Signed256 updatedBalance = balance.subtract(diff);
    Long updatedIceTime = icedTime == null && !updatedBalance.isPositive() ? currentTime : icedTime;
    return new ContractStorage(
        latestStorageFeeTime,
        updatedBalance,
        updatedIceTime,
        accountCoins,
        storedPendingTransfers,
        iceStakes,
        storedPendingIceStakes,
        mpcTokens);
  }

  Long getIcedTime() {
    return icedTime;
  }

  Signed256 getBalance() {
    return balance;
  }

  AvlTree<Hash, TransferInformation> getStoredPendingTransfers() {
    return storedPendingTransfers;
  }

  ContractStorage addPendingTransfer(Hash transactionId, TransferInformation transferInformation) {
    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        accountCoins,
        storedPendingTransfers.set(transactionId, transferInformation),
        iceStakes,
        storedPendingIceStakes,
        mpcTokens);
  }

  ContractStorage deductByoc(int coinIndex, String symbol, Unsigned256 amount) {
    Unsigned256 coinBalance = accountCoins.get(coinIndex).getBalance();
    if (coinBalance.compareTo(amount) < 0) {
      throw new IllegalArgumentException(
          "Can not deduct %s %s with a balance of %s".formatted(amount, symbol, coinBalance));
    }
    FixedList<AccountCoin> updatedByoc =
        AccountCoin.COINS_LIST.withUpdatedEntry(
            accountCoins, coinIndex, accountCoin -> accountCoin.deduct(amount));
    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        updatedByoc,
        storedPendingTransfers,
        iceStakes,
        storedPendingIceStakes,
        mpcTokens);
  }

  ContractStorage addByoc(int coinIndex, Unsigned256 amount) {
    FixedList<AccountCoin> updatedByoc =
        AccountCoin.COINS_LIST.withUpdatedEntry(
            accountCoins, coinIndex, accountCoin -> accountCoin.add(amount));
    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        updatedByoc,
        storedPendingTransfers,
        iceStakes,
        storedPendingIceStakes,
        mpcTokens);
  }

  ContractStorage addMpcToken(long mpcTokens) {
    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        accountCoins,
        storedPendingTransfers,
        iceStakes,
        storedPendingIceStakes,
        this.mpcTokens + mpcTokens);
  }

  ContractStorage deductMpcToken(long mpcTokens) {
    long updatedBalance = this.mpcTokens - mpcTokens;
    if (updatedBalance < 0) {
      throw new IllegalArgumentException("MPC token balance can not be negative");
    }
    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        accountCoins,
        storedPendingTransfers,
        iceStakes,
        storedPendingIceStakes,
        updatedBalance);
  }

  ContractStorage removePendingTransfer(Hash transactionId) {
    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        accountCoins,
        storedPendingTransfers.remove(transactionId),
        iceStakes,
        storedPendingIceStakes,
        mpcTokens);
  }

  /**
   * Get the balance of a particular BYOC held by this contract.
   *
   * @param index the index of the coin
   * @return the coin amount, 0 if it does not exist.
   */
  Unsigned256 getCoinBalance(int index) {
    if (index < accountCoins.size()) {
      return accountCoins.get(index).getBalance();
    } else {
      return Unsigned256.ZERO;
    }
  }

  FixedList<AccountCoin> getCoins() {
    return accountCoins;
  }

  IceStake getPendingIceStake(Hash transactionId) {
    return storedPendingIceStakes.getValue(transactionId);
  }

  long getIceStakeBy(BlockchainAddress account) {
    return iceStakes.getValue(account);
  }

  AvlTree<BlockchainAddress, Long> getIceStakes() {
    return iceStakes;
  }

  long getMpcTokens() {
    return mpcTokens;
  }

  ContractStorage addPendingIceStake(Hash transactionId, IceStake iceStake) {
    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        accountCoins,
        storedPendingTransfers,
        iceStakes,
        storedPendingIceStakes.set(transactionId, iceStake),
        mpcTokens);
  }

  private long getIceStakeAmount(BlockchainAddress account) {
    return Objects.requireNonNullElse(iceStakes.getValue(account), 0L);
  }

  private ContractStorage updateIceStakeAmount(BlockchainAddress account, long amount) {
    long newValue = getIceStakeAmount(account) + amount;
    if (newValue < 0) {
      throw new IllegalArgumentException("Ice stake amount cannot be negative");
    }

    AvlTree<BlockchainAddress, Long> updatedIceStakes = iceStakes;
    if (newValue == 0) {
      updatedIceStakes = updatedIceStakes.remove(account);
    } else {
      updatedIceStakes = updatedIceStakes.set(account, newValue);
    }

    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        accountCoins,
        storedPendingTransfers,
        updatedIceStakes,
        storedPendingIceStakes,
        mpcTokens);
  }

  ContractStorage addIceStakes(BlockchainAddress address, long amount) {
    return updateIceStakeAmount(address, amount);
  }

  ContractStorage subtractIceStakes(BlockchainAddress address, long amount) {
    return updateIceStakeAmount(address, -amount);
  }

  ContractStorage removePendingIceStake(Hash transactionId) {
    return new ContractStorage(
        latestStorageFeeTime,
        balance,
        icedTime,
        accountCoins,
        storedPendingTransfers,
        iceStakes,
        storedPendingIceStakes.remove(transactionId),
        mpcTokens);
  }
}
