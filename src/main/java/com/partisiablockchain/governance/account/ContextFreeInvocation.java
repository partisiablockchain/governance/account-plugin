package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.Set;

/**
 * Implementations for context-free invocations on the {@link GasAndCoinAccountPlugin}.
 *
 * <p>Associated with and operates on {@link AccountStateLocal}.
 */
public enum ContextFreeInvocation implements Invocation {
  /**
   * Collect fees for distribution for both service and infrastructure.
   *
   * <p>Invocation byte: 20
   */
  COLLECT_SERVICE_AND_INFRASTRUCTURE_FEES_FOR_DISTRIBUTION(20) {
    @Override
    AccountStateLocal invoke(
        AccountStateLocal local,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      long epoch = rpc.readLong();
      local =
          collectFeesForDistribution(
              local, returnValue, epoch, pluginContext.blockProductionTime());
      local.getInfrastructureFeesSum().write(returnValue);
      return local.resetInfrastructureFeesSum();
    }
  };

  ContextFreeInvocation(int invocationByte) {
    this.invocationByte = (byte) invocationByte;
  }

  private final byte invocationByte;

  @Override
  public byte getInvocationByte() {
    return invocationByte;
  }

  abstract AccountStateLocal invoke(
      AccountStateLocal local,
      SafeDataInputStream rpc,
      SafeDataOutputStream returnValue,
      PluginContext pluginContext);

  static ContextFreeInvocation getInvocation(SafeDataInputStream rpc) {
    final int invocationByte = rpc.readUnsignedByte();
    return Invocation.findInvocationBy(values(), invocationByte, "Context-Free");
  }

  static AccountStateLocal collectFeesForDistribution(
      AccountStateLocal local,
      SafeDataOutputStream returnValue,
      long epoch,
      long blockProductionTime) {
    if (epoch >= GasAndCoinAccountPlugin.convertBlockProductionTimeToEpoch(blockProductionTime)) {
      throw new IllegalArgumentException("Cannot distribute fees for epochs that has not ended");
    }

    FixedList<ConvertedExternalCoin> convertedCoins = local.convertedExternalCoins();
    returnValue.writeLong(convertedCoins.size());
    for (ConvertedExternalCoin convertedCoin : convertedCoins) {
      convertedCoin.getConvertedCoins().write(returnValue);
      convertedCoin.getConvertedGasSum().write(returnValue);
    }

    AvlTree<Long, Unsigned256> previousEpochsWithBpUsage = AvlTree.create();
    for (Long bpUsageEpoch : local.getBlockchainUsage().keySet()) {
      if (bpUsageEpoch <= epoch) {
        previousEpochsWithBpUsage =
            previousEpochsWithBpUsage.set(
                bpUsageEpoch, local.getBlockchainUsageForEpoch(bpUsageEpoch));
      }
    }

    returnValue.writeLong(previousEpochsWithBpUsage.size());
    for (Long bpUsageEpoch : previousEpochsWithBpUsage.keySet()) {
      returnValue.writeLong(bpUsageEpoch);
      previousEpochsWithBpUsage.getValue(bpUsageEpoch).write(returnValue);
    }

    Set<BlockchainAddress> producersWithFees = local.getServiceFees().keySet();
    returnValue.writeLong(producersWithFees.size());
    for (BlockchainAddress blockchainAddress : producersWithFees) {
      blockchainAddress.write(returnValue);
      local.getServiceFeesForAddress(blockchainAddress).write(returnValue);
    }

    return local.resetFees(previousEpochsWithBpUsage.keySet());
  }
}
