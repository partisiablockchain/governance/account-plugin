package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainAccountPlugin;
import com.partisiablockchain.blockchain.PayServiceFeesResult;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.io.ByteArrayOutputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Implementation of the default {@link BlockchainAccountPlugin} with support for gas and coins. */
@Immutable
public final class GasAndCoinAccountPlugin
    extends BlockchainAccountPlugin<
        AccountStateGlobal, AccountStateLocal, Balance, ContractStorage> {

  static final long NO_OF_BLOCK_BETWEEN_CHECKS = 256;
  static final long MILLIS_PER_YEAR = Duration.ofDays(365).toMillis();
  static final long MILLIS_PER_MONTH = Duration.ofDays(28).toMillis();
  static final long MILLIS_PER_WEEK = Duration.ofDays(7).toMillis();
  static final long FREE_TRANSACTIONS_PER_EPOCH = 100;
  static final long STAKED_TOKEN_THRESHOLD_FOR_FREE_TRANSACTION = 25_000_0000L;
  static final int MAX_NETWORK_SIZE_FOR_FREE_TRANSACTION = 100_000;
  private static final Logger logger = LoggerFactory.getLogger(GasAndCoinAccountPlugin.class);

  private static int getFirstByteOfContractIdentifier(BlockchainAddress address) {
    return address.getIdentifier()[0] & 0xFF;
  }

  private static long nearest256Floored(long blockTime) {
    return blockTime / 256 * 256;
  }

  @Override
  public long convertNetworkFee(AccountStateGlobal globalState, long bytes) {
    return computeGasPriceFor1000(bytes, globalState.getGasPriceFor1000Network());
  }

  @Override
  public boolean canCoverCost(
      PluginContext pluginContext,
      AccountStateGlobal global,
      AccountState<AccountStateLocal, Balance> localState,
      SignedTransaction transaction) {
    if (isFreeTransactionAttempt(transaction)) {
      boolean hasEnoughStaked =
          account(localState).stakeForJobs() >= STAKED_TOKEN_THRESHOLD_FOR_FREE_TRANSACTION;
      long epoch = convertBlockProductionTimeToEpoch(pluginContext.blockProductionTime());
      boolean isFreeTransactionLeftForEpoch =
          account(localState).spentFreeTransactions(epoch) < FREE_TRANSACTIONS_PER_EPOCH;
      long networkFee = transaction.computeNetworkByteCount();
      boolean isNetworkFeeBelowMax = networkFee <= MAX_NETWORK_SIZE_FOR_FREE_TRANSACTION;
      logger.trace(
          "Attempting free transaction {}"
              + " - Enough staked tokens: {}, epoch: {}, has free transactions left: {} "
              + " and networkFee low enough: {}",
          transaction,
          hasEnoughStaked,
          epoch,
          isFreeTransactionLeftForEpoch,
          isNetworkFeeBelowMax);
      return hasEnoughStaked && isFreeTransactionLeftForEpoch && isNetworkFeeBelowMax;
    } else if (!coversNetworkFee(global, transaction)) {
      return false;
    } else {
      Unsigned256 totalBalance = getTotalBalance(global, localState);
      return totalBalance.compareTo(Unsigned256.create(transaction.getCost())) >= 0;
    }
  }

  private boolean coversNetworkFee(AccountStateGlobal global, SignedTransaction transaction) {
    long cost = transaction.getCost();
    long networkFee = convertNetworkFee(global, transaction.computeNetworkByteCount());
    return cost >= networkFee;
  }

  @Override
  public PayCostResult<AccountStateLocal, Balance> payCost(
      PluginContext pluginContext,
      AccountStateGlobal globalState,
      AccountState<AccountStateLocal, Balance> localState,
      SignedTransaction signedTransaction) {
    long fees = signedTransaction.getCost();
    if (fees < 0) {
      throw new RuntimeException("Fee cannot be negative.");
    } else if (fees == 0) {
      BalanceMutable updatedBalance = account(localState).asMutable();
      updatedBalance.incrementFreeTransactionsUsed(
          convertBlockProductionTimeToEpoch(pluginContext.blockProductionTime()));
      return new PayCostResult<>(
          new AccountState<>(localState.local, updatedBalance.asImmutable()), 0);
    } else {
      StateWithRemaining withRemaining =
          payFeeFromCoins(globalState, localState, pluginContext, fees);
      long networkFee = convertNetworkFee(globalState, signedTransaction.computeNetworkByteCount());
      if (!withRemaining.remaining.equals(Unsigned256.ZERO)) {
        throw new IllegalStateException(
            "Calls to this function should be guarded with canCoverFee");
      }
      AccountState<AccountStateLocal, Balance> next = withRemaining.next;
      AccountStateLocal updatedLocal =
          registerBlockchainUsage(pluginContext, next.local, Unsigned256.create(networkFee));
      return new PayCostResult<>(new AccountState<>(updatedLocal, next.account), fees - networkFee);
    }
  }

  private static StateWithRemaining payFeeFromCoins(
      AccountStateGlobal global,
      AccountState<AccountStateLocal, Balance> current,
      PluginContext pluginContext,
      long amount) {
    Unsigned256 toPay = Unsigned256.create(amount);
    BalanceMutable nextAccount = account(current).asMutable();
    FixedList<AccountCoin> accountCoins = nextAccount.accountCoins();
    AccountStateLocal nextAccumulated = current.local;
    for (int i = 0; i < accountCoins.size() && !toPay.equals(Unsigned256.ZERO); i++) {
      Fraction conversionRate = ratioLookup(global, i);
      if (!conversionRate.isZero()) {
        AccountCoin accountCoin = accountCoins.get(i);
        Unsigned256 requiredCoins = accountCoin.requiredCoinsForAmount(toPay, conversionRate);

        nextAccount.deductCoins(i, requiredCoins, global.coinSymbols().get(i));
        Unsigned256 correspondingGas = externalCoinsToGas(requiredCoins, conversionRate);

        nextAccumulated = nextAccumulated.updateCollectedCoins(i, requiredCoins, correspondingGas);

        if (toPay.compareTo(correspondingGas) > 0) {
          toPay = toPay.subtract(correspondingGas);
        } else {
          // The remaining gas not used to pay is registered as blockchain usage.
          Unsigned256 remainingGas = correspondingGas.subtract(toPay);
          nextAccumulated = registerBlockchainUsage(pluginContext, nextAccumulated, remainingGas);
          toPay = Unsigned256.ZERO;
        }
        logger.debug(
            "Converted {} {} coins. Remaining unresolved amount: {}", requiredCoins, i, toPay);
      }
    }
    return new StateWithRemaining(
        new AccountState<>(nextAccumulated, nextAccount.asImmutable()), toPay);
  }

  private static Unsigned256 externalCoinsToGas(
      Unsigned256 externalCoinAmount, Fraction conversion) {
    return externalCoinAmount
        .multiply(Unsigned256.create(conversion.getNumerator()))
        .divide(Unsigned256.create(conversion.getDenominator()));
  }

  @Override
  public ContractState<AccountStateLocal, ContractStorage> contractCreated(
      PluginContext pluginContext,
      AccountStateGlobal globalState,
      AccountStateLocal contextFreeState,
      BlockchainAddress address) {
    Long latestStorageFeeTime;
    if (address.getType() == BlockchainAddress.Type.CONTRACT_SYSTEM) {
      latestStorageFeeTime = null;
    } else {
      latestStorageFeeTime = pluginContext.blockProductionTime();
    }
    return new ContractState<>(contextFreeState, ContractStorage.create(latestStorageFeeTime));
  }

  @Override
  public AccountStateLocal removeContract(
      PluginContext pluginContext,
      AccountStateGlobal globalState,
      ContractState<AccountStateLocal, ContractStorage> localState) {

    Unsigned256 remainingBalance = localState.contract.getBalance().getPositiveValue();
    AccountStateLocal updatedAccountStateLocal =
        registerBlockchainUsage(pluginContext, localState.local, remainingBalance);

    FixedList<AccountCoin> coins = localState.contract.getCoins();
    for (int i = 0; i < coins.size(); i++) {
      AccountCoin coin = coins.get(i);
      Fraction fraction = ratioLookup(globalState, i);
      Unsigned256 gas = coin.computeGasEquivalent(fraction);
      updatedAccountStateLocal =
          registerBlockchainUsage(pluginContext, updatedAccountStateLocal, gas)
              .updateCollectedCoins(i, coin.getBalance(), gas);
    }

    long mpcTokensOnContract = localState.contract.getMpcTokens();
    updatedAccountStateLocal = updatedAccountStateLocal.addMpcTokens(mpcTokensOnContract);

    return updatedAccountStateLocal;
  }

  private static Fraction ratioLookup(AccountStateGlobal global, int i) {
    return global.coinConversionRate(global.coinSymbols().get(i));
  }

  private static Balance account(AccountState<AccountStateLocal, Balance> current) {
    Balance account = current.account;
    return nullSafe(account);
  }

  private static Balance nullSafe(Balance account) {
    return Objects.requireNonNullElseGet(account, Balance::create);
  }

  private static final class StateWithRemaining {

    private final AccountState<AccountStateLocal, Balance> next;
    private final Unsigned256 remaining;

    private StateWithRemaining(
        AccountState<AccountStateLocal, Balance> next, Unsigned256 remaining) {
      this.next = next;
      this.remaining = remaining;
    }
  }

  @Override
  public ContractState<AccountStateLocal, ContractStorage> updateContractGasBalance(
      PluginContext pluginContext,
      AccountStateGlobal accountStateGlobal,
      ContractState<AccountStateLocal, ContractStorage> localState,
      BlockchainAddress contract,
      long gas) {
    ContractStorage storage = nullSafeContract(localState);
    if (gas <= -1) {
      Unsigned256 toDeduct = Unsigned256.create(-gas);
      storage = storage.deductBalance(toDeduct, pluginContext.blockProductionTime());
      if (!storage.getBalance().isPositive()) {
        throw new RuntimeException("Contract is unable to cover requested gas cost");
      }
      return new ContractState<>(localState.local, storage);
    } else {
      Unsigned256 diff = Unsigned256.create(gas);
      AccountStateLocal updatedLocal = localState.local;
      if (!storage.getBalance().isPositive()) {
        Unsigned256 toPay = MathUtil.min(diff, storage.getBalance().getAbsoluteValue());
        updatedLocal = registerBlockchainUsage(pluginContext, localState.local, toPay);
      }
      ContractStorage contractStorage = storage.addBalance(diff);
      return new ContractState<>(updatedLocal, contractStorage);
    }
  }

  @Override
  public AccountStateLocal registerBlockchainUsage(
      PluginContext pluginContext,
      AccountStateGlobal globalState,
      AccountStateLocal contextFreeState,
      long gas) {
    return registerBlockchainUsage(pluginContext, contextFreeState, Unsigned256.create(gas));
  }

  private static AccountStateLocal registerBlockchainUsage(
      PluginContext pluginContext, AccountStateLocal contextFreeState, Unsigned256 gas) {
    long epoch = convertBlockProductionTimeToEpoch(pluginContext.blockProductionTime());
    return contextFreeState.addBlockchainUsage(epoch, gas);
  }

  @Override
  public PayServiceFeesResult<ContractState<AccountStateLocal, ContractStorage>> payServiceFees(
      PluginContext pluginContext,
      AccountStateGlobal globalState,
      ContractState<AccountStateLocal, ContractStorage> contractState,
      List<PendingFee> pendingFees) {
    Unsigned256 sum = Unsigned256.create(pendingFees.stream().mapToLong(PendingFee::getFee).sum());

    ContractStorage contractStorage = nullSafeContract(contractState);
    AccountStateLocal localState = contractState.local;

    Unsigned256 contractBalance = contractStorage.getBalance().getPositiveValue();
    if (contractBalance.compareTo(sum) >= 0) {
      contractStorage = contractStorage.deductBalance(sum, pluginContext.blockProductionTime());
      for (PendingFee pendingFee : pendingFees) {
        localState = localState.addServiceFee(pendingFee.getNode(), pendingFee.getFee());
      }
      return PayServiceFeesResult.coveredFee(new ContractState<>(localState, contractStorage));
    } else {
      contractStorage =
          contractStorage.deductBalance(contractBalance, pluginContext.blockProductionTime());
      localState = distributeContractBalance(pendingFees, localState, contractBalance);
      return PayServiceFeesResult.failedToCoverFees(
          new ContractState<>(localState, contractStorage));
    }
  }

  @Override
  public AccountStateLocal payInfrastructureFees(
      PluginContext pluginContext,
      AccountStateGlobal accountStateGlobal,
      AccountStateLocal localState,
      List<PendingFee> pendingFees) {

    Unsigned256 sum = Unsigned256.ZERO;
    for (PendingFee pendingFee : pendingFees) {
      localState = localState.addServiceFee(pendingFee.getNode(), pendingFee.getFee());
      sum = sum.add(Unsigned256.create(pendingFee.getFee()));
    }
    return localState.addInfrastructureFees(sum);
  }

  private AccountStateLocal distributeContractBalance(
      List<PendingFee> pendingFees, AccountStateLocal localState, Unsigned256 contractBalance) {
    List<Long> weights = pendingFees.stream().map(PendingFee::getFee).collect(Collectors.toList());
    List<Unsigned256> shares = shareContractBalance(contractBalance, weights);
    for (int i = 0; i < shares.size(); i++) {
      BlockchainAddress node = pendingFees.get(i).getNode();
      Unsigned256 share = shares.get(i);
      localState = localState.addServiceFee(node, share);
    }
    return localState;
  }

  private List<Unsigned256> shareContractBalance(Unsigned256 gas, List<Long> weights) {
    List<Unsigned256> result = new ArrayList<>();
    Unsigned256 available = gas;
    Unsigned256 weightsSum = Unsigned256.create(weights.stream().mapToLong(Long::longValue).sum());

    for (Long w : weights) {
      Unsigned256 weight = Unsigned256.create(w);
      Unsigned256 scaledGas = weight.multiply(available);
      Unsigned256 distribution = scaledGas.divide(weightsSum);

      result.add(distribution);
      weightsSum = weightsSum.subtract(weight);
      available = available.subtract(distribution);
    }

    return result;
  }

  private boolean isFreeTransactionAttempt(SignedTransaction transaction) {
    BlockchainAddress targetContract = transaction.getTransaction().getTargetContract();
    boolean isSystemContract = targetContract.getType() == BlockchainAddress.Type.CONTRACT_GOV;
    long maximumCost = transaction.getCost();
    return isSystemContract && maximumCost == 0;
  }

  private static Unsigned256 getTotalBalance(
      AccountStateGlobal global, AccountState<AccountStateLocal, Balance> localState) {
    Balance account = account(localState);
    return account.totalBalance(i -> ratioLookup(global, i));
  }

  @Override
  public AccountStateLocal updateForBlock(
      AccountStateGlobal globalState,
      AccountStateLocal localState,
      BlockContext<ContractStorage> context) {
    long blockTime = context.currentBlock().getBlockTime();
    if (blockTime % NO_OF_BLOCK_BETWEEN_CHECKS == 0) {
      long blockTimeWithoutLastByte = blockTime / NO_OF_BLOCK_BETWEEN_CHECKS;
      long nearest256Floored = nearest256Floored(blockTimeWithoutLastByte);

      for (BlockchainAddress contract : context.contracts().keySet()) {
        long nextStorageFeeTime = nearest256Floored + getFirstByteOfContractIdentifier(contract);
        if (blockTimeWithoutLastByte == nextStorageFeeTime) {
          context.registerContractUpdate(contract);
        }
      }
    }
    return localState;
  }

  static long convertBlockProductionTimeToEpoch(long blockProductionTime) {
    return blockProductionTime >>> 21;
  }

  @Override
  public ContractState<AccountStateLocal, ContractStorage> updateActiveContract(
      PluginContext pluginContext,
      AccountStateGlobal globalState,
      ContractState<AccountStateLocal, ContractStorage> currentState,
      BlockchainAddress contract,
      long size) {
    throw new RuntimeException("Deprecated, use instead updateActiveContractIced");
  }

  @Override
  public IcedContractState<AccountStateLocal, ContractStorage> updateActiveContractIced(
      PluginContext pluginContext,
      AccountStateGlobal globalState,
      ContractState<AccountStateLocal, ContractStorage> currentState,
      BlockchainAddress contract,
      long size) {
    ContractStorage contractStorage = nullSafeContract(currentState);
    if (!shouldPayStorage(contract, contractStorage)) {
      return new IcedContractState<>(false, currentState);
    }

    long latestStorageFeeTime = contractStorage.latestStorageFeeTime();
    long blockProductionTime = pluginContext.blockProductionTime();
    long interval = blockProductionTime - latestStorageFeeTime;
    contractStorage = contractStorage.updateLatestStorageFeeTime(blockProductionTime);

    Unsigned256 fee =
        Unsigned256.create(
            computeStoragePrice(interval, size, globalState.getGasPriceFor1000Storage()));

    Unsigned256 toPay = computePositiveFee(contractStorage.getBalance(), fee);
    AccountStateLocal updated = registerBlockchainUsage(pluginContext, currentState.local, toPay);
    contractStorage = contractStorage.deductBalance(fee, pluginContext.blockProductionTime());
    if (contractStorage.getBalance().isPositive()) {
      return new IcedContractState<>(false, new ContractState<>(updated, contractStorage));
    }

    boolean contractIsIced =
        isIced(contract, pluginContext, contractStorage, globalState.getGasPriceFor1000IceStakes());
    if (contractIsIced) {
      return new IcedContractState<>(true, new ContractState<>(updated, contractStorage));
    } else {
      return new IcedContractState<>(false, null);
    }
  }

  private Unsigned256 computePositiveFee(Signed256 balance, Unsigned256 fee) {
    return MathUtil.min(balance.getPositiveValue(), fee);
  }

  private boolean isIced(
      BlockchainAddress contract,
      PluginContext pluginContext,
      ContractStorage contractStorage,
      int iceStakesFor1000Gas) {

    if (contract.getType() != BlockchainAddress.Type.CONTRACT_PUBLIC) {
      // Only public contracts can be iced
      return false;
    }

    Long icedTime = contractStorage.getIcedTime();
    long blockProductionTime = pluginContext.blockProductionTime();

    // the contract is in the negative here, and we need to decide whether to delete the contract or
    // not.
    // 1. If the contract was iced less than a week ago, it is not deleted.
    // 2. If the contract was iced less than a month ago, AND it has a positive amount of ice stakes
    //    associated, then it is not deleted.
    // 3. If the contract was iced more than a month ago, then it needs to have some amount X ice
    //    staked to it.
    // In all other cases, the contract is deleted.

    if (blockProductionTime <= MILLIS_PER_WEEK + icedTime) {
      // case 1
      return true;
    }

    long totalStakes =
        contractStorage.getIceStakes().values().stream().mapToLong(Long::longValue).sum();
    if (totalStakes == 0) {
      // case 2 where contract has no ice stakes
      return false;
    }

    if (blockProductionTime <= MILLIS_PER_MONTH + icedTime) {
      // case 2
      return true;
    }

    // balance is negative, otherwise we wouldn't be here
    Unsigned256 currentDebt = contractStorage.getBalance().getAbsoluteValue();
    Unsigned256 weightedStakes =
        Unsigned256.create(computeGasPriceFor1000(totalStakes, iceStakesFor1000Gas));

    // case 3
    return currentDebt.compareTo(weightedStakes) <= 0;
  }

  private boolean shouldPayStorage(BlockchainAddress contract, ContractStorage contractStorage) {
    return contractStorage.latestStorageFeeTime() != null
        && contract.getType() != BlockchainAddress.Type.CONTRACT_GOV;
  }

  private ContractStorage nullSafeContract(
      ContractState<AccountStateLocal, ContractStorage> currentState) {
    return Objects.requireNonNullElseGet(currentState.contract, ContractStorage::new);
  }

  private long computeStoragePrice(long interval, long count, long storagePrice) {
    long temp = MathUtil.ceilDiv(interval * count, MILLIS_PER_YEAR);
    return computeGasPriceFor1000(temp, storagePrice);
  }

  private static long computeGasPriceFor1000(long count, long ratio) {
    return MathUtil.ceilDiv(count * ratio, 1000);
  }

  @Override
  public Class<AccountStateGlobal> getGlobalStateClass() {
    return AccountStateGlobal.class;
  }

  @Override
  public List<Class<?>> getLocalStateClassTypeParameters() {
    return List.of(AccountStateLocal.class, Balance.class, ContractStorage.class);
  }

  @Override
  protected AccountStateLocal migrateContextFree(StateAccessor current) {
    if (current == null) {
      return AccountStateLocal.initial();
    } else {
      return AccountStateLocal.createFromStateAccessor(current);
    }
  }

  @Override
  protected ContractStorage migrateContract(
      BlockchainAddress blockchainAddress, StateAccessor stateAccessor) {
    return ContractStorage.createFromStateAccessor(stateAccessor);
  }

  @Override
  protected Balance migrateAccount(StateAccessor stateAccessor) {
    return Balance.createFromStateAccessor(stateAccessor);
  }

  @Override
  protected InvokeResult<ContractState<AccountStateLocal, ContractStorage>> invokeContract(
      PluginContext pluginContext,
      AccountStateGlobal accountStateGlobal,
      ContractState<AccountStateLocal, ContractStorage> contractState,
      BlockchainAddress blockchainAddress,
      byte[] bytes) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    SafeDataOutputStream ret = new SafeDataOutputStream(stream);
    ContractState<AccountStateLocal, ContractStorage> state =
        SafeDataInputStream.readFully(
            bytes,
            rpc -> {
              ContractInvocation invocation = ContractInvocation.getInvocation(rpc);
              return invocation.invoke(
                  pluginContext, accountStateGlobal, contractState, blockchainAddress, rpc, ret);
            });
    return new InvokeResult<>(state, stream.toByteArray());
  }

  @Override
  protected InvokeResult<AccountState<AccountStateLocal, Balance>> invokeAccount(
      PluginContext pluginContext,
      AccountStateGlobal global,
      AccountState<AccountStateLocal, Balance> accountState,
      byte[] bytes) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    SafeDataOutputStream safeDataOutputStream = new SafeDataOutputStream(stream);
    AccountState<AccountStateLocal, Balance> stateLocal =
        SafeDataInputStream.readFully(
            bytes,
            rpc -> {
              AccountInvocation invocation = AccountInvocation.getInvocation(rpc);
              return invocation.invoke(
                  global, accountState, rpc, safeDataOutputStream, pluginContext);
            });
    return new InvokeResult<>(stateLocal, stream.toByteArray());
  }

  @Override
  protected InvokeResult<AccountStateLocal> invokeContextFree(
      PluginContext pluginContext,
      AccountStateGlobal accountStateGlobal,
      AccountStateLocal local,
      byte[] bytes) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    SafeDataOutputStream safeDataOutputStream = new SafeDataOutputStream(stream);
    AccountStateLocal stateLocal =
        SafeDataInputStream.readFully(
            bytes,
            rpc -> {
              ContextFreeInvocation invocation = ContextFreeInvocation.getInvocation(rpc);
              return invocation.invoke(local, rpc, safeDataOutputStream, pluginContext);
            });
    return new InvokeResult<>(stateLocal, stream.toByteArray());
  }

  @Override
  protected AccountStateLocal payByocFees(
      PluginContext pluginContext,
      AccountStateGlobal global,
      AccountStateLocal local,
      Unsigned256 amount,
      String symbol,
      FixedList<BlockchainAddress> nodes) {
    int coinIndex = getCoinIndex(global, symbol);
    Fraction conversionRate = global.coinConversionRate(symbol);
    Unsigned256 converted = externalCoinsToGas(amount, conversionRate);
    AccountStateLocal updated = local.updateCollectedCoins(coinIndex, amount, converted);

    if (nodes.size() == 0) {
      return registerBlockchainUsage(pluginContext, updated, converted);
    } else {
      // each node gets the same fraction of the converted amount. Any leftover will be registered
      // as blockchain usage.
      Unsigned256 amountPerNode = converted.divide(Unsigned256.create(nodes.size()));
      for (BlockchainAddress node : nodes) {
        updated = updated.addServiceFee(node, amountPerNode);
        converted = converted.subtract(amountPerNode);
      }
      return registerBlockchainUsage(pluginContext, updated, converted);
    }
  }

  @Override
  public InvokeResult<AccountStateGlobal> invokeGlobal(
      PluginContext pluginContext, AccountStateGlobal accountStateGlobal, byte[] bytes) {
    return new InvokeResult<>(
        SafeDataInputStream.readFully(bytes, accountStateGlobal::invoke), null);
  }

  @Override
  public AccountStateGlobal migrateGlobal(StateAccessor currentGlobal, SafeDataInputStream rpc) {
    if (currentGlobal == null) {
      return AccountStateGlobal.create();
    } else {
      return AccountStateGlobal.createFromStateAccessor(currentGlobal);
    }
  }

  static int getCoinIndex(AccountStateGlobal global, String symbol) {
    int coinIndex = global.coinSymbols().indexOf(symbol);
    if (coinIndex != -1) {
      return coinIndex;
    } else {
      throw new IllegalArgumentException("Coin not found: " + symbol);
    }
  }
}
