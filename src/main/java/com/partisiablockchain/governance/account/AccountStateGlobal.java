package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/** The global account plugin state. */
@Immutable
public final class AccountStateGlobal implements StateSerializable {

  /** The price in gas for sending 1000 bytes on the network. */
  public final int gasPriceFor1000Network;

  /** The price in gas for storing 1000 bytes for 1 year. */
  public final int gasPriceFor1000Storage;

  /**
   * The amount of gas required for 1000 ice stakes. This value is used to determine the amount of
   * ice-stakes required in order to keep a contract that is in debt alive.
   *
   * <p>For example, if {@code gasPriceFor1000IceStakes} is set to 4000, then 4000 gas equals 1000
   * MPC. If a contract has 7000 MPC ice staked, then it will be able to keep a contract alive
   * provided its debt does not exceed (7000 * 4000) / 1000 = 28000 gas.
   */
  public final int gasPriceFor1000IceStakes;

  /** The external coins (BYOC) that exists on the blockchain. */
  public final ExternalCoins coins;

  /** Creates a new object for a default value. */
  @SuppressWarnings("unused")
  public AccountStateGlobal() {
    gasPriceFor1000Network = 0;
    gasPriceFor1000Storage = 0;
    gasPriceFor1000IceStakes = 0;
    coins = ExternalCoins.create();
  }

  private AccountStateGlobal(
      int gasPriceFor1000Network,
      int gasPriceFor1000Storage,
      int gasPriceFor1000IceStakes,
      ExternalCoins coins) {
    this.gasPriceFor1000Network = gasPriceFor1000Network;
    this.gasPriceFor1000Storage = gasPriceFor1000Storage;
    this.gasPriceFor1000IceStakes = gasPriceFor1000IceStakes;
    this.coins = coins;
  }

  static AccountStateGlobal createFromStateAccessor(StateAccessor accessor) {
    int gasPriceFor1000Network = accessor.get("gasPriceFor1000Network").intValue();
    int gasPriceFor1000Storage = accessor.get("gasPriceFor1000Storage").intValue();
    ExternalCoins coins = ExternalCoins.createFromStateAccessor(accessor.get("coins"));
    int iceStakesFor1000Gas = accessor.get("gasPriceFor1000IceStakes").intValue();
    return new AccountStateGlobal(
        gasPriceFor1000Network, gasPriceFor1000Storage, iceStakesFor1000Gas, coins);
  }

  static AccountStateGlobal create() {
    return new AccountStateGlobal(0, 0, 0, ExternalCoins.create());
  }

  /**
   * Get symbols identifying coins.
   *
   * @return list of symbols
   */
  public List<String> coinSymbols() {
    return coins.getCoins().stream().map(Coin::getSymbol).collect(Collectors.toList());
  }

  /**
   * Get coin conversion rate.
   *
   * @param symbol identifies coin
   * @return coin conversion rate
   */
  public Fraction coinConversionRate(String symbol) {
    return coins.getCoins().get(getCoinIndex(symbol)).getConversionRate();
  }

  /**
   * Get gas price for 1000 network.
   *
   * @return gas price for 1000 network
   */
  public long getGasPriceFor1000Network() {
    return gasPriceFor1000Network;
  }

  /**
   * Get gas price for 1000 storage.
   *
   * @return gas price for 1000 storage
   */
  public long getGasPriceFor1000Storage() {
    return gasPriceFor1000Storage;
  }

  /**
   * Get number of ice stakes.
   *
   * @return number of ice stakes.
   */
  public int getGasPriceFor1000IceStakes() {
    return gasPriceFor1000IceStakes;
  }

  AccountStateGlobal invoke(SafeDataInputStream rpc) {
    int interactionByte = rpc.readUnsignedByte();
    GlobalInteraction globalInteraction =
        Arrays.stream(GlobalInteraction.values())
            .filter(i -> i.getInteractionByte() == interactionByte)
            .findFirst()
            .orElse(null);
    ensure(globalInteraction != null, "Interaction does not exist");
    return globalInteraction.invoke(rpc, this);
  }

  private int getCoinIndex(String symbol) {
    return GasAndCoinAccountPlugin.getCoinIndex(this, symbol);
  }

  AccountStateGlobal setCoinInternal(String symbol, Fraction conversionRate) {
    return new AccountStateGlobal(
        gasPriceFor1000Network,
        gasPriceFor1000Storage,
        gasPriceFor1000IceStakes,
        coins.overwrite(Coin.create(symbol, conversionRate)));
  }

  AccountStateGlobal setGasPriceFor1000NetworkInternal(int gasPriceFor1000Network) {
    return new AccountStateGlobal(
        gasPriceFor1000Network, gasPriceFor1000Storage, gasPriceFor1000IceStakes, coins);
  }

  AccountStateGlobal setGasPriceFor1000StorageInternal(int gasPriceFor1000Storage) {
    return new AccountStateGlobal(
        gasPriceFor1000Network, gasPriceFor1000Storage, gasPriceFor1000IceStakes, coins);
  }

  AccountStateGlobal setGasPriceFor1000IceStakes(int gasPriceFor1000IceStakes) {
    return new AccountStateGlobal(
        gasPriceFor1000Network, gasPriceFor1000Storage, gasPriceFor1000IceStakes, coins);
  }

  /** Invocation to the global account state. */
  public enum GlobalInteraction {
    /**
     * Set gas price for 1000 bytes of network data.
     *
     * <p>The following is read from the RPC stream:
     *
     * <ul>
     *   <li><code>int</code> the new gas price.
     * </ul>
     *
     * <p>Invocation byte: 0
     */
    SET_GAS_PRICE_FOR_1000_NETWORK(0) {
      @Override
      AccountStateGlobal invoke(SafeDataInputStream rpc, AccountStateGlobal global) {
        int nextPrice = rpc.readInt();
        ensure(nextPrice >= 0, "Gas price has to be non-negative");
        return global.setGasPriceFor1000NetworkInternal(nextPrice);
      }
    },
    /**
     * Set gas price for 1000 bytes of storage.
     *
     * <p>The following is read from the RPC stream:
     *
     * <ul>
     *   <li><code>int</code> the new gas price.
     * </ul>
     *
     * <p>Invocation byte: 1
     */
    SET_GAS_PRICE_FOR_1000_STORAGE(1) {
      @Override
      AccountStateGlobal invoke(SafeDataInputStream rpc, AccountStateGlobal global) {
        int nextPrice = rpc.readInt();
        ensure(nextPrice >= 0, "Gas price has to be non-negative");
        return global.setGasPriceFor1000StorageInternal(nextPrice);
      }
    },
    /**
     * Overwrites the old conversion rate of an existing coin, or creates a new coin.
     *
     * <p>The following is read from the RPC stream:
     *
     * <ul>
     *   <li><code>String</code> symbol of the coin.
     *   <li><code>long</code> conversion numerator.
     *   <li><code>long</code> conversion denominator.
     * </ul>
     *
     * <p>Invocation byte: 2
     */
    SET_COIN(2) {
      @Override
      AccountStateGlobal invoke(SafeDataInputStream rpc, AccountStateGlobal global) {
        String symbol = rpc.readString();
        long conversionNumerator = rpc.readLong();
        long conversionDenominator = rpc.readLong();
        ensure(
            conversionDenominator > 0 && conversionNumerator >= 0,
            "Conversion rate for coins has to be non-negative");
        return global.setCoinInternal(
            symbol, new Fraction(conversionNumerator, conversionDenominator));
      }
    },
    /**
     * Set number of ice stakes equivalent to 1000 gas.
     *
     * <p>The following is read from the RPC stream:
     *
     * <ul>
     *   <li><code>int</code> the new number of ice stakes equivalent to 1000 gas.
     * </ul>
     *
     * <p>Invocation byte: 3
     */
    SET_GAS_PRICE_FOR_1000_ICE_STAKES(3) {
      @Override
      AccountStateGlobal invoke(SafeDataInputStream rpc, AccountStateGlobal global) {
        int nextPrice = rpc.readInt();
        ensure(nextPrice >= 0, "Number of ice stakes has to be non-negative");
        return global.setGasPriceFor1000IceStakes(nextPrice);
      }
    };

    GlobalInteraction(int interactionByte) {
      this.interactionByte = (byte) interactionByte;
    }

    private final byte interactionByte;

    byte getInteractionByte() {
      return interactionByte;
    }

    abstract AccountStateGlobal invoke(SafeDataInputStream rpc, AccountStateGlobal global);
  }

  private static void ensure(boolean predicate, String errorString) {
    if (!predicate) {
      throw new RuntimeException(errorString);
    }
  }
}
