package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Stores information of staked tokens being associated to a contract to be used for running jobs.
 */
@Immutable
public final class StakedToContract implements StateSerializable {

  /** The amount of tokens associated to the contract. */
  public final long amount;

  /** The timestamp when the association can no longer be used to run new jobs. */
  public final Long expirationTimestamp;

  @SuppressWarnings("unused")
  StakedToContract() {
    amount = 0;
    expirationTimestamp = null;
  }

  private StakedToContract(long amount, Long expirationTimestamp) {
    this.amount = amount;
    this.expirationTimestamp = expirationTimestamp;
  }

  static StakedToContract create(long amount, Long expirationTimestamp) {
    return new StakedToContract(amount, expirationTimestamp);
  }

  StakedToContract setAmount(long amount) {
    return create(amount, expirationTimestamp);
  }

  StakedToContract setExpirationTimestamp(Long expirationTimestamp) {
    return create(amount, expirationTimestamp);
  }
}
