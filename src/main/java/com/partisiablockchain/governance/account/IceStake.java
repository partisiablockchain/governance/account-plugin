package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;

/**
 * Used in the two phase ice staking protocol to represent a pending stake to a contract, for the
 * purpose of "icing" it.
 *
 * @param address From the account's point of view, this is the contract to ice stake towards. From
 *     the contract's point of view, this is the account doing the ice staking.
 * @param amount The amount of MPC tokens to ice stake
 * @param type The type of ice staking see {@link IceStakeType}
 */
@Immutable
public record IceStake(BlockchainAddress address, long amount, IceStakeType type)
    implements StateSerializable {

  /** Types of ice staking. */
  public enum IceStakeType {
    /** Associate ice stakes. */
    ASSOCIATE,
    /** Disassociate ice stakes. */
    DISASSOCIATE
  }

  @SuppressWarnings("EnumOrdinal")
  static IceStake createFromAccessor(StateAccessor value) {
    Enum<?> iceType = value.get("type").cast(Enum.class);
    return new IceStake(
        value.get("address").blockchainAddressValue(),
        value.get("amount").longValue(),
        IceStakeType.values()[iceType.ordinal()]);
  }

  static AvlTree<Hash, IceStake> createPendingIceStakes(StateAccessor accessor) {
    AvlTree<Hash, IceStake> pendingIceStakes = AvlTree.create();
    List<StateAccessorAvlLeafNode> treeLeaves = accessor.getTreeLeaves();
    for (StateAccessorAvlLeafNode treeLeaf : treeLeaves) {
      Hash transactionId = treeLeaf.getKey().hashValue();
      IceStake iceStake = createFromAccessor(treeLeaf.getValue());
      pendingIceStakes = pendingIceStakes.set(transactionId, iceStake);
    }
    return pendingIceStakes;
  }
}
