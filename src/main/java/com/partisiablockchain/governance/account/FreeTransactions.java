package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;

@Immutable
final class FreeTransactions implements StateSerializableInline {

  private final long epoch;
  private final long spentFreeTransactions;

  FreeTransactions() {
    epoch = 0;
    spentFreeTransactions = 0;
  }

  private FreeTransactions(long epoch, long spentFreeTransactions) {
    this.epoch = epoch;
    this.spentFreeTransactions = spentFreeTransactions;
  }

  static FreeTransactions create() {
    return new FreeTransactions(0, 0);
  }

  public static FreeTransactions createFromStateAccessor(StateAccessor accessor) {
    return new FreeTransactions(
        accessor.get("epoch").longValue(), accessor.get("spentFreeTransactions").longValue());
  }

  public long getEpoch() {
    return epoch;
  }

  public long getSpentFreeTransactions() {
    return spentFreeTransactions;
  }

  /**
   * Increment the number of spent free transactions for the epoch.
   *
   * @param epoch current epoch
   * @return updated state
   */
  public FreeTransactions incrementSpentTransactionsForEpoch(long epoch) {
    if (epoch == this.epoch) {
      return new FreeTransactions(epoch, spentFreeTransactions + 1);
    } else {
      return new FreeTransactions(epoch, 1);
    }
  }
}
