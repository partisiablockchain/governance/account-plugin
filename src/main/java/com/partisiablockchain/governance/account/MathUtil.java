package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;

/** Utility class for computing {@link #ceilDiv}. */
public final class MathUtil {

  private MathUtil() {}

  /**
   * Divides long by long rounding up in the lowest range. That is:<code><br>
   * if (dividend == 0) return 0<br>
   * else if (dividend &lt; divisor) return 0<br>
   * else return dividend / divisor</code>
   *
   * @param dividend the dividend
   * @param divisor the divisor
   * @return the result
   */
  public static long ceilDiv(long dividend, long divisor) {
    return Math.floorDiv(dividend + divisor - 1, divisor);
  }

  /**
   * Performs Unsigned256 division and rounds up the remainder.
   *
   * @param dividend the dividend
   * @param divisor the divisor
   * @return the result
   */
  public static Unsigned256 ceilDiv(Unsigned256 dividend, Unsigned256 divisor) {
    Unsigned256 x = dividend.add(divisor.subtract(Unsigned256.ONE));
    Unsigned256 r = x.divide(divisor);
    return r;
  }

  @SuppressWarnings({"ComparatorResultComparison", "CompareToZero"})
  static Unsigned256 min(Unsigned256 left, Unsigned256 right) {
    return left.compareTo(right) == -1 ? left : right;
  }
}
