package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Keeps track of an accounts balance of MPC tokens and BYOC coins.
 *
 * @param accountCoins The amount of each BYOC coin owned by the account. The index in this list
 *     matches the index in the <code>coins</code> list in {@link AccountStateGlobal}.
 * @param mpcTokens The total amount of free MPC tokens.
 * @param stakedTokens The total amount of MPC tokens that are staked. To run a service the staked
 *     MPC tokens must be associated to a specific contract address in {@link #stakedToContract}.
 * @param stakedToContract The amounts and expirations of staked MPC tokens that are staked towards
 *     running specific blockchain services. The staked tokens can have an expiration timestamp, at
 *     which they can no longer be used to run new blockchain services. The contract address
 *     specifies which service the MPC tokens are staked towards.
 * @param pendingUnstakes MPC tokens which were staked, and are now being freed. When unstaking MPC
 *     tokens there is a 7 days waiting period during which they are kept here. The keys in the map
 *     are unix timestamps specifying when MPC tokens will be freed, and the values are the amounts
 *     of MPC tokens that will be freed at that time.
 * @param spentFreeTransactions Keeps track of the number of free transactions used in the current
 *     epoch.
 * @param vestingAccounts MPC tokens that are vested which will only become free for transfer after
 *     the vesting period.
 * @param storedPendingTransfers Keeps track of incoming/outgoing transfers of MPC tokens and BYOC
 *     coins while a two-phase commit protocol is being executed. The keys in the map are
 *     transaction event hashes from the first phase of the transfer, and the values are information
 *     about the pending transfer.
 * @param storedPendingStakeDelegations Keeps track of incoming/outgoing delegated MPC tokens while
 *     a two-phase commit protocol is being executed. The keys in the map are transaction event
 *     hashes from the first phase of the delegation, and the values are information about the
 *     pending stake delegation.
 * @param delegatedStakesFromOthers MPC tokens delegated from other users. If they are accepted MPC
 *     tokens delegated from others can be used to run a service by associating to a contract.
 *     Delegated tokens can have an expiration timestamp, at which they can no longer be used to
 *     associate to contract. The keys in the map are addresses of the accounts that have delegated
 *     MPC tokens to this account, and the values are information about the delegated stakes.
 * @param delegatedStakesToOthers MPC tokens delegated to other users for them to use as stake. The
 *     keys in the map are addresses of the accounts that MPC tokens have been delegated to, and the
 *     values are the amount of MPC tokens delegated to the corresponding account.
 * @param pendingRetractedDelegatedStakes MPC tokens, which were delegated to another user and are
 *     now being retracted and freed. Retraction of delegated MPC tokens has a 7 days waiting period
 *     during which they are kept here. The keys in the map are unix timestamps specifying when MPC
 *     tokens will be freed, and the values are the amounts of MPC tokens that will be freed at that
 *     time.
 * @param stakeable Specifies if this account is allowed to stake MPC tokens.
 * @param storedPendingIceStakes Pending ice stakes. I.e., ice stakes that have been locked on the
 *     account, but aren't tracked on the address yet.
 * @param tokensInCustody The total number of MPC tokens for the account. Calculated as {@link
 *     #stakedTokens} + sum of {@link #delegatedStakesFromOthers} + sum of {@link
 *     #pendingRetractedDelegatedStakes} + sum of {@link #pendingUnstakes} + sum of outgoing {@link
 *     #storedPendingTransfers} + sum of {@link #storedPendingStakeDelegations} which are either
 *     DELEGATE_STAKES or RETURN_DELEGATED_STAKES + unreleased vesting schedules from {@link
 *     #vestingAccounts} + {@link #mpcTokens}
 * @param stakingGoal The amount of tokens that the account wants to have staked for jobs. {@link
 *     #stakedTokens} + sum of all accepted delegated stakes, counts towards reaching the goal.
 */
@Immutable
public record Balance(
    FixedList<AccountCoin> accountCoins,
    long mpcTokens,
    long stakedTokens,
    AvlTree<BlockchainAddress, StakedToContract> stakedToContract,
    AvlTree<Long, Long> pendingUnstakes,
    FreeTransactions spentFreeTransactions,
    FixedList<InitialVestingAccount> vestingAccounts,
    AvlTree<Hash, TransferInformation> storedPendingTransfers,
    AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations,
    AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers,
    AvlTree<BlockchainAddress, Long> delegatedStakesToOthers,
    AvlTree<Long, Long> pendingRetractedDelegatedStakes,
    boolean stakeable,
    AvlTree<Hash, IceStake> storedPendingIceStakes,
    long tokensInCustody,
    Custodian custodian,
    long stakingGoal)
    implements StateSerializable {

  static final long UNSTAKE_DURATION_MILLIS = 7 * 1000L * 60 * 60 * 24; // 7 days

  static Balance createFromStateAccessor(StateAccessor accessor) {
    AvlTree<Hash, TransferInformation> storedPendingTransfers =
        StoredPendingTransfers.createFromStateAccessor(accessor.get("storedPendingTransfers"));

    AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
        createStoredPendingStakeDelegations(accessor.get("storedPendingStakeDelegations"));

    AvlTree<Long, Long> pendingRetractedDelegatedStakes =
        createPendingRetractedDelegatedStakes(accessor.get("pendingRetractedDelegatedStakes"));

    AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers =
        createDelegatedStakesFromOthers(accessor.get("delegatedStakesFromOthers"));

    AvlTree<BlockchainAddress, Long> delegatedStakesToOthers =
        createDelegatedStakesToOthers(accessor.get("delegatedStakesToOthers"));

    FixedList<AccountCoin> accountCoins =
        FixedList.create(
            accessor.get("accountCoins").getListElements().stream()
                .map(AccountCoin::createFromStateAccessor)
                .toList());
    long mpcTokens = accessor.get("mpcTokens").longValue();
    long stakedTokens = accessor.get("stakedTokens").longValue();
    AvlTree<BlockchainAddress, StakedToContract> stakedToContract =
        createStakedToContract(accessor.get("stakedToContract"));
    AvlTree<Long, Long> pendingUnstakes =
        accessor.get("pendingUnstakes").typedAvlTree(Long.class, Long.class);
    FreeTransactions spentFreeTransactions =
        FreeTransactions.createFromStateAccessor(accessor.get("spentFreeTransactions"));
    FixedList<InitialVestingAccount> vestingAccounts =
        FixedList.create(
            accessor.get("vestingAccounts").getListElements().stream()
                .map(InitialVestingAccount::createFromStateAccessor)
                .toList());
    boolean stakeable = accessor.get("stakeable").booleanValue();

    AvlTree<Hash, IceStake> pendingIceStakes =
        IceStake.createPendingIceStakes(accessor.get("storedPendingIceStakes"));

    long tokensInCustody = accessor.get("tokensInCustody").longValue();

    Custodian custodian = Custodian.createFromStateAccessor(accessor.get("custodian"));

    long stakingGoal = 0;
    if (accessor.hasField("stakingGoal")) {
      stakingGoal = accessor.get("stakingGoal").longValue();
    }

    return new Balance(
        accountCoins,
        mpcTokens,
        stakedTokens,
        stakedToContract,
        pendingUnstakes,
        spentFreeTransactions,
        vestingAccounts,
        storedPendingTransfers,
        storedPendingStakeDelegations,
        delegatedStakesFromOthers,
        delegatedStakesToOthers,
        pendingRetractedDelegatedStakes,
        stakeable,
        pendingIceStakes,
        tokensInCustody,
        custodian,
        stakingGoal);
  }

  private static AvlTree<BlockchainAddress, StakedToContract> createStakedToContract(
      StateAccessor accessor) {
    AvlTree<BlockchainAddress, StakedToContract> stakedToContract = AvlTree.create();
    if (accessor.isAvlTreeOfType(BlockchainAddress.class, Long.class)) {
      for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
        Long amount = entry.getValue().cast(Long.class);
        BlockchainAddress contractAddress = entry.getKey().blockchainAddressValue();
        stakedToContract =
            stakedToContract.set(contractAddress, StakedToContract.create(amount, null));
      }
    } else {
      for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
        Long amount = entry.getValue().get("amount").cast(Long.class);
        Long timestamp = entry.getValue().get("expirationTimestamp").cast(Long.class);
        BlockchainAddress contractAddress = entry.getKey().blockchainAddressValue();
        stakedToContract =
            stakedToContract.set(contractAddress, StakedToContract.create(amount, timestamp));
      }
    }
    return stakedToContract;
  }

  private static AvlTree<Hash, StakeDelegation> createStoredPendingStakeDelegations(
      StateAccessor accessor) {
    AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      Hash id = entry.getKey().hashValue();
      StakeDelegation value = StakeDelegation.createFromStateAccessor(entry.getValue());
      storedPendingStakeDelegations = storedPendingStakeDelegations.set(id, value);
    }
    return storedPendingStakeDelegations;
  }

  private static AvlTree<Long, Long> createPendingRetractedDelegatedStakes(StateAccessor accessor) {
    AvlTree<Long, Long> pendingRetractedDelegatedStakes = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      Long epoch = entry.getKey().longValue();
      Long amount = entry.getValue().longValue();
      pendingRetractedDelegatedStakes = pendingRetractedDelegatedStakes.set(epoch, amount);
    }
    return pendingRetractedDelegatedStakes;
  }

  private static AvlTree<BlockchainAddress, StakesFromOthers> createDelegatedStakesFromOthers(
      StateAccessor accessor) {
    AvlTree<BlockchainAddress, StakesFromOthers> stakesFromOthers = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      BlockchainAddress address = entry.getKey().blockchainAddressValue();
      long acceptedDelegatedStakes = entry.getValue().get("acceptedDelegatedStakes").longValue();
      long pendingDelegatedStakes = entry.getValue().get("pendingDelegatedStakes").longValue();

      Long expirationTimestamp = null;
      if (entry.getValue().hasField("expirationTimestamp")) {
        expirationTimestamp = entry.getValue().get("expirationTimestamp").cast(Long.class);
      }

      stakesFromOthers =
          stakesFromOthers.set(
              address,
              StakesFromOthers.create(
                  acceptedDelegatedStakes, pendingDelegatedStakes, expirationTimestamp));
    }
    return stakesFromOthers;
  }

  private static AvlTree<BlockchainAddress, Long> createDelegatedStakesToOthers(
      StateAccessor accessor) {
    AvlTree<BlockchainAddress, Long> delegatedStakes = AvlTree.create();
    List<StateAccessorAvlLeafNode> treeLeaves = accessor.getTreeLeaves();
    for (StateAccessorAvlLeafNode entry : treeLeaves) {
      BlockchainAddress address = entry.getKey().blockchainAddressValue();
      Long value = entry.getValue().longValue();
      delegatedStakes = delegatedStakes.set(address, value);
    }
    return delegatedStakes;
  }

  /**
   * Create a new account state.
   *
   * @return the created account state
   */
  static Balance create() {
    return new Balance(
        FixedList.create(),
        0,
        0,
        AvlTree.create(),
        AvlTree.create(),
        FreeTransactions.create(),
        FixedList.create(),
        AvlTree.create(),
        AvlTree.create(),
        AvlTree.create(),
        AvlTree.create(),
        AvlTree.create(),
        true,
        AvlTree.create(),
        0,
        new Custodian(null, null),
        0);
  }

  /**
   * Get the number of coins for the coin at the given index owned by this account.
   *
   * @param index the index of the coin
   * @return the number of coins owned
   */
  Unsigned256 coinBalance(int index) {
    if (index < accountCoins.size()) {
      return accountCoins.get(index).getBalance();
    } else {
      return Unsigned256.ZERO;
    }
  }

  /**
   * Get the amount of currently released tokens for a specific vesting schedule.
   *
   * @param index the tier of sale determining the vesting schedule
   * @return amount of released tokens
   */
  long releasedVestedTokens(int index) {
    return vestingAccounts.get(index).releasedTokens;
  }

  /**
   * Gets the amount of tokens associated to a contract.
   *
   * @param address address of the contract
   * @return the amount associated to the contract
   */
  long getAmountAssociationToContract(BlockchainAddress address) {
    return stakedToContract.containsKey(address) ? stakedToContract.getValue(address).amount : 0L;
  }

  long spentFreeTransactions(long epoch) {
    if (epoch < spentFreeTransactions.getEpoch()) {
      throw new IllegalArgumentException("Cannot get free transactions for previous epoch");
    } else if (epoch > spentFreeTransactions.getEpoch()) {
      return 0;
    } else {
      return spentFreeTransactions.getSpentFreeTransactions();
    }
  }

  /**
   * Compute total gas balance.
   *
   * @param ratioLookup lookup function for coin conversion rate
   * @return the total gas balance including coin gas values
   */
  Unsigned256 totalBalance(RatioLookup ratioLookup) {
    Unsigned256 totalBalance = Unsigned256.ZERO;
    for (int i = 0; i < accountCoins.size(); i++) {
      AccountCoin accountCoin = accountCoins.get(i);
      totalBalance = totalBalance.add(accountCoin.computeGasEquivalent(ratioLookup.apply(i)));
    }
    return totalBalance;
  }

  interface RatioLookup extends Function<Integer, Fraction> {}

  /**
   * Get a pending iceStake from given transactionId.
   *
   * @param transactionId The ID of the transaction to get the iceStake from
   * @return The iceStake from given transactionId
   */
  public IceStake pendingIceStake(Hash transactionId) {
    return storedPendingIceStakes.getValue(transactionId);
  }

  /**
   * Determines whether the account has an accepted custodian.
   *
   * @return {@code true} if the account has an accepted custodian, otherwise {@code false}.
   */
  public boolean hasCustodian() {
    return custodian.accepted() != null;
  }

  /**
   * Determines whether the specified blockchain address is the custodian of the account.
   *
   * @param senderAddress the blockchain address to be tested
   * @return {@code true} if the specified blockchain address is the custodian of the account,
   *     otherwise {@code false}.
   */
  public boolean isCustodian(BlockchainAddress senderAddress) {
    return custodian.accepted().equals(senderAddress);
  }

  /**
   * Determines whether the account has an appointed custodian.
   *
   * @return {@code true} if the account has an appointed custodian, otherwise {@code false}.
   */
  public boolean hasAppointedCustodian() {
    return custodian.appointed() != null;
  }

  /**
   * Determines whether the specified blockchain address is an appointed custodian of the account.
   *
   * @param senderAddress the blockchain address to be tested
   * @return {@code true} the specified blockchain address is a appointed custodian of the account,
   *     otherwise {@code false}.
   */
  public boolean isAppointedCustodian(BlockchainAddress senderAddress) {
    return custodian.appointed().equals(senderAddress);
  }

  /**
   * The total of self staked and accepted delegated stakes from others.
   *
   * @return sum of staked tokens for jobs
   */
  long stakeForJobs() {
    return this.asMutable().stakeForJobs();
  }

  /**
   * Create a {@link BalanceMutable} for mutation.
   *
   * @return the created {@link BalanceMutable}
   */
  BalanceMutable asMutable() {
    return new BalanceMutable(this);
  }

  Balance mutate(Consumer<BalanceMutable> mutator) {
    BalanceMutable bm = asMutable();
    mutator.accept(bm);
    return bm.asImmutable();
  }
}
