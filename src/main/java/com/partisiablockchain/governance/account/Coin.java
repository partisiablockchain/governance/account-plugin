package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;

/** A coin on the chain. */
@Immutable
public final class Coin implements StateSerializableInline {

  /** The symbol of this coin, e.g. ETH. */
  public final String symbol;

  /** The gas conversion rate. Determines how much gas one unit of this coin can be converted to. */
  private final Fraction conversionRate;

  @SuppressWarnings("unused")
  Coin() {
    symbol = null;
    conversionRate = new Fraction();
  }

  private Coin(String symbol, Fraction conversionRate) {
    this.symbol = symbol;
    this.conversionRate = conversionRate;
  }

  /**
   * Create a new Coin object from the supplied state.
   *
   * @param accessor The state to initialize the Coin from.
   * @return A new Coin object, with its state initialized from the accessor.
   */
  public static Coin createFromStateAccessor(StateAccessor accessor) {
    String symbol = accessor.get("symbol").stringValue();
    Fraction conversionRate = Fraction.createFromStateAccessor(accessor.get("conversionRate"));
    return new Coin(symbol, conversionRate);
  }

  String getSymbol() {
    return symbol;
  }

  Fraction getConversionRate() {
    return conversionRate;
  }

  static Coin create(String symbol, Fraction conversionRate) {
    return new Coin(symbol, conversionRate);
  }
}
