package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;
import java.util.Objects;

/** Helper class for representing fractions, used for Coin Conversion Rates. */
@Immutable
final class Fraction implements StateSerializableInline {

  public final long numerator;
  public final long denominator;

  static Fraction createFromStateAccessor(StateAccessor accessor) {
    if (accessor.isNull()) {
      return null;
    } else {
      return new Fraction(
          accessor.get("numerator").longValue(), accessor.get("denominator").longValue());
    }
  }

  @SuppressWarnings("unused")
  public Fraction() {
    this.numerator = 0;
    this.denominator = 0;
  }

  /**
   * Creates an object representing a fraction for conversion rates.
   *
   * @param numerator numerator of fraction
   * @param denominator denominator of fraction
   */
  public Fraction(long numerator, long denominator) {
    this.numerator = numerator;
    this.denominator = denominator;
  }

  public long getNumerator() {
    return numerator;
  }

  public long getDenominator() {
    return denominator;
  }

  public boolean isZero() {
    return numerator == 0;
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (this.getClass() != other.getClass()) {
      return false;
    }
    if (this.numerator != ((Fraction) other).numerator) {
      return false;
    }
    return this.denominator == ((Fraction) other).denominator;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.numerator, this.denominator);
  }
}
