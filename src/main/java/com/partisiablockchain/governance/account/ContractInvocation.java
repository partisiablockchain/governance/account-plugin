package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.BlockchainAccountPlugin.ContractState;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainAccountPlugin;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/**
 * Implementations for contract state-based invocations on the {@link GasAndCoinAccountPlugin}.
 *
 * <p>These invocations are associated with blockchain user accounts states, and can only be invoked
 * by {@link BlockchainAddress}es of any of the contract {@link BlockchainAddress.Type}s.
 */
public enum ContractInvocation implements Invocation {
  /**
   * Initiate a two phased BYOC transfer as the sender.
   *
   * <p>Adds the transfer into pending transfers, such that it can be committed or aborted in the
   * second phase of the transfer. Fails if a deposit for the same contract and ID exists.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the transaction ID of the two phased BYOC transfer.
   *   <li><code>Unsigned256</code> amount to receive.
   *   <li><code>String</code> the symbol of BYOC.
   * </ul>
   *
   * <p>Invocation byte: 31
   */
  INITIATE_BYOC_TRANSFER_SENDER(31) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {
      Hash transactionId = Hash.read(rpc);
      Unsigned256 amount = Unsigned256.read(rpc);
      String symbol = rpc.readString();

      ensure(
          amount.compareTo(Unsigned256.ZERO) > 0, "Cannot withdraw a non-positive amount of BYOC");

      ContractStorage storage =
          initiateWithdraw(
              contractState.contract,
              transactionId,
              amount,
              GasAndCoinAccountPlugin.getCoinIndex(accountStateGlobal, symbol),
              symbol);
      return new ContractState<>(contractState.local, storage);
    }
  },
  /**
   * Initiate a two phased BYOC transfer as the receiver.
   *
   * <p>Adds the transfer into pending transfers, such that it can be committed or aborted in the
   * second phase of the transfer. Fails if a withdrawal for the same contract and ID exists.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the transaction ID of the two phased BYOC transfer.
   *   <li><code>Unsigned256</code> amount to receive.
   *   <li><code>String</code> the symbol of BYOC.
   * </ul>
   *
   * <p>Invocation byte: Same as {@link AccountInvocation#INITIATE_BYOC_TRANSFER_RECIPIENT}.
   */
  INITIATE_BYOC_TRANSFER_RECIPIENT(
      AccountInvocation.INITIATE_BYOC_TRANSFER_RECIPIENT.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {
      Hash transactionId = Hash.read(rpc);
      Unsigned256 amount = Unsigned256.read(rpc);
      String symbol = rpc.readString();

      ensure(
          amount.compareTo(Unsigned256.ZERO) > 0, "Cannot deposit a non-positive amount of BYOC");

      ContractStorage storage =
          initiateDeposit(
              contractState.contract,
              transactionId,
              amount,
              GasAndCoinAccountPlugin.getCoinIndex(accountStateGlobal, symbol));
      return new ContractState<>(contractState.local, storage);
    }
  },
  /**
   * Commit two phase transfer.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the transaction ID of the two phased transfer.
   * </ul>
   *
   * <p>Invocation byte: Same as {@link AccountInvocation#COMMIT_TRANSFER}.
   */
  COMMIT_TRANSFER(AccountInvocation.COMMIT_TRANSFER.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {

      Hash transactionId = Hash.read(rpc);

      ContractStorage storage = commitTransfer(contractState.contract, transactionId);
      return new ContractState<>(contractState.local, storage);
    }
  },
  /**
   * Abort two phase transfer.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the transaction ID of the two phased transfer.
   * </ul>
   *
   * <p>Invocation byte: Same as {@link AccountInvocation#ABORT_TRANSFER}.
   */
  ABORT_TRANSFER(AccountInvocation.ABORT_TRANSFER.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {

      Hash transactionId = Hash.read(rpc);

      ContractStorage storage = abortTransfer(contractState.contract, transactionId);
      return new ContractState<>(contractState.local, storage);
    }
  },

  /**
   * Initiate ice-box staking flow.
   *
   * <p>Starts the process of receiving a number of stakes from an account, for the purpose of
   * keeping the contract alive (but "on-ice") in case it runs out of gas. After this invocation,
   * the contract will have a pending ice-stake object in its state, denoting who is staking towards
   * it as well as the amount.
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> a transaction ID for this ice-box staking flow.
   *   <li><code>Long</code> the amount of tokens being staked towards this contract.
   *   <li><code>BlockchainAddress</code> the address of the account doing the staking.
   * </ul>
   *
   * <p>Invocation byte: 35
   */
  INITIATE_ICE_ASSOCIATE(35) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {

      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      BlockchainAddress account = BlockchainAddress.read(rpc);

      ensure(amount > 0, "Cannot stake non-positive amount of ice stakes");

      ContractStorage storage = contractState.contract;
      if (storage.getPendingIceStake(transactionId) == null) {
        IceStake iceStake = new IceStake(account, amount, IceStake.IceStakeType.ASSOCIATE);
        storage = storage.addPendingIceStake(transactionId, iceStake);
        return new ContractState<>(contractState.local, storage);
      } else {
        return contractState;
      }
    }
  },

  /**
   * Initiate the process of returning ice-box stakes to an account.
   *
   * <p>This invocation starts the process of returning ice-box stakes to an account. After the
   * invocation, the contract will have decreased the stakes of an account and will have created a
   * pending IceStake object that tracks this retract flow.
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> transaction ID to associated with this flow.
   *   <li><code>Long</code> the amount of tokens to retract.
   *   <li><code>BlockchainAddress</code> the address of the account that is retracting their
   *       stakes.
   * </ul>
   *
   * <p>Invocation byte: 36
   */
  INITIATE_ICE_DISASSOCIATE(36) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {

      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      BlockchainAddress account = BlockchainAddress.read(rpc);

      ensure(amount > 0, "Cannot retract non-positive amount of ice stakes");

      ContractStorage contract = contractState.contract;
      if (contract.getPendingIceStake(transactionId) == null) {
        IceStake iceStake = new IceStake(account, amount, IceStake.IceStakeType.DISASSOCIATE);
        contract = contract.addPendingIceStake(transactionId, iceStake);
        contract = contract.subtractIceStakes(iceStake.address(), iceStake.amount());
        return new ContractState<>(contractState.local, contract);
      } else {
        return contractState;
      }
    }
  },

  /**
   * Commit and finalize an ice-box staking flow.
   *
   * <p>After this invocation the contract will have an updated entry in its state denoting the
   * amount of stakes that an account has associated with it. The updated amount is either more or
   * less than the original amount, depending on whether the initiating invocation was made to
   * associate or disassociate ice stakes to the contract.
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> a transaction ID previously sent in an initiate invocation.
   * </ul>
   *
   * <p>Invocation byte: Same as {@link AccountInvocation#COMMIT_ICE}.
   */
  COMMIT_ICE(AccountInvocation.COMMIT_ICE.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {

      Hash transactionId = Hash.read(rpc);

      ContractStorage contract = contractState.contract;
      IceStake pendingIceStake = contract.getPendingIceStake(transactionId);
      if (pendingIceStake == null) {
        return contractState;
      } else {
        if (pendingIceStake.type() == IceStake.IceStakeType.ASSOCIATE) {
          contract = contract.addIceStakes(pendingIceStake.address(), pendingIceStake.amount());
        }
        contract = contract.removePendingIceStake(transactionId);
        return new ContractState<>(contractState.local, contract);
      }
    }
  },

  /**
   * Abort and revert an ice-box staking flow.
   *
   * <p>This invocation removes the object tracking the ice-stake flow that is being aborted. If the
   * ice-stake flow being aborted was made to disassociate stakes, then the disassociated stakes are
   * re-added to the contract's state.
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> a transaction ID previously sent during an initiate invocation.
   * </ul>
   *
   * <p>Invocation byte: Same as {@link AccountInvocation#ABORT_ICE}.
   */
  ABORT_ICE(AccountInvocation.ABORT_ICE.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {

      Hash transactionId = Hash.read(rpc);

      ContractStorage contract = contractState.contract;
      IceStake pendingIceStake = contract.getPendingIceStake(transactionId);
      if (pendingIceStake == null) {
        return contractState;
      } else {
        if (pendingIceStake.type() != IceStake.IceStakeType.ASSOCIATE) {
          contract = contract.addIceStakes(pendingIceStake.address(), pendingIceStake.amount());
        }
        contract = contract.removePendingIceStake(transactionId);
        return new ContractState<>(contractState.local, contract);
      }
    }
  },

  /**
   * Initiate a two-phase MPC token transfer as the sender.
   *
   * <p>This invocation starts the two-phase process of an MPC token transfer as a sender, adding
   * the transfer into pending transfers, such that it can be committed or aborted in the second
   * phase. Fails if a withdrawal for the same account and ID exists.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> used for custodians. Must be null as custodians are not
   *       supported for contracts.
   *   <li><code>Hash</code> the ID of the two-phased MPC token transfer.
   *   <li><code>long</code> amount to send. Must be positive.
   * </ul>
   */
  INITIATE_WITHDRAW(AccountInvocation.INITIATE_WITHDRAW.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {
      ensure(
          rpc.readOptional(BlockchainAddress::read) == null,
          "Custodians are not supported for contracts");
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(amount > 0, "Can not withdraw a non-positive amount of MPC tokens");
      ContractStorage contractStorage =
          initiateWithdraw(
              contractState.contract, transactionId, Unsigned256.create(amount), -1, null);
      return new ContractState<>(contractState.local, contractStorage);
    }
  },

  /**
   * Initiate a two-phase MPC token transfer as the receiver.
   *
   * <p>This invocation starts the two-phase process of an MPC token transfer as a receiver, adding
   * the transfer into pending transfers, such that it can be committed or aborted in the second
   * phase. Fails if a deposit for the same account and ID exists.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the ID of the two-phased MPC token transfer.
   *   <li><code>long</code> amount to send. Must be positive.
   * </ul>
   */
  INITIATE_DEPOSIT(AccountInvocation.INITIATE_DEPOSIT.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(amount > 0, "Can not deposit a non-positive amount of MPC tokens");
      ContractStorage contractStorage =
          initiateDeposit(contractState.contract, transactionId, Unsigned256.create(amount), -1);
      return new ContractState<>(contractState.local, contractStorage);
    }
  },

  /**
   * Adds MPC token to the contract's MPC token balance.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>long</code> amount to be added. Must be positive.
   * </ul>
   *
   * <p>Invocation byte: 2
   */
  ADD_MPC_TOKEN_BALANCE(AccountInvocation.ADD_MPC_TOKEN_BALANCE.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {
      long amountToAdd = rpc.readLong();
      ensure(amountToAdd > 0, "Can not add a non-positive amount of MPC tokens");
      ContractStorage contractStorageWithAddedTokens =
          contractState.contract.addMpcToken(amountToAdd);
      return new ContractState<>(contractState.local, contractStorageWithAddedTokens);
    }
  },

  /**
   * Deducts MPC token from the contract's MPC token balance.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>long</code> amount to be deducted. Must be positive.
   * </ul>
   *
   * <p>Invocation byte: 3
   */
  DEDUCT_MPC_TOKEN_BALANCE(AccountInvocation.DEDUCT_MPC_TOKEN_BALANCE.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {
      long amountToDeduct = rpc.readLong();
      ensure(amountToDeduct > 0, "Can not deduct a non-positive amount of MPC tokens");
      ContractStorage contractStorageWithDeductedTokens =
          contractState.contract.deductMpcToken(amountToDeduct);
      return new ContractState<>(contractState.local, contractStorageWithDeductedTokens);
    }
  },
  /**
   * Adds amount of the given coin, if it exists, to this contract' balance.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>String</code> symbol of the coin to add to.
   *   <li><code>Unsigned256</code> amount to be added.
   * </ul>
   *
   * <p>Invocation byte: 0
   */
  ADD_COIN_BALANCE(AccountInvocation.ADD_COIN_BALANCE.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {
      String symbol = rpc.readString();
      Unsigned256 amount = Unsigned256.read(rpc);
      ensure(
          amount.compareTo(Unsigned256.ZERO) > 0,
          "Can not add a non-positive amount of %s".formatted(symbol));
      ContractStorage contractStorage = contractState.contract;
      contractStorage =
          contractStorage.addByoc(
              GasAndCoinAccountPlugin.getCoinIndex(accountStateGlobal, symbol), amount);
      return new BlockchainAccountPlugin.ContractState<>(contractState.local, contractStorage);
    }
  },
  /**
   * Deducts amount of the given coin, if it exists, to this contract's balance.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>String</code> symbol of the coin to deduct.
   *   <li><code>Unsigned256</code> amount to be deducted.
   * </ul>
   *
   * <p>Invocation byte: 1
   */
  DEDUCT_COIN_BALANCE(AccountInvocation.DEDUCT_COIN_BALANCE.getInvocationByte()) {
    @Override
    ContractState<AccountStateLocal, ContractStorage> invoke(
        PluginContext pluginContext,
        AccountStateGlobal accountStateGlobal,
        ContractState<AccountStateLocal, ContractStorage> contractState,
        BlockchainAddress contractAddress,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue) {
      String symbol = rpc.readString();
      Unsigned256 amount = Unsigned256.read(rpc);
      ensure(
          amount.compareTo(Unsigned256.ZERO) > 0,
          "Can not deduct a non-positive amount of %s".formatted(symbol));
      ContractStorage contractStorage = contractState.contract;
      contractStorage =
          contractStorage.deductByoc(
              GasAndCoinAccountPlugin.getCoinIndex(accountStateGlobal, symbol), symbol, amount);
      return new BlockchainAccountPlugin.ContractState<>(contractState.local, contractStorage);
    }
  };

  /**
   * Determines the relevant {@link ContractInvocation} from the given RPC stream.
   *
   * @param rpc RPC stream to read invocation byte from.
   * @return invokable invocation
   * @throws UnsupportedOperationException if account plugin does not support the read invocation
   *     byte.
   */
  static ContractInvocation getInvocation(SafeDataInputStream rpc) {
    final int invocationByte = rpc.readUnsignedByte();
    return Invocation.findInvocationBy(values(), invocationByte, "Contract");
  }

  private static void ensure(boolean predicate, String errorString) {
    if (!predicate) {
      throw new RuntimeException(errorString);
    }
  }

  ContractInvocation(int invocationByte) {
    this.invocationByte = (byte) invocationByte;
  }

  private final byte invocationByte;

  @Override
  public byte getInvocationByte() {
    return invocationByte;
  }

  abstract ContractState<AccountStateLocal, ContractStorage> invoke(
      PluginContext pluginContext,
      AccountStateGlobal accountStateGlobal,
      ContractState<AccountStateLocal, ContractStorage> contractState,
      BlockchainAddress contractAddress,
      SafeDataInputStream rpc,
      SafeDataOutputStream returnValue);

  private static ContractStorage initiateDeposit(
      ContractStorage contract, Hash transactionId, Unsigned256 amount, int coinIndex) {

    TransferInformation transfer = contract.getStoredPendingTransfers().getValue(transactionId);

    if (transfer == null) {
      TransferInformation transferInformation =
          TransferInformation.createForDeposit(amount, coinIndex);
      contract = contract.addPendingTransfer(transactionId, transferInformation);
    } else if (transfer.isWithdraw()) {
      throw new IllegalArgumentException("Cannot initiate transfer between the same contract");
    }

    return contract;
  }

  private static ContractStorage initiateWithdraw(
      ContractStorage contract,
      Hash transactionId,
      Unsigned256 amount,
      int coinIndex,
      String symbol) {

    TransferInformation transfer = contract.getStoredPendingTransfers().getValue(transactionId);

    if (transfer == null) {
      TransferInformation transferInformation =
          TransferInformation.createForWithdraw(amount, coinIndex);
      contract = contract.addPendingTransfer(transactionId, transferInformation);
      contract =
          coinIndex == -1
              ? contract.deductMpcToken(amount.longValueExact())
              : contract.deductByoc(coinIndex, symbol, amount);
    } else if (transfer.isDeposit()) {
      throw new IllegalArgumentException("Cannot initiate transfer between the same contract");
    }

    return contract;
  }

  private static ContractStorage commitTransfer(ContractStorage contract, Hash transactionId) {
    TransferInformation transfer = contract.getStoredPendingTransfers().getValue(transactionId);

    if (transfer != null) {
      if (transfer.isDeposit()) {
        contract =
            transfer.getCoinIndex() == -1
                ? contract.addMpcToken(transfer.getAmount().longValueExact())
                : contract.addByoc(transfer.coinIndex(), transfer.getAmount());
      }
      contract = contract.removePendingTransfer(transactionId);
    }

    return contract;
  }

  private static ContractStorage abortTransfer(ContractStorage contract, Hash transactionId) {
    TransferInformation transfer = contract.getStoredPendingTransfers().getValue(transactionId);

    if (transfer != null) {
      if (transfer.isWithdraw()) {
        contract =
            transfer.getCoinIndex() == -1
                ? contract.addMpcToken(transfer.getAmount().longValueExact())
                : contract.addByoc(transfer.coinIndex(), transfer.getAmount());
      }
      contract = contract.removePendingTransfer(transactionId);
    }

    return contract;
  }
}
