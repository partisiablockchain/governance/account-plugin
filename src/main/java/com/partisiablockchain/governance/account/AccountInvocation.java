package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainAccountPlugin;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/**
 * Implementations for account state-based invocations on the {@link GasAndCoinAccountPlugin}.
 *
 * <p>These invocations are associated with blockchain user account states, and can only be invoked
 * by {@link BlockchainAddress}es of type {@link BlockchainAddress.Type#ACCOUNT}.
 */
public enum AccountInvocation implements Invocation {
  /**
   * Adds amount of the given coin if it exists.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>String</code> symbol of the coin to add to.
   *   <li><code>Unsigned256</code> amount to be added.
   * </ul>
   *
   * <p>Invocation byte: 0
   */
  ADD_COIN_BALANCE(0) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      String symbol = rpc.readString();
      Unsigned256 amount = Unsigned256.read(rpc);
      BalanceMutable balance = account(accountState).asMutable();
      balance.addCoins(GasAndCoinAccountPlugin.getCoinIndex(global, symbol), amount);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Deducts amount of the given coin if it exists.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>String</code> symbol of the coin to deduct from.
   *   <li><code>Unsigned256</code> amount to be deducted. Cannot be greater than the current amount
   *       of coins on this account.
   * </ul>
   *
   * <p>Invocation byte: 1
   */
  DEDUCT_COIN_BALANCE(1) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      ensure(
          account(accountState).custodian().accepted() == null,
          "Disabled for accounts with a custodian");

      String symbol = rpc.readString();
      Unsigned256 amount = Unsigned256.read(rpc);
      BalanceMutable balance = account(accountState).asMutable();
      balance.deductCoins(GasAndCoinAccountPlugin.getCoinIndex(global, symbol), amount, symbol);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Adds MPC token to balance.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>long</code> amount to be added. Must be positive.
   * </ul>
   *
   * <p>Invocation byte: 2
   */
  ADD_MPC_TOKEN_BALANCE(2) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot add a non-positive amount of tokens");
      BalanceMutable balance = account(accountState).asMutable();
      balance.addMpcTokenBalance(amount);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Deducts MPC token from balance.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>long</code> amount to be deducted. Must be positive.
   * </ul>
   *
   * <p>Invocation byte: 3
   */
  DEDUCT_MPC_TOKEN_BALANCE(3) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot deduct a non-positive amount of tokens");
      ensure(
          account(accountState).custodian().accepted() == null,
          "Can not deduct MPC tokens for an account with a custodian");
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.deductMpcTokenBalance(amount);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Get staked token balance for this account.
   *
   * <p>Invocation byte: 4
   */
  GET_STAKED_TOKEN_BALANCE(4) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      returnValue.writeLong(account.stakeForJobs());
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, account);
    }
  },
  /**
   * Associate tokens to the given contract if enough tokens are staked and available.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>long</code> amount to be associated. Must be positive.
   *   <li><code>BlockchainAddress</code> contract to associate staked tokens to.
   * </ul>
   *
   * <p>Invocation byte: 33
   */
  ASSOCIATE(33) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot associate non-positive amount");
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);
      BalanceMutable balance = account(accountState).asMutable();

      balance.associateWithCurrentExpiration(contractAddress, amount);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Associate tokens to the given contract with an expiration timestamp if enough tokens are staked
   * and available at the given timestamp.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>long</code> amount to be associated. Must be positive.
   *   <li><code>BlockchainAddress</code> contract to associate staked tokens to.
   *   <li><code>long</code> timestamp the association expires. Must be non-negative.
   * </ul>
   *
   * <p>Invocation byte: 78
   */
  ASSOCIATE_WITH_EXPIRATION(78) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot associate non-positive amount");
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);

      long expirationTimestamp = rpc.readLong();
      ensure(expirationTimestamp >= 0, "Expiration timestamp cannot be negative");
      BalanceMutable balance = account(accountState).asMutable();

      balance.associateWithExpiration(contractAddress, amount, expirationTimestamp);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Sets the expiration timestamp for when the associations can no longer be used for new jobs.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> contract associated to.
   *   <li><code>Long</code> (optional) the timestamp when the association are no longer valid to be
   *       used as collateral for new jobs.
   * </ul>
   *
   * <p>Invocation byte: 80
   */
  SET_EXPIRATION_TIMESTAMP_FOR_ASSOCIATION(80) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Balance account = account(accountState);
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);
      Long expirationTimestamp = rpc.readOptional(SafeDataInputStream::readLong);
      ensure(
          expirationTimestamp == null || expirationTimestamp >= 0,
          "Expiration timestamp cannot be negative");

      BalanceMutable updatedBalance = account.asMutable();
      updatedBalance.updateTimestampForAssociations(contractAddress, expirationTimestamp);

      Balance newBalance = updatedBalance.asImmutable();
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, newBalance);
    }
  },

  /**
   * Disassociate tokens from the given contract if enough are associated.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>long</code> amount to be disassociated. Must be positive.
   *   <li><code>BlockchainAddress</code> contract to disassociate tokens from.
   * </ul>
   *
   * <p>Invocation byte: 34
   */
  DISASSOCIATE(34) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot disassociate non-positive amount");
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);
      BalanceMutable balance = account(accountState).asMutable();
      balance.disassociate(contractAddress, amount);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Disassociate all tokens to a contract.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> contract to disassociate all tokens from.
   * </ul>
   *
   * <p>Invocation byte: 7
   */
  DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT(7) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);
      BalanceMutable balance = account(accountState).asMutable();
      balance.disassociateAllTokensForContract(contractAddress);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Burn MPC tokens.
   *
   * <p>Burns MPC tokens if enough tokens are associated to the given contract. The tokens burned
   * will be a weighted distribution of the account's own tokens and received delegated tokens.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>long</code> amount to burn. Must be positive.
   *   <li><code>BlockchainAddress</code> contract to burn tokens from.
   * </ul>
   *
   * <p>Invocation byte: 10
   */
  BURN_MPC_TOKENS(10) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot burn non-positive amount of tokens");
      BlockchainAddress contractAddress = BlockchainAddress.read(rpc);
      BalanceMutable balance = account(accountState).asMutable();

      balance.disassociateAndBurn(amount, contractAddress);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Initiate a two phased MPC token transfer as the recipient.
   *
   * <p>Adds the transfer into pending transfers, such that it can be committed or aborted in the
   * second phase of the transfer. Fails if a withdrawal for the same account and ID exists.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the ID of the two-phased MPC token transfer.
   *   <li><code>long</code> amount to receive. Must be positive.
   * </ul>
   *
   * <p>Invocation byte: 12
   */
  INITIATE_DEPOSIT(12) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot transfer a non-positive amount of tokens");
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.initiateDeposit(
          transactionId, TransferInformation.createForDeposit(Unsigned256.create(amount), -1));
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Initiate receiving delegated stakes.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the ID of the two-phased MPC tokens delegation.
   *   <li><code>long</code> the amount to receive. Must be positive.
   *   <li><code>BlockchainAddress</code> the counter part in the delegation.
   * </ul>
   *
   * <p>Invocation byte: 22
   */
  INITIATE_RECEIVE_DELEGATED_STAKES(22) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(
          amount > 0,
          "Cannot initiate the receive part of a stake delegation with a non-positive amount of"
              + " tokens");
      BlockchainAddress counterPart = BlockchainAddress.read(rpc);

      StakeDelegation stakeDelegation =
          StakeDelegation.createReceiveDelegation(
              counterPart, amount, pluginContext.blockProductionTime(), null);
      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
          account(accountState).asMutable().storedPendingStakeDelegations();
      ensureStakeActionIsBetweenDifferentAccounts(
          transactionId, stakeDelegation, storedPendingStakeDelegations);

      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.initiateReceiveDelegatedStakes(transactionId, stakeDelegation);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Initiate returning delegated stakes.
   *
   * <p>Tries to return the given amount of the delegated tokens received to <var>counterPart</var>.
   * Can only return available delegated tokens.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the ID of the two-phased return delegation.
   *   <li><code>long</code> the delegated amount to return to <var>counterPart</var>. Must be
   *       positive.
   *   <li><code>BlockchainAddress</code> the <var>counterPart</var> of the delegated stakes.
   * </ul>
   *
   * <p>Invocation byte: 24
   */
  INITIATE_RETURN_DELEGATED_STAKES(24) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(
          amount > 0,
          "Cannot initiate the return part of stake delegation with a non-positive amount of"
              + " tokens");
      BlockchainAddress counterPart = BlockchainAddress.read(rpc);

      StakeDelegation stakeDelegation = StakeDelegation.createReturnDelegation(counterPart, amount);

      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
          account(accountState).asMutable().storedPendingStakeDelegations();
      ensureStakeActionIsBetweenDifferentAccounts(
          transactionId, stakeDelegation, storedPendingStakeDelegations);

      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.initiateReturnDelegatedStakes(transactionId, stakeDelegation);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Commit two phase transfer.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the transaction ID of the two phased transfer.
   * </ul>
   *
   * <p>Invocation byte: 13
   */
  COMMIT_TRANSFER(13) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Hash transactionId = Hash.read(rpc);
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.commitTransfer(transactionId);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Abort two phase transfer.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the transaction ID of the two phased transfer.
   * </ul>
   *
   * <p>Invocation byte: 14
   */
  ABORT_TRANSFER(14) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Hash transactionId = Hash.read(rpc);
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.abortTransfer(transactionId);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Commit delegating stakes.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the transaction ID of the two phased delegation.
   * </ul>
   *
   * <p>Invocation byte: 25
   */
  COMMIT_DELEGATE_STAKES(25) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Hash transactionId = Hash.read(rpc);
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.commitDelegatedStakes(transactionId, pluginContext.blockProductionTime());
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Abort delegating stakes.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the transaction ID of the two phased delegation.
   * </ul>
   *
   * <p>Invocation byte: 26
   */
  ABORT_DELEGATE_STAKES(26) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Hash transactionId = Hash.read(rpc);
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.abortDelegateStakes(transactionId);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Create vesting account.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>long</code> amount to vest. Must be positive.
   *   <li><code>long</code> release duration, total amount of time before getting all tokens.
   *   <li><code>long</code> release interval, how often to release tokens.
   *   <li><code>long</code> token generation event, when the vesting duration starts.
   * </ul>
   *
   * <p>Invocation byte: 15
   */
  CREATE_VESTING_ACCOUNT(15) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      long amountOfVestedTokens = rpc.readLong();
      long releaseDuration = rpc.readLong();
      long releaseInterval = rpc.readLong();
      long tokenGenerationEvent = rpc.readLong();
      ensure(
          amountOfVestedTokens > 0,
          "Cannot create vesting account for non-positive amount of tokens");
      BalanceMutable balance = account(accountState).asMutable();
      balance.createVestingAccount(
          amountOfVestedTokens, tokenGenerationEvent, releaseDuration, releaseInterval);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Flag an account, making it unable to stake.
   *
   * <p>Invocation byte: 18
   */
  MAKE_ACCOUNT_NON_STAKEABLE(18) {
    @Override
    public BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      BalanceMutable account = account(accountState).asMutable();
      account.makeNonStakeable();
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, account.asImmutable());
    }
  },
  /**
   * Initiate a two phased BYOC transfer as the receiver.
   *
   * <p>Adds the transfer into pending transfers, such that it can be committed or aborted in the
   * second phase of the transfer. Fails if there is a withdrawal for the same account and ID.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the transaction ID of the two phased BYOC transfer.
   *   <li><code>Unsigned256</code> amount to receive transfer.
   *   <li><code>String</code> the symbol of BYOC.
   * </ul>
   *
   * <p>Invocation byte: 32
   */
  INITIATE_BYOC_TRANSFER_RECIPIENT(32) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Hash transactionId = Hash.read(rpc);
      Unsigned256 amount = Unsigned256.read(rpc);
      String symbol = rpc.readString();
      ensure(
          amount.compareTo(Unsigned256.ZERO) > 0, "Cannot transfer a non-positive amount of BYOC");
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.initiateDeposit(
          transactionId,
          TransferInformation.createForDeposit(
              amount, GasAndCoinAccountPlugin.getCoinIndex(global, symbol)));
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Commit an ice-box stake.
   *
   * <p>This invocation finalizes the process of associating stakes towards a contract for the sake
   * of keeping it alive in case it runs out of gas.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> a transaction ID previously sent in the initiate invocation.
   * </ul>
   *
   * <p>Invocation byte: 37
   */
  COMMIT_ICE(37) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Hash transactionId = Hash.read(rpc);
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.commitIce(transactionId);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Abort an ice-box stake.
   *
   * <p>This invocation reverts and aborts an ongoing ice-box staking flow. After this invocation,
   * the tokens associated in the initiate call will be disassociated.
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> a transaction ID previously sent in an initiate invocation.
   * </ul>
   *
   * <p>Invocation byte: 38
   */
  ABORT_ICE(38) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Hash transactionId = Hash.read(rpc);
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.abortIce(transactionId);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Appoint a custodian for the account.
   *
   * <p>The initial custodian has to be set by the owner of the account. Appointing a new custodian
   * has to be done by the custodian.
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>BlockchainAddress</code> address of the custodian to appoint.
   * </ul>
   *
   * <p>Invocation byte: 39
   */
  APPOINT_CUSTODIAN(39) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);

      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      BlockchainAddress custodianAddress = BlockchainAddress.read(rpc);

      Balance updatedAccount =
          account.mutate(mutable -> mutable.appointCustodian(custodianAddress));
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, updatedAccount);
    }
  },
  /**
   * Cancel the appointment of a custodian for the account.
   *
   * <p>Cancelling the appointment of the initial custodian can only be done by the owner of the
   * account. Cancelling the appointment a new custodian has to be done by the custodian.
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   * </ul>
   *
   * <p>Invocation byte: 40
   */
  CANCEL_APPOINTED_CUSTODIAN(40) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);

      ensure(account.hasAppointedCustodian(), "No custodian is currently appointed");
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);

      Balance updatedAccount = account.mutate(BalanceMutable::cancelAppointedCustodian);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, updatedAccount);
    }
  },
  /**
   * Reset both the accepted- and the appointed custodian for the account. Only the custodian can
   * reset the custodian.
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> address of the sender
   * </ul>
   *
   * <p>Invocation byte: 41
   */
  RESET_CUSTODIAN(41) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);

      BlockchainAddress senderAddress = BlockchainAddress.read(rpc);

      ensure(account.hasCustodian(), "No custodian has been accepted for the account");
      ensure(account.isCustodian(senderAddress), "Only the custodian can reset the custodian");
      Balance updatedAccount = account.mutate(BalanceMutable::resetCustodian);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, updatedAccount);
    }
  },
  /**
   * Accept the role as custodian for the account. Only the appointed custodian can accept
   * custodianship.
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> address of the sender
   * </ul>
   *
   * <p>Invocation byte: 42
   */
  ACCEPT_CUSTODIANSHIP(42) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);

      ensure(account.hasAppointedCustodian(), "No custodian has been appointed for the account");
      BlockchainAddress senderAddress = BlockchainAddress.read(rpc);
      ensure(
          account.isAppointedCustodian(senderAddress),
          "Only the appointed custodian can accept custodianship");

      Balance updatedAccount = account.mutate(BalanceMutable::acceptCustodian);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, updatedAccount);
    }
  },
  /**
   * Stake tokens.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>long</code> amount to be staked. Must be positive.
   * </ul>
   *
   * <p>Invocation byte: 46
   */
  STAKE_TOKENS(46) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot stake a non-positive amount of tokens");
      BalanceMutable balance = account(accountState).asMutable();
      balance.stakeTokens(amount);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Unstake tokens if enough tokens are available.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>long</code> amount to unstake. Must be positive.
   * </ul>
   *
   * <p>Invocation byte: 47
   */
  UNSTAKE_TOKENS(47) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot unstake non-positive amount of tokens");
      BalanceMutable balance = account(accountState).asMutable();
      balance.unstakeTokens(amount, pluginContext.blockProductionTime());
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Initiate a two phased MPC token transfer as the sender.
   *
   * <p>Adds the transfer into pending transfers, such that it can be committed or aborted in the
   * second phase of the transfer. Fails if a deposit for the same account and ID exists.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>Hash</code> the ID of the two-phased MPC token transfer.
   *   <li><code>long</code> amount to transfer. Must be positive.
   * </ul>
   *
   * <p>Invocation byte: 49
   */
  INITIATE_WITHDRAW(49) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(
          amount > 0,
          "Cannot initiate the withdraw part of a transfer with a non-positive amount of tokens");
      TransferInformation transferInformation = TransferInformation.createForWithdraw(amount, -1);
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.initiateWithdraw(transactionId, transferInformation);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Sets the goal for how many staked tokens the account wants for jobs. If the goal is set higher
   * than the current amount of staked tokens to jobs, pending delegated stakes will be accepted to
   * reach the goal.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>long</code> goal amount of tokens to have staked for jobs.
   * </ul>
   *
   * <p>Invocation byte: 70
   */
  SET_STAKING_GOAL(70) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      long stakingGoal = rpc.readLong();
      ensure(stakingGoal >= 0, "Staking goal cannot be negative");

      BalanceMutable updatedBalance = account.asMutable();
      updatedBalance.setStakingGoal(stakingGoal);

      Balance newBalance = updatedBalance.asImmutable();
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, newBalance);
    }
  },
  /**
   * Sets the expiration timestamp for when the delegated stakes can no longer be used for new jobs.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>BlockchainAddress</code> the delegator.
   *   <li><code>Long</code> (optional) the timestamp the delegated stakes are no longer valid for
   *       new jobs.
   * </ul>
   *
   * <p>Invocation byte: 72
   */
  SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES(72) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {

      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      BlockchainAddress delegator = BlockchainAddress.read(rpc);
      Long expirationTimestamp = rpc.readOptional(SafeDataInputStream::readLong);

      BalanceMutable updatedBalance = account.asMutable();
      updatedBalance.updateExpirationForStakedTokens(delegator, expirationTimestamp);

      Balance newBalance = updatedBalance.asImmutable();
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, newBalance);
    }
  },
  /**
   * Initiate receiving delegated stakes with an expiration timestamp.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>Hash</code> the ID of the two-phased MPC tokens delegation.
   *   <li><code>long</code> the amount to receive. Must be positive.
   *   <li><code>BlockchainAddress</code> the counter part in the delegation.
   *   <li><code>Long</code> the timestamp the delegated stakes are no longer valid for jobs.
   * </ul>
   *
   * <p>Invocation byte: 87
   */
  INITIATE_RECEIVE_DELEGATED_STAKES_WITH_EXPIRATION(87) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(
          amount > 0,
          "Cannot initiate the receive part of a stake delegation with a non-positive amount of"
              + " tokens");
      BlockchainAddress counterPart = BlockchainAddress.read(rpc);
      long expirationTimestamp = rpc.readLong();

      StakeDelegation stakeDelegation =
          StakeDelegation.createReceiveDelegation(
              counterPart, amount, pluginContext.blockProductionTime(), expirationTimestamp);

      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
          account(accountState).asMutable().storedPendingStakeDelegations();
      ensureStakeActionIsBetweenDifferentAccounts(
          transactionId, stakeDelegation, storedPendingStakeDelegations);

      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.initiateReceiveDelegatedStakes(transactionId, stakeDelegation);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Check vested tokens.
   *
   * <p>Check if enough time has passed for a vesting period and if so, release the vested tokens.
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   * </ul>
   *
   * <p>Invocation byte: 53
   */
  CHECK_VESTED_TOKENS(53) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      long blockProductionTimestamp = pluginContext.blockProductionTime();
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.checkVestedTokens(blockProductionTimestamp);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Release pending unstaked tokens if enough time have passed.
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   * </ul>
   *
   * <p>Invocation byte: 54
   */
  CHECK_PENDING_UNSTAKES(54) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      BalanceMutable resultAccount = account(accountState).asMutable();
      resultAccount.handlePendingUnstakes(pluginContext.blockProductionTime());
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, resultAccount.asImmutable());
    }
  },
  /**
   * Initiate delegating stakes.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>Hash</code> the ID of the two-phased MPC tokens delegation.
   *   <li><code>long</code> amount to delegate. Must be positive.
   *   <li><code>BlockchainAddress</code> the recipient of the delegated tokens.
   * </ul>
   *
   * <p>Invocation byte: 55
   */
  INITIATE_DELEGATE_STAKES(55) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot initiate delegate with a non-positive amount of tokens");
      BlockchainAddress counterPart = BlockchainAddress.read(rpc);

      StakeDelegation stakeDelegation =
          StakeDelegation.createDelegation(
              counterPart, amount, pluginContext.blockProductionTime());
      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
          account(accountState).asMutable().storedPendingStakeDelegations();
      ensureStakeActionIsBetweenDifferentAccounts(
          transactionId, stakeDelegation, storedPendingStakeDelegations);
      BalanceMutable balance = account(accountState).asMutable();
      balance.initiateDelegateStakes(transactionId, stakeDelegation);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Initiate retracting delegated stakes.
   *
   * <p>Tries to retract the delegated tokens for the given <var>counterPart</var>. Can only retract
   * the amount delegated.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>Hash</code> the ID of the two-phased retract delegation.
   *   <li><code>long</code> the amount to retract from <var>counterPart</var>. Must be positive.
   *   <li><code>BlockchainAddress</code> the <var>counterPart</var> of the delegated stakes to
   *       retract from.
   * </ul>
   *
   * <p>Invocation byte: 57
   */
  INITIATE_RETRACT_DELEGATED_STAKES(57) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(
          amount > 0,
          "Cannot initiate the retract part of a stake delegation with a non-positive amount of"
              + " tokens");
      BlockchainAddress counterPart = BlockchainAddress.read(rpc);

      StakeDelegation stakeDelegation =
          StakeDelegation.createRetractDelegation(counterPart, amount);

      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations =
          account(accountState).asMutable().storedPendingStakeDelegations();
      ensureStakeActionIsBetweenDifferentAccounts(
          transactionId, stakeDelegation, storedPendingStakeDelegations);
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.initiateRetractDelegatedStakes(transactionId, stakeDelegation, counterPart);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Accept delegated stakes from an account.
   *
   * <p>Accepts the given amount of delegated tokens which allows the account to associate them to
   * contracts for running jobs
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>BlockchainAddress</code> the owner of the delegated stakes.
   *   <li><code>long</code> the delegated amount to return to <var>counterPart</var>.
   * </ul>
   *
   * <p>Invocation byte: 61
   */
  ACCEPT_DELEGATED_STAKES(61) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      BlockchainAddress sender = BlockchainAddress.read(rpc);
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot accept delegate stakes with a non-positive amount of tokens");
      BalanceMutable balance = account(accountState).asMutable();
      balance.acceptPendingDelegatedStakes(sender, amount);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Reduce delegated stakes from an account.
   *
   * <p>Reduces the amount of accepted tokens. The tokens must not be associated to a contract.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>BlockchainAddress</code> the owner of the delegated stakes.
   *   <li><code>long</code> the delegated amount to return to <var>counterPart</var>.
   * </ul>
   *
   * <p>Invocation byte: 62
   */
  REDUCE_DELEGATED_STAKES(62) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      BlockchainAddress sender = BlockchainAddress.read(rpc);
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot reduce delegate stakes with a non-positive amount of tokens");
      BalanceMutable balance = account(accountState).asMutable();
      balance.reducePendingDelegatedStakes(sender, amount);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Initiate a two phased BYOC transfer as the sender.
   *
   * <p>Adds the transfer into pending transfers, such that it can be committed or aborted in the
   * second phase of the transfer. Fails if there is a deposit for the same account and ID.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>Hash</code> the transaction ID of the two phased BYOC transfer.
   *   <li><code>Unsigned256</code> amount to transfer.
   *   <li><code>String</code> the symbol of BYOC.
   * </ul>
   *
   * <p>Invocation byte: 63
   */
  INITIATE_BYOC_TRANSFER_SENDER(63) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      Hash transactionId = Hash.read(rpc);
      Unsigned256 amount = Unsigned256.read(rpc);
      String symbol = rpc.readString();
      ensure(
          amount.compareTo(Unsigned256.ZERO) > 0, "Cannot transfer a non-positive amount of BYOC");
      int coinIndex = GasAndCoinAccountPlugin.getCoinIndex(global, symbol);
      TransferInformation transferInformation =
          TransferInformation.createForWithdraw(amount, coinIndex);
      BalanceMutable updatedBalance = account(accountState).asMutable();
      updatedBalance.initiateWithdraw(transactionId, transferInformation, symbol);
      return new BlockchainAccountPlugin.AccountState<>(
          accountState.local, updatedBalance.asImmutable());
    }
  },
  /**
   * Initiate ice-box staking towards a contract.
   *
   * <p>Starts the process of associating stakes of an account towards a contract for the sake of
   * keeping it alive should it run out of gas. After this invocation, the calling account will have
   * a set of tokens associated with a contract and will have generated a "pendingIceStake" object
   * in its state.
   *
   * <p>The following is read from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>Hash</code> a transaction ID that will be associated with this ice stake flow.
   *   <li><code>Long</code> the amount of stakes to associate to the contract.
   *   <li><code>BlockchainAddress</code> the address of the contract to associate tokens towards.
   * </ul>
   *
   * <p>Invocation byte: 65
   */
  INITIATE_ICE_ASSOCIATE(65) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot ice stake non-positive amount");
      BlockchainAddress target = BlockchainAddress.read(rpc);
      ensure(
          target.getType() == BlockchainAddress.Type.CONTRACT_PUBLIC,
          "Can only ice stake against contracts");

      BalanceMutable balance = account(accountState).asMutable();
      balance.initiateIceAssociate(transactionId, target, amount);
      return new BlockchainAccountPlugin.AccountState<>(accountState.local, balance.asImmutable());
    }
  },
  /**
   * Disassociate previously associated ice-box stakes.
   *
   * <p>Starts the process of disassociating previously associated ice stakes to a contract. After
   * this invocation, a pending {@link IceStake} object will be stored in the account's state
   *
   * <p>Reads the following values from the RPC stream:
   *
   * <ul>
   *   <li><code>BlockchainAddress</code> (optional) address of the sender if sent as a custodian or
   *       {@code null} if sent by the account owner.
   *   <li><code>Hash</code> a transaction ID.
   *   <li><code>Long</code> the amount of stakes to retract.
   *   <li><code>BlockchainAddress</code> the address of the contract to retract stakes from.
   * </ul>
   *
   * <p>Invocation byte: 66
   */
  INITIATE_ICE_DISASSOCIATE(66) {
    @Override
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
        AccountStateGlobal global,
        BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
        SafeDataInputStream rpc,
        SafeDataOutputStream returnValue,
        PluginContext pluginContext) {
      Balance account = account(accountState);
      BlockchainAddress senderAddress = rpc.readOptional(BlockchainAddress::read);
      validateSender(account, senderAddress);
      Hash transactionId = Hash.read(rpc);
      long amount = rpc.readLong();
      ensure(amount > 0, "Cannot retract a non-positive amount of ice stakes");
      BlockchainAddress target = BlockchainAddress.read(rpc);

      BalanceMutable balance = account(accountState).asMutable();
      if (balance.pendingIceStake(transactionId) == null) {
        balance.initiateIceDisassociate(transactionId, target, amount);
        return new BlockchainAccountPlugin.AccountState<>(
            accountState.local, balance.asImmutable());
      } else {
        return accountState;
      }
    }
  };

  static AccountInvocation getInvocation(SafeDataInputStream rpc) {
    final int invocationByte = rpc.readUnsignedByte();
    return Invocation.findInvocationBy(values(), invocationByte, "Account");
  }

  AccountInvocation(int invocationByte) {
    this.invocationByte = (byte) invocationByte;
  }

  private final byte invocationByte;

  @Override
  public byte getInvocationByte() {
    return invocationByte;
  }

  abstract BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> invoke(
      AccountStateGlobal global,
      BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> accountState,
      SafeDataInputStream rpc,
      SafeDataOutputStream returnValue,
      PluginContext pluginContext);

  private static void ensure(boolean predicate, String errorString) {
    if (!predicate) {
      throw new RuntimeException(errorString);
    }
  }

  private static Balance account(
      BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> current) {
    Balance account = current.account;
    return nullSafe(account);
  }

  private static Balance nullSafe(Balance account) {
    return Objects.requireNonNullElseGet(account, Balance::create);
  }

  /**
   * Validates the sender for a custodian enabled invocation.
   *
   * @param account state of the account
   * @param senderAddress address of the sender if sent as a custodian or {@code null} if sent by
   *     the account owner
   */
  private static void validateSender(Balance account, BlockchainAddress senderAddress) {
    if (account.hasCustodian()) {
      ensure(
          account.isCustodian(senderAddress),
          "Only the custodian of the account is allowed to send this invocation");
    } else {
      ensure(
          senderAddress == null,
          "Only the owner of the account is allowed to send this invocation");
    }
  }

  private static void ensureStakeActionIsBetweenDifferentAccounts(
      Hash transactionId,
      StakeDelegation stakeDelegation,
      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations) {

    if (storedPendingStakeDelegations.getValue(transactionId) != null
        && !(storedPendingStakeDelegations.getValue(transactionId).delegationType
            == stakeDelegation.delegationType)) {
      throw new IllegalArgumentException(
          "Cannot perform any delegation of stakes between the same account");
    }
  }
}
