package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.governance.account.Balance.UNSTAKE_DURATION_MILLIS;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

final class BalanceMutable {

  private FixedList<AccountCoin> accountCoins;
  private long mpcTokens;
  private long stakedTokens;
  private AvlTree<BlockchainAddress, StakedToContract> stakedToContract;
  private AvlTree<Long, Long> pendingUnstakes;
  private FreeTransactions spentFreeTransactions;
  private FixedList<InitialVestingAccount> vestingAccounts;
  private AvlTree<Hash, TransferInformation> storedPendingTransfers;
  private AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations;
  private AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers;
  private AvlTree<BlockchainAddress, Long> delegatedStakesToOthers;
  private AvlTree<Long, Long> pendingRetractedDelegatedStakes;
  private boolean stakeable;
  private AvlTree<Hash, IceStake> storedPendingIceStakes;
  private long tokensInCustody;
  private Custodian custodian;
  private long stakingGoal;

  BalanceMutable(Balance balance) {
    accountCoins = balance.accountCoins();
    mpcTokens = balance.mpcTokens();
    stakedTokens = balance.stakedTokens();
    stakedToContract = balance.stakedToContract();
    pendingUnstakes = balance.pendingUnstakes();
    spentFreeTransactions = balance.spentFreeTransactions();
    vestingAccounts = balance.vestingAccounts();
    storedPendingTransfers = balance.storedPendingTransfers();
    storedPendingStakeDelegations = balance.storedPendingStakeDelegations();
    delegatedStakesFromOthers = balance.delegatedStakesFromOthers();
    delegatedStakesToOthers = balance.delegatedStakesToOthers();
    pendingRetractedDelegatedStakes = balance.pendingRetractedDelegatedStakes();
    stakeable = balance.stakeable();
    storedPendingIceStakes = balance.storedPendingIceStakes();
    tokensInCustody = balance.tokensInCustody();
    custodian = balance.custodian();
    stakingGoal = balance.stakingGoal();
  }

  Balance asImmutable() {
    return new Balance(
        accountCoins,
        mpcTokens,
        stakedTokens,
        stakedToContract,
        pendingUnstakes,
        spentFreeTransactions,
        vestingAccounts,
        storedPendingTransfers,
        storedPendingStakeDelegations,
        delegatedStakesFromOthers,
        delegatedStakesToOthers,
        pendingRetractedDelegatedStakes,
        stakeable,
        storedPendingIceStakes,
        tokensInCustody,
        custodian,
        stakingGoal);
  }

  long tokensInCustody() {
    return tokensInCustody;
  }

  long mpcTokens() {
    return mpcTokens;
  }

  long stakedTokens() {
    return stakedTokens;
  }

  long stakingGoal() {
    return stakingGoal;
  }

  AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations() {
    return storedPendingStakeDelegations;
  }

  FixedList<InitialVestingAccount> vestingAccounts() {
    return vestingAccounts;
  }

  AvlTree<Long, Long> pendingUnstakes() {
    return pendingUnstakes;
  }

  AvlTree<Long, Long> pendingRetractedDelegatedStakes() {
    return pendingRetractedDelegatedStakes;
  }

  AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers() {
    return delegatedStakesFromOthers;
  }

  AvlTree<BlockchainAddress, StakedToContract> stakedToContract() {
    return stakedToContract;
  }

  FixedList<AccountCoin> accountCoins() {
    return accountCoins;
  }

  AvlTree<BlockchainAddress, Long> delegatedStakesToOthers() {
    return delegatedStakesToOthers;
  }

  public IceStake pendingIceStake(Hash transactionId) {
    return this.asImmutable().pendingIceStake(transactionId);
  }

  private void setAccountCoins(FixedList<AccountCoin> accountCoins) {
    this.accountCoins = accountCoins;
  }

  private void setMpcTokens(long mpcTokens) {
    this.mpcTokens = mpcTokens;
  }

  private void setStakedTokens(long stakedTokens) {
    this.stakedTokens = stakedTokens;
  }

  private void setStakedToContract(AvlTree<BlockchainAddress, StakedToContract> stakedToContract) {
    this.stakedToContract = stakedToContract;
  }

  private void setPendingUnstakes(AvlTree<Long, Long> pendingUnstakes) {
    this.pendingUnstakes = pendingUnstakes;
  }

  private void setSpentFreeTransactions(FreeTransactions spentFreeTransactions) {
    this.spentFreeTransactions = spentFreeTransactions;
  }

  private void setVestingAccounts(FixedList<InitialVestingAccount> vestingAccounts) {
    this.vestingAccounts = vestingAccounts;
  }

  private void setStoredPendingTransfers(
      AvlTree<Hash, TransferInformation> storedPendingTransfers) {
    this.storedPendingTransfers = storedPendingTransfers;
  }

  private void setStoredPendingStakeDelegations(
      AvlTree<Hash, StakeDelegation> storedPendingStakeDelegations) {
    this.storedPendingStakeDelegations = storedPendingStakeDelegations;
  }

  private void setDelegatedStakesFromOthers(
      AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers) {
    this.delegatedStakesFromOthers = delegatedStakesFromOthers;
  }

  private void setDelegatedStakesToOthers(
      AvlTree<BlockchainAddress, Long> delegatedStakesToOthers) {
    this.delegatedStakesToOthers = delegatedStakesToOthers;
  }

  private void setPendingRetractedDelegatedStakes(
      AvlTree<Long, Long> pendingRetractedDelegatedStakes) {
    this.pendingRetractedDelegatedStakes = pendingRetractedDelegatedStakes;
  }

  private void setStakeable(boolean stakeable) {
    this.stakeable = stakeable;
  }

  private void setStoredPendingIceStakes(AvlTree<Hash, IceStake> storedPendingIceStakes) {
    this.storedPendingIceStakes = storedPendingIceStakes;
  }

  /**
   * Update coin balance for the given coin index.
   *
   * @param coinIndex index of coin to update
   * @param diff how much to change
   */
  void addCoins(int coinIndex, Unsigned256 diff) {
    FixedList<AccountCoin> newCoins =
        AccountCoin.COINS_LIST.withUpdatedEntry(
            this.accountCoins, coinIndex, accountCoin -> accountCoin.add(diff));
    setAccountCoins(newCoins);
  }

  void deductCoins(int coinIndex, Unsigned256 diff, String symbol) {
    Unsigned256 balance = accountCoins.get(coinIndex).getBalance();
    if (balance.compareTo(diff) < 0) {
      throw new IllegalArgumentException(
          "Cannot deduct %s %s with a balance of %s".formatted(diff, symbol, balance));
    }
    FixedList<AccountCoin> newCoins =
        AccountCoin.COINS_LIST.withUpdatedEntry(
            this.accountCoins, coinIndex, accountCoin -> accountCoin.deduct(diff));
    setAccountCoins(newCoins);
  }

  /**
   * Add MPC tokens to mpcToken balance.
   *
   * @param amount to increase mpcToken with
   */
  private void addMpcTokens(long amount) {
    if (amount == 0) {
      throw new IllegalArgumentException("Cannot add 0 tokens");
    }
    setMpcTokens(mpcTokens + amount);
  }

  /**
   * Add MPC tokens to mpcTokenBalance while keeping tokensInCustody consistent.
   *
   * @param amount to increase mpcTokens with
   */
  void addMpcTokenBalance(long amount) {
    tokensInCustody += amount;
    addMpcTokens(amount);
  }

  /**
   * Deduct MPC tokens from mpcToken balance.
   *
   * @param amount to deduct mpcToken with
   */
  void deductMpcTokens(long amount) {
    if (amount > mpcTokens) {
      throw new IllegalArgumentException(
          "Cannot deduct %s MPC tokens with a balance of %s".formatted(amount, mpcTokens));
    }
    setMpcTokens(mpcTokens - amount);
  }

  /**
   * Deduct MPC tokens from mpcToken balance while keeping tokensInCustody consistent.
   *
   * @param amount to deduct mpcTokens with
   */
  void deductMpcTokenBalance(long amount) {
    deductMpcTokens(amount);
    tokensInCustody -= amount;
  }

  void addPendingTransfer(Hash transactionId, TransferInformation transferInformation) {
    setStoredPendingTransfers(storedPendingTransfers.set(transactionId, transferInformation));
  }

  void removePendingTransfer(Hash transactionId) {
    setStoredPendingTransfers(storedPendingTransfers.remove(transactionId));
  }

  void removePendingTransferAbort(Hash transactionId) {
    setStoredPendingTransfers(storedPendingTransfers.remove(transactionId));
  }

  void addPendingStakeDelegation(Hash transactionId, StakeDelegation stakeDelegation) {
    setStoredPendingStakeDelegations(
        storedPendingStakeDelegations.set(transactionId, stakeDelegation));
  }

  void removePendingStakeDelegation(Hash transactionId) {
    setStoredPendingStakeDelegations(storedPendingStakeDelegations.remove(transactionId));
  }

  public void incrementFreeTransactionsUsed(long epoch) {
    setSpentFreeTransactions(spentFreeTransactions.incrementSpentTransactionsForEpoch(epoch));
  }

  void stakeTokens(long amount) {
    if (!stakeable) {
      throw new IllegalStateException("This account cannot stake");
    }

    long totalPendingUnstakes = 0;
    for (Long epoch : pendingUnstakes.keySet()) {
      Long unstakeAmount = pendingUnstakes.getValue(epoch);
      totalPendingUnstakes += unstakeAmount;
    }

    checkIfEnoughToStake(amount, totalPendingUnstakes);

    AvlTree<Long, Long> newPendingUnstakes = pendingUnstakes;
    long remainingAmount = amount;

    // Ensure that the newest unstaked tokens are the ones staked first.
    List<Long> keys = new ArrayList<>(pendingUnstakes.keySet());
    for (int i = keys.size() - 1; i >= 0; i--) {
      Long epoch = keys.get(i);
      Long unstakeAmount = newPendingUnstakes.getValue(epoch);

      if (unstakeAmount <= remainingAmount) {
        remainingAmount -= unstakeAmount;
        newPendingUnstakes = newPendingUnstakes.remove(epoch);
      } else {
        newPendingUnstakes = newPendingUnstakes.set(epoch, unstakeAmount - remainingAmount);
        remainingAmount = 0;
      }
    }
    setMpcTokens(mpcTokens - remainingAmount);
    setStakedTokens(stakedTokens + amount);
    setPendingUnstakes(newPendingUnstakes);
    redistributeDelegatedStakes();
  }

  public void associateWithExpiration(
      BlockchainAddress contractAddress, long amount, Long expirationTimestamp) {
    AvlTree<BlockchainAddress, StakedToContract> updatedStakedToContract =
        createOrUpdateAssociationToContract(contractAddress, amount, expirationTimestamp);
    ensureStakeForJobsCanCoverAssociations(delegatedStakesFromOthers, updatedStakedToContract);
    setStakedToContract(updatedStakedToContract);
  }

  void associateWithCurrentExpiration(BlockchainAddress contractAddress, long amount) {
    Long expiration = null;
    if (stakedToContract.containsKey(contractAddress)) {
      expiration = stakedToContract.getValue(contractAddress).expirationTimestamp;
    }

    associateWithExpiration(contractAddress, amount, expiration);
  }

  /**
   * If an association exists for a contract, updates the amount and expiration timestamp for
   * association, otherwise creates a new association with amount and expiration.
   *
   * @param contractAddress the address of the contract to associate to
   * @param amount the amount to associate
   * @param expirationTimestamp the timestamp when the association is no longer valid
   * @return the association to the contract
   */
  private AvlTree<BlockchainAddress, StakedToContract> createOrUpdateAssociationToContract(
      BlockchainAddress contractAddress, long amount, Long expirationTimestamp) {
    StakedToContract stakedToContract;
    if (this.stakedToContract.containsKey(contractAddress)) {
      long currentAmount = this.stakedToContract.getValue(contractAddress).amount;
      stakedToContract =
          this.stakedToContract
              .getValue(contractAddress)
              .setAmount(currentAmount + amount)
              .setExpirationTimestamp(expirationTimestamp);
    } else {
      stakedToContract = StakedToContract.create(amount, expirationTimestamp);
    }

    return this.stakedToContract.set(contractAddress, stakedToContract);
  }

  public void disassociate(BlockchainAddress contractAddress, long amount) {
    long valueForContract = 0;
    if (stakedToContract.containsKey(contractAddress)) {
      valueForContract = stakedToContract.getValue(contractAddress).amount;
    }

    long newAssociatedAmount = valueForContract - amount;
    if (newAssociatedAmount < 0) {
      throw new IllegalArgumentException("Amount cannot make contract association negative");
    }

    AvlTree<BlockchainAddress, StakedToContract> newStakedToContract;
    if (newAssociatedAmount == 0) {
      newStakedToContract = stakedToContract.remove(contractAddress);
    } else {
      StakedToContract stakedToContract =
          this.stakedToContract.getValue(contractAddress).setAmount(newAssociatedAmount);
      newStakedToContract = this.stakedToContract.set(contractAddress, stakedToContract);
    }
    setStakedToContract(newStakedToContract);
  }

  public void disassociateAllTokensForContract(BlockchainAddress address) {
    AvlTree<BlockchainAddress, StakedToContract> newStakedToContract = stakedToContract;
    if (stakedToContract.containsKey(address)) {
      newStakedToContract = newStakedToContract.remove(address);
    }
    setStakedToContract(newStakedToContract);
  }

  public void handlePendingUnstakes(long currentBlockTime) {
    long newMpcTokenBalance = mpcTokens;
    AvlTree<Long, Long> newPendingUnstake = pendingUnstakes;
    for (Long releaseTime : pendingUnstakes.keySet()) {
      if (currentBlockTime >= releaseTime) {
        newMpcTokenBalance += pendingUnstakes.getValue(releaseTime);
        newPendingUnstake = newPendingUnstake.remove(releaseTime);
      }
    }
    AvlTree<Long, Long> newPendingRetractedDelegatedStakes = pendingRetractedDelegatedStakes;
    for (Long releaseTime : pendingRetractedDelegatedStakes.keySet()) {
      if (currentBlockTime >= releaseTime) {
        newMpcTokenBalance += pendingRetractedDelegatedStakes.getValue(releaseTime);
        newPendingRetractedDelegatedStakes = newPendingRetractedDelegatedStakes.remove(releaseTime);
      }
    }
    setMpcTokens(newMpcTokenBalance);
    setPendingUnstakes(newPendingUnstake);
    setPendingRetractedDelegatedStakes(newPendingRetractedDelegatedStakes);
  }

  public void unstakeTokens(long amount, long currentBlockTime) {
    if (amount > stakedTokens) {
      throw new IllegalArgumentException(
          "Cannot unstake more than the amount of staked tokens on the account");
    }

    long releaseTime = currentBlockTime + UNSTAKE_DURATION_MILLIS;
    AvlTree<Long, Long> newPendingUnstake;
    if (pendingUnstakes.containsKey(releaseTime)) {
      newPendingUnstake =
          pendingUnstakes.set(releaseTime, amount + pendingUnstakes.getValue(releaseTime));
    } else {
      newPendingUnstake = pendingUnstakes.set(releaseTime, amount);
    }
    setStakedTokens(stakedTokens - amount);
    ensureStakeForJobsCanCoverAssociations(delegatedStakesFromOthers, stakedToContract);
    setPendingUnstakes(newPendingUnstake);
    redistributeDelegatedStakes();
  }

  /**
   * Ensures that the provided associations can be covered by the provided delegations.
   *
   * <p>Throws exception if associations cannot be covered by delegations
   *
   * @param delegations the delegations to cover the associations
   * @param associations the associations to be covered
   */
  private void ensureStakeForJobsCanCoverAssociations(
      AvlTree<BlockchainAddress, StakesFromOthers> delegations,
      AvlTree<BlockchainAddress, StakedToContract> associations) {
    Map<Long, List<AssociationEntry>> prioritizedAssociations =
        getPrioritizedAssociations(associations);
    Map<Long, List<DelegationEntry>> prioritizedDelegationBuckets =
        getPrioritizedDelegatedStakes(delegations);

    ArrayDeque<DelegationEntry> prioritizedDelegations =
        new ArrayDeque<>(
            prioritizedDelegationBuckets.values().stream().flatMap(Collection::stream).toList());

    long totalAssociationsAtTime = 0;
    // Self stakes are always available to cover associations
    long totalStakesForJobsAtTime = this.stakedTokens();

    for (Long associationExpiration : prioritizedAssociations.keySet()) {

      // All associations in the bucket must be covered
      for (AssociationEntry association : prioritizedAssociations.get(associationExpiration)) {
        totalAssociationsAtTime += association.association().amount;
      }

      // All delegations that expire at or after the association expiration are available to cover
      // the association
      while (!prioritizedDelegations.isEmpty()
          && prioritizedDelegations.peekFirst().expiresAtOrAfter(associationExpiration)) {
        DelegationEntry delegationEntry = prioritizedDelegations.removeFirst();
        totalStakesForJobsAtTime += delegationEntry.stakes().acceptedDelegatedStakes;
      }

      // Check that the stakes for jobs is sufficient to cover the associations
      if (totalAssociationsAtTime > totalStakesForJobsAtTime) {
        throw new IllegalArgumentException("Associations cannot be covered");
      }
    }
  }

  /**
   * Disassociate tokens from a contract and burns delegated and self staked tokens for an account.
   * Tokens are burning evenly amongst delegated stakes and self staked tokens that are not
   * necessary to cover collateral for other associations.
   *
   * @param amount the amount to burn
   * @param contractAddress the contract address to disassociate from
   */
  void disassociateAndBurn(long amount, BlockchainAddress contractAddress) {
    disassociate(contractAddress, amount);

    Map<Long, List<AssociationEntry>> prioritizedAssociations =
        getPrioritizedAssociations(stakedToContract);
    Map<Long, List<DelegationEntry>> burnableDelegations =
        getPrioritizedDelegatedStakes(delegatedStakesFromOthers);

    long stakedTokensAvailableToBurn = stakedTokens;
    for (List<AssociationEntry> associations : prioritizedAssociations.values()) {
      for (AssociationEntry entry : associations) {
        Long associationExpiration = entry.association().expirationTimestamp;
        long associationAmount = entry.association().amount;
        List<DelegationEntry> delegationsToCoverAssociation =
            getDelegationsAtTimestamp(associationExpiration, burnableDelegations);

        List<Unsigned256> tokensToCoverAssociation =
            new ArrayList<>(
                delegationsToCoverAssociation.stream()
                    .map(s -> Unsigned256.create(s.stakes().acceptedDelegatedStakes))
                    .toList());
        if (stakedTokensAvailableToBurn > 0) {
          tokensToCoverAssociation.add(Unsigned256.create(stakedTokensAvailableToBurn));
        }
        List<Unsigned256> nonBurnableTokens =
            weightedShareOfAmount(associationAmount, tokensToCoverAssociation);

        for (int i = 0; i < delegationsToCoverAssociation.size(); i++) {
          DelegationEntry delegation = delegationsToCoverAssociation.get(i);
          long reduceAmount = nonBurnableTokens.get(i).longValueExact();
          List<DelegationEntry> previousDelegations =
              burnableDelegations.get(delegation.stakes().expirationTimestamp);
          List<DelegationEntry> updatedDelegations = new ArrayList<>();
          for (DelegationEntry previousDelegation : previousDelegations) {
            if (previousDelegation.delegator().equals(delegation.delegator())) {
              updatedDelegations.add(
                  new DelegationEntry(
                      delegation.delegator(), delegation.stakes().updateAccepted(-reduceAmount)));
            } else {
              updatedDelegations.add(previousDelegation);
            }
          }
          burnableDelegations.put(delegation.stakes().expirationTimestamp, updatedDelegations);
        }

        if (stakedTokensAvailableToBurn > 0) {
          long notBurnableSelfStakedAmount =
              nonBurnableTokens.get(delegationsToCoverAssociation.size()).longValueExact();
          stakedTokensAvailableToBurn -= notBurnableSelfStakedAmount;
        }
      }
    }

    List<BlockchainAddress> addressesToBurn = new ArrayList<>();
    List<Unsigned256> availableToBurn = new ArrayList<>();
    for (List<DelegationEntry> potentialBurns : burnableDelegations.values()) {
      for (DelegationEntry entry : potentialBurns) {
        long availableForBurning = entry.stakes().acceptedDelegatedStakes;
        if (availableForBurning >= 1) {
          availableToBurn.add(Unsigned256.create(availableForBurning));
          addressesToBurn.add(entry.delegator());
        }
      }
    }

    boolean burnOwnStakedTokens = stakedTokensAvailableToBurn > 0;
    if (burnOwnStakedTokens) {
      availableToBurn.add(Unsigned256.create(stakedTokensAvailableToBurn));
    }
    List<Unsigned256> burnAmounts = weightedShareOfAmount(amount, availableToBurn);
    AvlTree<BlockchainAddress, StakesFromOthers> updatedDelegatedStakesFromOthers =
        delegatedStakesFromOthers;

    long newStakedTokens = stakedTokens;
    for (int i = 0; i < burnAmounts.size(); i++) {
      long burnAmount = burnAmounts.get(i).longValueExact();
      if (burnOwnStakedTokens && i == burnAmounts.size() - 1) {
        newStakedTokens = stakedTokens - burnAmount;
      } else {
        BlockchainAddress address = addressesToBurn.get(i);
        long stakeAmount =
            updatedDelegatedStakesFromOthers.getValue(address).acceptedDelegatedStakes;
        if (stakeAmount - burnAmount == 0
            && updatedDelegatedStakesFromOthers.getValue(address).pendingDelegatedStakes == 0) {
          updatedDelegatedStakesFromOthers = updatedDelegatedStakesFromOthers.remove(address);
        } else {
          updatedDelegatedStakesFromOthers =
              updatedDelegatedStakesFromOthers.set(
                  address,
                  updatedDelegatedStakesFromOthers.getValue(address).updateAccepted(-burnAmount));
        }
      }
    }

    tokensInCustody -= amount;
    setStakedTokens(newStakedTokens);
    setDelegatedStakesFromOthers(updatedDelegatedStakesFromOthers);
  }

  /**
   * Gets a prioritized list of {@link DelegationEntry} that will not expire before the association.
   *
   * <p>The delegations are prioritized after expiration, with later expirations placed at the start
   * of the list.
   *
   * @param associationExpiration the expiration of the association
   * @param prioritizedDelegations a map of from prioritized expirations to a list of delegations
   *     with that expiration
   * @return sorted list of delegations that has not expired at association timestamp
   */
  List<DelegationEntry> getDelegationsAtTimestamp(
      Long associationExpiration, Map<Long, List<DelegationEntry>> prioritizedDelegations) {
    List<DelegationEntry> delegations = new ArrayList<>();
    for (Long delegationTimestamp : prioritizedDelegations.keySet()) {
      if (associationExpiration == null) {
        if (delegationTimestamp == null) {
          delegations.addAll(prioritizedDelegations.get(delegationTimestamp));
        }
      } else if (delegationTimestamp == null || associationExpiration <= delegationTimestamp) {
        delegations.addAll(prioritizedDelegations.get(delegationTimestamp));
      }
    }

    return delegations;
  }

  /**
   * Splits an amount into sub-amounts based on an inputted list of weights.
   *
   * @param totalAmount the amount to split
   * @param weights the weights the split of the amount is based on
   * @return a list with the sub-amounts
   */
  private List<Unsigned256> weightedShareOfAmount(long totalAmount, List<Unsigned256> weights) {
    List<Unsigned256> result = new ArrayList<>();
    Unsigned256 available = Unsigned256.create(totalAmount);
    Unsigned256 weightsSum = Unsigned256.ZERO;
    for (Unsigned256 big : weights) {
      weightsSum = weightsSum.add(big);
    }

    for (Unsigned256 weight : weights) {
      Unsigned256 scaledAmount = weight.multiply(available);
      Unsigned256 distribution = scaledAmount.divide(weightsSum);

      result.add(distribution);
      weightsSum = weightsSum.subtract(weight);
      available = available.subtract(distribution);
    }

    return result;
  }

  /**
   * Set vesting account for a specific vesting schedule for this account. If a vesting account
   * already exists, the amount of vested tokens is added to the total.
   *
   * @param amountOfTokens amount of vested tokens
   */
  public void createVestingAccount(
      long amountOfTokens, long tokenGenerationEvent, long releaseDuration, long releaseInterval) {

    InitialVestingAccount vestingAccount =
        InitialVestingAccount.create(
            amountOfTokens, tokenGenerationEvent, releaseDuration, releaseInterval);
    tokensInCustody += amountOfTokens;
    setVestingAccounts(vestingAccounts.addElement(vestingAccount));
  }

  private StakesFromOthers getDelegatedStakesFrom(BlockchainAddress sender) {
    return delegatedStakesFromOthers.containsKey(sender)
        ? delegatedStakesFromOthers.getValue(sender)
        : new StakesFromOthers();
  }

  public void makeNonStakeable() {
    if (stakedTokens != 0) {
      throw new IllegalStateException("Cannot make account non-stakeable if it has staked tokens");
    }
    setStakeable(false);
  }

  public void delegateStakes(long amount) {
    checkIfEnoughToStake(amount, 0);
    setMpcTokens(mpcTokens - amount);
  }

  private void checkIfEnoughToStake(long amount, long pendingUnstake) {
    long vestedTokensLeftToRelease = calculateTotalNonReleasedVestedTokens();
    if (amount > mpcTokens + vestedTokensLeftToRelease + pendingUnstake) {
      throw new IllegalArgumentException(
          "Cannot stake "
              + amount
              + " tokens with a balance of "
              + mpcTokens
              + " and "
              + vestedTokensLeftToRelease
              + " vested tokens left to release"
              + " and "
              + pendingUnstake
              + " pending unstakes.");
    }
  }

  long calculateTotalNonReleasedVestedTokens() {
    return vestingAccounts.stream().mapToLong(InitialVestingAccount::leftToRelease).sum();
  }

  public void retractDelegatedStakes(BlockchainAddress recipient, long amount) {
    long delegatedToRecipient = getDelegatedStakesTo(recipient);
    long newAmountForRecipient = delegatedToRecipient - amount;
    if (newAmountForRecipient < 0) {
      throw new IllegalArgumentException(
          "Cannot retract more delegated stakes than has been delegated to the account");
    }

    AvlTree<BlockchainAddress, Long> updateDelegatedStakesToOthers = delegatedStakesToOthers;
    if (newAmountForRecipient == 0) {
      updateDelegatedStakesToOthers = updateDelegatedStakesToOthers.remove(recipient);
    } else {
      updateDelegatedStakesToOthers = delegatedStakesToOthers.set(recipient, newAmountForRecipient);
    }
    setDelegatedStakesToOthers(updateDelegatedStakesToOthers);
  }

  public void setStakingGoal(long stakingGoal) {
    this.stakingGoal = stakingGoal;
    redistributeDelegatedStakes();
  }

  /**
   * Redistributes the delegated stakes for an account. Own staked tokens are prioritized, then
   * delegated stakes with no expiration timestamp and thereafter later expiration timestamps are
   * prioritized.
   *
   * <p>If multiple delegator has the same expiration and the node operator will not accept them
   * all, due to {@link BalanceMutable#stakingGoal}, the priority is to change as little as possible
   * from the current delegations.
   */
  private void redistributeDelegatedStakes() {
    if (stakingGoal() == 0) {
      return;
    }

    Map<Long, List<DelegationEntry>> prioritizedDelegations =
        getPrioritizedDelegatedStakes(delegatedStakesFromOthers);

    long amountToStakingGoal = stakingGoal() - stakedTokens();
    AvlTree<BlockchainAddress, StakesFromOthers> updatedDelegatedStakesFromOther =
        delegatedStakesFromOthers;
    for (List<DelegationEntry> bucket : prioritizedDelegations.values()) {
      long sumOfAccepted = 0L;
      for (DelegationEntry entry : bucket) {
        sumOfAccepted += entry.stakes().acceptedDelegatedStakes;
      }

      amountToStakingGoal -= sumOfAccepted;
      if (amountToStakingGoal < 1) {
        long toDecrease = Math.abs(amountToStakingGoal);
        for (DelegationEntry delegationEntry : bucket) {
          StakesFromOthers delegatedStakes = delegationEntry.stakes();
          long accepted = delegatedStakes.acceptedDelegatedStakes;

          long amountToDecrease = Math.min(toDecrease, accepted);
          updatedDelegatedStakesFromOther =
              updatedDelegatedStakesFromOther.set(
                  delegationEntry.delegator(),
                  delegatedStakes
                      .updateAccepted(-amountToDecrease)
                      .updatePending(amountToDecrease));
          toDecrease -= amountToDecrease;
        }
      } else {
        for (DelegationEntry entry : bucket) {
          StakesFromOthers delegatedStakes = entry.stakes();
          long availableToAccept = delegatedStakes.pendingDelegatedStakes;

          long amountToAccept = Math.min(amountToStakingGoal, availableToAccept);
          updatedDelegatedStakesFromOther =
              updatedDelegatedStakesFromOther.set(
                  entry.delegator(),
                  delegatedStakes.updateAccepted(amountToAccept).updatePending(-amountToAccept));
          amountToStakingGoal -= amountToAccept;
        }
      }
    }
    setDelegatedStakesFromOthers(updatedDelegatedStakesFromOther);
  }

  /**
   * Sorts the delegated stakes for the accounts based on timestamps into buckets with the same
   * timestamp. Highest priority is non-expiring delegation, followed by later expiration having
   * higher priority than earlier expirations.
   *
   * <p>Highest priority expiration timestamps are placed at the start of the map.
   *
   * @return a sorted map with keys being expiration timestamps and values a list of {@link
   *     DelegationEntry} with that expiration timestamp matching the key.
   */
  private Map<Long, List<DelegationEntry>> getPrioritizedDelegatedStakes(
      AvlTree<BlockchainAddress, StakesFromOthers> delegations) {
    Map<Long, List<DelegationEntry>> prioritizedDelegations =
        new TreeMap<>(Comparator.nullsLast(Long::compare).reversed());

    for (BlockchainAddress address : delegations.keySet()) {
      StakesFromOthers stakes = delegations.getValue(address);
      prioritizedDelegations
          .computeIfAbsent(stakes.expirationTimestamp, key -> new ArrayList<>())
          .add(new DelegationEntry(address, stakes));
    }
    return prioritizedDelegations;
  }

  /**
   * Sorts the associations for the accounts based on timestamps into buckets with the same
   * timestamp. Highest priority is non-expiring associations, followed by later expiration having
   * higher priority than earlier expirations.
   *
   * <p>Highest priority expiration timestamps are placed at the start of the map.
   *
   * @return a sorted map with keys being expiration timestamps and values a list of {@link
   *     AssociationEntry} with that expiration timestamp matching the key.
   */
  private Map<Long, List<AssociationEntry>> getPrioritizedAssociations(
      AvlTree<BlockchainAddress, StakedToContract> associations) {
    Map<Long, List<AssociationEntry>> prioritizedAssociations =
        new TreeMap<>(Comparator.nullsLast(Long::compare).reversed());

    for (BlockchainAddress address : associations.keySet()) {
      StakedToContract stakes = associations.getValue(address);
      prioritizedAssociations
          .computeIfAbsent(stakes.expirationTimestamp, key -> new ArrayList<>())
          .add(new AssociationEntry(address, stakes));
    }
    return prioritizedAssociations;
  }

  public void commitReceiveDelegatedStakes(
      BlockchainAddress sender, long amount, Long expirationTimestamp) {
    AvlTree<BlockchainAddress, StakesFromOthers> updatedDelegatedStakesFromOthers =
        delegatedStakesFromOthers;

    StakesFromOthers stakesFromOthers;
    if (delegatedStakesFromOthers.containsKey(sender)) {
      stakesFromOthers =
          getDelegatedStakesFrom(sender)
              .updatePending(amount)
              .updateTimestampsIfLaterExpiration(expirationTimestamp);
    } else {
      stakesFromOthers = StakesFromOthers.create(0, amount, expirationTimestamp);
    }

    updatedDelegatedStakesFromOthers =
        updatedDelegatedStakesFromOthers.set(sender, stakesFromOthers);

    setDelegatedStakesFromOthers(updatedDelegatedStakesFromOthers);
    redistributeDelegatedStakes();
  }

  public void returnDelegatedStakes(BlockchainAddress sender, long amount) {
    StakesFromOthers stakesFromSender = getDelegatedStakesFrom(sender);
    long pendingDelegatedStakes = stakesFromSender.pendingDelegatedStakes;
    long stakedFromSender = stakesFromSender.acceptedDelegatedStakes + pendingDelegatedStakes;
    if (stakedFromSender < amount) {
      throw new IllegalArgumentException(
          "Cannot return more delegated stakes than you have been delegated");
    }

    AvlTree<BlockchainAddress, StakesFromOthers> updatedDelegatedStakesFromOthers =
        delegatedStakesFromOthers;

    long newPendingAmount = 0;
    newPendingAmount = getNewPendingAmount(amount, newPendingAmount, pendingDelegatedStakes);

    long newAcceptedAmount = stakesFromSender.acceptedDelegatedStakes;
    if (newPendingAmount == 0) {
      long remainingAmount = amount - pendingDelegatedStakes;
      newAcceptedAmount = newAcceptedAmount - remainingAmount;
    }

    if (newAcceptedAmount + newPendingAmount == 0) {
      updatedDelegatedStakesFromOthers = updatedDelegatedStakesFromOthers.remove(sender);
    } else {
      updatedDelegatedStakesFromOthers =
          updatedDelegatedStakesFromOthers.set(
              sender,
              StakesFromOthers.create(
                  newAcceptedAmount,
                  newPendingAmount,
                  updatedDelegatedStakesFromOthers.getValue(sender).expirationTimestamp));
    }

    ensureStakeForJobsCanCoverAssociations(updatedDelegatedStakesFromOthers, stakedToContract);
    setDelegatedStakesFromOthers(updatedDelegatedStakesFromOthers);
  }

  long getNewPendingAmount(long amount, long newPendingAmount, long pendingDelegatedStakes) {
    if (pendingDelegatedStakes > amount) {
      newPendingAmount = pendingDelegatedStakes - amount;
    }
    return newPendingAmount;
  }

  public void abortRetractDelegatedStakes(BlockchainAddress recipient, long amount) {
    long currentAmount = getDelegatedStakesTo(recipient);
    AvlTree<BlockchainAddress, Long> updatedDelegatedStakesToOthers =
        delegatedStakesToOthers.set(recipient, currentAmount + amount);
    setDelegatedStakesToOthers(updatedDelegatedStakesToOthers);
  }

  private long getDelegatedStakesTo(BlockchainAddress recipient) {
    return delegatedStakesToOthers.containsKey(recipient)
        ? delegatedStakesToOthers.getValue(recipient)
        : 0L;
  }

  public void addPendingRetractedDelegatedStakes(long amount, long blockProductionTime) {
    long releaseTime = blockProductionTime + UNSTAKE_DURATION_MILLIS;
    if (pendingRetractedDelegatedStakes.containsKey(releaseTime)) {
      amount += pendingRetractedDelegatedStakes.getValue(releaseTime);
    }
    AvlTree<Long, Long> newPendingRetractedDelegatedStakes =
        pendingRetractedDelegatedStakes.set(releaseTime, amount);
    setPendingRetractedDelegatedStakes(newPendingRetractedDelegatedStakes);
  }

  public void abortReturnDelegatedStakes(BlockchainAddress sender, long amount) {
    AvlTree<BlockchainAddress, StakesFromOthers> updatedDelegatedStakesFromOthers =
        delegatedStakesFromOthers.set(sender, getDelegatedStakesFrom(sender).updatePending(amount));
    setDelegatedStakesFromOthers(updatedDelegatedStakesFromOthers);
  }

  public void commitDelegateStakes(BlockchainAddress recipient, long amount) {
    boolean isNewRecipient = !delegatedStakesToOthers.containsKey(recipient);

    AvlTree<BlockchainAddress, Long> updatedDelegatedStakesToOthers = delegatedStakesToOthers;
    if (isNewRecipient) {
      updatedDelegatedStakesToOthers = updatedDelegatedStakesToOthers.set(recipient, amount);
    } else {
      long currentDelegatedStakes = updatedDelegatedStakesToOthers.getValue(recipient);
      updatedDelegatedStakesToOthers =
          updatedDelegatedStakesToOthers.set(recipient, amount + currentDelegatedStakes);
    }
    setDelegatedStakesToOthers(updatedDelegatedStakesToOthers);
  }

  public void acceptPendingDelegatedStakes(BlockchainAddress sender, long amount) {
    if (amount <= 0) {
      throw new IllegalArgumentException("Amount must be positive");
    }

    if (stakingGoal() != 0) {
      throw new IllegalArgumentException(
          "Cannot manually accept delegated stakes if a staking goal has been set");
    }

    StakesFromOthers stakesFromSender = getDelegatedStakesFrom(sender);
    long pendingDelegatedStakes = stakesFromSender.pendingDelegatedStakes;
    if (pendingDelegatedStakes < amount) {
      throw new IllegalArgumentException(
          amount
              + " exceeds the amount of pending delegated stakes available from "
              + sender
              + ", which is: "
              + pendingDelegatedStakes);
    }

    AvlTree<BlockchainAddress, StakesFromOthers> updatedDelegatedStakesFromOthers =
        delegatedStakesFromOthers.set(
            sender, stakesFromSender.updateAccepted(amount).updatePending(-amount));
    setDelegatedStakesFromOthers(updatedDelegatedStakesFromOthers);
  }

  public void reducePendingDelegatedStakes(BlockchainAddress sender, long amount) {
    if (amount <= 0) {
      throw new IllegalArgumentException("Amount must be positive");
    }

    StakesFromOthers stakesFromSender = getDelegatedStakesFrom(sender);
    long acceptedDelegatedStakes = stakesFromSender.acceptedDelegatedStakes;
    if (acceptedDelegatedStakes < amount) {
      throw new IllegalArgumentException(
          "You cannot reduce your accepted delegated stakes below 0");
    }

    AvlTree<BlockchainAddress, StakesFromOthers> updatedDelegatedStakesFromOthers =
        delegatedStakesFromOthers.set(
            sender, stakesFromSender.updateAccepted(-amount).updatePending(amount));

    ensureStakeForJobsCanCoverAssociations(updatedDelegatedStakesFromOthers, stakedToContract);
    setDelegatedStakesFromOthers(updatedDelegatedStakesFromOthers);
  }

  public void addPendingIceStake(Hash transactionId, IceStake iceStake) {
    setStoredPendingIceStakes(storedPendingIceStakes.set(transactionId, iceStake));
  }

  public void removePendingIceStake(Hash transactionId) {
    setStoredPendingIceStakes(storedPendingIceStakes.remove(transactionId));
  }

  void commitTransfer(Hash transactionId) {
    TransferInformation transferInformation = storedPendingTransfers.getValue(transactionId);
    if (transferInformation != null) {
      if (transferInformation.getCoinIndex() == -1) {
        long amount = transferInformation.getAmount().longValueExact();
        if (transferInformation.addTokensOrCoinsIfTransferSuccessful()) {
          tokensInCustody += amount;
          addMpcTokens(amount);
        } else {
          tokensInCustody -= amount;
        }
      } else if (transferInformation.addTokensOrCoinsIfTransferSuccessful()) {
        addCoins(transferInformation.getCoinIndex(), transferInformation.getAmount());
      }
      removePendingTransfer(transactionId);
    }
  }

  void abortTransfer(Hash transactionId) {
    TransferInformation transferInformation = storedPendingTransfers.getValue(transactionId);
    if (transferInformation != null) {
      if (!transferInformation.addTokensOrCoinsIfTransferSuccessful()) {
        if (transferInformation.getCoinIndex() != -1) {
          addCoins(transferInformation.getCoinIndex(), transferInformation.amount());
        } else {
          addMpcTokens(transferInformation.getAmount().longValueExact());
        }
      }
      removePendingTransferAbort(transactionId);
    }
  }

  void commitDelegatedStakes(Hash transactionId, Long blockProductionTime) {
    StakeDelegation stakeDelegation = storedPendingStakeDelegations.getValue(transactionId);

    if (stakeDelegation != null) {
      long amount = stakeDelegation.amount;
      StakeDelegation.DelegationType delegationType = stakeDelegation.delegationType;
      BlockchainAddress counterPart = stakeDelegation.counterPart;
      if (delegationType.equals(StakeDelegation.DelegationType.DELEGATE_STAKES)) {
        commitDelegateStakes(counterPart, amount);
        tokensInCustody -= amount;
      } else if (delegationType.equals(StakeDelegation.DelegationType.RECEIVE_DELEGATED_STAKES)) {
        commitReceiveDelegatedStakes(counterPart, amount, stakeDelegation.expirationTimestamp);
        tokensInCustody += amount;
      } else if (delegationType.equals(StakeDelegation.DelegationType.RETRACT_DELEGATED_STAKES)) {
        addPendingRetractedDelegatedStakes(amount, blockProductionTime);
        tokensInCustody += amount;
      } else { // StakeDelegation.DelegationType.RETURN_DELEGATED_STAKES
        tokensInCustody -= amount;
      }
      removePendingStakeDelegation(transactionId);
    }
  }

  void abortDelegateStakes(Hash transactionId) {
    StakeDelegation stakeDelegation = storedPendingStakeDelegations.getValue(transactionId);

    if (stakeDelegation != null) {
      StakeDelegation.DelegationType delegationType = stakeDelegation.delegationType;
      long amount = stakeDelegation.amount;
      if (delegationType.equals(StakeDelegation.DelegationType.DELEGATE_STAKES)) {
        addMpcTokens(amount);
      } else {
        BlockchainAddress counterPart = stakeDelegation.counterPart;
        if (delegationType.equals(StakeDelegation.DelegationType.RETURN_DELEGATED_STAKES)) {
          abortReturnDelegatedStakes(counterPart, amount);
        } else if (delegationType.equals(StakeDelegation.DelegationType.RETRACT_DELEGATED_STAKES)) {
          abortRetractDelegatedStakes(counterPart, amount);
        }
      }
      removePendingStakeDelegation(transactionId);
    }
  }

  void commitIce(Hash transactionId) {
    IceStake pendingIceStake = storedPendingIceStakes.getValue(transactionId);
    if (pendingIceStake != null) {
      if (pendingIceStake.type() == IceStake.IceStakeType.ASSOCIATE) {
        removePendingIceStake(transactionId);
      } else {
        disassociate(pendingIceStake.address(), pendingIceStake.amount());
        removePendingIceStake(transactionId);
      }
    }
  }

  void abortIce(Hash transactionId) {
    IceStake pendingIceStake = storedPendingIceStakes.getValue(transactionId);
    if (pendingIceStake != null) {
      if (pendingIceStake.type() == IceStake.IceStakeType.ASSOCIATE) {
        disassociate(pendingIceStake.address(), pendingIceStake.amount());
        removePendingIceStake(transactionId);
      } else {
        removePendingIceStake(transactionId);
      }
    }
  }

  void initiateWithdraw(
      Hash transactionId, TransferInformation transferInformation, String symbol) {
    TransferInformation storedTransferInformation = storedPendingTransfers.getValue(transactionId);
    boolean isRetry = storedTransferInformation != null;
    if (isRetry) {
      ensureTransferIsBetweenDifferentAccounts(transferInformation, storedTransferInformation);
    } else {
      addPendingTransfer(transactionId, transferInformation);
      if (transferInformation.coinIndex() != -1) {
        deductCoins(transferInformation.coinIndex(), transferInformation.getAmount(), symbol);
      } else {
        deductMpcTokens(transferInformation.getAmount().longValueExact());
      }
    }
  }

  void initiateWithdraw(Hash transactionId, TransferInformation transferInformation) {
    initiateWithdraw(transactionId, transferInformation, null);
  }

  void initiateDeposit(Hash transactionId, TransferInformation transferInformation) {
    TransferInformation storedTransferInformation = storedPendingTransfers.getValue(transactionId);
    boolean isRetry = storedTransferInformation != null;
    if (isRetry) {
      ensureTransferIsBetweenDifferentAccounts(transferInformation, storedTransferInformation);
    } else {
      addPendingTransfer(transactionId, transferInformation);
    }
  }

  /**
   * The total amount of self staked and accepted delegated stakes from others.
   *
   * @return sum of staked tokens for jobs
   */
  long stakeForJobs() {
    long stakeForJobs = stakedTokens;
    for (StakesFromOthers stakesFromOthers : delegatedStakesFromOthers.values()) {
      stakeForJobs += stakesFromOthers.acceptedDelegatedStakes;
    }
    return stakeForJobs;
  }

  private static void ensureTransferIsBetweenDifferentAccounts(
      TransferInformation transferInformation, TransferInformation storedTransferInformation) {
    if (storedTransferInformation.addTokensOrCoinsIfTransferSuccessful()
        != transferInformation.addTokensOrCoinsIfTransferSuccessful()) {
      throw new IllegalArgumentException("Cannot initiate transfer between the same account");
    }
  }

  void initiateDelegateStakes(Hash transactionId, StakeDelegation stakeDelegation) {
    long amount = stakeDelegation.amount;
    boolean isRetry = storedPendingStakeDelegations.getValue(transactionId) != null;
    if (!isRetry) {
      addPendingStakeDelegation(transactionId, stakeDelegation);
      delegateStakes(amount);
    }
  }

  void initiateReceiveDelegatedStakes(Hash transactionId, StakeDelegation stakeDelegation) {
    boolean isRetry = storedPendingStakeDelegations.getValue(transactionId) != null;
    if (!isRetry) {
      addPendingStakeDelegation(transactionId, stakeDelegation);
    }
  }

  void initiateRetractDelegatedStakes(
      Hash transactionId, StakeDelegation stakeDelegation, BlockchainAddress recipient) {
    long amount = stakeDelegation.amount;
    boolean isRetry = storedPendingStakeDelegations.getValue(transactionId) != null;
    if (!isRetry) {
      addPendingStakeDelegation(transactionId, stakeDelegation);
      retractDelegatedStakes(recipient, amount);
    }
  }

  void initiateReturnDelegatedStakes(Hash transactionId, StakeDelegation stakeDelegation) {
    boolean isRetry = storedPendingStakeDelegations().getValue(transactionId) != null;
    if (!isRetry) {
      addPendingStakeDelegation(transactionId, stakeDelegation);
      returnDelegatedStakes(stakeDelegation.counterPart, stakeDelegation.amount);
      redistributeDelegatedStakes();
    }
  }

  void initiateIceAssociate(Hash transactionId, BlockchainAddress target, long amount) {
    if (pendingIceStake(transactionId) != null) {
      return;
    }
    IceStake iceStake = new IceStake(target, amount, IceStake.IceStakeType.ASSOCIATE);
    addPendingIceStake(transactionId, iceStake);
    associateWithExpiration(target, amount, null);
  }

  void initiateIceDisassociate(Hash transactionId, BlockchainAddress target, long amount) {
    IceStake iceStake = new IceStake(target, amount, IceStake.IceStakeType.DISASSOCIATE);
    addPendingIceStake(transactionId, iceStake);
  }

  void checkVestedTokens(long blockProductionTime) {
    FixedList<InitialVestingAccount> updatedVestingAccounts = FixedList.create();
    for (InitialVestingAccount accountTier : vestingAccounts()) {
      long amountToRelease = accountTier.getAmountToRelease(blockProductionTime);
      if (amountToRelease > 0) {
        accountTier = accountTier.releaseTokens(amountToRelease);
        addMpcTokens(amountToRelease);
      }
      if (accountTier.leftToRelease() > 0) {
        updatedVestingAccounts = updatedVestingAccounts.addElement(accountTier);
      }
    }
    setVestingAccounts(updatedVestingAccounts);
  }

  void appointCustodian(BlockchainAddress custodianAddress) {
    custodian = new Custodian(custodian.accepted(), custodianAddress);
  }

  void cancelAppointedCustodian() {
    custodian = new Custodian(custodian.accepted(), null);
  }

  void acceptCustodian() {
    custodian = new Custodian(custodian.appointed(), null);
  }

  void resetCustodian() {
    custodian = new Custodian(null, null);
  }

  void updateTimestampForAssociations(
      BlockchainAddress contractAddress, Long newExpirationTimestamp) {
    StakedToContract stakedToContract = this.stakedToContract.getValue(contractAddress);

    if (stakedToContract == null) {
      throw new IllegalArgumentException(
          "Cannot update association expiration timestamp for contract that has not been associated"
              + " to");
    }

    AvlTree<BlockchainAddress, StakedToContract> updatedStakedToContract = stakedToContract();
    stakedToContract = stakedToContract.setExpirationTimestamp(newExpirationTimestamp);
    updatedStakedToContract = updatedStakedToContract.set(contractAddress, stakedToContract);
    ensureStakeForJobsCanCoverAssociations(delegatedStakesFromOthers, updatedStakedToContract);
    setStakedToContract(updatedStakedToContract);
  }

  void updateExpirationForStakedTokens(BlockchainAddress delegator, Long newExpirationTimestamp) {
    StakesFromOthers stakesFromDelegator = delegatedStakesFromOthers.getValue(delegator);
    if (stakesFromDelegator == null) {
      throw new IllegalArgumentException(
          "Cannot update delegation expiration timestamp for account that has not been delegated"
              + " to");
    }

    StakesFromOthers updatedStakesFromDelegator =
        StakesFromOthers.create(
            stakesFromDelegator.acceptedDelegatedStakes,
            stakesFromDelegator.pendingDelegatedStakes,
            newExpirationTimestamp);

    AvlTree<BlockchainAddress, StakesFromOthers> updatedDelegatedStakesFromOthers =
        delegatedStakesFromOthers().set(delegator, updatedStakesFromDelegator);
    ensureStakeForJobsCanCoverAssociations(updatedDelegatedStakesFromOthers, stakedToContract);
    setDelegatedStakesFromOthers(updatedDelegatedStakesFromOthers);
    redistributeDelegatedStakes();
  }

  /**
   * Stores information about a single entry of {@link BalanceMutable#delegatedStakesFromOthers}.
   *
   * @param delegator the delegator of the stakes
   * @param stakes the stakes from the delegator
   */
  record DelegationEntry(BlockchainAddress delegator, StakesFromOthers stakes) {

    public boolean expiresAtOrAfter(Long timestamp) {
      return stakes.expirationTimestamp == null
          || (timestamp != null && stakes.expirationTimestamp >= timestamp);
    }
  }

  /**
   * Stores information about a single entry of {@link BalanceMutable#stakedToContract()}.
   *
   * @param contractAddress the contract associated to
   * @param association the association of the contract
   */
  record AssociationEntry(BlockchainAddress contractAddress, StakedToContract association) {}
}
