package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/** Manages external coins that exist on the chain. */
@Immutable
public final class ExternalCoins implements StateSerializableInline {

  /**
   * The list of coins that exist. The index of a coin in this list defines the index used in many
   * other lists containing information about coins, see for example {@link Balance#accountCoins}.
   */
  public final FixedList<Coin> coins;

  @SuppressWarnings("unused")
  ExternalCoins() {
    coins = null;
  }

  private ExternalCoins(FixedList<Coin> coins) {
    this.coins = coins;
  }

  /**
   * Create an external coin object from its state.
   *
   * @param accessor The state of the external coin
   * @return A new external coin object with the state of the accessor
   */
  public static ExternalCoins createFromStateAccessor(StateAccessor accessor) {
    return new ExternalCoins(
        FixedList.create(
            accessor.get("coins").getListElements().stream()
                .map(Coin::createFromStateAccessor)
                .toList()));
  }

  FixedList<Coin> getCoins() {
    return coins;
  }

  /**
   * Sets a coin. Existing will be overwritten, otherwise created.
   *
   * @param newCoin the coin with conversion rate
   * @return the updated version of the external coins object
   */
  ExternalCoins overwrite(Coin newCoin) {
    int coinIndex = getCoinIndex(newCoin.getSymbol());
    if (coinIndex != -1) {
      return new ExternalCoins(coins.setElement(coinIndex, newCoin));
    } else {
      return new ExternalCoins(coins.addElement(newCoin));
    }
  }

  int getCoinIndex(String symbol) {
    for (int i = 0; i < coins.size(); i++) {
      if (symbol.equals(coins.get(i).getSymbol())) {
        return i;
      }
    }
    return -1;
  }

  static ExternalCoins create() {
    return create(List.of());
  }

  static ExternalCoins create(List<Coin> coins) {
    return new ExternalCoins(FixedList.create(coins));
  }
}
