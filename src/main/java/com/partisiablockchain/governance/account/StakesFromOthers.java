package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/** Stores information of stakes from another account. */
@Immutable
public final class StakesFromOthers implements StateSerializable {

  /** The amount of accepted stakes from a delegator. Accepted stakes can be used to run jobs. */
  public final long acceptedDelegatedStakes;

  /** The amount of pending stakes from a delegator. Pending stakes can not be used to run jobs. */
  public final long pendingDelegatedStakes;

  /** The timestamp when the delegated stakes can no longer be used to run new jobs. */
  public final Long expirationTimestamp;

  @SuppressWarnings("unused")
  StakesFromOthers() {
    acceptedDelegatedStakes = 0;
    pendingDelegatedStakes = 0;
    expirationTimestamp = null;
  }

  private StakesFromOthers(
      long acceptedDelegatedStakes, long pendingDelegatedStakes, Long expirationTimestamp) {
    this.acceptedDelegatedStakes = acceptedDelegatedStakes;
    this.pendingDelegatedStakes = pendingDelegatedStakes;
    this.expirationTimestamp = expirationTimestamp;
  }

  static StakesFromOthers create(
      long acceptedDelegatedStakes, long pendingDelegatedStakes, Long expirationTimestamp) {
    return new StakesFromOthers(
        acceptedDelegatedStakes, pendingDelegatedStakes, expirationTimestamp);
  }

  StakesFromOthers updatePending(long amount) {
    return create(acceptedDelegatedStakes, pendingDelegatedStakes + amount, expirationTimestamp);
  }

  StakesFromOthers updateAccepted(long amount) {
    return create(acceptedDelegatedStakes + amount, pendingDelegatedStakes, expirationTimestamp);
  }

  /**
   * Updates both timestamps for delegation if the new expiration is not null and later than the
   * current delegation.
   *
   * @param expirationTimestamp the new expiration timestamp
   * @return the delegation with new timestamp if the expiration is later, otherwise same delegation
   */
  StakesFromOthers updateTimestampsIfLaterExpiration(Long expirationTimestamp) {
    if (this.expirationTimestamp == null || expirationTimestamp == null) {
      return this;
    }

    return create(
        acceptedDelegatedStakes,
        pendingDelegatedStakes,
        Math.max(this.expirationTimestamp, expirationTimestamp));
  }
}
