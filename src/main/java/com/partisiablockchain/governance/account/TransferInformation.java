package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;

/**
 * Stores information of pending transactions for Two Phase Commit of token or BYOC transfers.
 *
 * @param amount amount being transferred
 * @param addTokensOrCoinsIfTransferSuccessful true if this transfer is a deposit, otherwise false
 * @param coinIndex coin index of the BYOC being transferred or -1 if transfer is of MPC tokens
 */
@Immutable
public record TransferInformation(
    Unsigned256 amount, boolean addTokensOrCoinsIfTransferSuccessful, int coinIndex)
    implements StateSerializableInline {

  static TransferInformation createForDeposit(Unsigned256 amount, int coinIndex) {
    return new TransferInformation(amount, true, coinIndex);
  }

  static TransferInformation createForDeposit(long amount, int coinIndex) {
    return createForDeposit(Unsigned256.create(amount), coinIndex);
  }

  static TransferInformation createForWithdraw(Unsigned256 amount, int coinIndex) {
    return new TransferInformation(amount, false, coinIndex);
  }

  static TransferInformation createForWithdraw(long amount, int coinIndex) {
    return createForWithdraw(Unsigned256.create(amount), coinIndex);
  }

  /**
   * Create a TransferInformation object from a state.
   *
   * @param accessor The state to create the object from.
   * @return A new TransferInformation object with its state initialized from the accessor.
   */
  public static TransferInformation createFromStateAccessor(StateAccessor accessor) {
    int coinIndex = accessor.get("coinIndex").intValue();
    boolean ifTransferSuccessful =
        accessor.get("addTokensOrCoinsIfTransferSuccessful").booleanValue();
    Unsigned256 amount = accessor.get("amount").cast(Unsigned256.class);

    return new TransferInformation(amount, ifTransferSuccessful, coinIndex);
  }

  /**
   * The amount in this transfer.
   *
   * @return the transfer amount
   */
  public Unsigned256 getAmount() {
    return amount;
  }

  /**
   * The coin index of the coin in this transfer. If the transfer is of MPC tokens, the index is -1.
   *
   * @return the coin index
   */
  public int getCoinIndex() {
    return coinIndex;
  }

  /**
   * Determines whether this transfer is for a deposit.
   *
   * @return true if this transfer is for a deposit
   */
  public boolean isDeposit() {
    return addTokensOrCoinsIfTransferSuccessful;
  }

  /**
   * Determines whether this transfer is for a withdrawal.
   *
   * @return true if this transfer is for a withdrawal
   */
  public boolean isWithdraw() {
    return !isDeposit();
  }
}
