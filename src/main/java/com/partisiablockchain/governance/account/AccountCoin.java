package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;
import com.partisiablockchain.util.ListUtil;

/** The balance of an external coin (BYOC) that can be held by accounts and contracts. */
@Immutable
public final class AccountCoin implements StateSerializableInline {

  /** Utility for updating a list of AccountCoin. */
  static final ListUtil<AccountCoin> COINS_LIST =
      new ListUtil<>(AccountCoin::create, AccountCoin::isEmpty);

  /** The balance of this particular coin. */
  public final Unsigned256 balance;

  @SuppressWarnings("unused")
  AccountCoin() {
    balance = Unsigned256.ZERO;
  }

  private AccountCoin(Unsigned256 balance) {
    this.balance = balance;
  }

  static AccountCoin createFromStateAccessor(StateAccessor accessor) {
    return new AccountCoin(accessor.get("balance").cast(Unsigned256.class));
  }

  /**
   * Return the number of coins required for a weighted gas amount. Cannot exceed coin balance.
   *
   * @param amount the requested amount
   * @param ratio the ratio for conversion
   * @return the amount to extract - tho never higher than the balance.
   */
  Unsigned256 requiredCoinsForAmount(Unsigned256 amount, Fraction ratio) {
    Unsigned256 weightedBalance = computeGasEquivalent(ratio);
    Unsigned256 withdrawn = MathUtil.min(weightedBalance, amount);
    return MathUtil.ceilDiv(
        withdrawn.multiply(Unsigned256.create(ratio.getDenominator())),
        Unsigned256.create(ratio.getNumerator()));
  }

  AccountCoin add(Unsigned256 diff) {
    return new AccountCoin(balance.add(diff));
  }

  AccountCoin deduct(Unsigned256 diff) {
    return new AccountCoin(balance.subtract(diff));
  }

  Unsigned256 getBalance() {
    return balance;
  }

  Unsigned256 computeGasEquivalent(Fraction ratio) {
    return balance
        .multiply(Unsigned256.create(ratio.getNumerator()))
        .divide(Unsigned256.create(ratio.getDenominator()));
  }

  static AccountCoin create() {
    return new AccountCoin(Unsigned256.ZERO);
  }

  boolean isEmpty() {
    return getBalance().equals(Unsigned256.ZERO);
  }
}
