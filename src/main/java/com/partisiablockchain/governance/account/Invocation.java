package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.Arrays;

/**
 * Supertype of the three kinds of invocations in the fee account plugin: {@link AccountInvocation},
 * {@link ContractInvocation} and {@link ContextFreeInvocation}.
 */
public sealed interface Invocation
    permits AccountInvocation, ContractInvocation, ContextFreeInvocation {

  /**
   * Unique prefix byte for the given {@link Invocation}.
   *
   * @return Invocation byte of {@link Invocation}.
   */
  byte getInvocationByte();

  /**
   * Finds wanted {@link Invocation} by searching through given list of {@link Invocation}s for the
   * first {@link Invocation} with the correct {@link #getInvocationByte}.
   *
   * @param <T> Type of {@link Invocation}.
   * @param invocations {@link Invocation}s to sort among.
   * @param invocationByte Invocation byte of wanted {@link Invocation}.
   * @param humanReadableType Human readable name for the invocation type. Used for error messages.
   * @return invokable invocation
   * @throws UnsupportedOperationException if account plugin does not support the given invocation
   *     byte.
   */
  static <T extends Invocation> T findInvocationBy(
      T[] invocations, int invocationByte, String humanReadableType) {
    final T invocation =
        Arrays.stream(invocations)
            .filter(inv -> inv.getInvocationByte() == invocationByte)
            .findFirst()
            .orElse(null);
    if (invocation == null) {
      throw new UnsupportedOperationException(
          "Unknown invocation in Account Plugin (%s) with invocation byte: %d"
              .formatted(humanReadableType, invocationByte));
    }
    return invocation;
  }
}
