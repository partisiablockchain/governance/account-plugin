package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializableInline;

/** Stores information of pending delegation of stakes. */
@Immutable
public final class StakeDelegation implements StateSerializableInline {

  /** The other account participating in the delegation. */
  public final BlockchainAddress counterPart;

  /** The amount of delegated stakes. */
  public final long amount;

  /** The type of delegation. See {@link DelegationType} */
  public final DelegationType delegationType;

  /** The timestamp of when the delegation is made. */
  public final Long delegationTimestamp;

  /** The timestamp when the delegated stakes can no longer be used to run new jobs. */
  public final Long expirationTimestamp;

  @SuppressWarnings("unused")
  StakeDelegation() {
    this.counterPart = null;
    this.amount = 0;
    this.delegationType = null;
    this.expirationTimestamp = null;
    this.delegationTimestamp = null;
  }

  private StakeDelegation(
      BlockchainAddress counterPart,
      long amount,
      DelegationType delegationType,
      Long delegationTimestamp,
      Long expirationTimestamp) {
    this.counterPart = counterPart;
    this.amount = amount;
    this.delegationType = delegationType;
    this.delegationTimestamp = delegationTimestamp;
    this.expirationTimestamp = expirationTimestamp;
  }

  static StakeDelegation createDelegation(
      BlockchainAddress counterPart, long amount, long delegationTimestamp) {
    return new StakeDelegation(
        counterPart, amount, DelegationType.DELEGATE_STAKES, delegationTimestamp, null);
  }

  static StakeDelegation createRetractDelegation(BlockchainAddress counterPart, long amount) {
    return new StakeDelegation(
        counterPart, amount, DelegationType.RETRACT_DELEGATED_STAKES, null, null);
  }

  static StakeDelegation createReceiveDelegation(
      BlockchainAddress counterPart,
      long amount,
      Long delegationTimestamp,
      Long expirationTimestamp) {
    return new StakeDelegation(
        counterPart,
        amount,
        DelegationType.RECEIVE_DELEGATED_STAKES,
        delegationTimestamp,
        expirationTimestamp);
  }

  static StakeDelegation createReturnDelegation(BlockchainAddress counterPart, long amount) {
    return new StakeDelegation(
        counterPart, amount, DelegationType.RETURN_DELEGATED_STAKES, null, null);
  }

  @SuppressWarnings("EnumOrdinal")
  static StakeDelegation createFromStateAccessor(StateAccessor accessor) {
    Enum<?> enumValue = accessor.get("delegationType").cast(Enum.class);

    Long expirationTimestamp = null;
    if (accessor.hasField("expirationTimestamp")) {
      expirationTimestamp = accessor.get("expirationTimestamp").cast(Long.class);
    }

    Long delegationTimestamp = null;
    if (accessor.hasField("delegationTimestamp")) {
      delegationTimestamp = accessor.get("delegationTimestamp").cast(Long.class);
    }

    return new StakeDelegation(
        accessor.get("counterPart").blockchainAddressValue(),
        accessor.get("amount").longValue(),
        DelegationType.values()[enumValue.ordinal()],
        delegationTimestamp,
        expirationTimestamp);
  }

  /** The different types of delegation of stakes. */
  public enum DelegationType {
    /** Delegate stakes to another account. */
    DELEGATE_STAKES,
    /** Retract delegated stakes from another account. */
    RETRACT_DELEGATED_STAKES,
    /** Receive delegated stakes from another account. */
    RECEIVE_DELEGATED_STAKES,
    /** Return delegated stakes to the account that delegated them. */
    RETURN_DELEGATED_STAKES
  }
}
