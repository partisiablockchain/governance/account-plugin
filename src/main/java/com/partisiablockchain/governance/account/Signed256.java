package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Objects;

/** Represents a signed 256-bit value. */
@Immutable
public final class Signed256 implements StateSerializable {

  private final Unsigned256 value;
  private final boolean sign;

  /** Serializable constructor. */
  @SuppressWarnings("unused")
  private Signed256() {
    this.value = Unsigned256.ZERO;
    this.sign = true;
  }

  /**
   * Default constructor.
   *
   * @param value is the absolute value.
   * @param sign is current sign of the value.
   */
  private Signed256(Unsigned256 value, boolean sign) {
    this.value = value;
    this.sign = sign;
  }

  /**
   * Create a Signed256.
   *
   * @return a new Signed256.
   */
  public static Signed256 create() {
    return new Signed256(Unsigned256.ZERO, true);
  }

  /**
   * Create a Signed256 from an Unsigned256.
   *
   * @param value an Unsigned256
   * @return a new Signed256
   */
  public static Signed256 create(Unsigned256 value) {
    return new Signed256(value, true);
  }

  static Signed256 createFromStateAccessor(StateAccessor stateAccessor) {
    Unsigned256 value = stateAccessor.get("value").cast(Unsigned256.class);
    boolean sign = stateAccessor.get("sign").booleanValue();
    return new Signed256(value, sign);
  }

  /**
   * Add an unsigned 256-bit value to this value.
   *
   * @param addend the value to be added.
   * @return a new {@code Signed256} with the altered value.
   */
  public Signed256 add(Unsigned256 addend) {
    if (isPositive()) {
      return new Signed256(getAbsoluteValue().add(addend), true);
    } else {
      return subtractWithSign(addend, getAbsoluteValue(), false);
    }
  }

  /**
   * Subtracts un unsigned 256-bit value from this value.
   *
   * @param subtrahend the value to be subtracted.
   * @return a new {@code Signed256} with altered value.
   */
  public Signed256 subtract(Unsigned256 subtrahend) {
    if (!isPositive()) {
      return new Signed256(getAbsoluteValue().add(subtrahend), false);
    } else {
      return subtractWithSign(subtrahend, getAbsoluteValue(), true);
    }
  }

  /**
   * Calculates a signed value.
   *
   * @param currentValue is subtracted from if sign does not flip.
   * @param term is subtracted from if sign flips.
   * @return a new signed value.
   */
  private static Signed256 subtractWithSign(
      Unsigned256 term, Unsigned256 currentValue, boolean sign) {

    if (willFlipSign(currentValue, term, sign)) {
      return new Signed256(term.subtract(currentValue), !sign);
    } else {
      return new Signed256(currentValue.subtract(term), sign);
    }
  }

  /**
   * Get the absolute value of this Signed256.
   *
   * @return the absolute value.
   */
  public Unsigned256 getAbsoluteValue() {
    return value;
  }

  /**
   * Get the value as a positive value. That is, return the absolute value if this Signed256 is
   * positive and Unsigned.ZERO otherwise.
   *
   * @return the positive value
   */
  public Unsigned256 getPositiveValue() {
    return isPositive() ? getAbsoluteValue() : Unsigned256.ZERO;
  }

  /**
   * Check if the current value is positive.
   *
   * @return whether the current value is positive.
   */
  public boolean isPositive() {
    return sign;
  }

  private static boolean willFlipSign(Unsigned256 currentValue, Unsigned256 term, boolean sign) {
    int res = currentValue.compareTo(term);
    // Is the value negative and will become zero?
    if (!sign && res == 0) {
      // Then we flip the sign.
      return true;
    }
    // Otherwise, only flip if going from negative to positive or vice versa.
    return res <= -1;
    // Using the above comparison instead of "res < 0", because Pitest might change "<"
    // to "<=", which the test will survive.
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Signed256 signed256 = (Signed256) o;
    return sign == signed256.sign && Objects.equals(value, signed256.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value, sign);
  }
}
