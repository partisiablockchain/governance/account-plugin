package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.Set;

/** The state an account. */
@Immutable
public final class AccountStateLocal implements StateSerializable {
  /**
   * Keeps track of external coins (BYOC) that have been converted to gas. The index of a coin
   * corresponds to a coin in the global state, see {@link AccountStateGlobal#coins}.
   */
  public final FixedList<ConvertedExternalCoin> convertedExternalCoins;

  /**
   * Nodes that have performed service work and the gas fee they are to be paid for this work, but
   * have not received yet.
   */
  public final AvlTree<BlockchainAddress, Unsigned256> serviceFees;

  /**
   * Mapping of epochs to the total blockchain usage in gas for this epoch, that has not been paid
   * out yet. The blockchain usage is the collective use of CPU, network and storage.
   */
  public final AvlTree<Long, Unsigned256> blockchainUsage;

  /**
   * The sum of infrastructure fees in gas that have not been paid out yet. The infrastructure fees
   * are continuously saved up, to cover payment of certain services that are not carried out by a
   * single clear user such as price oracle services.
   */
  public final Unsigned256 infrastructureFeesSum;

  /** Sum of MPC tokens that have been removed from deleted contracts. */
  public final long removedContractsMpcTokens;

  /** Creates a new object for a default value. */
  @SuppressWarnings("unused")
  public AccountStateLocal() {
    convertedExternalCoins = null;
    serviceFees = null;
    blockchainUsage = null;
    infrastructureFeesSum = null;
    removedContractsMpcTokens = 0;
  }

  private AccountStateLocal(
      FixedList<ConvertedExternalCoin> convertedExternalCoins,
      AvlTree<BlockchainAddress, Unsigned256> serviceFees,
      AvlTree<Long, Unsigned256> blockchainUsage,
      Unsigned256 infrastructureFeesSum,
      long removedContractsMpcTokens) {
    this.convertedExternalCoins = convertedExternalCoins;
    this.serviceFees = serviceFees;
    this.blockchainUsage = blockchainUsage;
    this.infrastructureFeesSum = infrastructureFeesSum;
    this.removedContractsMpcTokens = removedContractsMpcTokens;
  }

  static AccountStateLocal createFromStateAccessor(StateAccessor accessor) {
    FixedList<ConvertedExternalCoin> convertedExternalCoins =
        FixedList.create(
            accessor.get("convertedExternalCoins").getListElements().stream()
                .map(ConvertedExternalCoin::createFromStateAccessor)
                .toList());
    AvlTree<BlockchainAddress, Unsigned256> serviceFees =
        accessor.get("serviceFees").typedAvlTree(BlockchainAddress.class, Unsigned256.class);
    AvlTree<Long, Unsigned256> blockchainUsage =
        accessor.get("blockchainUsage").typedAvlTree(Long.class, Unsigned256.class);
    Unsigned256 infrastructureFeesSum =
        accessor.get("infrastructureFeesSum").cast(Unsigned256.class);
    long removedContractsMpcTokens = 0;
    if (accessor.hasField("removedContractsMpcTokens")) {
      removedContractsMpcTokens = accessor.get("removedContractsMpcTokens").longValue();
    }
    return new AccountStateLocal(
        convertedExternalCoins,
        serviceFees,
        blockchainUsage,
        infrastructureFeesSum,
        removedContractsMpcTokens);
  }

  static AccountStateLocal initial() {
    return new AccountStateLocal(
        FixedList.create(), AvlTree.create(), AvlTree.create(), Unsigned256.ZERO, 0);
  }

  AccountStateLocal updateCollectedCoins(
      int coinIndex, Unsigned256 addedCoins, Unsigned256 correspondingGas) {
    FixedList<ConvertedExternalCoin> newCoins =
        ConvertedExternalCoin.COINS_LIST.withUpdatedEntry(
            convertedExternalCoins,
            coinIndex,
            accountCoin -> accountCoin.addConvertedCoins(addedCoins, correspondingGas));
    return new AccountStateLocal(
        newCoins, serviceFees, blockchainUsage, infrastructureFeesSum, removedContractsMpcTokens);
  }

  FixedList<ConvertedExternalCoin> convertedExternalCoins() {
    return convertedExternalCoins;
  }

  AccountStateLocal addBlockchainUsage(long currentEpoch, Unsigned256 gas) {
    Unsigned256 currentUsage = getBlockchainUsageForEpoch(currentEpoch);
    return new AccountStateLocal(
        convertedExternalCoins,
        serviceFees,
        blockchainUsage.set(currentEpoch, currentUsage.add(gas)),
        infrastructureFeesSum,
        removedContractsMpcTokens);
  }

  AccountStateLocal addMpcTokens(long mpcTokens) {
    return new AccountStateLocal(
        convertedExternalCoins,
        serviceFees,
        blockchainUsage,
        infrastructureFeesSum,
        this.removedContractsMpcTokens + mpcTokens);
  }

  Unsigned256 getBlockchainUsageForEpoch(long epoch) {
    return blockchainUsage.containsKey(epoch) ? blockchainUsage.getValue(epoch) : Unsigned256.ZERO;
  }

  AvlTree<Long, Unsigned256> getBlockchainUsage() {
    return blockchainUsage;
  }

  long getRemovedContractsMpcTokens() {
    return removedContractsMpcTokens;
  }

  AccountStateLocal addServiceFee(BlockchainAddress serviceNodeAddress, long fee) {
    return addServiceFee(serviceNodeAddress, Unsigned256.create(fee));
  }

  AccountStateLocal addServiceFee(BlockchainAddress serviceNodeAddress, Unsigned256 fee) {
    Unsigned256 currentServiceFee = getServiceFeesForAddress(serviceNodeAddress);
    return new AccountStateLocal(
        convertedExternalCoins,
        serviceFees.set(serviceNodeAddress, currentServiceFee.add(fee)),
        blockchainUsage,
        infrastructureFeesSum,
        removedContractsMpcTokens);
  }

  AccountStateLocal addInfrastructureFees(Unsigned256 sum) {
    return new AccountStateLocal(
        convertedExternalCoins,
        serviceFees,
        blockchainUsage,
        infrastructureFeesSum.add(sum),
        removedContractsMpcTokens);
  }

  Unsigned256 getServiceFeesForAddress(BlockchainAddress nodeAddress) {
    return serviceFees.containsKey(nodeAddress)
        ? serviceFees.getValue(nodeAddress)
        : Unsigned256.ZERO;
  }

  AvlTree<BlockchainAddress, Unsigned256> getServiceFees() {
    return serviceFees;
  }

  Unsigned256 getInfrastructureFeesSum() {
    return infrastructureFeesSum;
  }

  AccountStateLocal resetFees(Set<Long> epochsToReset) {
    AvlTree<Long, Unsigned256> updatedBlockchainUsage = blockchainUsage;
    for (Long epoch : epochsToReset) {
      updatedBlockchainUsage = updatedBlockchainUsage.remove(epoch);
    }
    return new AccountStateLocal(
        FixedList.create(),
        AvlTree.create(),
        updatedBlockchainUsage,
        infrastructureFeesSum,
        removedContractsMpcTokens);
  }

  AccountStateLocal resetInfrastructureFeesSum() {
    return new AccountStateLocal(
        convertedExternalCoins,
        serviceFees,
        blockchainUsage,
        Unsigned256.ZERO,
        removedContractsMpcTokens);
  }
}
