package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.immutable.FixedList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ExternalCoinsTest {

  private final ExternalCoins initial =
      ExternalCoins.create(List.of(Coin.create("sym", new Fraction(23, 1))));

  @Test
  public void empty() {
    ExternalCoins externalCoins = ExternalCoins.create();
    Assertions.assertThat(externalCoins.getCoins()).hasSize(0);
  }

  @Test
  public void overwriteExisting() {
    verifyInitial();
    ExternalCoins next = initial.overwrite(Coin.create("sym", new Fraction(22, 1)));
    verifyCoin(next, 0, new Fraction(22, 1), "sym");
    verifyInitial();
  }

  @Test
  public void overwriteNew() {
    verifyInitial();
    ExternalCoins next = initial.overwrite(Coin.create("new", new Fraction(70, 1)));
    verifyCoin(next, 0, new Fraction(23, 1), "sym");
    verifyCoin(next, 1, new Fraction(70, 1), "new");
    verifyInitial();
  }

  private void verifyInitial() {
    Assertions.assertThat(initial.getCoinIndex("sym")).isEqualTo(0);
    verifyCoin(initial, 0, new Fraction(23, 1), "sym");
  }

  private void verifyCoin(ExternalCoins value, int index, Fraction conversionRate, String symbol) {
    FixedList<Coin> coins = value.getCoins();
    Assertions.assertThat(coins).hasSizeGreaterThan(index);
    Coin coin = coins.get(index);
    Assertions.assertThat(coin.getConversionRate()).isEqualTo(conversionRate);
    Assertions.assertThat(coin.getSymbol()).isEqualTo(symbol);
    Assertions.assertThat(value.getCoinIndex(symbol)).isEqualTo(index);
  }
}
