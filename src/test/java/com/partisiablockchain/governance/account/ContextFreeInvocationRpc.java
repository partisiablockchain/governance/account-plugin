package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataOutputStream;
import java.util.function.Consumer;

/** Helper for generating RPC for {@link ContextFreeInvocation} invocations. */
public final class ContextFreeInvocationRpc {

  /**
   * Generate RPC for the invocation {@link
   * ContextFreeInvocation#COLLECT_SERVICE_AND_INFRASTRUCTURE_FEES_FOR_DISTRIBUTION}.
   *
   * @param epoch epoch to collect for
   * @return generated RPC
   */
  public static byte[] collectServiceAndInfrastructureFeesForDistribution(long epoch) {
    return serialize(
        ContextFreeInvocation.COLLECT_SERVICE_AND_INFRASTRUCTURE_FEES_FOR_DISTRIBUTION,
        s -> s.writeLong(epoch));
  }

  private static byte[] serialize(
      ContextFreeInvocation invocation, Consumer<SafeDataOutputStream> rpc) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(invocation.getInvocationByte());
          rpc.accept(stream);
        });
  }
}
