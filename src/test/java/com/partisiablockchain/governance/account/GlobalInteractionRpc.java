package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.governance.account.AccountStateGlobal.GlobalInteraction;
import com.secata.stream.SafeDataOutputStream;
import java.util.function.Consumer;

/** Helper for generating RPC for {@link GlobalInteraction} invocations. */
public final class GlobalInteractionRpc {

  /**
   * Generate RPC for the invocation {@link GlobalInteraction#SET_GAS_PRICE_FOR_1000_NETWORK}.
   *
   * @param nextPrice gas price for 1000 network
   * @return generated RPC
   */
  public static byte[] setGasPriceFor1000Network(int nextPrice) {
    return serialize(GlobalInteraction.SET_GAS_PRICE_FOR_1000_NETWORK, s -> s.writeInt(nextPrice));
  }

  /**
   * Generate RPC for the invocation {@link GlobalInteraction#SET_GAS_PRICE_FOR_1000_STORAGE}.
   *
   * @param nextPrice gas price for 1000 storage
   * @return generated RPC
   */
  public static byte[] setGasPriceFor1000Storage(int nextPrice) {
    return serialize(GlobalInteraction.SET_GAS_PRICE_FOR_1000_STORAGE, s -> s.writeInt(nextPrice));
  }

  /**
   * Generate RPC for the invocation {@link GlobalInteraction#SET_COIN}.
   *
   * @param symbol coin symbol
   * @param conversionRateNumerator conversion rate numerator
   * @param conversionRateDenominator conversion rate denominator
   * @return generated RPC
   */
  public static byte[] setCoin(
      String symbol, long conversionRateNumerator, long conversionRateDenominator) {
    return serialize(
        GlobalInteraction.SET_COIN,
        s -> {
          s.writeString(symbol);
          s.writeLong(conversionRateNumerator);
          s.writeLong(conversionRateDenominator);
        });
  }

  /**
   * Generate RPC for the invocation {@link GlobalInteraction#SET_GAS_PRICE_FOR_1000_ICE_STAKES}.
   *
   * @param nextPrice gas price for 1000 ice stakes
   * @return generated RPC
   */
  public static byte[] setGasPriceFor1000IceStakes(int nextPrice) {
    return serialize(
        GlobalInteraction.SET_GAS_PRICE_FOR_1000_ICE_STAKES, s -> s.writeInt(nextPrice));
  }

  private static byte[] serialize(
      GlobalInteraction invocation, Consumer<SafeDataOutputStream> rpc) {
    return GlobalPluginStateUpdate.create(
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeByte(invocation.getInteractionByte());
                  rpc.accept(stream);
                }))
        .getRpc();
  }
}
