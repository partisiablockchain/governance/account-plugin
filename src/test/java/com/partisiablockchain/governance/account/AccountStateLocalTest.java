package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.MemoryStateStorage;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.tools.immutable.FixedList;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AccountStateLocalTest {

  @Test
  public void verifySerialization() {
    int coinIndex = 1;
    Unsigned256 convertedCoins = Unsigned256.create(23);
    Unsigned256 correspondingGas = Unsigned256.create(40);
    AccountStateLocal withEverything =
        AccountStateLocal.initial()
            .updateCollectedCoins(coinIndex, convertedCoins, correspondingGas);
    StateSerializer serializer = new StateSerializer(new MemoryStateStorage(), true);
    SerializationResult result = serializer.write(withEverything);
    AccountStateLocal read = serializer.read(result.hash(), AccountStateLocal.class);

    Assertions.assertThat(read).isNotNull();
    FixedList<ConvertedExternalCoin> accountCoins = read.convertedExternalCoins();
    Assertions.assertThat(accountCoins.size()).isEqualTo(2);
    Assertions.assertThat(accountCoins.get(0).getConvertedCoins()).isEqualTo(Unsigned256.ZERO);
    Assertions.assertThat(accountCoins.get(coinIndex).getConvertedCoins())
        .isEqualTo(convertedCoins);
    Assertions.assertThat(accountCoins.get(coinIndex).getConvertedGasSum())
        .isEqualTo(correspondingGas);
  }

  @Test
  public void updateCollectedCoins_deleteEmpty() {
    AccountStateLocal accountStateLocal =
        AccountStateLocal.initial()
            .updateCollectedCoins(0, Unsigned256.create(100), Unsigned256.create(250))
            .updateCollectedCoins(1, Unsigned256.create(1), Unsigned256.create(0))
            .updateCollectedCoins(2, Unsigned256.create(0), Unsigned256.create(1))
            .updateCollectedCoins(3, Unsigned256.create(0), Unsigned256.create(0));

    Assertions.assertThat(accountStateLocal.convertedExternalCoins().size()).isEqualTo(3);
  }
}
