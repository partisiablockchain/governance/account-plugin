package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.governance.account.Balance.UNSTAKE_DURATION_MILLIS;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.MemoryStateStorage;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BalanceTest {

  private final Balance empty = Balance.create();
  private static final BlockchainAddress firstAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private static final BlockchainAddress secondAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000009");
  private static final Hash HASH = Hash.create(s -> s.writeByte(0));

  @Test
  void disassociateTokensRemovesContract() {
    BalanceMutable balance = empty.asMutable();
    balance.addMpcTokenBalance(123);
    balance.stakeTokens(123);
    balance.associateWithExpiration(firstAddress, 123, null);
    Assertions.assertThat(balance.stakedToContract().containsKey(firstAddress)).isTrue();

    BlockchainAddress anotherAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    balance.disassociateAllTokensForContract(anotherAddress);
    Assertions.assertThat(balance.stakedToContract().containsKey(firstAddress)).isTrue();

    balance.disassociateAllTokensForContract(firstAddress);
    Assertions.assertThat(balance.stakedToContract().containsKey(firstAddress)).isFalse();
  }

  @Test
  public void verifySerialization() {
    int coinIndex = 1;
    Unsigned256 coinAmount = Unsigned256.create(28);
    long mpcTokens = 2;
    long stakedTokens = 2;
    BlockchainAddress contract =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(h -> h.writeString("contract")));
    long stakedToContract = 1;
    long pendingUnstake = 1;
    int currentBlockTime = 1;
    long freeTransactionsEpoch = 1;
    InitialVestingAccount vestingAccount = InitialVestingAccount.create(1, 1, 1, 1);
    TransferInformation pendingTransactionInfo = TransferInformation.createForDeposit(1, -1);
    Hash pendingTransactionHash = Hash.create(h -> h.writeInt(1));
    StakeDelegation stakeDelegation = StakeDelegation.createDelegation(firstAddress, 1, 100L);
    Hash pendingDelegationHash = Hash.create(h -> h.writeInt(2));
    Hash pendingIceHash = Hash.create(s -> s.writeString("ice"));
    IceStake iceStake = new IceStake(firstAddress, 10, IceStake.IceStakeType.ASSOCIATE);

    BalanceMutable withEverything = empty.asMutable();
    withEverything.addCoins(coinIndex, coinAmount);
    withEverything.addMpcTokenBalance(mpcTokens);
    withEverything.stakeTokens(stakedTokens);
    withEverything.associateWithExpiration(contract, stakedToContract, null);
    withEverything.unstakeTokens(pendingUnstake, currentBlockTime);
    withEverything.addPendingTransfer(pendingTransactionHash, pendingTransactionInfo);
    withEverything.addPendingStakeDelegation(pendingDelegationHash, stakeDelegation);
    withEverything.commitReceiveDelegatedStakes(firstAddress, 10, null);
    withEverything.acceptPendingDelegatedStakes(firstAddress, 5);
    withEverything.incrementFreeTransactionsUsed(freeTransactionsEpoch);
    withEverything.createVestingAccount(
        vestingAccount.tokens,
        vestingAccount.tokenGenerationEvent,
        vestingAccount.releaseDuration,
        vestingAccount.releaseInterval);
    withEverything.addPendingIceStake(pendingIceHash, iceStake);

    StateSerializer serializer = new StateSerializer(new MemoryStateStorage(), true);
    SerializationResult result = serializer.write(withEverything.asImmutable());
    Balance read = serializer.read(result.hash(), Balance.class);
    StateSerializableEquality.assertStatesEqual(read, withEverything.asImmutable());
  }

  @Test
  @DisplayName("Burning all available staked tokens, succeeds")
  public void burnAllStakedAndDelegatedTokens() {
    long tokensInBalance = 10_000_100_0000L;
    long receiveDelegateStakesAmount = 4000L;
    final BlockchainAddress firstSender =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    final BlockchainAddress secondSender =
        BlockchainAddress.fromString("000000000000000000000000000000000000000003");

    BalanceMutable balance = empty.asMutable();
    balance.addMpcTokenBalance(tokensInBalance);
    balance.stakeTokens(tokensInBalance);
    balance.commitReceiveDelegatedStakes(firstSender, receiveDelegateStakesAmount, null);
    balance.acceptPendingDelegatedStakes(firstSender, receiveDelegateStakesAmount);
    balance.commitReceiveDelegatedStakes(secondSender, receiveDelegateStakesAmount, null);
    balance.acceptPendingDelegatedStakes(secondSender, receiveDelegateStakesAmount);
    long burnAmount = tokensInBalance + receiveDelegateStakesAmount * 2;
    balance.associateWithExpiration(Contracts.ONE, burnAmount, null);
    balance.disassociateAndBurn(burnAmount, Contracts.ONE);

    Assertions.assertThat(balance.stakedTokens()).isEqualTo(0L);
    AvlTree<BlockchainAddress, StakesFromOthers> delegatedStakesFromOthers =
        balance.delegatedStakesFromOthers();
    StakesFromOthers firstSenderDelegate = delegatedStakesFromOthers.getValue(firstSender);
    StakesFromOthers secondSenderDelegate = delegatedStakesFromOthers.getValue(secondSender);
    Assertions.assertThat(firstSenderDelegate).isNull();
    Assertions.assertThat(secondSenderDelegate).isNull();
  }

  @Test
  public void updateCoinBalance() {
    BalanceMutable withCoinsMut = empty.asMutable();
    withCoinsMut.addCoins(1, Unsigned256.create(100));
    Balance withCoins = withCoinsMut.asImmutable();
    Assertions.assertThat(withCoins.accountCoins()).hasSize(2);
    Assertions.assertThat(withCoins.accountCoins().get(0).isEmpty()).isTrue();
    Assertions.assertThat(withCoins.accountCoins().get(1).getBalance())
        .isEqualTo(Unsigned256.create(100));
    Assertions.assertThat(withCoins.coinBalance(0)).isEqualTo(Unsigned256.create(0));
    Assertions.assertThat(withCoins.coinBalance(1)).isEqualTo(Unsigned256.create(100));
    Assertions.assertThat(withCoins.coinBalance(2)).isEqualTo(Unsigned256.create(0));

    BalanceMutable newBalanceMut = withCoins.asMutable();
    newBalanceMut.deductCoins(1, Unsigned256.create(100), "");
    Balance newBalance = newBalanceMut.asImmutable();
    Assertions.assertThat(newBalance.accountCoins()).hasSize(0);
    Assertions.assertThat(newBalance.coinBalance(0)).isEqualTo(Unsigned256.create(0));
    Assertions.assertThat(newBalance.coinBalance(1)).isEqualTo(Unsigned256.create(0));
    Assertions.assertThat(newBalance.coinBalance(2)).isEqualTo(Unsigned256.create(0));
  }

  @Test
  public void updateCoinBalanceThrowsException() {
    BalanceMutable withCoins = empty.asMutable();
    long balance = 100;
    long toDeduct = 101;
    String coinSymbol = "coinSymbol";
    withCoins.addCoins(1, Unsigned256.create(balance));
    Assertions.assertThatThrownBy(
            () -> {
              withCoins.deductCoins(1, Unsigned256.create(toDeduct), "coinSymbol");
              throw new RuntimeException("should never happen " + withCoins.mpcTokens());
            })
        .hasMessage(
            "Cannot deduct %s %s with a balance of %s".formatted(toDeduct, coinSymbol, balance));
  }

  @Test
  public void getTotalBalance() {
    BalanceMutable withCoins = empty.asMutable();
    withCoins.addCoins(1, Unsigned256.create(100));
    Assertions.assertThat(withCoins.asImmutable().totalBalance(index -> new Fraction(index + 1, 1)))
        .isEqualTo(Unsigned256.create(100 * 2));
  }

  @Test
  public void handlePendingUpdates() {
    BalanceMutable setup = empty.asMutable();
    setup.addMpcTokenBalance(10);
    setup.stakeTokens(10);
    setup.unstakeTokens(4, 0);
    setup.unstakeTokens(1, 0);
    setup.addPendingRetractedDelegatedStakes(1, 1);
    setup.addPendingRetractedDelegatedStakes(1, 1);
    setup.asImmutable();
    Assertions.assertThat(setup.stakedTokens()).isEqualTo(5);
    Assertions.assertThat(setup.mpcTokens()).isEqualTo(0);

    setup.handlePendingUnstakes(UNSTAKE_DURATION_MILLIS - 1);
    Assertions.assertThat(setup.stakedTokens()).isEqualTo(5);
    Assertions.assertThat(setup.mpcTokens()).isEqualTo(0);

    setup.handlePendingUnstakes(UNSTAKE_DURATION_MILLIS);
    Assertions.assertThat(setup.stakedTokens()).isEqualTo(5);
    Assertions.assertThat(setup.mpcTokens()).isEqualTo(5);

    setup.handlePendingUnstakes(UNSTAKE_DURATION_MILLIS + 1);
    Assertions.assertThat(setup.stakedTokens()).isEqualTo(5);
    Assertions.assertThat(setup.mpcTokens()).isEqualTo(7);
  }

  @Test
  public void pendingUnstakesShouldClearAllRelevantEntries() {
    BalanceMutable setup = empty.asMutable();
    setup.addMpcTokenBalance(10);
    setup.stakeTokens(10);
    setup.unstakeTokens(4, 0);
    setup.unstakeTokens(1, 1);
    setup.addPendingRetractedDelegatedStakes(1, 0);
    setup.addPendingRetractedDelegatedStakes(1, 1);
    Assertions.assertThat(setup.pendingUnstakes().size()).isEqualTo(2);
    Assertions.assertThat(setup.pendingRetractedDelegatedStakes().size()).isEqualTo(2);

    setup.handlePendingUnstakes(UNSTAKE_DURATION_MILLIS + 1);
    Assertions.assertThat(setup.pendingUnstakes().size()).isEqualTo(0);
    Assertions.assertThat(setup.pendingRetractedDelegatedStakes().size()).isEqualTo(0);
  }

  @Test
  public void getDefaultStakedToContractAmount() {
    long defaultValue = empty.getAmountAssociationToContract(firstAddress);
    Assertions.assertThat(defaultValue).isEqualTo(0);
  }

  @Test
  public void addMpcTokensErrors() {
    Assertions.assertThatThrownBy(() -> empty.asMutable().addMpcTokenBalance(0))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot add 0 tokens");
  }

  @Test
  public void createFromStateAccessor() {
    BalanceMutable balanceMutable = empty.asMutable();
    balanceMutable.addMpcTokenBalance(200);
    Hash pendingTransactionHash = Hash.create(h -> h.writeString("transaction"));
    TransferInformation transferInformation = TransferInformation.createForDeposit(100L, -1);
    balanceMutable.addPendingTransfer(pendingTransactionHash, transferInformation);

    Hash pendingStakeHash = Hash.create(h -> h.writeString("stake"));
    StakeDelegation stakeDelegation =
        StakeDelegation.createReceiveDelegation(secondAddress, 100, null, null);
    balanceMutable.commitReceiveDelegatedStakes(secondAddress, 10, null);
    balanceMutable.acceptPendingDelegatedStakes(secondAddress, 5);

    Hash pendingStakeHashWithTimestamp = Hash.create(h -> h.writeString("stakeTimestamp"));
    StakeDelegation stakeWithTimestamp =
        StakeDelegation.createReceiveDelegation(secondAddress, 100, 20L, 100L);
    balanceMutable.commitReceiveDelegatedStakes(secondAddress, 10, 100L);
    balanceMutable.acceptPendingDelegatedStakes(secondAddress, 5);

    balanceMutable.addPendingStakeDelegation(pendingStakeHashWithTimestamp, stakeWithTimestamp);
    balanceMutable.addPendingStakeDelegation(pendingStakeHash, stakeDelegation);
    balanceMutable.createVestingAccount(200, InitialVestingAccount.TGE, 202, 203);
    balanceMutable.incrementFreeTransactionsUsed(1L);
    balanceMutable.addPendingRetractedDelegatedStakes(100, 10);
    balanceMutable.commitDelegateStakes(secondAddress, 100);
    balanceMutable.stakeTokens(300);
    balanceMutable.handlePendingUnstakes(400L);
    balanceMutable.addCoins(1, Unsigned256.create(69));
    balanceMutable.addPendingIceStake(
        HASH, new IceStake(firstAddress, 10L, IceStake.IceStakeType.ASSOCIATE));
    balanceMutable.appointCustodian(firstAddress);
    balanceMutable.acceptCustodian();
    balanceMutable.appointCustodian(secondAddress);
    balanceMutable.setStakingGoal(50);
    balanceMutable.associateWithExpiration(Contracts.ONE, 1, 100L);

    Balance balance = balanceMutable.asImmutable();
    Balance balanceUpdated = Balance.createFromStateAccessor(StateAccessor.create(balance));
    StateSerializableEquality.assertStatesEqual(balanceUpdated, balance);
  }

  @Test
  public void migrateEmptyAccount() {
    Balance balanceUpdated = Balance.createFromStateAccessor(StateAccessor.create(empty));
    StateSerializableEquality.assertStatesEqual(balanceUpdated, empty);
  }

  @Test
  @DisplayName(
      "Migrating old account state, creates new account state with staking goal 0, and null"
          + " expiration timestamp and delegation timestamp.")
  public void migrateOldBalanceState() {
    AvlTree<BlockchainAddress, OldStakesFromOthers> stakesFromOthers = AvlTree.create();
    OldStakesFromOthers oldStakeFromOther = new OldStakesFromOthers(10, 10);
    stakesFromOthers = stakesFromOthers.set(firstAddress, oldStakeFromOther);

    Hash stakeDelegationHash = Hash.create(h -> h.writeString("stake"));
    AvlTree<Hash, OldStakeDelegation> oldStakeDelegations = AvlTree.create();
    OldStakeDelegation stakeDelegation =
        new OldStakeDelegation(firstAddress, 10, StakeDelegation.DelegationType.DELEGATE_STAKES);
    oldStakeDelegations = oldStakeDelegations.set(stakeDelegationHash, stakeDelegation);

    AvlTree<BlockchainAddress, Long> oldStakedToContract = AvlTree.create();
    oldStakedToContract = oldStakedToContract.set(Contracts.ONE, 100L).set(Contracts.TWO, 200L);

    Balance newStateFromAccessor =
        Balance.createFromStateAccessor(
            StateAccessor.create(
                new OldBalance(
                    FixedList.create(),
                    0,
                    0,
                    oldStakedToContract,
                    AvlTree.create(),
                    FreeTransactions.create(),
                    FixedList.create(),
                    AvlTree.create(),
                    oldStakeDelegations,
                    stakesFromOthers,
                    AvlTree.create(),
                    AvlTree.create(),
                    true,
                    AvlTree.create(),
                    0,
                    new Custodian(null, null))));
    Assertions.assertThat(newStateFromAccessor.asMutable().stakingGoal()).isEqualTo(0);
    Assertions.assertThat(
            newStateFromAccessor
                .asMutable()
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .expirationTimestamp)
        .isNull();
    Assertions.assertThat(
            newStateFromAccessor
                .asMutable()
                .storedPendingStakeDelegations()
                .getValue(stakeDelegationHash)
                .delegationTimestamp)
        .isNull();
    Assertions.assertThat(
            newStateFromAccessor
                .asMutable()
                .storedPendingStakeDelegations()
                .getValue(stakeDelegationHash)
                .expirationTimestamp)
        .isNull();
    Assertions.assertThat(
            newStateFromAccessor
                .asMutable()
                .stakedToContract()
                .getValue(Contracts.ONE)
                .expirationTimestamp)
        .isNull();
    Assertions.assertThat(
            newStateFromAccessor
                .asMutable()
                .stakedToContract()
                .getValue(Contracts.TWO)
                .expirationTimestamp)
        .isNull();
  }

  private record OldBalance(
      FixedList<AccountCoin> accountCoins,
      long mpcTokens,
      long stakedTokens,
      AvlTree<BlockchainAddress, Long> stakedToContract,
      AvlTree<Long, Long> pendingUnstakes,
      FreeTransactions spentFreeTransactions,
      FixedList<InitialVestingAccount> vestingAccounts,
      AvlTree<Hash, TransferInformation> storedPendingTransfers,
      AvlTree<Hash, OldStakeDelegation> storedPendingStakeDelegations,
      AvlTree<BlockchainAddress, OldStakesFromOthers> delegatedStakesFromOthers,
      AvlTree<BlockchainAddress, Long> delegatedStakesToOthers,
      AvlTree<Long, Long> pendingRetractedDelegatedStakes,
      boolean stakeable,
      AvlTree<Hash, IceStake> storedPendingIceStakes,
      long tokensInCustody,
      Custodian custodian)
      implements StateSerializable {}

  @Immutable
  private record OldStakesFromOthers(long acceptedDelegatedStakes, long pendingDelegatedStakes)
      implements StateSerializable {}

  @Immutable
  private record OldStakeDelegation(
      BlockchainAddress counterPart, long amount, StakeDelegation.DelegationType delegationType)
      implements StateSerializable {}
}
