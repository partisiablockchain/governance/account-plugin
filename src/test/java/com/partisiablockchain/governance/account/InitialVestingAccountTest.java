package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.time.Instant;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class InitialVestingAccountTest {

  @Test
  public void constructor() {
    InitialVestingAccount initialVestingAccount = new InitialVestingAccount();
    Assertions.assertThat(initialVestingAccount.tokens).isEqualTo(0);
    Assertions.assertThat(initialVestingAccount.releasedTokens).isEqualTo(0);
  }

  @Test
  void pauseStartTime() {
    assertUnixMillisDate(InitialVestingAccount.PAUSE_RELEASE_START_TIME, "2023-05-20T00:00:00Z");
  }

  @Test
  void pauseEndTime() {
    assertUnixMillisDate(InitialVestingAccount.PAUSE_RELEASE_END_TIME, "2024-03-20T00:00:00Z");
  }

  private static void assertUnixMillisDate(long unixMillis, String dateString) {
    Instant epochInstant = Instant.ofEpochMilli(unixMillis);
    Instant dateInstant = Instant.parse(dateString);
    Assertions.assertThat(epochInstant).isEqualTo(dateInstant);
  }

  @Test
  void subtractPause() {
    Assertions.assertThat(
            InitialVestingAccount.subtractPause(InitialVestingAccount.PAUSE_RELEASE_START_TIME - 1))
        .isEqualTo(InitialVestingAccount.PAUSE_RELEASE_START_TIME - 1);
    Assertions.assertThat(
            InitialVestingAccount.subtractPause(InitialVestingAccount.PAUSE_RELEASE_START_TIME))
        .isEqualTo(InitialVestingAccount.PAUSE_RELEASE_START_TIME);
    Assertions.assertThat(
            InitialVestingAccount.subtractPause(InitialVestingAccount.PAUSE_RELEASE_START_TIME - 1))
        .isEqualTo(InitialVestingAccount.PAUSE_RELEASE_START_TIME - 1);
    Assertions.assertThat(
            InitialVestingAccount.subtractPause(InitialVestingAccount.PAUSE_RELEASE_END_TIME - 1))
        .isEqualTo(InitialVestingAccount.PAUSE_RELEASE_START_TIME);
    Assertions.assertThat(
            InitialVestingAccount.subtractPause(InitialVestingAccount.PAUSE_RELEASE_END_TIME))
        .isEqualTo(InitialVestingAccount.PAUSE_RELEASE_START_TIME);
    Assertions.assertThat(
            InitialVestingAccount.subtractPause(InitialVestingAccount.PAUSE_RELEASE_END_TIME + 1))
        .isEqualTo(InitialVestingAccount.PAUSE_RELEASE_START_TIME + 1);
  }

  @Test
  void tgeUntouched() {
    Assertions.assertThat(InitialVestingAccount.subtractPause(InitialVestingAccount.TGE))
        .isEqualTo(InitialVestingAccount.TGE);
    Assertions.assertThat(
            InitialVestingAccount.subtractPause(InitialVestingAccount.PAUSE_RELEASE_START_TIME))
        .isEqualTo(InitialVestingAccount.PAUSE_RELEASE_START_TIME);
  }

  @Test
  void afterPauseSane() {
    Assertions.assertThat(
            InitialVestingAccount.subtractPause(
                InitialVestingAccount.PAUSE_RELEASE_END_TIME + 1000))
        .isEqualTo(InitialVestingAccount.PAUSE_RELEASE_START_TIME + 1000);
  }
}
