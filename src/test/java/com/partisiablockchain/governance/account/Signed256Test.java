package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class Signed256Test {

  private static final Signed256 POSITIVE_TEN = Signed256.create(Unsigned256.TEN);
  private static final Signed256 NEGATIVE_TEN = Signed256.create().subtract(Unsigned256.TEN);
  private static final Signed256 POSITIVE_ONE = Signed256.create(Unsigned256.ONE);
  private static final Signed256 NEGATIVE_ONE = Signed256.create().subtract(Unsigned256.ONE);

  @Test
  void createTest() {
    Assertions.assertThat(Signed256.create().getAbsoluteValue()).isEqualTo(Unsigned256.ZERO);
    Assertions.assertThat(Signed256.create(Unsigned256.ONE).getAbsoluteValue())
        .isEqualTo(Unsigned256.ONE);
  }

  @Test
  void createFromStateAccessorTest() {
    StateAccessor state = StateAccessor.create(POSITIVE_TEN);
    Assertions.assertThat(Signed256.createFromStateAccessor(state).getAbsoluteValue())
        .isEqualTo(Unsigned256.TEN);
  }

  @Test
  void isPositiveTest() {
    Assertions.assertThat(POSITIVE_ONE.isPositive()).isTrue();
    Assertions.assertThat(NEGATIVE_ONE.isPositive()).isFalse();
  }

  @Test
  void positiveValueTest() {
    Assertions.assertThat(POSITIVE_ONE.getPositiveValue()).isEqualTo(Unsigned256.ONE);
    Assertions.assertThat(NEGATIVE_ONE.getPositiveValue()).isEqualTo(Unsigned256.ZERO);
  }

  @Test
  void absoluteValueTest() {
    Assertions.assertThat(POSITIVE_ONE.getAbsoluteValue()).isEqualTo(Unsigned256.ONE);
    Assertions.assertThat(NEGATIVE_ONE.getAbsoluteValue()).isEqualTo(Unsigned256.ONE);
  }

  @Test
  void addToPositiveNumberTest() {
    Signed256 tenPlusOne = POSITIVE_TEN.add(Unsigned256.ONE);
    Assertions.assertThat(tenPlusOne.isPositive()).isTrue();
    Assertions.assertThat(tenPlusOne.getPositiveValue()).isEqualTo(Unsigned256.create(11L));
  }

  @Test
  void addToNegativeNumberTest() {
    Signed256 minusOnePlusTen = NEGATIVE_ONE.add(Unsigned256.TEN);
    Assertions.assertThat(minusOnePlusTen.isPositive()).isTrue();
    Assertions.assertThat(minusOnePlusTen.getPositiveValue()).isEqualTo(Unsigned256.create(9L));
  }

  @Test
  void addToNegativeNumberWithSameValueTest() {
    Signed256 minusOnePlusOne = NEGATIVE_ONE.add(Unsigned256.ONE);
    Assertions.assertThat(minusOnePlusOne.isPositive()).isTrue();
    Assertions.assertThat(minusOnePlusOne.getPositiveValue()).isEqualTo(Unsigned256.ZERO);
  }

  @Test
  void subtractFromLargerPositiveNumberTest() {
    Signed256 tenMinusOne = POSITIVE_TEN.subtract(Unsigned256.ONE);
    Assertions.assertThat(tenMinusOne.isPositive()).isTrue();
    Assertions.assertThat(tenMinusOne.getAbsoluteValue()).isEqualTo(Unsigned256.create(9L));
  }

  @Test
  void subtractFromSmallerPositiveNumberTest() {
    Signed256 oneMinusTen = POSITIVE_ONE.subtract(Unsigned256.TEN);
    Assertions.assertThat(oneMinusTen.isPositive()).isFalse();
    Assertions.assertThat(oneMinusTen.getAbsoluteValue()).isEqualTo(Unsigned256.create(9L));
  }

  @Test
  void subtractFromNegativeNumberTest() {
    Signed256 minusTenMinusTen = NEGATIVE_TEN.subtract(Unsigned256.TEN);
    Assertions.assertThat(minusTenMinusTen.isPositive()).isFalse();
    Assertions.assertThat(minusTenMinusTen.getAbsoluteValue()).isEqualTo(Unsigned256.create(20L));
  }

  @Test
  void equalsTest() {
    EqualsVerifier.simple().forClass(Signed256.class).verify();
  }
}
