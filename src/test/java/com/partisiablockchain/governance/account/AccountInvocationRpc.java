package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.util.function.Consumer;

/** Helper for generating RPC for {@link AccountInvocation} invocations. */
public final class AccountInvocationRpc {

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ADD_COIN_BALANCE}.
   *
   * @param symbol coin symbol
   * @param amount amount to add
   * @return generated RPC
   */
  public static byte[] addCoinBalance(String symbol, Unsigned256 amount) {
    return serialize(
        AccountInvocation.ADD_COIN_BALANCE,
        s -> {
          s.writeString(symbol);
          amount.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#DEDUCT_COIN_BALANCE}.
   *
   * @param symbol coin symbol
   * @param amount amount to deduct
   * @return generated RPC
   */
  public static byte[] deductCoinBalance(String symbol, Unsigned256 amount) {
    return serialize(
        AccountInvocation.DEDUCT_COIN_BALANCE,
        s -> {
          s.writeString(symbol);
          amount.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ADD_MPC_TOKEN_BALANCE}.
   *
   * @param amount amount to add
   * @return generated RPC
   */
  public static byte[] addMpcTokenBalance(long amount) {
    return serialize(AccountInvocation.ADD_MPC_TOKEN_BALANCE, s -> s.writeLong(amount));
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#DEDUCT_MPC_TOKEN_BALANCE}.
   *
   * @param amount amount to deduct
   * @return generated RPC
   */
  public static byte[] deductMpcTokenBalance(long amount) {
    return serialize(AccountInvocation.DEDUCT_MPC_TOKEN_BALANCE, s -> s.writeLong(amount));
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#GET_STAKED_TOKEN_BALANCE}.
   *
   * @return generated RPC
   */
  public static byte[] getStakedTokenBalance() {
    return serialize(AccountInvocation.GET_STAKED_TOKEN_BALANCE);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#SET_STAKING_GOAL}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param stakingGoal goal amount of tokens to have staked for jobs.
   * @return generated RPC
   */
  public static byte[] setStakingGoal(BlockchainAddress custodianAddress, long stakingGoal) {
    return serialize(
        AccountInvocation.SET_STAKING_GOAL,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          s.writeLong(stakingGoal);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#STAKE_TOKENS}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param amount amount to stake
   * @return generated RPC
   */
  public static byte[] stakeTokens(BlockchainAddress custodianAddress, long amount) {
    return serialize(
        AccountInvocation.STAKE_TOKENS,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          s.writeLong(amount);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#STAKE_TOKENS} without custodian.
   *
   * @param amount amount to stake
   * @return generated RPC
   */
  public static byte[] stakeTokens(long amount) {
    return stakeTokens(null, amount);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#UNSTAKE_TOKENS}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param amount amount to unstake
   * @return generated RPC
   */
  public static byte[] unstakeTokens(BlockchainAddress custodianAddress, long amount) {
    return serialize(
        AccountInvocation.UNSTAKE_TOKENS,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          s.writeLong(amount);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#UNSTAKE_TOKENS} without custodian.
   *
   * @param amount amount to unstake
   * @return generated RPC
   */
  public static byte[] unstakeTokens(long amount) {
    return unstakeTokens(null, amount);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ASSOCIATE}.
   *
   * @param amount amount to associate
   * @param contractAddress address of contract to associate to
   * @return generated RPC
   */
  public static byte[] associate(long amount, BlockchainAddress contractAddress) {
    return serialize(
        AccountInvocation.ASSOCIATE,
        s -> {
          s.writeLong(amount);
          contractAddress.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ASSOCIATE_WITH_EXPIRATION}.
   *
   * @param amount amount to associate
   * @param contractAddress address of contract to associate to
   * @param expirationTimestamp the timestamp the associate can no longer be used as collateral
   * @return generated RPC
   */
  public static byte[] associateWithExpiration(
      long amount, BlockchainAddress contractAddress, long expirationTimestamp) {
    return serialize(
        AccountInvocation.ASSOCIATE_WITH_EXPIRATION,
        s -> {
          s.writeLong(amount);
          contractAddress.write(s);
          s.writeLong(expirationTimestamp);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#DISASSOCIATE}.
   *
   * @param amount amount to disassociate
   * @param contractAddress address of contract to disassociate from
   * @return generated RPC
   */
  public static byte[] disassociate(long amount, BlockchainAddress contractAddress) {
    return serialize(
        AccountInvocation.DISASSOCIATE,
        s -> {
          s.writeLong(amount);
          contractAddress.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT}.
   *
   * @param contractAddress address of contract to disassociate from
   * @return generated RPC
   */
  public static byte[] disassociateAllTokensForContract(BlockchainAddress contractAddress) {
    return serialize(
        AccountInvocation.DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT,
        s -> {
          contractAddress.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#BURN_MPC_TOKENS}.
   *
   * @param amount amount to burn
   * @param contractAddress address of contract to burn from
   * @return generated RPC
   */
  public static byte[] burnMpcTokens(long amount, BlockchainAddress contractAddress) {
    return serialize(
        AccountInvocation.BURN_MPC_TOKENS,
        s -> {
          s.writeLong(amount);
          contractAddress.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_WITHDRAW}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param transactionId 2PC transaction identifier
   * @param amount amount to withdraw
   * @return generated RPC
   */
  public static byte[] initiateWithdraw(
      BlockchainAddress custodianAddress, Hash transactionId, long amount) {
    return serialize(
        AccountInvocation.INITIATE_WITHDRAW,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          transactionId.write(s);
          s.writeLong(amount);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_WITHDRAW} without custodian.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to withdraw
   * @return generated RPC
   */
  public static byte[] initiateWithdraw(Hash transactionId, long amount) {
    return initiateWithdraw(null, transactionId, amount);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_DEPOSIT}.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to deposit
   * @return generated RPC
   */
  public static byte[] initiateDeposit(Hash transactionId, long amount) {
    return serialize(
        AccountInvocation.INITIATE_DEPOSIT,
        s -> {
          transactionId.write(s);
          s.writeLong(amount);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_DELEGATE_STAKES}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param transactionId 2PC transaction identifier
   * @param amount amount to delegate
   * @param counterPart address of counterpart
   * @return generated RPC
   */
  public static byte[] initiateDelegateStakes(
      BlockchainAddress custodianAddress,
      Hash transactionId,
      long amount,
      BlockchainAddress counterPart) {
    return serialize(
        AccountInvocation.INITIATE_DELEGATE_STAKES,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          transactionId.write(s);
          s.writeLong(amount);
          counterPart.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_DELEGATE_STAKES} without
   * custodian.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to delegate
   * @param counterPart address of counterpart
   * @return generated RPC
   */
  public static byte[] initiateDelegateStakes(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return initiateDelegateStakes(null, transactionId, amount, counterPart);
  }

  /**
   * Generate RPC for the invocation {@link
   * AccountInvocation#SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param delegator address of counterpart
   * @param expirationTimestamp expiration timestamp for the delegated stakes
   * @return generated RPC
   */
  public static byte[] setExpirationTimestampForDelegatedStakes(
      BlockchainAddress custodianAddress, BlockchainAddress delegator, Long expirationTimestamp) {
    return serialize(
        AccountInvocation.SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          delegator.write(s);
          s.writeOptional((value, rpc) -> rpc.writeLong(value), expirationTimestamp);
        });
  }

  /**
   * Generate RPC for the invocation {@link
   * AccountInvocation#SET_EXPIRATION_TIMESTAMP_FOR_ASSOCIATION}.
   *
   * @param contractAddress address of the contract associated to
   * @param expirationTimestamp new expiration timestamp for association
   * @return generated RPC
   */
  public static byte[] setExpirationTimestampForAssociation(
      BlockchainAddress contractAddress, Long expirationTimestamp) {
    return serialize(
        AccountInvocation.SET_EXPIRATION_TIMESTAMP_FOR_ASSOCIATION,
        s -> {
          contractAddress.write(s);
          s.writeOptional((value, rpc) -> rpc.writeLong(value), expirationTimestamp);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_RECEIVE_DELEGATED_STAKES}.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to receive
   * @param counterPart address of counterpart
   * @return generated RPC
   */
  public static byte[] initiateReceiveDelegatedStakes(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return serialize(
        AccountInvocation.INITIATE_RECEIVE_DELEGATED_STAKES,
        s -> {
          transactionId.write(s);
          s.writeLong(amount);
          counterPart.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_RECEIVE_DELEGATED_STAKES}.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to receive
   * @param counterPart address of counterpart
   * @param expirationTimestamp expiration timestamp for the delegation
   * @return generated RPC
   */
  public static byte[] initiateReceiveDelegatedStakesWithExpiration(
      Hash transactionId, long amount, BlockchainAddress counterPart, long expirationTimestamp) {
    return serialize(
        AccountInvocation.INITIATE_RECEIVE_DELEGATED_STAKES_WITH_EXPIRATION,
        s -> {
          transactionId.write(s);
          s.writeLong(amount);
          counterPart.write(s);
          s.writeLong(expirationTimestamp);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_RETRACT_DELEGATED_STAKES}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param transactionId 2PC transaction identifier
   * @param amount amount to retract
   * @param counterPart address of counterpart
   * @return generated RPC
   */
  public static byte[] initiateRetractDelegatedStakes(
      BlockchainAddress custodianAddress,
      Hash transactionId,
      long amount,
      BlockchainAddress counterPart) {
    return serialize(
        AccountInvocation.INITIATE_RETRACT_DELEGATED_STAKES,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          transactionId.write(s);
          s.writeLong(amount);
          counterPart.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_RETRACT_DELEGATED_STAKES}
   * without custodian.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to retract
   * @param counterPart address of counterpart
   * @return generated RPC
   */
  public static byte[] initiateRetractDelegatedStakes(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return initiateRetractDelegatedStakes(null, transactionId, amount, counterPart);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_RETURN_DELEGATED_STAKES}.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to return
   * @param counterPart address of counterpart
   * @return generated RPC
   */
  public static byte[] initiateReturnDelegatedStakes(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return serialize(
        AccountInvocation.INITIATE_RETURN_DELEGATED_STAKES,
        s -> {
          transactionId.write(s);
          s.writeLong(amount);
          counterPart.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#COMMIT_TRANSFER}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] commitTransfer(Hash transactionId) {
    return serialize(AccountInvocation.COMMIT_TRANSFER, transactionId::write);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ABORT_TRANSFER}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] abortTransfer(Hash transactionId) {
    return serialize(AccountInvocation.ABORT_TRANSFER, transactionId::write);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#COMMIT_DELEGATE_STAKES}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] commitDelegateStakes(Hash transactionId) {
    return serialize(AccountInvocation.COMMIT_DELEGATE_STAKES, transactionId::write);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ABORT_DELEGATE_STAKES}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] abortDelegateStakes(Hash transactionId) {
    return serialize(AccountInvocation.ABORT_DELEGATE_STAKES, transactionId::write);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ACCEPT_DELEGATED_STAKES}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param amount amount to accept
   * @param sender address of the account to accept delegated stakes from
   * @return generated RPC
   */
  public static byte[] acceptDelegatedStakes(
      BlockchainAddress custodianAddress, long amount, BlockchainAddress sender) {
    return serialize(
        AccountInvocation.ACCEPT_DELEGATED_STAKES,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          sender.write(s);
          s.writeLong(amount);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ACCEPT_DELEGATED_STAKES} without
   * custodian.
   *
   * @param amount amount to accept
   * @param sender address of the account to accept delegated stakes from
   * @return generated RPC
   */
  public static byte[] acceptDelegatedStakes(long amount, BlockchainAddress sender) {
    return acceptDelegatedStakes(null, amount, sender);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#REDUCE_DELEGATED_STAKES}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param sender address of the account to reduce delegated stakes from
   * @param amount amount to reduce
   * @return generated RPC
   */
  public static byte[] reduceDelegatedStakes(
      BlockchainAddress custodianAddress, BlockchainAddress sender, long amount) {
    return serialize(
        AccountInvocation.REDUCE_DELEGATED_STAKES,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          sender.write(s);
          s.writeLong(amount);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#REDUCE_DELEGATED_STAKES} without
   * custodian.
   *
   * @param sender address of the account to reduce delegated stakes from
   * @param amount amount to reduce
   * @return generated RPC
   */
  public static byte[] reduceDelegatedStakes(BlockchainAddress sender, long amount) {
    return reduceDelegatedStakes(null, sender, amount);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#CREATE_VESTING_ACCOUNT}.
   *
   * @param amountOfVestedTokens amount of tokens
   * @param releaseDuration duration of vesting schedule
   * @param releaseInterval interval of vesting schedule
   * @param tokenGenerationEvent TGE of vesting schedule
   * @return generated RPC
   */
  public static byte[] createVestingAccount(
      long amountOfVestedTokens,
      long releaseDuration,
      long releaseInterval,
      long tokenGenerationEvent) {
    return serialize(
        AccountInvocation.CREATE_VESTING_ACCOUNT,
        s -> {
          s.writeLong(amountOfVestedTokens);
          s.writeLong(releaseDuration);
          s.writeLong(releaseInterval);
          s.writeLong(tokenGenerationEvent);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#CHECK_VESTED_TOKENS}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @return generated RPC
   */
  public static byte[] checkVestedTokens(BlockchainAddress custodianAddress) {
    return serialize(
        AccountInvocation.CHECK_VESTED_TOKENS,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#CHECK_VESTED_TOKENS} without
   * custodian.
   *
   * @return generated RPC
   */
  public static byte[] checkVestedTokens() {
    return checkVestedTokens(null);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#CHECK_PENDING_UNSTAKES}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @return generated RPC
   */
  public static byte[] checkPendingUnstakes(BlockchainAddress custodianAddress) {
    return serialize(
        AccountInvocation.CHECK_PENDING_UNSTAKES,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#CHECK_PENDING_UNSTAKES} without
   * custodian.
   *
   * @return generated RPC
   */
  public static byte[] checkPendingUnstakes() {
    return checkPendingUnstakes(null);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#MAKE_ACCOUNT_NON_STAKEABLE}.
   *
   * @return generated RPC
   */
  public static byte[] makeAccountNonStakeable() {
    return serialize(AccountInvocation.MAKE_ACCOUNT_NON_STAKEABLE);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_BYOC_TRANSFER_SENDER}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param transactionId 2PC transaction identifier
   * @param amount amount to send
   * @param symbol coin symbol
   * @return generated RPC
   */
  public static byte[] initiateByocTransferSender(
      BlockchainAddress custodianAddress, Hash transactionId, Unsigned256 amount, String symbol) {
    return serialize(
        AccountInvocation.INITIATE_BYOC_TRANSFER_SENDER,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          transactionId.write(s);
          amount.write(s);
          s.writeString(symbol);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_BYOC_TRANSFER_SENDER} without
   * custodian.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to send
   * @param symbol coin symbol
   * @return generated RPC
   */
  public static byte[] initiateByocTransferSender(
      Hash transactionId, Unsigned256 amount, String symbol) {
    return initiateByocTransferSender(null, transactionId, amount, symbol);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_BYOC_TRANSFER_RECIPIENT}.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to receive
   * @param symbol coin symbol
   * @return generated RPC
   */
  public static byte[] initiateByocTransferRecipient(
      Hash transactionId, Unsigned256 amount, String symbol) {
    return serialize(
        AccountInvocation.INITIATE_BYOC_TRANSFER_RECIPIENT,
        s -> {
          transactionId.write(s);
          amount.write(s);
          s.writeString(symbol);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_ICE_ASSOCIATE}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param amount amount to ice associate
   * @param target address to associate to
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] initiateIceAssociate(
      BlockchainAddress custodianAddress,
      long amount,
      BlockchainAddress target,
      Hash transactionId) {
    return serialize(
        AccountInvocation.INITIATE_ICE_ASSOCIATE,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          transactionId.write(s);
          s.writeLong(amount);
          target.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_ICE_ASSOCIATE} without
   * custodian.
   *
   * @param amount amount to ice associate
   * @param target address to associate to
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] initiateIceAssociate(
      long amount, BlockchainAddress target, Hash transactionId) {
    return initiateIceAssociate(null, amount, target, transactionId);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_ICE_DISASSOCIATE}.
   *
   * @param custodianAddress (optional) address of the sender if sent as a custodian or {@code null}
   *     if sent by the account owner.
   * @param amount amount to disassociate
   * @param target address to associate from
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] initiateIceDisassociate(
      BlockchainAddress custodianAddress,
      long amount,
      BlockchainAddress target,
      Hash transactionId) {
    return serialize(
        AccountInvocation.INITIATE_ICE_DISASSOCIATE,
        s -> {
          s.writeOptional(BlockchainAddress::write, custodianAddress);
          transactionId.write(s);
          s.writeLong(amount);
          target.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#INITIATE_ICE_DISASSOCIATE} without
   * custodian.
   *
   * @param amount amount to disassociate
   * @param target address to associate from
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] initiateIceDisassociate(
      long amount, BlockchainAddress target, Hash transactionId) {
    return initiateIceDisassociate(null, amount, target, transactionId);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#COMMIT_ICE}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] commitIce(Hash transactionId) {
    return serialize(AccountInvocation.COMMIT_ICE, transactionId::write);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ABORT_ICE}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] abortIce(Hash transactionId) {
    return serialize(AccountInvocation.ABORT_ICE, transactionId::write);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#APPOINT_CUSTODIAN}.
   *
   * @param senderAddress (optional) address of the sender if sent as a custodian or {@code null} if
   *     sent by the account owner.
   * @param custodianAddress address of the custodian to appoint
   * @return generated RPC
   */
  public static byte[] appointCustodian(
      BlockchainAddress senderAddress, BlockchainAddress custodianAddress) {
    return serialize(
        AccountInvocation.APPOINT_CUSTODIAN,
        s -> {
          s.writeOptional(BlockchainAddress::write, senderAddress);
          custodianAddress.write(s);
        });
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#APPOINT_CUSTODIAN} without custodian.
   *
   * @param custodianAddress address of the custodian to appoint
   * @return generated RPC
   */
  public static byte[] appointCustodian(BlockchainAddress custodianAddress) {
    return appointCustodian(null, custodianAddress);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#CANCEL_APPOINTED_CUSTODIAN}.
   *
   * @param senderAddress (optional) address of the sender if sent as a custodian or {@code null} if
   *     sent by the account owner.
   * @return generated RPC
   */
  public static byte[] cancelAppointedCustodian(BlockchainAddress senderAddress) {
    return serialize(
        AccountInvocation.CANCEL_APPOINTED_CUSTODIAN,
        s -> s.writeOptional(BlockchainAddress::write, senderAddress));
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#RESET_CUSTODIAN}.
   *
   * @param senderAddress address of the sender
   * @return generated RPC
   */
  public static byte[] resetCustodian(BlockchainAddress senderAddress) {
    return serialize(AccountInvocation.RESET_CUSTODIAN, senderAddress::write);
  }

  /**
   * Generate RPC for the invocation {@link AccountInvocation#ACCEPT_CUSTODIANSHIP}.
   *
   * @param senderAddress address of the sender
   * @return generated RPC
   */
  public static byte[] acceptCustodianship(BlockchainAddress senderAddress) {
    return serialize(AccountInvocation.ACCEPT_CUSTODIANSHIP, senderAddress::write);
  }

  private static byte[] serialize(AccountInvocation invocation) {
    return serialize(invocation, FunctionUtility.noOpConsumer());
  }

  private static byte[] serialize(
      AccountInvocation invocation, Consumer<SafeDataOutputStream> rpc) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(invocation.getInvocationByte());
          rpc.accept(stream);
        });
  }
}
