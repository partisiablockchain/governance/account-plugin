package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataOutputStream;

/** Helper for generating RPC for {@link ContractInvocation} invocations. */
public final class ContractInvocationRpc {

  /**
   * Generate RPC for the invocation {@link ContractInvocation#INITIATE_BYOC_TRANSFER_RECIPIENT}.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to receive
   * @param symbol coin symbol
   * @return generated RPC
   */
  public static byte[] initiateByocTransferRecipient(
      Hash transactionId, Unsigned256 amount, String symbol) {
    return AccountInvocationRpc.initiateByocTransferRecipient(transactionId, amount, symbol);
  }

  /**
   * Generate RPC for the invocation {@link ContractInvocation#INITIATE_BYOC_TRANSFER_SENDER}.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to receive
   * @param symbol coin symbol
   * @return generated RPC
   */
  public static byte[] initiateByocTransferSender(
      Hash transactionId, Unsigned256 amount, String symbol) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ContractInvocation.INITIATE_BYOC_TRANSFER_SENDER.getInvocationByte());
          transactionId.write(stream);
          amount.write(stream);
          stream.writeString(symbol);
        });
  }

  /**
   * Generate RPC for the invocation {@link ContractInvocation#COMMIT_TRANSFER}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] commitTransfer(Hash transactionId) {
    return AccountInvocationRpc.commitTransfer(transactionId);
  }

  /**
   * Generate RPC for the invocation {@link ContractInvocation#ABORT_TRANSFER}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] abortTransfer(Hash transactionId) {
    return AccountInvocationRpc.abortTransfer(transactionId);
  }

  /**
   * Generate RPC for the invocation {@link ContractInvocation#INITIATE_ICE_ASSOCIATE}.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to associate
   * @param account address of account to associate from
   * @return generated RPC
   */
  public static byte[] initiateIceAssociate(
      Hash transactionId, long amount, BlockchainAddress account) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ContractInvocation.INITIATE_ICE_ASSOCIATE.getInvocationByte());
          transactionId.write(stream);
          stream.writeLong(amount);
          account.write(stream);
        });
  }

  /**
   * Generate RPC for the invocation {@link ContractInvocation#INITIATE_ICE_DISASSOCIATE}.
   *
   * @param transactionId 2PC transaction identifier
   * @param amount amount to disassociate
   * @param account address of account to disassociate from
   * @return generated RPC
   */
  public static byte[] initiateIceDisassociate(
      Hash transactionId, long amount, BlockchainAddress account) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ContractInvocation.INITIATE_ICE_DISASSOCIATE.getInvocationByte());
          transactionId.write(stream);
          stream.writeLong(amount);
          account.write(stream);
        });
  }

  /**
   * Generate RPC for the invocation {@link ContractInvocation#COMMIT_ICE}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] commitIce(Hash transactionId) {
    return AccountInvocationRpc.commitIce(transactionId);
  }

  /**
   * Generate RPC for the invocation {@link ContractInvocation#ABORT_ICE}.
   *
   * @param transactionId 2PC transaction identifier
   * @return generated RPC
   */
  public static byte[] abortIce(Hash transactionId) {
    return AccountInvocationRpc.abortIce(transactionId);
  }

  static byte[] initiateMpcTokenDeposit(Hash transactionId, Unsigned256 amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ContractInvocation.INITIATE_DEPOSIT.getInvocationByte());
          transactionId.write(stream);
          stream.writeLong(amount.longValueExact());
        });
  }

  static byte[] initiateMpcTokenWithdrawal(Hash transactionId, Unsigned256 amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ContractInvocation.INITIATE_WITHDRAW.getInvocationByte());
          // Custodians are not supported
          stream.writeOptional(BlockchainAddress::write, null);
          transactionId.write(stream);
          stream.writeLong(amount.longValueExact());
        });
  }

  static byte[] addMpcTokens(long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ContractInvocation.ADD_MPC_TOKEN_BALANCE.getInvocationByte());
          stream.writeLong(amount);
        });
  }

  static byte[] deductMpcTokens(long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ContractInvocation.DEDUCT_MPC_TOKEN_BALANCE.getInvocationByte());
          stream.writeLong(amount);
        });
  }

  static byte[] addByoc(String symbol, Unsigned256 amount) {
    return SafeDataOutputStream.serialize(
        s -> {
          s.writeByte(ContractInvocation.ADD_COIN_BALANCE.getInvocationByte());
          s.writeString(symbol);
          amount.write(s);
        });
  }

  static byte[] deductByoc(String symbol, Unsigned256 amount) {
    return SafeDataOutputStream.serialize(
        s -> {
          s.writeByte(ContractInvocation.DEDUCT_COIN_BALANCE.getInvocationByte());
          s.writeString(symbol);
          amount.write(s);
        });
  }
}
