package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainAccountPlugin;
import com.partisiablockchain.blockchain.BlockchainPlugin;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import java.time.Duration;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class TokensInCustodyTest {
  private static final String SYMBOL = "symbol";
  private static final Balance EMPTY = Balance.create();
  private static final Hash DUMMY_HASH = Hash.create(h -> h.writeInt(1234));
  private static final AccountStateGlobal DUMMY_GLOBAL =
      new AccountStateGlobal()
          .setGasPriceFor1000NetworkInternal(100)
          .setGasPriceFor1000StorageInternal(1000)
          .setGasPriceFor1000IceStakes(4000)
          .setCoinInternal(SYMBOL, new Fraction(1, 1));
  GasAndCoinAccountPlugin plugin = new GasAndCoinAccountPlugin();

  @Test
  void shouldIncludeDelegatedFromOthers() {
    long tokensInBalance = 100_0000L;
    BalanceMutable balance = EMPTY.asMutable();
    balance.addMpcTokenBalance(tokensInBalance);
    Assertions.assertThat(balance.tokensInCustody()).isEqualTo(tokensInBalance);
    balance.stakeTokens(tokensInBalance);
    Assertions.assertThat(balance.tokensInCustody()).isEqualTo(tokensInBalance);

    long receiveDelegateStakesAmount = 4000L;
    Hash transactionId = Hash.create(s -> s.writeByte(99));
    PluginContext pluginContext = () -> 0;
    final BlockchainAddress senderAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    StakeDelegation firstStakeDelegation =
        StakeDelegation.createReceiveDelegation(
            senderAddress, receiveDelegateStakesAmount, null, null);
    balance.addPendingStakeDelegation(transactionId, firstStakeDelegation);
    balance.commitDelegatedStakes(transactionId, pluginContext.blockProductionTime());

    long secondReceiveDelegateStakesAmount = 8000L;
    final BlockchainAddress secondSenderAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000003");
    StakeDelegation seconStakeDelegation =
        StakeDelegation.createReceiveDelegation(
            secondSenderAddress, secondReceiveDelegateStakesAmount, null, null);
    balance.addPendingStakeDelegation(transactionId, seconStakeDelegation);
    balance.commitDelegatedStakes(transactionId, pluginContext.blockProductionTime());
    Assertions.assertThat(balance.tokensInCustody())
        .isEqualTo(
            tokensInBalance + receiveDelegateStakesAmount + secondReceiveDelegateStakesAmount);
  }

  @Test
  void shouldIncludePendingUnstakes() {
    long tokensInBalance = 100_0000L;
    BalanceMutable balance = EMPTY.asMutable();
    balance.addMpcTokenBalance(tokensInBalance);
    Assertions.assertThat(balance.tokensInCustody()).isEqualTo(tokensInBalance);
    balance.stakeTokens(tokensInBalance);
    Assertions.assertThat(balance.tokensInCustody()).isEqualTo(tokensInBalance);
    int currentBlockTime = 1;
    balance.unstakeTokens(500_000L, currentBlockTime);
    balance.unstakeTokens(500_000L, currentBlockTime);
    Assertions.assertThat(balance.tokensInCustody()).isEqualTo(tokensInBalance);
  }

  @Test
  void shouldIncludeStakedTokens() {
    long tokensInBalance = 100_0000L;
    BalanceMutable balance = EMPTY.asMutable();
    balance.addMpcTokenBalance(tokensInBalance);
    Assertions.assertThat(balance.tokensInCustody()).isEqualTo(tokensInBalance);
    balance.stakeTokens(tokensInBalance);
    Assertions.assertThat(balance.mpcTokens()).isEqualTo(0);
    Assertions.assertThat(balance.stakedTokens()).isEqualTo(tokensInBalance);
    Assertions.assertThat(balance.stakedTokens()).isEqualTo(tokensInBalance);
    Assertions.assertThat(balance.tokensInCustody()).isEqualTo(tokensInBalance);
  }

  @Test
  void deductMpcTokenBalance_deductFromTokensInCustody() {
    Balance balance = Balance.create();
    BalanceMutable balanceMutable = balance.asMutable();
    long initialAmount = 100_000L;
    balanceMutable.addMpcTokenBalance(initialAmount);
    long deductAmount = 50_000L;
    balanceMutable.deductMpcTokenBalance(deductAmount);
    Assertions.assertThat(balanceMutable.tokensInCustody()).isEqualTo(initialAmount - deductAmount);
  }

  @Test
  void transferCommit_sumOfTokensInCustodyUnchanged() {
    BalanceMutable senderMutable = Balance.create().asMutable();
    int transferAmount = 1000;
    senderMutable.addMpcTokenBalance(transferAmount);
    Balance sender = senderMutable.asImmutable();
    Balance recipient = Balance.create();

    Assertions.assertThat(sender.tokensInCustody()).isEqualTo(transferAmount);
    Assertions.assertThat(recipient.tokensInCustody()).isEqualTo(0);
    Assertions.assertThat(sender.tokensInCustody() + recipient.tokensInCustody())
        .isEqualTo(transferAmount);

    senderMutable = sender.asMutable();
    BalanceMutable recipientMutable = recipient.asMutable();
    senderMutable.initiateWithdraw(
        DUMMY_HASH, TransferInformation.createForWithdraw(transferAmount, -1));
    recipientMutable.initiateDeposit(
        DUMMY_HASH, TransferInformation.createForDeposit(transferAmount, -1));

    sender = senderMutable.asImmutable();
    recipient = recipientMutable.asImmutable();

    Assertions.assertThat(sender.storedPendingTransfers().containsKey(DUMMY_HASH)).isTrue();
    Assertions.assertThat(recipient.storedPendingTransfers().containsKey(DUMMY_HASH)).isTrue();

    // 1a) Commit
    BalanceMutable senderCommitMutable = sender.asMutable();
    BalanceMutable recipientCommitMutable = recipient.asMutable();
    senderCommitMutable.commitTransfer(DUMMY_HASH);
    recipientCommitMutable.commitTransfer(DUMMY_HASH);

    sender = senderCommitMutable.asImmutable();
    recipient = recipientCommitMutable.asImmutable();
    Assertions.assertThat(sender.tokensInCustody()).isEqualTo(0);
    Assertions.assertThat(recipientCommitMutable.tokensInCustody()).isEqualTo(transferAmount);
    Assertions.assertThat(sender.tokensInCustody() + recipient.tokensInCustody())
        .isEqualTo(transferAmount);

    // 1b) Abort
    BalanceMutable senderAbortMutable = sender.asMutable();
    BalanceMutable recipientAbortMutable = recipient.asMutable();
    senderAbortMutable.abortTransfer(DUMMY_HASH);
    recipientAbortMutable.abortTransfer(DUMMY_HASH);

    sender = senderAbortMutable.asImmutable();
    recipient = recipientAbortMutable.asImmutable();
    Assertions.assertThat(sender.tokensInCustody()).isEqualTo(0);
    Assertions.assertThat(recipientAbortMutable.tokensInCustody()).isEqualTo(transferAmount);
    Assertions.assertThat(sender.tokensInCustody() + recipient.tokensInCustody())
        .isEqualTo(transferAmount);
  }

  @Test
  void delegateStakes_sumOfTwoAccountsShouldNotChangeWhenDelegatingAndRetractingBetweenThem() {
    BlockchainAddress senderAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000008");
    BlockchainAddress recipientAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000009");
    long initialSenderBalance = 75_000L;
    long stakeDelegationAmount = 50_000L;

    Balance senderBalance = Balance.create();
    BalanceMutable senderBalanceMutable = senderBalance.asMutable();
    senderBalanceMutable.addMpcTokenBalance(initialSenderBalance);
    senderBalance = senderBalanceMutable.asImmutable();
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> beforeDelegateStakes =
        new BlockchainAccountPlugin.AccountState<>(AccountStateLocal.initial(), senderBalance);
    Balance recipientBalance = Balance.create();
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> beforeReceiveDelegateStakes =
        new BlockchainAccountPlugin.AccountState<>(AccountStateLocal.initial(), recipientBalance);

    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> afterDelegateStakes =
        plugin
            .invokeAccount(
                () -> 0,
                DUMMY_GLOBAL,
                beforeDelegateStakes,
                AccountInvocationRpc.initiateDelegateStakes(
                    DUMMY_HASH, stakeDelegationAmount, recipientAddress))
            .updatedState();
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> afterReceiveDelegatedStakes =
        plugin
            .invokeAccount(
                () -> 0,
                DUMMY_GLOBAL,
                beforeReceiveDelegateStakes,
                AccountInvocationRpc.initiateReceiveDelegatedStakes(
                    DUMMY_HASH, stakeDelegationAmount, senderAddress))
            .updatedState();

    Assertions.assertThat(afterDelegateStakes.account.storedPendingStakeDelegations().size())
        .isEqualTo(1);
    Assertions.assertThat(
            afterReceiveDelegatedStakes.account.storedPendingStakeDelegations().size())
        .isEqualTo(1);

    // 1a) Commit
    byte[] commitDelegation = AccountInvocationRpc.commitDelegateStakes(DUMMY_HASH);

    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> senderAfterCommit =
        plugin
            .invokeAccount(() -> 0, DUMMY_GLOBAL, afterDelegateStakes, commitDelegation)
            .updatedState();
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        plugin
            .invokeAccount(() -> 0, DUMMY_GLOBAL, afterReceiveDelegatedStakes, commitDelegation)
            .updatedState();

    long senderTokensInCustody = senderAfterCommit.account.tokensInCustody();
    long recipientTokensInCustody = recipientAfterCommit.account.tokensInCustody();

    Assertions.assertThat(senderTokensInCustody)
        .isEqualTo(initialSenderBalance - stakeDelegationAmount);
    Assertions.assertThat(recipientTokensInCustody).isEqualTo(stakeDelegationAmount);

    // 2a) Retract delegated stakes
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> senderAfterRetract =
        plugin
            .invokeAccount(
                () -> 0,
                DUMMY_GLOBAL,
                senderAfterCommit,
                AccountInvocationRpc.initiateRetractDelegatedStakes(
                    DUMMY_HASH, stakeDelegationAmount, recipientAddress))
            .updatedState();
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> recipientAfterRetract =
        plugin
            .invokeAccount(
                () -> 0,
                DUMMY_GLOBAL,
                recipientAfterCommit,
                AccountInvocationRpc.initiateReturnDelegatedStakes(
                    DUMMY_HASH, stakeDelegationAmount, senderAddress))
            .updatedState();

    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> senderAfterReturnCommit =
        plugin
            .invokeAccount(() -> 0, DUMMY_GLOBAL, senderAfterRetract, commitDelegation)
            .updatedState();
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> recipientAfterRetractCommit =
        plugin
            .invokeAccount(() -> 0, DUMMY_GLOBAL, recipientAfterRetract, commitDelegation)
            .updatedState();

    Assertions.assertThat(senderAfterReturnCommit.account.tokensInCustody())
        .isEqualTo(initialSenderBalance);
    Assertions.assertThat(recipientAfterRetractCommit.account.tokensInCustody()).isEqualTo(0);

    // 1b) Abort
    byte[] abortDelegation = AccountInvocationRpc.abortDelegateStakes(DUMMY_HASH);

    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> senderAfterAbort =
        plugin
            .invokeAccount(() -> 0, DUMMY_GLOBAL, afterDelegateStakes, abortDelegation)
            .updatedState();
    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> recipientAfterAbort =
        plugin
            .invokeAccount(() -> 0, DUMMY_GLOBAL, afterReceiveDelegatedStakes, abortDelegation)
            .updatedState();

    Balance senderAccount = senderAfterAbort.account;
    Balance recipientAccount = recipientAfterAbort.account;
    Assertions.assertThat(senderAccount.tokensInCustody()).isEqualTo(initialSenderBalance);
    Assertions.assertThat(recipientAccount.tokensInCustody()).isEqualTo(0);
  }

  @Test
  void byocTransferShouldNotChange() {
    long amount = 4000;
    TransferInformation transferInformation = TransferInformation.createForWithdraw(amount, 2);
    Balance balance = Balance.create();
    BalanceMutable updatedBalance = balance.asMutable();
    updatedBalance.addCoins(2, Unsigned256.create(4001));
    updatedBalance.initiateWithdraw(DUMMY_HASH, transferInformation);
    Assertions.assertThat(updatedBalance.tokensInCustody()).isEqualTo(0);
  }

  @Test
  void shouldBeConsistentDuringReleaseFromVestingSchedules() {
    long millisPerMonth = Duration.ofDays(28).toMillis();
    long tokenGenerationEvent = 10 * millisPerMonth;

    long duration = 36 * millisPerMonth;
    long interval = 3 * millisPerMonth;

    long mpcTokens = 10;

    BalanceMutable initialBalance = EMPTY.asMutable();
    initialBalance.addMpcTokenBalance(mpcTokens);

    Assertions.assertThat(initialBalance.tokensInCustody()).isEqualTo(mpcTokens);

    long vestingAccountTokens1 = 50;
    initialBalance.createVestingAccount(
        vestingAccountTokens1, tokenGenerationEvent, duration, interval);
    long vestingAccountTokens2 = 65;
    initialBalance.createVestingAccount(
        vestingAccountTokens2, tokenGenerationEvent, duration, interval);

    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> initial =
        new BlockchainAccountPlugin.AccountState<>(
            AccountStateLocal.initial(), initialBalance.asImmutable());

    Assertions.assertThat(initial.account.tokensInCustody())
        .isEqualTo(mpcTokens + vestingAccountTokens1 + vestingAccountTokens2);

    GasAndCoinAccountPlugin plugin = new GasAndCoinAccountPlugin();
    AccountStateGlobal global = new AccountStateGlobal();

    BlockchainPlugin.InvokeResult<BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance>>
        result =
            plugin.invokeAccount(
                () -> 13 * millisPerMonth,
                global,
                initial,
                AccountInvocationRpc.checkVestedTokens());

    BlockchainAccountPlugin.AccountState<AccountStateLocal, Balance> updatedState =
        result.updatedState();

    double threeMonths = (double) 3 * millisPerMonth;
    double amountOfTimes =
        Math.floor((13 * millisPerMonth - tokenGenerationEvent) / Math.floor(threeMonths));
    double amount1 =
        Math.min(amountOfTimes * Math.floor(50 / (duration / Math.floor(threeMonths))), 50);
    double amount2 =
        Math.min(amountOfTimes * Math.floor(65 / (duration / Math.floor(threeMonths))), 65);

    Assertions.assertThat(updatedState.account.vestingAccounts().get(0).releasedTokens)
        .isEqualTo((long) amount1);
    Assertions.assertThat(updatedState.account.vestingAccounts().get(1).releasedTokens)
        .isEqualTo((long) amount2);
    Assertions.assertThat(updatedState.account.mpcTokens())
        .isEqualTo(mpcTokens + (long) amount1 + (long) amount2);
    Assertions.assertThat(updatedState.account.tokensInCustody())
        .isEqualTo(mpcTokens + vestingAccountTokens1 + vestingAccountTokens2);
  }

  @Test
  void burningTokensShouldRemoveThem() {
    long tokensInBalance = 10_000_100_0000L;
    long receiveDelegateStakesAmount = 4000L;
    final BlockchainAddress firstSender =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    final BlockchainAddress secondSender =
        BlockchainAddress.fromString("000000000000000000000000000000000000000003");

    BalanceMutable balance = EMPTY.asMutable();
    balance.addMpcTokenBalance(tokensInBalance);
    balance.stakeTokens(tokensInBalance);
    Hash transactionId = Hash.create(s -> s.writeByte(99));
    PluginContext pluginContext = () -> 0;
    StakeDelegation firstStakeDelegation =
        StakeDelegation.createReceiveDelegation(
            firstSender, receiveDelegateStakesAmount, null, null);
    balance.addPendingStakeDelegation(transactionId, firstStakeDelegation);
    balance.commitDelegatedStakes(transactionId, pluginContext.blockProductionTime());
    balance.acceptPendingDelegatedStakes(firstSender, receiveDelegateStakesAmount);

    Hash secondTransactionId = Hash.create(s -> s.writeByte(98));
    StakeDelegation secondStakeDelegation =
        StakeDelegation.createReceiveDelegation(
            secondSender, receiveDelegateStakesAmount, null, null);
    balance.addPendingStakeDelegation(secondTransactionId, secondStakeDelegation);
    balance.commitDelegatedStakes(secondTransactionId, pluginContext.blockProductionTime());
    balance.acceptPendingDelegatedStakes(secondSender, receiveDelegateStakesAmount);
    long burnAmount = tokensInBalance + receiveDelegateStakesAmount * 2;
    balance.associateWithExpiration(Contracts.ONE, burnAmount, null);

    Assertions.assertThat(balance.tokensInCustody()).isEqualTo(burnAmount);
    balance.disassociateAndBurn(burnAmount, Contracts.ONE);
    Assertions.assertThat(balance.tokensInCustody()).isEqualTo(0L);
  }
}
