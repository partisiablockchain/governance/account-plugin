package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AccountCoinTest {

  private final AccountCoin initial = AccountCoin.create().add(Unsigned256.create(112L));
  private final AccountCoin accountCoin =
      AccountCoin.create().add(Unsigned256.create("10000000000000000000000"));

  @Test
  public void requiredCoinsForAmount() {
    Assertions.assertThat(
            initial.requiredCoinsForAmount(Unsigned256.create(112), new Fraction(1, 1)))
        .isEqualTo(Unsigned256.create(112));

    Assertions.assertThat(
            initial.requiredCoinsForAmount(Unsigned256.create(113), new Fraction(1, 1)))
        .isEqualTo(Unsigned256.create(112));

    Assertions.assertThat(
            initial.requiredCoinsForAmount(Unsigned256.create(111), new Fraction(1, 1)))
        .isEqualTo(Unsigned256.create(111));

    Assertions.assertThat(initial.requiredCoinsForAmount(Unsigned256.create(7), new Fraction(2, 1)))
        .isEqualTo(Unsigned256.create(4));

    Assertions.assertThat(
            initial.requiredCoinsForAmount(Unsigned256.create(12), new Fraction(7, 3)))
        .isEqualTo(Unsigned256.create(6));

    Assertions.assertThat(
            initial.requiredCoinsForAmount(Unsigned256.create(100), new Fraction(1, 4)))
        .isEqualTo(Unsigned256.create(112));

    Assertions.assertThat(
            initial.requiredCoinsForAmount(Unsigned256.create(19), new Fraction(9, 2)))
        .isEqualTo(Unsigned256.create(5));

    Assertions.assertThat(
            initial.requiredCoinsForAmount(Unsigned256.create(20), new Fraction(3, 4)))
        .isEqualTo(Unsigned256.create(27));

    Assertions.assertThat(
            accountCoin.requiredCoinsForAmount(
                Unsigned256.create(1000), new Fraction(1, 10000000000L)))
        .isEqualTo(Unsigned256.create(10000000000000L));
  }

  @Test
  public void updatedBalance() {
    AccountCoin updated = initial.add(Unsigned256.create(10));
    Assertions.assertThat(updated.getBalance()).isEqualTo(Unsigned256.create(122));
    Assertions.assertThat(initial.getBalance()).isEqualTo(Unsigned256.create(112));
  }

  @Test
  public void getBalance() {
    Assertions.assertThat(initial.getBalance()).isEqualTo(Unsigned256.create(112));
  }

  @Test
  public void computeGasEquivalent() {
    Assertions.assertThat(initial.computeGasEquivalent(new Fraction(0, 1)))
        .isEqualTo(Unsigned256.create(0));
    Assertions.assertThat(initial.computeGasEquivalent(new Fraction(1, 1)))
        .isEqualTo(Unsigned256.create(112));
    Assertions.assertThat(initial.computeGasEquivalent(new Fraction(7, 1)))
        .isEqualTo(Unsigned256.create(7 * 112));
  }

  @Test
  public void requiredCoinsForAmount_computeGasEquivalent() {
    Fraction oldEthConversionRate = new Fraction(4, 10_000_000_000L);
    Fraction newEthConversionRate = new Fraction(182_304_426L, 1_000_000_000_000_000_000L);

    assertGasAndCoinsEquivalent(5000, oldEthConversionRate, 12_500_000_000_000L);
    assertGasAndCoinsEquivalent(5000, newEthConversionRate, 27_426_651_725_944L);
    assertGasAndCoinsEquivalent(1, oldEthConversionRate, 2_500_000_000L);
    assertGasAndCoinsEquivalent(1, newEthConversionRate, 5_485_330_346L);

    Fraction maxConversionRate = new Fraction(Long.MAX_VALUE, 1);
    Fraction minConversionRate = new Fraction(1, Long.MAX_VALUE);

    assertGasAndCoinsEquivalent(Long.MAX_VALUE, maxConversionRate, 1);
    assertGasAndCoinsEquivalent(1, minConversionRate, Long.MAX_VALUE);

    assertGasAndCoinsEquivalent(Long.MAX_VALUE, new Fraction(1, 1), Long.MAX_VALUE);
  }

  private static void assertGasAndCoinsEquivalent(
      long gas, Fraction conversionRate, long expectedCoins) {
    AccountCoin maxBalance = AccountCoin.create().add(Unsigned256.create(Long.MAX_VALUE));
    Unsigned256 coins = maxBalance.requiredCoinsForAmount(Unsigned256.create(gas), conversionRate);
    Assertions.assertThat(coins).isEqualTo(Unsigned256.create(expectedCoins));
    Unsigned256 gasEquivalent =
        AccountCoin.create().add(coins).computeGasEquivalent(conversionRate);
    Assertions.assertThat(gasEquivalent.longValueExact()).isEqualTo(gas);
  }

  @Test
  public void isEmpty() {
    Assertions.assertThat(initial.isEmpty()).isFalse();
    Assertions.assertThat(AccountCoin.create().isEmpty()).isTrue();
  }
}
