package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainAccountPlugin;
import com.partisiablockchain.blockchain.BlockchainAccountPlugin.AccountState;
import com.partisiablockchain.blockchain.BlockchainAccountPlugin.BlockContext;
import com.partisiablockchain.blockchain.BlockchainAccountPlugin.ContractState;
import com.partisiablockchain.blockchain.BlockchainPlugin;
import com.partisiablockchain.blockchain.PayServiceFeesResult;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.PendingFee;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/** Test. */
public final class GasAndCoinAccountPluginTest {

  private static final String SYMBOL_0 = "sym0";
  private static final String SYMBOL_1 = "sym1";
  private static final String SYMBOL_2 = "sym2";
  private static final String SYMBOL_3 = "sym3";
  private static final Hash EMPTY = Hash.create(FunctionUtility.noOpConsumer());
  private static final long FIRST_TIME =
      GasAndCoinAccountPlugin.NO_OF_BLOCK_BETWEEN_CHECKS * 256 * 256 * 2;
  private static final long MILLIS_PER_MONTH = Duration.ofDays(28).toMillis();
  private static final Fraction SYMBOL_0_CONVERSION_RATE = new Fraction(2, 1);
  private static final Fraction SYMBOL_1_CONVERSION_RATE = new Fraction(3, 1);
  private static final Fraction SYMBOL_2_CONVERSION_RATE = new Fraction(1, 1);
  private static final Fraction SYMBOL_3_CONVERSION_RATE = new Fraction(1, 2);
  private static final long MOCK_SIGNED_TRANSACTION_NETWORK_BYTE_COUNT = 115;

  private static final long TOKEN_GENERATION_EVENT_0 = 10 * MILLIS_PER_MONTH;
  private static final long TOKEN_GENERATION_EVENT_1 = 10 * MILLIS_PER_MONTH + 5;
  private static final long LISTING_PAUSE =
      InitialVestingAccount.PAUSE_RELEASE_END_TIME - InitialVestingAccount.PAUSE_RELEASE_START_TIME;

  private static final BlockchainAddress firstAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000007");
  private static final BlockchainAddress secondAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000008");
  private static final BlockchainAddress thirdAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000009");
  private static final BlockchainAddress contractAddress =
      BlockchainAddress.fromString("020000000000000000000000000000000000000007");
  private static final long DURATION_0 = 36 * MILLIS_PER_MONTH;
  private static final long INTERVAL_0 = 3 * MILLIS_PER_MONTH;
  private static final long DURATION_1 = 24 * MILLIS_PER_MONTH;
  private static final long INTERVAL_1 = 3 * MILLIS_PER_MONTH;
  private static final byte INVALID_INVOCATION_BYTE = 100;
  private static final long ACTUAL_TOKEN_GENERATION = 1661904000000L;

  private AccountStateGlobal global;
  private final GasAndCoinAccountPlugin plugin = new GasAndCoinAccountPlugin();
  private final PluginContext pluginContextDefault = () -> 0;

  /** Setup plugin. */
  @BeforeEach
  public void setUp() {
    global =
        new AccountStateGlobal()
            .setGasPriceFor1000NetworkInternal(100)
            .setGasPriceFor1000StorageInternal(1000)
            .setGasPriceFor1000IceStakes(4000) // 4 gas per 1 BEET
            .setCoinInternal(SYMBOL_0, SYMBOL_0_CONVERSION_RATE)
            .setCoinInternal(SYMBOL_1, SYMBOL_1_CONVERSION_RATE)
            .setCoinInternal(SYMBOL_2, SYMBOL_2_CONVERSION_RATE)
            .setCoinInternal(SYMBOL_3, SYMBOL_3_CONVERSION_RATE);
  }

  @Test
  public void convertNetworkFee() {
    assertThat(plugin.convertNetworkFee(global, 7000)).isEqualTo(700);
    assertThat(plugin.convertNetworkFee(global, 9)).isEqualTo(1);
  }

  @Test
  public void payFee() {
    testFee(setCoinAccount(500), 0);
    testFee(setCoinAccount(500), 500);
    testFee(setCoinAccount(500), 499);
    testFeeInsufficientFunds(setCoinAccount(500), 501);
    testFeeInsufficientFunds(setCoinAccount(0), 1);
  }

  @Test
  public void payNegativeFee() {
    SignedTransaction signedTransaction = mockSignedTransaction(-1, Accounts.THREE, 0);

    assertThatThrownBy(
            () -> plugin.payCost(() -> 0, global, setCoinAccount(500), signedTransaction))
        .hasMessageContaining("Fee cannot be negative.");
  }

  @Test
  public void rejectNegativeAmountsOrZero() {
    AccountState<AccountStateLocal, Balance> initialSender = setCoinAccount(10);
    final byte[] stakeTokensNegative = AccountInvocationRpc.stakeTokens(-10);
    final byte[] stakeTokensZero = AccountInvocationRpc.stakeTokens(0);
    final byte[] associateZero = AccountInvocationRpc.associate(0, firstAddress);
    final byte[] disassociateZero = AccountInvocationRpc.disassociate(0, firstAddress);
    assertErrorThrownInInvoke(
        stakeTokensNegative, initialSender, "Cannot stake a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        stakeTokensZero, initialSender, "Cannot stake a non-positive amount of tokens");
    assertErrorThrownInInvoke(associateZero, initialSender, "Cannot associate non-positive amount");
    assertErrorThrownInInvoke(
        disassociateZero, initialSender, "Cannot disassociate non-positive amount");

    Unsigned256 unsignedAmount = Unsigned256.create(0);
    final byte[] twoPhaseRpcWithdrawZero =
        AccountInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount, SYMBOL_2);
    final byte[] twoPhaseRpcDepositZero =
        AccountInvocationRpc.initiateByocTransferRecipient(EMPTY, Unsigned256.create(0), SYMBOL_2);
    assertErrorThrownInInvoke(
        AccountInvocationRpc.initiateByocTransferSender(EMPTY, Unsigned256.ZERO, SYMBOL_2),
        initialSender,
        "Cannot transfer a non-positive amount of BYOC");
    assertErrorThrownInInvoke(
        AccountInvocationRpc.initiateByocTransferRecipient(EMPTY, Unsigned256.ZERO, SYMBOL_2),
        initialSender,
        "Cannot transfer a non-positive amount of BYOC");
    assertErrorThrownInInvoke(
        twoPhaseRpcWithdrawZero, initialSender, "Cannot transfer a non-positive amount of BYOC");
    assertErrorThrownInInvoke(
        twoPhaseRpcDepositZero, initialSender, "Cannot transfer a non-positive amount of BYOC");

    final byte[] twoPhaseRpcTokenWithdrawNegativeAmount =
        AccountInvocationRpc.initiateWithdraw(EMPTY, -10);
    final byte[] twoPhaseRpcTokenWithdrawZero = AccountInvocationRpc.initiateWithdraw(EMPTY, 0);
    final byte[] twoPhaseRpcTokenDepositNegativeAmount =
        AccountInvocationRpc.initiateDeposit(EMPTY, -10);
    final byte[] twoPhaseRpcTokenDepositZero = AccountInvocationRpc.initiateDeposit(EMPTY, 0);
    assertErrorThrownInInvoke(
        twoPhaseRpcTokenWithdrawNegativeAmount,
        initialSender,
        "Cannot initiate the withdraw part of a transfer with a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        twoPhaseRpcTokenDepositNegativeAmount,
        initialSender,
        "Cannot transfer a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        twoPhaseRpcTokenWithdrawZero,
        initialSender,
        "Cannot initiate the withdraw part of a transfer with a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        twoPhaseRpcTokenDepositZero,
        initialSender,
        "Cannot transfer a non-positive amount of tokens");

    final byte[] delegateReceiveNegative =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, -10, Accounts.THREE);
    final byte[] delegateRetractNegative =
        AccountInvocationRpc.initiateRetractDelegatedStakes(EMPTY, -10, Accounts.THREE);
    final byte[] delegateReturnNegative =
        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, -10, Accounts.THREE);
    final byte[] delegateReceiveZero =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 0, Accounts.THREE);
    final byte[] delegateRetractZero =
        AccountInvocationRpc.initiateRetractDelegatedStakes(EMPTY, 0, Accounts.THREE);
    final byte[] delegateReturnZero =
        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, 0, Accounts.THREE);
    final byte[] delegateAcceptNegative =
        AccountInvocationRpc.acceptDelegatedStakes(-1, Accounts.THREE);
    final byte[] delegateAcceptZero = AccountInvocationRpc.acceptDelegatedStakes(0, Accounts.THREE);
    final byte[] delegateReduceNegative =
        AccountInvocationRpc.reduceDelegatedStakes(Accounts.THREE, 0);
    final byte[] delegateReduceZero = AccountInvocationRpc.reduceDelegatedStakes(Accounts.THREE, 0);
    final byte[] delegateInitiateNegative =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, -1, Accounts.THREE);
    final byte[] delegateInitiateZero =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 0, Accounts.THREE);
    assertErrorThrownInInvoke(
        delegateReceiveNegative,
        initialSender,
        "Cannot initiate the receive part of a stake delegation with a non-positive amount of"
            + " tokens");
    assertErrorThrownInInvoke(
        delegateRetractNegative,
        initialSender,
        "Cannot initiate the retract part of a stake delegation with a non-positive amount of"
            + " tokens");
    assertErrorThrownInInvoke(
        delegateReturnNegative,
        initialSender,
        "Cannot initiate the return part of stake delegation with a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        delegateReceiveZero,
        initialSender,
        "Cannot initiate the receive part of a stake delegation with a non-positive amount of"
            + " tokens");
    assertErrorThrownInInvoke(
        delegateRetractZero,
        initialSender,
        "Cannot initiate the retract part of a stake delegation with a non-positive amount of"
            + " tokens");
    assertErrorThrownInInvoke(
        delegateReturnZero,
        initialSender,
        "Cannot initiate the return part of stake delegation with a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        delegateAcceptNegative,
        initialSender,
        "Cannot accept delegate stakes with a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        delegateAcceptZero,
        initialSender,
        "Cannot accept delegate stakes with a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        delegateReduceNegative,
        initialSender,
        "Cannot reduce delegate stakes with a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        delegateReduceZero,
        initialSender,
        "Cannot reduce delegate stakes with a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        delegateInitiateNegative,
        initialSender,
        "Cannot initiate delegate with a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        delegateInitiateZero,
        initialSender,
        "Cannot initiate delegate with a non-positive amount of tokens");

    final byte[] vestingAccountNegative =
        AccountInvocationRpc.createVestingAccount(
            -1, DURATION_0, INTERVAL_0, TOKEN_GENERATION_EVENT_0);
    final byte[] vestingAccountZero =
        AccountInvocationRpc.createVestingAccount(
            0, DURATION_0, INTERVAL_0, TOKEN_GENERATION_EVENT_0);
    assertErrorThrownInInvoke(
        vestingAccountNegative,
        initialSender,
        "Cannot create vesting account for non-positive amount of tokens");
    assertErrorThrownInInvoke(
        vestingAccountZero,
        initialSender,
        "Cannot create vesting account for non-positive amount of tokens");
  }

  @Test
  public void negativeAmountsErrors() {
    AccountState<AccountStateLocal, Balance> initial = setCoinAccount(10);
    final byte[] addMpcTokensNegative = AccountInvocationRpc.addMpcTokenBalance(-1);
    final byte[] addMpcTokensZero = AccountInvocationRpc.addMpcTokenBalance(0);
    assertErrorThrownInInvoke(
        addMpcTokensNegative, initial, "Cannot add a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        addMpcTokensZero, initial, "Cannot add a non-positive amount of tokens");

    final byte[] deductMpcTokensNegative = AccountInvocationRpc.deductMpcTokenBalance(-10);
    final byte[] deductMpcTokensZero = AccountInvocationRpc.deductMpcTokenBalance(0);
    assertErrorThrownInInvoke(
        deductMpcTokensNegative, initial, "Cannot deduct a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        deductMpcTokensZero, initial, "Cannot deduct a non-positive amount of tokens");

    final byte[] stakeMpcTokens = AccountInvocationRpc.stakeTokens(-10);
    final byte[] stakeMpcTokensZero = AccountInvocationRpc.stakeTokens(0);
    assertErrorThrownInInvoke(
        stakeMpcTokens, initial, "Cannot stake a non-positive amount of tokens");
    assertErrorThrownInInvoke(
        stakeMpcTokensZero, initial, "Cannot stake a non-positive amount of tokens");

    final byte[] unstakeMpcTokens = AccountInvocationRpc.unstakeTokens(-10);
    final byte[] unstakeMpcTokensZero = AccountInvocationRpc.unstakeTokens(0);
    assertErrorThrownInInvoke(
        unstakeMpcTokens, initial, "Cannot unstake non-positive amount of tokens");
    assertErrorThrownInInvoke(
        unstakeMpcTokensZero, initial, "Cannot unstake non-positive amount of tokens");

    final byte[] burnMpcTokensNegative = AccountInvocationRpc.burnMpcTokens(-10, contractAddress);
    final byte[] burnMpcTokensZero = AccountInvocationRpc.burnMpcTokens(0, contractAddress);
    assertErrorThrownInInvoke(
        burnMpcTokensNegative, initial, "Cannot burn non-positive amount of tokens");
    assertErrorThrownInInvoke(
        burnMpcTokensZero, initial, "Cannot burn non-positive amount of tokens");
  }

  @Test
  public void payFeeWithGasAndCoins() {
    int coins = 13;
    Unsigned256 addedCoins = Unsigned256.create(coins);
    long correspondingGas =
        coins * SYMBOL_0_CONVERSION_RATE.getNumerator() / SYMBOL_0_CONVERSION_RATE.getDenominator();

    long symbolZeroCoins = 10;
    long symbolOneCoins = 10;
    BalanceMutable balance = Balance.create().asMutable();
    balance.addCoins(0, Unsigned256.create(symbolZeroCoins));
    balance.addCoins(1, Unsigned256.create(symbolOneCoins));
    AccountState<AccountStateLocal, Balance> withCoins =
        new AccountState<>(
            AccountStateLocal.initial()
                .updateCollectedCoins(0, addedCoins, Unsigned256.create(correspondingGas)),
            balance.asImmutable());
    testFee(withCoins, 0, symbolZeroCoins, symbolOneCoins, coins, 0, correspondingGas, 0, 0);
    long symbolZeroAllCoinsGasSum =
        (coins + symbolZeroCoins)
            * SYMBOL_0_CONVERSION_RATE.getNumerator()
            / SYMBOL_0_CONVERSION_RATE.getDenominator();
    long symbolZeroCoinToGas =
        symbolZeroCoins
            * SYMBOL_0_CONVERSION_RATE.getNumerator()
            / SYMBOL_0_CONVERSION_RATE.getDenominator();
    testFee(
        withCoins,
        19,
        0,
        symbolOneCoins,
        coins + symbolZeroCoins,
        0,
        symbolZeroAllCoinsGasSum,
        0,
        symbolZeroCoinToGas - 19);
    testFee(
        withCoins,
        20,
        0,
        symbolOneCoins,
        coins + symbolZeroCoins,
        0,
        symbolZeroAllCoinsGasSum,
        0,
        symbolZeroCoinToGas - 20);
    testFee(
        withCoins,
        21,
        0,
        symbolOneCoins - 1,
        coins + symbolZeroCoins,
        1,
        symbolZeroAllCoinsGasSum,
        SYMBOL_1_CONVERSION_RATE.getNumerator() / SYMBOL_1_CONVERSION_RATE.getDenominator(),
        symbolZeroCoinToGas
            + SYMBOL_1_CONVERSION_RATE.getNumerator() / SYMBOL_1_CONVERSION_RATE.getDenominator()
            - 21);
    long symbolOneCoinToGas =
        symbolOneCoins
            * SYMBOL_1_CONVERSION_RATE.getNumerator()
            / SYMBOL_0_CONVERSION_RATE.getDenominator();
    testFee(
        withCoins,
        48,
        0,
        0,
        coins + symbolZeroCoins,
        symbolOneCoins,
        symbolZeroAllCoinsGasSum,
        symbolOneCoins
            * SYMBOL_1_CONVERSION_RATE.getNumerator()
            / SYMBOL_0_CONVERSION_RATE.getDenominator(),
        symbolZeroCoinToGas + symbolOneCoinToGas - 48);
    testFee(
        withCoins,
        50,
        0,
        0,
        coins + symbolZeroCoins,
        symbolOneCoins,
        symbolZeroAllCoinsGasSum,
        symbolOneCoins
            * SYMBOL_1_CONVERSION_RATE.getNumerator()
            / SYMBOL_1_CONVERSION_RATE.getDenominator(),
        symbolZeroCoinToGas + symbolOneCoinToGas - 50);

    long addedCoins2 = 10;
    long correspondingGas2 =
        addedCoins2
            * SYMBOL_3_CONVERSION_RATE.getNumerator()
            / SYMBOL_3_CONVERSION_RATE.getDenominator();

    long symbolThreeCoins = 50;
    long symbolThreeGas = 30;
    long coinsToConvert = 50;

    BalanceMutable accountBalance = Balance.create().asMutable();
    accountBalance.addCoins(3, Unsigned256.create(coinsToConvert));
    AccountState<AccountStateLocal, Balance> withCoinsAndFractionConversionRate =
        new AccountState<>(
            AccountStateLocal.initial()
                .updateCollectedCoins(
                    3, Unsigned256.create(addedCoins2), Unsigned256.create(correspondingGas2)),
            accountBalance.asImmutable());

    testFee(withCoinsAndFractionConversionRate, addedCoins2 + symbolThreeCoins, symbolThreeGas);

    testFeeInsufficientFunds(withCoins, 551);
  }

  @Test
  public void remainingIsRegisteredAsBlockchainUsage() {
    int coins = 13;
    Unsigned256 addedCoins = Unsigned256.create(coins);
    long correspondingGas =
        coins * SYMBOL_0_CONVERSION_RATE.getNumerator() / SYMBOL_0_CONVERSION_RATE.getDenominator();

    long symbolZeroCoins = 10;

    BalanceMutable balance = Balance.create().asMutable();
    balance.addCoins(0, Unsigned256.create(symbolZeroCoins));
    AccountState<AccountStateLocal, Balance> state =
        new AccountState<>(
            AccountStateLocal.initial()
                .updateCollectedCoins(0, addedCoins, Unsigned256.create(correspondingGas)),
            balance.asImmutable());
    long toPay = 20;
    SignedTransaction signedTransaction = mockSignedTransaction(toPay, Accounts.THREE, 0);
    AtomicInteger counter = new AtomicInteger(0);
    plugin.payCost(
        () -> {
          counter.incrementAndGet();
          return 0;
        },
        global,
        state,
        signedTransaction);
    // Getting the block time is called two times. One for registering remaining gas and one for the
    // network fee.
    assertThat(counter.get()).isEqualTo(2);
  }

  @Test
  public void forceDeleteAccount() {
    ContractState<AccountStateLocal, ContractStorage> withCoins =
        new ContractState<>(
            AccountStateLocal.initial(),
            new ContractStorage(
                0L,
                Signed256.create(Unsigned256.create(10)),
                0L,
                FixedList.create(),
                AvlTree.create(),
                AvlTree.create(),
                AvlTree.create(),
                0));

    AccountStateLocal transferred = plugin.removeContract(pluginContextDefault, global, withCoins);
    assertThat(transferred).isNotNull();
  }

  @Test
  public void deleteContractReleasesSurplus() {
    long balance = 1000;
    ContractState<AccountStateLocal, ContractStorage> withCoins =
        new ContractState<>(
            AccountStateLocal.initial(),
            new ContractStorage(
                0L,
                Signed256.create(Unsigned256.create(balance)),
                0L,
                FixedList.create(),
                AvlTree.create(),
                AvlTree.create(),
                AvlTree.create(),
                0));

    AccountStateLocal remover = plugin.removeContract(pluginContextDefault, global, withCoins);
    Unsigned256 surplus =
        remover.getBlockchainUsage().getValue(pluginContextDefault.blockProductionTime());
    assertThat(surplus).isEqualTo(Unsigned256.create(balance));
  }

  @Test
  public void updateContractGasBalance() {
    ContractState<AccountStateLocal, ContractStorage> state =
        new ContractState<>(AccountStateLocal.initial(), new ContractStorage());

    assertThat(state.contract.getBalance().getAbsoluteValue()).isEqualTo(Unsigned256.create(0L));
    state = plugin.updateContractGasBalance(pluginContextDefault, global, state, Contracts.ONE, 50);
    assertThat(state.contract.getBalance().getAbsoluteValue()).isEqualTo(Unsigned256.create(50L));
    state = plugin.updateContractGasBalance(pluginContextDefault, global, state, Contracts.ONE, 25);
    assertThat(state.contract.getBalance().getAbsoluteValue()).isEqualTo(Unsigned256.create(75L));
  }

  @Test
  public void updateContractGasBalanceWithInitialNullStorage() {
    ContractState<AccountStateLocal, ContractStorage> state =
        new ContractState<>(AccountStateLocal.initial(), null);

    state = plugin.updateContractGasBalance(pluginContextDefault, global, state, Contracts.ONE, 99);
    assertThat(state.contract.getBalance()).isEqualTo(Signed256.create(Unsigned256.create(99L)));
    state =
        plugin.updateContractGasBalance(pluginContextDefault, global, state, Contracts.ONE, 101);
    assertThat(state.contract.getBalance()).isEqualTo(Signed256.create(Unsigned256.create(200L)));
    state = plugin.updateContractGasBalance(pluginContextDefault, global, state, Contracts.ONE, -1);
    assertThat(state.contract.getBalance()).isEqualTo(Signed256.create(Unsigned256.create(199L)));
    state =
        plugin.updateContractGasBalance(pluginContextDefault, global, state, Contracts.ONE, -199);
    assertThat(state.contract.getBalance()).isEqualTo(Signed256.create(Unsigned256.create(0L)));
  }

  @Test
  public void updateContractGasBalanceNegative() {
    Unsigned256 gasBalance = Unsigned256.create(50);
    ContractState<AccountStateLocal, ContractStorage> stateWithGas =
        new ContractState<>(
            AccountStateLocal.initial(), new ContractStorage().addBalance(gasBalance));

    ContractState<AccountStateLocal, ContractStorage> deductGas =
        plugin.updateContractGasBalance(
            pluginContextDefault, global, stateWithGas, Contracts.ONE, -50);
    assertThat(deductGas.contract.getBalance()).isEqualTo(Signed256.create());
    assertThat(deductGas.local.getBlockchainUsageForEpoch(0)).isEqualTo(Unsigned256.ZERO);

    assertThatThrownBy(
            () ->
                plugin.updateContractGasBalance(
                    pluginContextDefault, global, stateWithGas, Contracts.ONE, -51))
        .hasMessageContaining("Contract is unable to cover requested gas cost");

    ContractState<AccountStateLocal, ContractStorage> stateWithDebt =
        new ContractState<>(
            AccountStateLocal.initial(), new ContractStorage().deductBalance(Unsigned256.ONE, 0L));
    assertThatThrownBy(
            () ->
                plugin.updateContractGasBalance(
                    pluginContextDefault, global, stateWithDebt, Contracts.ONE, -1))
        .hasMessageContaining("Contract is unable to cover requested gas cost");
  }

  /**
   * When deducting gas from a contract in {@link
   * GasAndCoinAccountPlugin#updateContractGasBalance(PluginContext, AccountStateGlobal,
   * ContractState, BlockchainAddress, long)} the gas is used to pay for an outbound event. No work
   * is done as part of the allocation and the gas should thus not be allocated as blockchain usage.
   */
  @Test
  public void gasAllocatedFromContractToEventsShouldNotBeRegisteredAsBlockchainUsage() {
    Unsigned256 gasBalance = Unsigned256.create(50);
    ContractState<AccountStateLocal, ContractStorage> stateWithGas =
        new ContractState<>(
            AccountStateLocal.initial(), new ContractStorage().addBalance(gasBalance));
    ContractState<AccountStateLocal, ContractStorage> deductGas =
        plugin.updateContractGasBalance(
            pluginContextDefault, global, stateWithGas, Contracts.ONE, -10);
    assertThat(deductGas.contract.getBalance())
        .isEqualTo(Signed256.create(gasBalance.subtract(Unsigned256.create(10))));
    assertThat(deductGas.local.getBlockchainUsageForEpoch(0)).isEqualTo(Unsigned256.ZERO);
  }

  @Test
  public void registerBlockchainUsage() {
    AccountStateLocal contextFreeState = AccountStateLocal.initial();
    long gas = 100L;
    AccountStateLocal newState =
        plugin.registerBlockchainUsage(pluginContextDefault, global, contextFreeState, gas);
    assertThat(newState.getBlockchainUsage().getValue(0L)).isEqualTo(Unsigned256.create(gas));
    AccountStateLocal moreUsageState =
        plugin.registerBlockchainUsage(pluginContextDefault, global, newState, gas);
    assertThat(moreUsageState.getBlockchainUsage().getValue(0L))
        .isEqualTo(Unsigned256.create(gas * 2));
  }

  @Test
  public void contractCanPayServiceFees() {
    Unsigned256 gas = Unsigned256.create(600);
    ContractState<AccountStateLocal, ContractStorage> initialState =
        new ContractState<>(AccountStateLocal.initial(), new ContractStorage().addBalance(gas));

    List<PendingFee> pendingFees =
        List.of(
            PendingFee.create(Accounts.ONE, 100L),
            PendingFee.create(Accounts.TWO, 200L),
            PendingFee.create(Accounts.THREE, 300L));

    PayServiceFeesResult<ContractState<AccountStateLocal, ContractStorage>> state =
        plugin.payServiceFees(pluginContextDefault, global, initialState, pendingFees);
    assertThat(state.contractCoveredFee()).isTrue();
    assertThat(state.getResult().contract.getBalance()).isEqualTo(Signed256.create());
    assertThat(state.getResult().local.getServiceFeesForAddress(Accounts.ONE))
        .isEqualTo(Unsigned256.create(100L));
    assertThat(state.getResult().local.getServiceFeesForAddress(Accounts.TWO))
        .isEqualTo(Unsigned256.create(200L));
    assertThat(state.getResult().local.getServiceFeesForAddress(Accounts.THREE))
        .isEqualTo(Unsigned256.create(300L));
  }

  @Test
  public void contractRetainsSurplusAfterPayingServiceFees() {
    Unsigned256 gas = Unsigned256.create(600);
    ContractState<AccountStateLocal, ContractStorage> initialState =
        new ContractState<>(
            AccountStateLocal.initial(),
            new ContractStorage().addBalance(gas.add(Unsigned256.ONE)));

    List<PendingFee> pendingFees =
        List.of(
            PendingFee.create(Accounts.ONE, 200L),
            PendingFee.create(Accounts.TWO, 100L),
            PendingFee.create(Accounts.THREE, 300L));

    PayServiceFeesResult<ContractState<AccountStateLocal, ContractStorage>> state =
        plugin.payServiceFees(pluginContextDefault, global, initialState, pendingFees);
    assertThat(state.contractCoveredFee()).isTrue();
    assertThat(state.getResult().contract.getBalance())
        .isEqualTo(Signed256.create(Unsigned256.create(1L)));
    assertThat(state.getResult().local.getServiceFeesForAddress(Accounts.ONE))
        .isEqualTo(Unsigned256.create(200L));
    assertThat(state.getResult().local.getServiceFeesForAddress(Accounts.TWO))
        .isEqualTo(Unsigned256.create(100L));
    assertThat(state.getResult().local.getServiceFeesForAddress(Accounts.THREE))
        .isEqualTo(Unsigned256.create(300L));
  }

  @Test
  public void accountsReceiveOnlyContractsBalanceIfUnableToPayServiceFees() {
    Unsigned256 gas = Unsigned256.create(600);
    ContractState<AccountStateLocal, ContractStorage> initialState =
        new ContractState<>(
            AccountStateLocal.initial(),
            new ContractStorage().addBalance(gas.subtract(Unsigned256.ONE)));

    List<PendingFee> pendingFees =
        List.of(
            PendingFee.create(Accounts.ONE, 100L),
            PendingFee.create(Accounts.TWO, 300L),
            PendingFee.create(Accounts.THREE, 200L));

    PayServiceFeesResult<ContractState<AccountStateLocal, ContractStorage>> state =
        plugin.payServiceFees(pluginContextDefault, global, initialState, pendingFees);
    assertThat(state.contractCoveredFee()).isFalse();
    assertThat(state.getResult().contract.getBalance().getAbsoluteValue())
        .isEqualTo(Unsigned256.create(0L));
    assertThat(state.getResult().local.getServiceFeesForAddress(Accounts.ONE))
        .isEqualTo(Unsigned256.create(99L));
    assertThat(state.getResult().local.getServiceFeesForAddress(Accounts.TWO))
        .isEqualTo(Unsigned256.create(300L));
    assertThat(state.getResult().local.getServiceFeesForAddress(Accounts.THREE))
        .isEqualTo(Unsigned256.create(200L));
  }

  @Test
  public void canCoverFee_cost() {
    AccountState<AccountStateLocal, Balance> initial =
        setCoinAccount(100 + MOCK_SIGNED_TRANSACTION_NETWORK_BYTE_COUNT);
    assertThat(testCanCoverFee(initial, 99 + MOCK_SIGNED_TRANSACTION_NETWORK_BYTE_COUNT, 0, 0))
        .isTrue();
    assertThat(testCanCoverFee(initial, 100 + MOCK_SIGNED_TRANSACTION_NETWORK_BYTE_COUNT, 0, 0))
        .isTrue();
    assertThat(testCanCoverFee(initial, 101 + MOCK_SIGNED_TRANSACTION_NETWORK_BYTE_COUNT, 0, 0))
        .isFalse();
  }

  @Test
  public void payInfrastructureFees() {
    long pendingFeeOne = 100L;
    long pendingFeeTwo = 200L;
    long pendingFeeThree = 300L;
    List<PendingFee> pendingFees =
        List.of(
            PendingFee.create(Accounts.ONE, pendingFeeOne),
            PendingFee.create(Accounts.TWO, pendingFeeTwo),
            PendingFee.create(Accounts.THREE, pendingFeeThree));

    AccountStateLocal state =
        plugin.payInfrastructureFees(
            pluginContextDefault, global, AccountStateLocal.initial(), pendingFees);

    assertThat(state.getServiceFeesForAddress(Accounts.ONE))
        .isEqualTo(Unsigned256.create(pendingFeeOne));
    assertThat(state.getServiceFeesForAddress(Accounts.TWO))
        .isEqualTo(Unsigned256.create(pendingFeeTwo));
    assertThat(state.getServiceFeesForAddress(Accounts.THREE))
        .isEqualTo(Unsigned256.create(pendingFeeThree));
    assertThat(state.getInfrastructureFeesSum())
        .isEqualTo(Unsigned256.create(pendingFeeOne + pendingFeeTwo + pendingFeeThree));
  }

  @Test
  public void canCoverFee_costLessThanNetworkFee() {
    AccountState<AccountStateLocal, Balance> initial =
        setCoinAccount(100 + MOCK_SIGNED_TRANSACTION_NETWORK_BYTE_COUNT);
    assertThat(testCanCoverFee(initial, 21, 0, 100)).isFalse();
    assertThat(testCanCoverFee(initial, 22, 0, 100)).isTrue();
    assertThat(testCanCoverFee(initial, 112, 0, 100)).isTrue();
    assertThat(testCanCoverFee(initial, MOCK_SIGNED_TRANSACTION_NETWORK_BYTE_COUNT, 0, 0)).isTrue();
  }

  @Test
  public void canCoverFee_freeTransactions() {
    long currentEpoch = 0;

    BalanceMutable enoughStakedTokens =
        stakeAccount(GasAndCoinAccountPlugin.STAKED_TOKEN_THRESHOLD_FOR_FREE_TRANSACTION)
            .account
            .asMutable();
    AccountState<AccountStateLocal, Balance> availableFreeTransactions =
        new AccountState<>(AccountStateLocal.initial(), enoughStakedTokens.asImmutable());
    assertThat(testCanCoverFee(availableFreeTransactions, 0, 0, 0)).isTrue();
    assertThat(
            testCanCoverFee(
                availableFreeTransactions,
                0,
                currentEpoch,
                GasAndCoinAccountPlugin.MAX_NETWORK_SIZE_FOR_FREE_TRANSACTION))
        .isFalse();

    assertThat(
            testCanCoverFee(
                availableFreeTransactions,
                0,
                currentEpoch,
                GasAndCoinAccountPlugin.MAX_NETWORK_SIZE_FOR_FREE_TRANSACTION - 114))
        .isTrue();

    for (long i = 0; i < GasAndCoinAccountPlugin.FREE_TRANSACTIONS_PER_EPOCH; i++) {
      enoughStakedTokens.incrementFreeTransactionsUsed(currentEpoch);
    }
    AccountState<AccountStateLocal, Balance> noAvailableFreeTransactions =
        new AccountState<>(AccountStateLocal.initial(), enoughStakedTokens.asImmutable());
    assertThat(testCanCoverFee(noAvailableFreeTransactions, 0, currentEpoch, 0)).isFalse();
    BalanceMutable accountBalance =
        stakeAccount(GasAndCoinAccountPlugin.STAKED_TOKEN_THRESHOLD_FOR_FREE_TRANSACTION - 1)
            .account
            .asMutable();
    accountBalance.incrementFreeTransactionsUsed(currentEpoch);
    AccountState<AccountStateLocal, Balance> notEnoughStaked =
        new AccountState<>(AccountStateLocal.initial(), accountBalance.asImmutable());
    assertThat(testCanCoverFee(notEnoughStaked, 0, currentEpoch, 0)).isFalse();
    assertThat(
            plugin.canCoverCost(
                pluginContextDefault,
                global,
                availableFreeTransactions,
                mockSignedTransaction(0, Contracts.PUB_CONTRACT, 0)))
        .isFalse();
  }

  @Test
  void stakeTokensForJobs() {
    BalanceMutable accountBalance = Balance.create().asMutable();
    long amount = GasAndCoinAccountPlugin.STAKED_TOKEN_THRESHOLD_FOR_FREE_TRANSACTION / 2;
    accountBalance.commitReceiveDelegatedStakes(firstAddress, amount, null);
    accountBalance.commitReceiveDelegatedStakes(secondAddress, amount, null);
    AccountState<AccountStateLocal, Balance> cannotSendFreeWithPendingDelegatedStakes =
        new AccountState<>(AccountStateLocal.initial(), accountBalance.asImmutable());
    assertThat(testCanCoverFee(cannotSendFreeWithPendingDelegatedStakes, 0, 0, 0)).isFalse();

    accountBalance.acceptPendingDelegatedStakes(firstAddress, amount);
    accountBalance.acceptPendingDelegatedStakes(secondAddress, amount);
    AccountState<AccountStateLocal, Balance> enoughWithDelegatedFromOthers =
        new AccountState<>(AccountStateLocal.initial(), accountBalance.asImmutable());
    assertThat(testCanCoverFee(enoughWithDelegatedFromOthers, 0, 0, 0)).isTrue();

    accountBalance.returnDelegatedStakes(firstAddress, amount);
    accountBalance.addMpcTokenBalance(amount);
    accountBalance.stakeTokens(amount);
    AccountState<AccountStateLocal, Balance> mixtureOfOwnStakesAndDelegates =
        new AccountState<>(AccountStateLocal.initial(), accountBalance.asImmutable());
    assertThat(testCanCoverFee(mixtureOfOwnStakesAndDelegates, 0, 0, 0)).isTrue();
  }

  @Test
  public void canCoverFee_newEpoch() {
    Balance enoughStakedTokens =
        stakeAccount(GasAndCoinAccountPlugin.STAKED_TOKEN_THRESHOLD_FOR_FREE_TRANSACTION).account;
    long blockProductionTime = (long) Math.pow(2, 21);
    long epoch = blockProductionTime >>> 21;
    BalanceMutable balance = enoughStakedTokens.asMutable();
    balance.incrementFreeTransactionsUsed(epoch);
    AccountState<AccountStateLocal, Balance> availableFreeTransactions =
        new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());
    assertThat(testCanCoverFee(availableFreeTransactions, 0, blockProductionTime, 0)).isTrue();
  }

  @Test
  public void canCoverFee_pastOrFutureEpoch() {
    BalanceMutable enoughStakedTokens =
        stakeAccount(GasAndCoinAccountPlugin.STAKED_TOKEN_THRESHOLD_FOR_FREE_TRANSACTION)
            .account
            .asMutable();
    enoughStakedTokens.incrementFreeTransactionsUsed(1);
    AccountState<AccountStateLocal, Balance> pastEpoch =
        new AccountState<>(AccountStateLocal.initial(), enoughStakedTokens.asImmutable());
    assertThatThrownBy(() -> testCanCoverFee(pastEpoch, 0, 0, 0))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot get free transactions for previous epoch");
    enoughStakedTokens =
        stakeAccount(GasAndCoinAccountPlugin.STAKED_TOKEN_THRESHOLD_FOR_FREE_TRANSACTION)
            .account
            .asMutable();
    enoughStakedTokens.incrementFreeTransactionsUsed(0);
    AccountState<AccountStateLocal, Balance> futureEpoch =
        new AccountState<>(AccountStateLocal.initial(), enoughStakedTokens.asImmutable());
    assertThat(testCanCoverFee(futureEpoch, 0, 0, 1)).isTrue();
  }

  @Test
  public void getSpentFreeTransactionsOnFutureEpochIs0() {
    BalanceMutable stakedAccountBalance = stakeAccount(25).account.asMutable();
    stakedAccountBalance.incrementFreeTransactionsUsed(1);
    AccountState<AccountStateLocal, Balance> epochOne =
        new AccountState<>(AccountStateLocal.initial(), stakedAccountBalance.asImmutable());
    assertThat(epochOne.account.spentFreeTransactions(4)).isEqualTo(0);
  }

  @Test
  public void payFees() {
    AccountState<AccountStateLocal, Balance> local = setCoinAccount(100L);
    BlockchainAccountPlugin.PayCostResult<AccountStateLocal, Balance> updated =
        plugin.payCost(
            pluginContextDefault, global, local, mockSignedTransaction(100L, Accounts.ONE, 0));
    assertThat(updated.accountState().local.convertedExternalCoins().get(2).getConvertedGasSum())
        .isEqualTo(Unsigned256.create(100));
    assertThat(updated.accountState().local.getBlockchainUsageForEpoch(0))
        .isEqualTo(Unsigned256.create(12));
    BlockchainAccountPlugin.PayCostResult<AccountStateLocal, Balance> updated2 =
        plugin.payCost(() -> 0, global, local, mockSignedTransaction(0L, Accounts.ONE, 0));
    assertThat(updated2.accountState().account.spentFreeTransactions(0)).isEqualTo(1);
  }

  @Test
  public void payFees_zeroValueCoin() {
    AccountState<AccountStateLocal, Balance> local = setCoinAccount(100L);
    AccountStateGlobal zeroCoinGlobal = global.setCoinInternal(SYMBOL_1, new Fraction(0, 2));
    BlockchainAccountPlugin.PayCostResult<AccountStateLocal, Balance> updated =
        plugin.payCost(
            pluginContextDefault,
            zeroCoinGlobal,
            local,
            mockSignedTransaction(100L, Accounts.ONE, 0));
    BlockchainAccountPlugin.PayCostResult<AccountStateLocal, Balance> updated2 =
        plugin.payCost(() -> 0, global, local, mockSignedTransaction(0L, Accounts.ONE, 0));
    assertThat(updated2.accountState().account.spentFreeTransactions(0)).isEqualTo(1);
    assertThat(updated.accountState().local.getBlockchainUsageForEpoch(0))
        .isEqualTo(Unsigned256.create(12));
    assertThat(updated.accountState().local.convertedExternalCoins().get(2).getConvertedGasSum())
        .isEqualTo(Unsigned256.create(100));
  }

  @Test
  public void getGlobalStateClass() {
    assertThat(plugin.getGlobalStateClass()).isEqualTo(AccountStateGlobal.class);
  }

  @Test
  public void getLocalStateClass() {
    assertThat(plugin.getLocalStateClassTypeParameters())
        .containsExactly(AccountStateLocal.class, Balance.class, ContractStorage.class);
  }

  @Test
  public void sysContractCreatedWithoutEnablingStorageFees() {
    ContractState<AccountStateLocal, ContractStorage> contractState =
        plugin.contractCreated(() -> 100, global, AccountStateLocal.initial(), Contracts.ONE);
    ContractStorage contractStorage = contractState.contract;
    assertThat(contractStorage.getBalance().getAbsoluteValue()).isEqualTo(Unsigned256.ZERO);
    assertThat(contractStorage.latestStorageFeeTime()).isNull();
  }

  @Test
  public void nonSysContractCreatedWithEnabledStorageFees() {
    ContractState<AccountStateLocal, ContractStorage> contractState =
        plugin.contractCreated(
            () -> 100, global, AccountStateLocal.initial(), Contracts.PUB_CONTRACT);
    ContractStorage contractStorage = contractState.contract;
    assertThat(contractStorage.getBalance().getAbsoluteValue()).isEqualTo(Unsigned256.ZERO);
    assertThat(contractStorage.latestStorageFeeTime()).isEqualTo(100L);
  }

  @Test
  public void updateForBlockRegisterContracts() {
    TestBlockContext context = new TestBlockContext(FIRST_TIME, 1000);
    AccountStateLocal accountStateLocal =
        plugin.updateForBlock(global, AccountStateLocal.initial(), context);
    assertThat(context.registeredUpdates).containsExactly(Contracts.ONE, Contracts.TWO);
    assertThat(accountStateLocal).isNotNull();

    TestBlockContext secondContext =
        new TestBlockContext(FIRST_TIME + GasAndCoinAccountPlugin.NO_OF_BLOCK_BETWEEN_CHECKS, 1000);
    plugin.updateForBlock(global, AccountStateLocal.initial(), secondContext);
    assertThat(secondContext.registeredUpdates).containsExactly(Contracts.THREE);
  }

  @Test
  public void updateForBlockOutsideCheck() {
    long blockProductionTime = (long) Math.pow(2, 21);
    TestBlockContext context = new TestBlockContext(FIRST_TIME - 1, blockProductionTime);
    AccountStateLocal localState = AccountStateLocal.initial();
    plugin.updateForBlock(global, localState, context);
    assertThat(context.registeredUpdates).isEmpty();

    TestBlockContext secondContext = new TestBlockContext(FIRST_TIME + 1, blockProductionTime);
    plugin.updateForBlock(global, localState, secondContext);
    assertThat(secondContext.registeredUpdates).isEmpty();
  }

  @Test
  public void updateActiveContractIcedNoStorage() {
    AccountStateLocal stateLocal = AccountStateLocal.initial();
    BlockchainAccountPlugin.IcedContractState<AccountStateLocal, ContractStorage> updated =
        plugin.updateActiveContractIced(
            pluginContextDefault,
            global,
            new ContractState<>(stateLocal, null),
            Contracts.ONE,
            123);
    assertThat(updated.iced()).isFalse();
    assertThat(updated.contractState().local).isSameAs(stateLocal);
    assertThat(updated.contractState().contract).isNull();
  }

  @Test
  public void updateActiveContractIced_gov() {
    AccountStateLocal stateLocal = AccountStateLocal.initial();
    long latestStorageTime = 123456;
    int balance = 5000;
    BlockchainAccountPlugin.IcedContractState<AccountStateLocal, ContractStorage> updated =
        plugin.updateActiveContractIced(
            () -> latestStorageTime + GasAndCoinAccountPlugin.MILLIS_PER_YEAR,
            global,
            new ContractState<>(
                stateLocal,
                new ContractStorage(
                    latestStorageTime,
                    Signed256.create(Unsigned256.create(balance)),
                    0L,
                    FixedList.create(),
                    AvlTree.create(),
                    AvlTree.create(),
                    AvlTree.create(),
                    0)),
            Contracts.GOV_CONTRACT,
            100);
    assertThat(updated).isNotNull();
    assertThat(updated.contractState().contract.getBalance().getAbsoluteValue())
        .isEqualTo(Unsigned256.create(balance));
  }

  @Test
  public void updateActiveContractError() {
    AccountStateLocal stateLocal = AccountStateLocal.initial();
    assertThatThrownBy(
            () ->
                plugin.updateActiveContract(
                    () -> 123456L + GasAndCoinAccountPlugin.MILLIS_PER_YEAR,
                    global,
                    new ContractState<>(
                        stateLocal,
                        new ContractStorage(
                            123456L,
                            Signed256.create(),
                            0L,
                            FixedList.create(),
                            AvlTree.create(),
                            AvlTree.create(),
                            AvlTree.create(),
                            0)),
                    Contracts.GOV_CONTRACT,
                    100))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Deprecated, use instead updateActiveContractIced");
  }

  @Test
  public void migrateAccount() {
    Balance balance = Balance.create();
    StateAccessor accessor = StateAccessor.create(balance);
    StateSerializableEquality.assertStatesEqual(plugin.migrateAccount(accessor), balance);
    BalanceMutable mutable = balance.asMutable();
    int amount = 123;
    mutable.addMpcTokenBalance(amount);
    StateAccessor accessorUpdated = StateAccessor.create(mutable.asImmutable());
    Balance balanceUpdated = plugin.migrateAccount(accessorUpdated);
    assertThat(balanceUpdated.mpcTokens()).isEqualTo(amount);
  }

  @Test
  public void migrateContextFree() {
    AccountStateLocal stateLocal = AccountStateLocal.initial();
    StateAccessor accessor = StateAccessor.create(stateLocal);
    StateSerializableEquality.assertStatesEqual(plugin.migrateContextFree(accessor), stateLocal);

    stateLocal = stateLocal.addBlockchainUsage(1L, Unsigned256.TEN);
    stateLocal = stateLocal.addServiceFee(Accounts.ONE, 100);
    stateLocal =
        stateLocal.updateCollectedCoins(0, Unsigned256.create(100), Unsigned256.create(250));
    StateAccessor accessorUpdated = StateAccessor.create(stateLocal);
    AccountStateLocal updatedLocal = plugin.migrateContextFree(accessorUpdated);
    StateSerializableEquality.assertStatesEqual(
        updatedLocal.convertedExternalCoins().get(0), stateLocal.convertedExternalCoins().get(0));
    StateSerializableEquality.assertStatesEqual(updatedLocal, stateLocal);
  }

  @Test
  public void migrateOldContextFree() {
    OldAccountStateLocal oldAccountStateLocal =
        new OldAccountStateLocal(
            FixedList.create(), AvlTree.create(), AvlTree.create(), Unsigned256.ZERO);
    StateAccessor accessor = StateAccessor.create(oldAccountStateLocal);
    AccountStateLocal migrated = AccountStateLocal.createFromStateAccessor(accessor);
    assertThat(migrated.getRemovedContractsMpcTokens()).isEqualTo(0);
  }

  @Test
  public void migrateNullValuesForAllMigrates() {
    // Migrate global
    AccountStateGlobal nullGlobal = plugin.migrateGlobal(null, null);
    assertThat(nullGlobal.getGasPriceFor1000Network())
        .isEqualTo(AccountStateGlobal.create().getGasPriceFor1000Network());

    // Migrate context fee
    AccountStateLocal nullContextFree = plugin.migrateContextFree(null);
    StateSerializableEquality.assertStatesEqual(nullContextFree, AccountStateLocal.initial());
  }

  @Test
  public void migrateGlobal() {
    StateAccessor accessor = StateAccessor.create(global);

    AccountStateGlobal updatedGlobal = plugin.migrateGlobal(accessor, null);
    assertThat(updatedGlobal.coinSymbols().get(0)).isEqualTo(SYMBOL_0);
    assertThat(updatedGlobal.coinConversionRate(SYMBOL_0)).isEqualTo(SYMBOL_0_CONVERSION_RATE);
    assertThat(updatedGlobal.getGasPriceFor1000Network()).isEqualTo(100);
    assertThat(updatedGlobal.getGasPriceFor1000Storage()).isEqualTo(1000);
    StateSerializableEquality.assertStatesEqual(updatedGlobal, global);

    AccountStateGlobal global2 =
        new AccountStateGlobal()
            .setGasPriceFor1000NetworkInternal(100)
            .setGasPriceFor1000StorageInternal(1000)
            .setCoinInternal(SYMBOL_0, null)
            .setCoinInternal(SYMBOL_1, SYMBOL_1_CONVERSION_RATE);
    StateAccessor accessor2 = StateAccessor.create(global2);
    AccountStateGlobal updatedGlobal2 = plugin.migrateGlobal(accessor2, null);
    assertThat(updatedGlobal2.coinConversionRate(SYMBOL_0)).isNull();
    assertThat(updatedGlobal2.coinConversionRate(SYMBOL_1)).isEqualTo(SYMBOL_1_CONVERSION_RATE);
  }

  @Test
  public void invokeCheckVestedTokens() {
    long tokenGenEvent = TOKEN_GENERATION_EVENT_0;
    byte[] account1Rpc = AccountInvocationRpc.checkVestedTokens();

    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>> result =
        invokeAndCheckState(
            () -> 13 * MILLIS_PER_MONTH,
            global,
            createVestingAccount(2000, tokenGenEvent, DURATION_0, INTERVAL_0, Balance.create()),
            account1Rpc);
    AccountState<AccountStateLocal, Balance> account1 = result.updatedState();
    assertThat(account1.account.mpcTokens())
        .isEqualTo(
            outputAmountForVestedTokenFor3MonthsInterval(
                tokenGenEvent, 13 * MILLIS_PER_MONTH, 36 * MILLIS_PER_MONTH, 2000));
    assertThat(account1.account.releasedVestedTokens(0))
        .isEqualTo(
            outputAmountForVestedTokenFor3MonthsInterval(
                tokenGenEvent, 13 * MILLIS_PER_MONTH, 36 * MILLIS_PER_MONTH, 2000));

    byte[] account2Rpc = AccountInvocationRpc.checkVestedTokens();

    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>> result2 =
        invokeAndCheckState(
            () -> 0,
            global,
            createVestingAccount(
                5000, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0, Balance.create()),
            account2Rpc);
    AccountState<AccountStateLocal, Balance> accountCallingTooEarly = result2.updatedState();

    assertThat(accountCallingTooEarly.account.mpcTokens()).isEqualTo(0);
    assertThat(accountCallingTooEarly.account.releasedVestedTokens(0)).isEqualTo(0);

    byte[] account2RpcAfterTime = AccountInvocationRpc.checkVestedTokens();

    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>> result3 =
        invokeAndCheckState(
            () -> 13 * MILLIS_PER_MONTH,
            global,
            createVestingAccount(
                5000, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0, Balance.create()),
            account2RpcAfterTime);
    AccountState<AccountStateLocal, Balance> account2AfterTime = result3.updatedState();

    assertThat(account2AfterTime.account.mpcTokens())
        .isEqualTo(
            outputAmountForVestedTokenFor3MonthsInterval(
                tokenGenEvent, 13 * MILLIS_PER_MONTH, 36 * MILLIS_PER_MONTH, 5000));
    assertThat(account2AfterTime.account.releasedVestedTokens(0))
        .isEqualTo(
            outputAmountForVestedTokenFor3MonthsInterval(
                tokenGenEvent, 13 * MILLIS_PER_MONTH, 36 * MILLIS_PER_MONTH, 5000));
  }

  @Test
  public void severalVestingAccountsPossible() {
    long tokenGenEvent = TOKEN_GENERATION_EVENT_0;
    byte[] account2Rpc = AccountInvocationRpc.checkVestedTokens();

    BalanceMutable balance = Balance.create().asMutable();
    balance.createVestingAccount(3600, tokenGenEvent, DURATION_0, INTERVAL_0);
    balance.createVestingAccount(2400, tokenGenEvent, DURATION_1, INTERVAL_1);
    AccountState<AccountStateLocal, Balance> doubleVestingAccount =
        new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());

    AccountState<AccountStateLocal, Balance> accountAfterInvocation =
        invokeAndCheckState(
                () -> tokenGenEvent + 3L * MILLIS_PER_MONTH,
                global,
                doubleVestingAccount,
                account2Rpc)
            .updatedState();

    int released0 = 3600 / (36 / 3);
    int released1 = 2400 / (24 / 3);
    assertThat(accountAfterInvocation.account.mpcTokens()).isEqualTo(released0 + released1);
    assertThat(accountAfterInvocation.account.releasedVestedTokens(0)).isEqualTo(released0);
    assertThat(accountAfterInvocation.account.releasedVestedTokens(1)).isEqualTo(released1);
  }

  @Test
  public void vestedAmountToReleaseIsZero() {
    byte[] account1Rpc = AccountInvocationRpc.checkVestedTokens();

    AccountState<AccountStateLocal, Balance> account1 =
        invokeAndCheckState(
                () -> TOKEN_GENERATION_EVENT_0,
                global,
                createVestingAccount(
                    2000, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0, Balance.create()),
                account1Rpc)
            .updatedState();

    assertThat(account1.account.mpcTokens()).isEqualTo(0);
  }

  @Test
  public void invokeCreateVestingAccount() {
    byte[] rpc =
        AccountInvocationRpc.createVestingAccount(
            2000, DURATION_0, INTERVAL_0, TOKEN_GENERATION_EVENT_0);
    AccountState<AccountStateLocal, Balance> account =
        invokeAndCheckState(
                () -> TOKEN_GENERATION_EVENT_0,
                global,
                createVestingAccount(
                    0, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0, Balance.create()),
                rpc)
            .updatedState();

    assertThat(account.account.vestingAccounts()).isNotNull();
    assertThat(account.account.vestingAccounts().size()).isEqualTo(2);
  }

  @Test
  public void twoExemptionsForAdjustTeamTge() {
    long tokenGenEvent = 1604880000000L;

    InitialVestingAccount vestingAccount =
        InitialVestingAccount.create(2000, tokenGenEvent, DURATION_0, INTERVAL_0);
    assertThat(vestingAccount.tokenGenerationEvent).isEqualTo(1604880000000L);
    assertThat(
            InitialVestingAccount.createFromStateAccessor(StateAccessor.create(vestingAccount))
                .tokenGenerationEvent)
        .isEqualTo(1604880000000L);
  }

  @Test
  public void vestedReleasesAllAfterTimestamp() {
    long tokenGenEvent = ACTUAL_TOKEN_GENERATION;

    byte[] account1Rpc = AccountInvocationRpc.checkVestedTokens();

    AccountState<AccountStateLocal, Balance> account1 =
        invokeAndCheckState(
                () -> tokenGenEvent + DURATION_0 + LISTING_PAUSE,
                global,
                createVestingAccount(2000, tokenGenEvent, DURATION_0, INTERVAL_0, Balance.create()),
                account1Rpc)
            .updatedState();

    assertThat(account1.account.mpcTokens()).isEqualTo(2000);
  }

  @Test
  public void vestedReleasesEverythingButLastJustBeforeListingPause() {
    long tokenGenEvent = ACTUAL_TOKEN_GENERATION;
    byte[] account1Rpc = AccountInvocationRpc.checkVestedTokens();

    AccountState<AccountStateLocal, Balance> account2 =
        invokeAndCheckState(
                () -> tokenGenEvent + DURATION_0 + LISTING_PAUSE - 1,
                global,
                createVestingAccount(2000, tokenGenEvent, DURATION_0, INTERVAL_0, Balance.create()),
                account1Rpc)
            .updatedState();
    // Just before the end of listing pause we only get every but the last trance
    assertThat(account2.account.mpcTokens()).isEqualTo(1826L);
  }

  @Test
  public void publicSaleHasNoPause() {
    long tokenGenEvent = ACTUAL_TOKEN_GENERATION;
    byte[] account1Rpc = AccountInvocationRpc.checkVestedTokens();

    AccountState<AccountStateLocal, Balance> account1 =
        invokeAndCheckState(
                () -> tokenGenEvent + 36L * MILLIS_PER_MONTH,
                global,
                createVestingAccount(
                    2000, tokenGenEvent, 62899200000L, 7862400000L, Balance.create()),
                account1Rpc)
            .updatedState();

    assertThat(account1.account.mpcTokens()).isEqualTo(2000);
  }

  @Test
  public void publicSaleEdgeCasesHasPause() {
    long tokenGenEvent = ACTUAL_TOKEN_GENERATION;
    byte[] account1Rpc = AccountInvocationRpc.checkVestedTokens();
    long publicSaleDuration = 62899200000L;
    long publicSaleInterval = 7862400000L;

    AccountState<AccountStateLocal, Balance> account1 =
        invokeAndCheckState(
                () -> tokenGenEvent + 2 * publicSaleDuration,
                global,
                createVestingAccount(
                    2000,
                    tokenGenEvent,
                    2 * publicSaleDuration,
                    publicSaleInterval,
                    Balance.create()),
                account1Rpc)
            .updatedState();
    assertThat(account1.account.mpcTokens()).isEqualTo(1500L);
    account1 =
        invokeAndCheckState(
                () -> tokenGenEvent + LISTING_PAUSE,
                global,
                createVestingAccount(
                    2000,
                    tokenGenEvent,
                    2 * publicSaleDuration,
                    publicSaleInterval,
                    Balance.create()),
                account1Rpc)
            .updatedState();
    assertThat(account1.account.mpcTokens()).isEqualTo(250L);

    AccountState<AccountStateLocal, Balance> account2 =
        invokeAndCheckState(
                () -> tokenGenEvent + publicSaleDuration,
                global,
                createVestingAccount(
                    2000,
                    tokenGenEvent,
                    publicSaleDuration,
                    publicSaleInterval / 2,
                    Balance.create()),
                account1Rpc)
            .updatedState();
    assertThat(account2.account.mpcTokens()).isEqualTo(1125L);
    account2 =
        invokeAndCheckState(
                () -> tokenGenEvent + LISTING_PAUSE,
                global,
                createVestingAccount(
                    2000,
                    tokenGenEvent,
                    publicSaleDuration,
                    publicSaleInterval / 2,
                    Balance.create()),
                account1Rpc)
            .updatedState();
    assertThat(account2.account.mpcTokens()).isEqualTo(625L);
  }

  @Test
  public void invokeVestedForDifferentPlans() {
    long tokenGen = TOKEN_GENERATION_EVENT_1;
    long blockTimestamp = 14 * MILLIS_PER_MONTH;
    byte[] account = AccountInvocationRpc.checkVestedTokens();
    long duration = DURATION_1;

    AccountState<AccountStateLocal, Balance> invokeAccount =
        invokeAndCheckState(
                () -> blockTimestamp,
                global,
                createVestingAccount(22222, tokenGen, DURATION_1, INTERVAL_1, Balance.create()),
                account)
            .updatedState();

    assertThat(invokeAccount.account.mpcTokens())
        .isEqualTo(
            outputAmountForVestedTokenFor3MonthsInterval(
                tokenGen, blockTimestamp, duration, 22222));
    assertThat(invokeAccount.account.releasedVestedTokens(0))
        .isEqualTo(
            outputAmountForVestedTokenFor3MonthsInterval(
                tokenGen, blockTimestamp, duration, 22222));
  }

  @Test
  public void invokeVestedBoundaryTests() {
    long tokenGenEvent = TOKEN_GENERATION_EVENT_0;
    long blockTimestampAccount2AfterLongTime = 67 * MILLIS_PER_MONTH;
    byte[] account2RpcAfter12Months = AccountInvocationRpc.checkVestedTokens();

    AccountState<AccountStateLocal, Balance> account2After12Months =
        invokeAndCheckState(
                () -> blockTimestampAccount2AfterLongTime,
                global,
                createVestingAccount(5000, tokenGenEvent, DURATION_0, INTERVAL_0, Balance.create()),
                account2RpcAfter12Months)
            .updatedState();

    assertThat(account2After12Months.account.mpcTokens())
        .isEqualTo(
            outputAmountForVestedTokenFor3MonthsInterval(
                tokenGenEvent, blockTimestampAccount2AfterLongTime, 36 * MILLIS_PER_MONTH, 5000));

    AccountState<AccountStateLocal, Balance> accountJustAtTge =
        invokeAndCheckState(
                () -> tokenGenEvent,
                global,
                createVestingAccount(5000, tokenGenEvent, DURATION_0, INTERVAL_0, Balance.create()),
                AccountInvocationRpc.checkVestedTokens())
            .updatedState();

    assertThat(accountJustAtTge.account.mpcTokens()).isEqualTo(0);
    assertThat(accountJustAtTge.account.releasedVestedTokens(0)).isEqualTo(0);
  }

  @Test
  public void testVestedTokenForSeveralTimestamps() {
    byte[] accountRpc = AccountInvocationRpc.checkVestedTokens();

    AccountState<AccountStateLocal, Balance> accountAfterFirstCall =
        invokeAndCheckState(
                () -> 0,
                global,
                createVestingAccount(
                    2000, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0, Balance.create()),
                accountRpc)
            .updatedState();
    assertThat(accountAfterFirstCall.account.mpcTokens()).isEqualTo(0);
    assertThat(accountAfterFirstCall.account.releasedVestedTokens(0)).isEqualTo(0);

    long timeStamp6MonthsAfterTokenGenEvent = TOKEN_GENERATION_EVENT_0 + 6 * MILLIS_PER_MONTH;
    AccountState<AccountStateLocal, Balance> accountAfterSecondCall =
        invokeAndCheckState(
                () -> timeStamp6MonthsAfterTokenGenEvent, global, accountAfterFirstCall, accountRpc)
            .updatedState();

    assertThat(accountAfterSecondCall.account.mpcTokens()).isEqualTo(2 * 166);
    assertThat(accountAfterSecondCall.account.releasedVestedTokens(0)).isEqualTo(2 * 166);

    long timestampAfterEntireVestingIsDone = TOKEN_GENERATION_EVENT_0 + 44 * MILLIS_PER_MONTH;
    AccountState<AccountStateLocal, Balance> accountAfterFullVesting =
        invokeAndCheckState(
                () -> timestampAfterEntireVestingIsDone, global, accountAfterSecondCall, accountRpc)
            .updatedState();

    assertThat(accountAfterFullVesting.account.mpcTokens()).isEqualTo(2000);
    assertThat(accountAfterFullVesting.account.vestingAccounts()).isEmpty();
  }

  @Test
  public void testGetAmountToRelease() {
    long tokenGenEvent = 10;

    InitialVestingAccount vestingAccount = InitialVestingAccount.create(2000, tokenGenEvent, 36, 3);

    long amountAfterTimeZero = vestingAccount.getAmountToRelease(0);
    assertThat(amountAfterTimeZero).isEqualTo(0);

    long amountAfterOneInterval = vestingAccount.getAmountToRelease(tokenGenEvent + 3);
    assertThat(amountAfterOneInterval).isEqualTo(166);
  }

  @Test
  public void shouldAccountForAlreadyReleasedTokens() {
    int releaseInterval = 333;
    int releaseDuration = releaseInterval * 3;

    InitialVestingAccount vestingAccount =
        InitialVestingAccount.create(10, 0, releaseDuration, releaseInterval).releaseTokens(3);
    long amount = vestingAccount.getAmountToRelease(2 * 333);
    assertThat(amount).isEqualTo(3);
  }

  @Test
  public void vestingAccountsCanHaveSeveralVestings() {
    BalanceMutable accountBalance = Balance.create().asMutable();
    accountBalance.createVestingAccount(2000, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0);
    AccountState<AccountStateLocal, Balance> account =
        new AccountState<>(AccountStateLocal.initial(), accountBalance.asImmutable());

    BalanceMutable doubleVestingAccountBalance = account.account.asMutable();
    doubleVestingAccountBalance.createVestingAccount(
        5000, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0);
    AccountState<AccountStateLocal, Balance> doubleVestingAccount =
        new AccountState<>(AccountStateLocal.initial(), doubleVestingAccountBalance.asImmutable());

    assertThat(doubleVestingAccount.account.vestingAccounts().get(0).tokens).isEqualTo(2000);
    assertThat(doubleVestingAccount.account.vestingAccounts().get(1).tokens).isEqualTo(5000);
  }

  @Test
  public void invokeAddCoins() {
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            Accounts.THREE,
            AccountInvocationRpc.addCoinBalance(SYMBOL_1, Unsigned256.create("10")));
    AccountState<AccountStateLocal, Balance> updated =
        invokeAndCheckState(null, global, setCoinAccount(0), update.getRpc()).updatedState();
    assertThat(updated.account.coinBalance(0)).isEqualTo(Unsigned256.create(0));
    assertThat(updated.account.coinBalance(1)).isEqualTo(Unsigned256.create(10));
  }

  @Test
  public void invokeDeductCoins() {
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            Accounts.THREE,
            AccountInvocationRpc.deductCoinBalance(SYMBOL_2, Unsigned256.create("10")));
    AccountState<AccountStateLocal, Balance> updated =
        invokeAndCheckState(null, global, setCoinAccount(10), update.getRpc()).updatedState();
    assertThat(updated.account.coinBalance(0)).isEqualTo(Unsigned256.create(0));
    assertThat(updated.account.coinBalance(2)).isEqualTo(Unsigned256.create(0));
  }

  @Test
  public void deductTooMuchByoc() {
    Unsigned256 toDeduct = Unsigned256.create("11");
    long balance = 10L;
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            Accounts.ONE, AccountInvocationRpc.deductCoinBalance(SYMBOL_2, toDeduct));
    assertThatThrownBy(
            () ->
                invokeAndCheckState(null, global, setCoinAccount(balance), update.getRpc())
                    .updatedState())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining(
            "Cannot deduct %s %s with a balance of %s".formatted(toDeduct, SYMBOL_2, balance));
  }

  @Test
  public void invokeConvertBlockTimeToEpoch() {
    long epoch = GasAndCoinAccountPlugin.convertBlockProductionTimeToEpoch(0);
    assertThat(epoch).isEqualTo(0);
    long epoch2 = GasAndCoinAccountPlugin.convertBlockProductionTimeToEpoch((long) Math.pow(2, 21));
    assertThat(epoch2).isEqualTo(1);
  }

  @Test
  public void invokeInvalidInvocation() {
    assertThatThrownBy(
            () -> {
              byte[] rpcBytes =
                  SafeDataOutputStream.serialize(
                      stream -> stream.writeByte(INVALID_INVOCATION_BYTE));
              invokeAndCheckState(pluginContextDefault, global, tokenAccount(3), rpcBytes);
            })
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Unknown invocation in Account Plugin (Account) with invocation byte: 100");
    assertThatThrownBy(
            () -> {
              byte[] rpcBytes =
                  SafeDataOutputStream.serialize(
                      stream -> stream.writeByte(INVALID_INVOCATION_BYTE));
              plugin.invokeContextFree(pluginContextDefault, null, null, rpcBytes);
            })
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage(
            "Unknown invocation in Account Plugin (Context-Free) with invocation byte: 100");
    assertThatThrownBy(
            () -> {
              byte[] rpcBytes =
                  SafeDataOutputStream.serialize(
                      stream -> stream.writeByte(INVALID_INVOCATION_BYTE));
              plugin.invokeContract(pluginContextDefault, null, null, Contracts.ONE, rpcBytes);
            })
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Unknown invocation in Account Plugin (Contract) with invocation byte: 100");
  }

  @Test
  public void checkNoDuplicateInvocationBytes() {
    // Account invocations
    List<Byte> accountInvocations = new ArrayList<>();
    for (AccountInvocation invocation : AccountInvocation.values()) {
      byte invocationByte = invocation.getInvocationByte();
      assertThat(accountInvocations).doesNotContain(invocationByte);
      accountInvocations.add(invocationByte);
    }
    // Context free invocations
    List<Byte> contextFreeInvocations = new ArrayList<>();
    for (ContextFreeInvocation invocation : ContextFreeInvocation.values()) {
      byte invocationByte = invocation.getInvocationByte();
      assertThat(contextFreeInvocations).doesNotContain(invocationByte);
      contextFreeInvocations.add(invocationByte);
    }
    // GlobalInteraction
    List<Byte> globalInteractions = new ArrayList<>();
    for (AccountStateGlobal.GlobalInteraction globalInteraction :
        AccountStateGlobal.GlobalInteraction.values()) {
      assertThat(globalInteractions.contains(globalInteraction.getInteractionByte())).isFalse();
      globalInteractions.add(globalInteraction.getInteractionByte());
    }
    // Context free invocations
    List<Byte> contractInvocations = new ArrayList<>();
    for (ContractInvocation invocation : ContractInvocation.values()) {
      byte invocationByte = invocation.getInvocationByte();
      assertThat(contractInvocations).doesNotContain(invocationByte);
      contractInvocations.add(invocationByte);
    }
  }

  @Test
  public void invokeAddAndDeductMpcToken() {
    long toDeduct = 10;
    long balance = 3;
    assertThatThrownBy(
            () -> {
              byte[] rpcBytes = AccountInvocationRpc.deductMpcTokenBalance(toDeduct);
              invokeAndCheckState(pluginContextDefault, global, tokenAccount(balance), rpcBytes);
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Cannot deduct %s MPC tokens with a balance of %s".formatted(toDeduct, balance));

    assertThatThrownBy(
            () -> {
              byte[] rpcBytes = AccountInvocationRpc.deductMpcTokenBalance(0);
              invokeAndCheckState(pluginContextDefault, global, tokenAccount(3), rpcBytes);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot deduct a non-positive amount of tokens");

    assertThatThrownBy(
            () -> {
              invokeAndCheckState(
                  pluginContextDefault,
                  global,
                  tokenAccount(3),
                  AccountInvocationRpc.addMpcTokenBalance(0));
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot add a non-positive amount of tokens");

    byte[] tenPlusThree = AccountInvocationRpc.addMpcTokenBalance(10);
    AccountState<AccountStateLocal, Balance> update =
        invokeAndCheckState(pluginContextDefault, global, tokenAccount(3), tenPlusThree)
            .updatedState();
    assertThat(update.account.mpcTokens()).isEqualTo(13);

    byte[] deductTenFromSeven = AccountInvocationRpc.deductMpcTokenBalance(7);
    AccountState<AccountStateLocal, Balance> update2 =
        invokeAndCheckState(pluginContextDefault, global, tokenAccount(10), deductTenFromSeven)
            .updatedState();
    assertThat(update2.account.mpcTokens()).isEqualTo(3);

    byte[] deductAll = AccountInvocationRpc.deductMpcTokenBalance(10);
    AccountState<AccountStateLocal, Balance> update3 =
        invokeAndCheckState(pluginContextDefault, global, tokenAccount(10), deductAll)
            .updatedState();
    assertThat(update3.account.mpcTokens()).isEqualTo(0);
  }

  /** It is not possible to deduct MPC tokens from an account with a custodian enabled. */
  @Test
  public void cannotDeductMpcWithCustodian() {
    AccountState<AccountStateLocal, Balance> accountWithCustodian =
        appointAndAcceptCustodian(tokenAccount(101), firstAddress);
    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                        pluginContextDefault,
                        global,
                        accountWithCustodian,
                        AccountInvocationRpc.deductMpcTokenBalance(10))
                    .updatedState())
        .hasMessageContaining("Can not deduct MPC tokens for an account with a custodian");
  }

  @Test
  public void invokeGetTokenBalance() {
    byte[] getTokens = AccountInvocationRpc.getStakedTokenBalance();
    AccountState<AccountStateLocal, Balance> before = stakeAccount(10);
    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>> result =
        invokeAndCheckState(pluginContextDefault, global, before, getTokens);
    SafeDataInputStream returnValueInputStream =
        SafeDataInputStream.createFromBytes(result.result());
    long returnedBalance = returnValueInputStream.readLong();
    assertThat(returnedBalance).isEqualTo(10);
  }

  @Test
  public void invokeGetTokenBalanceWithDelegates() {
    byte[] getTokens = AccountInvocationRpc.getStakedTokenBalance();
    BalanceMutable balance = Balance.create().asMutable();
    balance.commitReceiveDelegatedStakes(firstAddress, 10, null);
    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>> pendingDelegatedStakes =
        invokeAndCheckState(
            pluginContextDefault,
            global,
            new AccountState<>(AccountStateLocal.initial(), balance.asImmutable()),
            getTokens);
    SafeDataInputStream returnValueInputStream =
        SafeDataInputStream.createFromBytes(pendingDelegatedStakes.result());
    long tokensForJob = returnValueInputStream.readLong();
    assertThat(tokensForJob).isEqualTo(0);

    balance.acceptPendingDelegatedStakes(firstAddress, 10);
    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>> acceptDelegatedStakes =
        invokeAndCheckState(
            pluginContextDefault,
            global,
            new AccountState<>(AccountStateLocal.initial(), balance.asImmutable()),
            getTokens);
    SafeDataInputStream stakesForJobReturnValue =
        SafeDataInputStream.createFromBytes(acceptDelegatedStakes.result());
    tokensForJob = stakesForJobReturnValue.readLong();
    assertThat(tokensForJob).isEqualTo(10);
  }

  @Test
  public void invokeStakeTokens() {
    long errorValue = 10;
    long tokenBalance = 3;
    AccountState<AccountStateLocal, Balance> accountState = tokenAccount(tokenBalance);
    assertThatThrownBy(
            () -> {
              byte[] rpcBytes = AccountInvocationRpc.stakeTokens(errorValue);
              invokeAndCheckState(pluginContextDefault, global, accountState, rpcBytes);
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(stakeTooMuchErrorMessage(errorValue, tokenBalance, 0, 0));

    assertThatThrownBy(
            () -> {
              byte[] rpcBytes = AccountInvocationRpc.stakeTokens(0);
              invokeAndCheckState(pluginContextDefault, global, tokenAccount(3), rpcBytes);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot stake a non-positive amount of tokens");

    byte[] stakeFive = AccountInvocationRpc.stakeTokens(7);
    AccountState<AccountStateLocal, Balance> update =
        invokeAndCheckState(pluginContextDefault, global, tokenAccount(10), stakeFive)
            .updatedState();
    assertThat(update.account.stakedTokens()).isEqualTo(7);
    assertThat(update.account.mpcTokens()).isEqualTo(3);
  }

  @Test
  public void invokeStakeTwice() {
    long initialTokenBalance = 13;
    AccountState<AccountStateLocal, Balance> accountState = tokenAccount(initialTokenBalance);
    long firstStakeAmount = 5;
    byte[] firstStake = AccountInvocationRpc.stakeTokens(firstStakeAmount);

    AccountState<AccountStateLocal, Balance> initialStakeState =
        invokeAndCheckState(pluginContextDefault, global, accountState, firstStake).updatedState();
    assertThat(initialStakeState.account.stakedTokens()).isEqualTo(firstStakeAmount);
    assertThat(initialStakeState.account.mpcTokens())
        .isEqualTo(initialTokenBalance - firstStakeAmount);

    long secondStakeAmount = 7;
    byte[] secondStake = AccountInvocationRpc.stakeTokens(secondStakeAmount);

    AccountState<AccountStateLocal, Balance> secondStakeState =
        invokeAndCheckState(pluginContextDefault, global, initialStakeState, secondStake)
            .updatedState();
    assertThat(secondStakeState.account.stakedTokens())
        .isEqualTo(firstStakeAmount + secondStakeAmount);
    assertThat(secondStakeState.account.mpcTokens())
        .isEqualTo(initialTokenBalance - firstStakeAmount - secondStakeAmount);
  }

  @Test
  public void invokeStakeWithNonStakeableAccount() {
    BalanceMutable nonStakeable = Balance.create().asMutable();
    nonStakeable.makeNonStakeable();
    nonStakeable.addMpcTokenBalance(10);
    AccountState<AccountStateLocal, Balance> nonStakeableAccount =
        new AccountState<>(AccountStateLocal.initial(), nonStakeable.asImmutable());

    assertThatThrownBy(
            () -> {
              byte[] rpcBytes = AccountInvocationRpc.stakeTokens(7);
              invokeAndCheckState(pluginContextDefault, global, nonStakeableAccount, rpcBytes);
            })
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("This account cannot stake");
  }

  @Test
  public void checkBalanceNullSafe() {
    AccountState<AccountStateLocal, Balance> nullBalanceAccount =
        new AccountState<>(AccountStateLocal.initial(), null);
    invokeAndCheckState(
        pluginContextDefault,
        global,
        nullBalanceAccount,
        AccountInvocationRpc.addMpcTokenBalance(10));
  }

  @Test
  @DisplayName("Associating to a contract with an existing association, only updates amount.")
  void associatingToContractDoesNotPutItToNonExpiring() {
    BalanceMutable balance = Balance.create().asMutable();
    long expiration = 20L;
    long delegationAmount = 20;
    balance =
        delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress, expiration);
    balance =
        delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress, expiration);

    balance = associateWithTimestamp(balance, delegationAmount, expiration, Contracts.ONE);

    assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
        .isEqualTo(expiration);
    assertThat(balance.stakedToContract().getValue(Contracts.ONE).amount)
        .isEqualTo(delegationAmount);

    balance.addMpcTokenBalance(delegationAmount);
    balance.stakeTokens(delegationAmount);
    balance = associate(balance, delegationAmount, Contracts.ONE);

    assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
        .isEqualTo(expiration);
    assertThat(balance.stakedToContract().getValue(Contracts.ONE).amount)
        .isEqualTo(delegationAmount * 2);
  }

  @Nested
  @DisplayName(
      "Accounts automatically regulates amount of accepted delegated stakes when positive staking"
          + " goal is set.")
  final class AutomationOfDelegatedStakesTest {

    @Nested
    @DisplayName("Setting the expiration timestamp for an association to a contract.")
    final class SetAssociationTimestamp {

      @Test
      @DisplayName("Earlier than current expiration, updates the expiration timestamp.")
      void setAssociationExpirationEarlierThanCurrentAlwaysWorks() {
        BalanceMutable balance = Balance.create().asMutable();
        long amount = 200;
        balance.addMpcTokenBalance(amount);
        balance.stakeTokens(amount);
        balance = associate(balance, 100, Contracts.ONE);

        assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp).isNull();

        long expirationTimestamp = 100L;
        balance = setAssociationExpirationTime(balance, expirationTimestamp, Contracts.ONE);

        assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
            .isEqualTo(expirationTimestamp);
      }

      @Test
      @DisplayName(
          "To never expire for a contract that is already associated to, when no more unused tokens"
              + " are available, succeeds.")
      void stakedToContractShouldNotCountWhenOnlyMovingTimestamp() {
        BalanceMutable balance = Balance.create().asMutable();
        long expirationTimestamp = 100L;
        balance = delegateAndAccept(balance, 500L, 500L, firstAddress);
        balance = delegateAndAccept(balance, 500L, 500L, secondAddress);

        balance.associateWithExpiration(Contracts.ONE, 500L, expirationTimestamp);
        balance.associateWithExpiration(Contracts.TWO, 500L, expirationTimestamp);
        balance = setAssociationExpirationTime(balance, null, Contracts.TWO);

        assertThat(unusedStakes(balance)).isEqualTo(0);
        assertThat(balance.stakedToContract().getValue(Contracts.TWO).expirationTimestamp).isNull();
      }

      @Test
      @DisplayName("That has not been associated to, throws exception.")
      void setAssociationExpirationToContractNotAssociatedToFails() {
        BalanceMutable balance = Balance.create().asMutable();
        long amount = 100;
        balance.addMpcTokenBalance(amount);
        balance.stakeTokens(amount);

        assertThatThrownBy(() -> setAssociationExpirationTime(balance, 100L, Contracts.ONE))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage(
                "Cannot update association expiration timestamp for contract that has not been"
                    + " associated to");
      }

      @Test
      @DisplayName("To zero, updates expiration timestamp.")
      void setZeroExpirationForAssociationFails() {
        BalanceMutable balance = Balance.create().asMutable();
        long amount = 100;
        balance.addMpcTokenBalance(amount);
        balance.stakeTokens(amount);
        balance.associateWithExpiration(Contracts.ONE, amount, null);

        assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp).isNull();
        long zeroTimestamp = 0L;
        balance = setAssociationExpirationTime(balance, zeroTimestamp, Contracts.ONE);
        assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
            .isEqualTo(zeroTimestamp);
      }

      @Test
      @DisplayName("To a negative value, throws exception.")
      void setNegativeExpirationForAssociationFails() {
        BalanceMutable balance = Balance.create().asMutable();
        assertThatThrownBy(() -> setAssociationExpirationTime(balance, -1L, Contracts.ONE))
            .isInstanceOf(RuntimeException.class)
            .hasMessage("Expiration timestamp cannot be negative");
      }

      @Test
      @DisplayName(
          "Later than current expiration timestamp, with not enough delegated"
              + " stakes at new expiration timestamp, throws exception.")
      void setLaterExpirationForAssociationWithInsufficientDelegatedStakesFails() {
        BalanceMutable balance = Balance.create().asMutable();
        long delegationAmount = 20;
        long lowDelegationAmount = 10;
        long earlyDelegationExpirationTimestamp = 20L;
        long lateDelegationExpirationTimestamp = 100L;
        balance =
            delegateAndAccept(
                balance,
                delegationAmount,
                delegationAmount,
                firstAddress,
                earlyDelegationExpirationTimestamp);
        balance =
            delegateAndAccept(
                balance,
                lowDelegationAmount,
                lowDelegationAmount,
                secondAddress,
                lateDelegationExpirationTimestamp);

        BalanceMutable afterAssociation =
            associateWithTimestamp(
                balance, delegationAmount, earlyDelegationExpirationTimestamp, Contracts.ONE);
        assertThat(afterAssociation.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
            .isEqualTo(earlyDelegationExpirationTimestamp);

        long lateAssociationExpiration = 40L;
        assertThatThrownBy(
                () ->
                    setAssociationExpirationTime(
                        afterAssociation, lateAssociationExpiration, Contracts.ONE))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Associations cannot be covered");
      }

      @Test
      @DisplayName(
          "Later than current expiration timestamp, with all stakes associated, throws exception.")
      void setLaterExpirationForAssociationWithInsufficientDelegatedStakes() {
        BalanceMutable balance = Balance.create().asMutable();
        long delegationAmount = 20;
        long lowDelegationAmount = 10;
        long earlyDelegationExpirationTimestamp = 20L;
        long lateDelegationExpirationTimestamp = 100L;
        balance =
            delegateAndAccept(
                balance,
                delegationAmount,
                delegationAmount,
                firstAddress,
                earlyDelegationExpirationTimestamp);
        balance =
            delegateAndAccept(
                balance,
                lowDelegationAmount,
                lowDelegationAmount,
                secondAddress,
                lateDelegationExpirationTimestamp);

        balance =
            associateWithTimestamp(
                balance, lowDelegationAmount, lateDelegationExpirationTimestamp, Contracts.TWO);
        assertThat(balance.stakedToContract().getValue(Contracts.TWO).expirationTimestamp)
            .isEqualTo(lateDelegationExpirationTimestamp);

        BalanceMutable afterAssociation =
            associateWithTimestamp(
                balance, delegationAmount, earlyDelegationExpirationTimestamp, Contracts.ONE);
        assertThat(afterAssociation.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
            .isEqualTo(earlyDelegationExpirationTimestamp);

        long lateAssociationExpiration = 40L;
        assertThatThrownBy(
                () ->
                    setAssociationExpirationTime(
                        afterAssociation, lateAssociationExpiration, Contracts.ONE))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Associations cannot be covered");
      }
    }

    @Nested
    @DisplayName("Associating to a contract with expiration timestamp.")
    final class AssociateWithTimestamp {

      @Test
      @DisplayName("Of zero, successfully associates to contract that expires immediately.")
      void associateWithZeroExpirationSucceeds() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 20;
        balance.addMpcTokenBalance(delegationAmount);
        balance.stakeTokens(delegationAmount);
        long expirationTimestamp = 0L;

        balance =
            associateWithTimestamp(balance, delegationAmount, expirationTimestamp, Contracts.ONE);
        assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
            .isEqualTo(expirationTimestamp);
      }

      @Test
      @DisplayName(
          "When the contract is already associated to, updates both"
              + " the expiration and the amount.")
      void associatingWithNewTimestampUpdatesBothTimestampAndAmount() {
        BalanceMutable balance = Balance.create().asMutable();
        long lateExpiration = 20L;
        long delegationAmount = 20;
        balance =
            delegateAndAccept(
                balance, delegationAmount, delegationAmount, firstAddress, lateExpiration);
        balance =
            delegateAndAccept(
                balance, delegationAmount, delegationAmount, secondAddress, lateExpiration);

        balance = associateWithTimestamp(balance, delegationAmount, lateExpiration, Contracts.ONE);

        assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
            .isEqualTo(lateExpiration);
        assertThat(balance.stakedToContract().getValue(Contracts.ONE).amount)
            .isEqualTo(delegationAmount);

        long earlyExpiration = 10L;
        balance = associateWithTimestamp(balance, delegationAmount, earlyExpiration, Contracts.ONE);

        assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
            .isEqualTo(earlyExpiration);
        assertThat(balance.stakedToContract().getValue(Contracts.ONE).amount)
            .isEqualTo(delegationAmount * 2);
      }

      @Test
      @DisplayName("While all your staked tokens are associated, throws exception.")
      public void cannotAssociateMoreThanDelegated() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 1;
        balance =
            delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress, 100L);
        balance =
            delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress, 100L);
        balance =
            delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress, 100L);

        BalanceMutable balanceNew = associateWithTimestamp(balance, 3, 50L, Contracts.ONE);
        assertThatThrownBy(() -> associateWithTimestamp(balanceNew, 3, 60L, Contracts.TWO))
            .hasMessage("Associations cannot be covered");
      }

      @Test
      @DisplayName(
          "While all your staked tokens are associated to different contracts, throws exception.")
      public void cannotAssociateMoreThanDelegatedDifferentAmounts() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 1;
        balance =
            delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress, 100L);
        balance =
            delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress, 100L);
        balance =
            delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress, 100L);

        balance = associateWithTimestamp(balance, 2, 50L, Contracts.THREE);
        BalanceMutable balanceNew = associateWithTimestamp(balance, 1, 51L, Contracts.ONE);
        assertThatThrownBy(() -> associateWithTimestamp(balanceNew, 3, 52L, Contracts.TWO))
            .hasMessage("Associations cannot be covered");
      }

      @Test
      @DisplayName(
          "When delegated stakes at timestamp has been used on"
              + " another association, throws exception.")
      public void associateMultipleTimesWithTooLongExpiration() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 20;
        balance.addMpcTokenBalance(delegationAmount);
        balance.stakeTokens(delegationAmount);

        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress);
        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress);
        balance = setDelegationExpirationTime(balance, 10L, secondAddress);
        balance = setDelegationExpirationTime(balance, 30L, firstAddress);
        balance = setDelegationExpirationTime(balance, 30L, thirdAddress);

        balance.setStakingGoal(100);

        checkDelegationInfo(balance, firstAddress, 30L, 0, delegationAmount);
        checkDelegationInfo(balance, secondAddress, 10L, 0, delegationAmount);
        checkDelegationInfo(balance, thirdAddress, 30L, 0, delegationAmount);

        assertThat(unusedStakes(balance)).isEqualTo(delegationAmount * 4);

        balance = associate(balance, delegationAmount, Contracts.FOUR);
        balance = associateWithTimestamp(balance, delegationAmount, 20L, Contracts.THREE);
        BalanceMutable lastSuccessfulAssociation =
            associateWithTimestamp(balance, delegationAmount, 30L, Contracts.ONE);

        assertThatThrownBy(
                () ->
                    associateWithTimestamp(
                        lastSuccessfulAssociation, delegationAmount, 20L, Contracts.TWO))
            .isInstanceOf(RuntimeException.class)
            .hasMessage("Associations cannot be covered");
      }

      @Test
      @DisplayName("That is negative, throws exception.")
      void associateWithNegativeExpirationFails() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 20;
        balance.addMpcTokenBalance(delegationAmount);
        balance.stakeTokens(delegationAmount);

        assertThatThrownBy(
                () -> associateWithTimestamp(balance, delegationAmount, -1L, Contracts.ONE))
            .isInstanceOf(RuntimeException.class)
            .hasMessage("Expiration timestamp cannot be negative");
      }

      @Test
      @DisplayName("And a negative amount, throws exception.")
      public void associateWithExpirationCannotHaveNegativeAmount() {
        assertThatThrownBy(
                () -> {
                  byte[] associateZero =
                      AccountInvocationRpc.associateWithExpiration(0, Contracts.ONE, 0L);
                  invokeAndCheckState(
                      pluginContextDefault, global, tokenAccount(100), associateZero);
                })
            .isInstanceOf(RuntimeException.class)
            .hasMessage("Cannot associate non-positive amount");
      }
    }

    @Nested
    @DisplayName("Setting the staking goal.")
    final class StakingGoalTest {

      @Test
      @DisplayName("As a non-custodian, when a custodian has been appointed, throws an exception.")
      void invokeSetStakingGoalAsIncorrectCustodian() {
        long initialStakedBalance = 10;
        Balance accountBefore =
            appointAndAcceptCustodian(stakeAccount(initialStakedBalance), secondAddress).account;

        byte[] illegalCustodianInvocation = AccountInvocationRpc.setStakingGoal(firstAddress, 10);
        assertErrorThrownInInvoke(
            illegalCustodianInvocation,
            accountBefore,
            "Only the custodian of the account is allowed to send this invocation");
      }

      @Test
      @DisplayName(
          "To a value that affects delegators with different timestamps, accepts the best"
              + " delegations.")
      void threeExpirationBuckets() {
        BalanceMutable mutable = Balance.create().asMutable();

        mutable = delegateAndAccept(mutable, 10, 10, firstAddress, 10L);
        mutable = delegateAndAccept(mutable, 10, 5, secondAddress, 7L);
        mutable = delegateAndAccept(mutable, 10, 5, thirdAddress, 5L);

        mutable.setStakingGoal(13);

        checkDelegationInfo(mutable, firstAddress, 10L, 0, 10);
        checkDelegationInfo(mutable, secondAddress, 7L, 7, 3);
        checkDelegationInfo(mutable, thirdAddress, 5L, 10, 0);
      }

      @Test
      @DisplayName("Can be done by the appointed custodian.")
      public void invokeSetStakingGoalTokensAsCustodian() {
        long initialStakedBalance = 10;
        Balance accountBefore =
            appointAndAcceptCustodian(stakeAccount(initialStakedBalance), secondAddress).account;

        long stakingGoal = 7;
        byte[] setStakingGoalRpc = AccountInvocationRpc.setStakingGoal(secondAddress, stakingGoal);

        Balance accountAfter = invokeAccount(accountBefore, setStakingGoalRpc);
        assertThat(accountAfter.asMutable().stakingGoal()).isEqualTo(stakingGoal);
      }

      @Test
      @DisplayName("To a negative value, throws an exception.")
      void stakingGoalCannotBeNegative() {
        BalanceMutable accountStateBalance = Balance.create().asMutable();
        AccountState<AccountStateLocal, Balance> accountState =
            new AccountState<>(AccountStateLocal.initial(), accountStateBalance.asImmutable());

        long newStakingGoal = -1;
        byte[] setStakingGoalRpc = AccountInvocationRpc.setStakingGoal(null, newStakingGoal);

        assertThatThrownBy(
                () ->
                    invokeAndCheckState(() -> 0, global, accountState, setStakingGoalRpc)
                        .updatedState())
            .isInstanceOf(RuntimeException.class)
            .hasMessage("Staking goal cannot be negative");
      }

      @Test
      @DisplayName(
          "Higher than the current amount of staked tokens to jobs, accepts"
              + " pending delegated to reach the goal.")
      void changingStakingGoalAcceptsPendingDelegatedStakes() {
        long delegateAmount = 10;
        byte[] initiateReceiveDelegatedStakes =
            AccountInvocationRpc.initiateReceiveDelegatedStakes(
                EMPTY, delegateAmount, firstAddress);
        AccountState<AccountStateLocal, Balance> recipientAfterCommit =
            successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(1000));
        BalanceMutable balanceAfterCommit = recipientAfterCommit.account.asMutable();

        initiateReceiveDelegatedStakes =
            AccountInvocationRpc.initiateReceiveDelegatedStakes(
                EMPTY, delegateAmount, secondAddress);
        recipientAfterCommit =
            successfullyDelegateStakes(
                initiateReceiveDelegatedStakes,
                new AccountState<>(AccountStateLocal.initial(), balanceAfterCommit.asImmutable()));
        balanceAfterCommit = recipientAfterCommit.account.asMutable();

        balanceAfterCommit.setStakingGoal(delegateAmount);
        assertThat(getSumOfAcceptedStakes(balanceAfterCommit)).isEqualTo(delegateAmount);
      }

      @Test
      @DisplayName(
          "Higher than the current amount of staked tokens to jobs, accepts"
              + " pending delegated to reach the goal.")
      void changingStakingGoalWhileHavingPendingStakesAcceptsThem() {
        long delegateAmount = 10;
        long firstAcceptAmount = 10;

        byte[] initiateReceiveDelegatedStakes =
            AccountInvocationRpc.initiateReceiveDelegatedStakes(
                EMPTY, delegateAmount, firstAddress);
        AccountState<AccountStateLocal, Balance> recipientAfterCommit =
            successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(1000));
        BalanceMutable balance = recipientAfterCommit.account.asMutable();
        balance.acceptPendingDelegatedStakes(firstAddress, firstAcceptAmount);

        initiateReceiveDelegatedStakes =
            AccountInvocationRpc.initiateReceiveDelegatedStakes(
                EMPTY, delegateAmount, secondAddress);
        recipientAfterCommit =
            successfullyDelegateStakes(
                initiateReceiveDelegatedStakes,
                new AccountState<>(AccountStateLocal.initial(), balance.asImmutable()));
        balance = recipientAfterCommit.account.asMutable();

        long secondAcceptAmount = 5;
        balance.acceptPendingDelegatedStakes(secondAddress, secondAcceptAmount);

        long stakedTokens = 30;
        balance.stakeTokens(stakedTokens);

        long totalAcceptedAmount = firstAcceptAmount + secondAcceptAmount;
        long totalStakedTokens = stakedTokens + totalAcceptedAmount;
        assertThat(getSumOfAcceptedStakes(balance)).isEqualTo(totalAcceptedAmount);
        assertThat(balance.stakedTokens()).isEqualTo(stakedTokens);
        assertThat(balance.stakeForJobs()).isEqualTo(totalStakedTokens);

        long pendingStakesToBeAutoAccepted = 1;
        balance.setStakingGoal(totalStakedTokens + pendingStakesToBeAutoAccepted);

        assertThat(getSumOfAcceptedStakes(balance))
            .isEqualTo(totalAcceptedAmount + pendingStakesToBeAutoAccepted);
      }

      @Test
      @DisplayName(
          "To not be reached by the accounts own staked tokens, redistributes accepted tokens to"
              + " delegators with the latest expiration timestamp.")
      void prioritizeDelegatedStakesWithLaterExpirationTimestamp() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 20;
        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress);
        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress);

        long earliestExpiration = 10L;
        long latestExpiration = 20L;
        balance = setDelegationExpirationTime(balance, earliestExpiration, secondAddress);
        balance = setDelegationExpirationTime(balance, latestExpiration, firstAddress);

        balance.setStakingGoal(50);

        checkDelegationInfo(balance, firstAddress, latestExpiration, 0, delegationAmount);
        checkDelegationInfo(balance, secondAddress, earliestExpiration, 10, 10);
        checkDelegationInfo(balance, thirdAddress, null, 0, delegationAmount);
      }

      @Test
      @DisplayName("Lower than staked for jobs, does not affect staked tokens or accepted tokens.")
      void stakedTokensAndAcceptedDelegatedStakesAreNotAffectedByChangesToStakingGoal() {
        BalanceMutable balance = Balance.create().asMutable();
        long stakedTokens = 100;
        balance.addMpcTokenBalance(stakedTokens);
        balance.stakeTokens(stakedTokens);
        assertThat(balance.stakedTokens()).isEqualTo(stakedTokens);
        assertThat(getSumOfAcceptedStakes(balance)).isEqualTo(0);

        balance.setStakingGoal(50);
        assertThat(balance.stakedTokens()).isEqualTo(stakedTokens);
        assertThat(getSumOfAcceptedStakes(balance)).isEqualTo(0);

        balance.setStakingGoal(stakedTokens);
        assertThat(balance.stakedTokens()).isEqualTo(stakedTokens);
        assertThat(getSumOfAcceptedStakes(balance)).isEqualTo(0);
      }

      @Test
      @DisplayName("To a non-negative value, changes the goal for the account.")
      void changingTheStakingGoalOfAccountChangesTheStakingGoal() {
        BalanceMutable accountStateBalance = Balance.create().asMutable();
        AccountState<AccountStateLocal, Balance> accountState =
            new AccountState<>(AccountStateLocal.initial(), accountStateBalance.asImmutable());

        long stakingGoal = 50;
        byte[] setStakingGoalRpc = AccountInvocationRpc.setStakingGoal(null, stakingGoal);
        AccountState<AccountStateLocal, Balance> invokeSetStakingGoalState =
            invokeAndCheckState(() -> 0, global, accountState, setStakingGoalRpc).updatedState();

        assertThat(invokeSetStakingGoalState.account.asMutable().stakingGoal())
            .isEqualTo(stakingGoal);

        stakingGoal = 0;
        setStakingGoalRpc = AccountInvocationRpc.setStakingGoal(null, stakingGoal);
        invokeSetStakingGoalState =
            invokeAndCheckState(() -> 0, global, accountState, setStakingGoalRpc).updatedState();

        assertThat(invokeSetStakingGoalState.account.asMutable().stakingGoal())
            .isEqualTo(stakingGoal);
      }

      @Test
      @DisplayName(
          "To be reached by the accounts own staked tokens, puts all accepted delegated stakes to"
              + " pending.")
      void setStakingGoalToAmountOfStakedTokensRedistributesAcceptedDelegatedStakes() {
        BalanceMutable balance = Balance.create().asMutable();
        long stakeAmount = 100;
        balance.addMpcTokenBalance(stakeAmount);
        balance.stakeTokens(stakeAmount);

        long delegationAmount = 20;
        balance = delegateAndAccept(balance, delegationAmount, 5, firstAddress);
        balance = delegateAndAccept(balance, delegationAmount, 1, secondAddress);
        balance = delegateAndAccept(balance, delegationAmount, 9, thirdAddress);
        checkDelegationInfo(balance, firstAddress, null, delegationAmount - 5, 5);
        checkDelegationInfo(balance, secondAddress, null, delegationAmount - 1, 1);
        checkDelegationInfo(balance, thirdAddress, null, delegationAmount - 9, 9);

        balance.setStakingGoal(stakeAmount);

        checkDelegationInfo(balance, firstAddress, null, delegationAmount, 0);
        checkDelegationInfo(balance, secondAddress, null, delegationAmount, 0);
        checkDelegationInfo(balance, thirdAddress, null, delegationAmount, 0);
      }

      @Test
      @DisplayName(
          "To not be reached by the accounts own staked tokens, prioritizes own staked tokens to"
              + " reach the staking goal, thereafter delegated stakes gets accepted to reach the"
              + " staking goal.")
      void setStakingGoalLowerThanStakedAmountAcceptsDelegatedStakes() {
        BalanceMutable balance = Balance.create().asMutable();
        long stakeAmount = 100;
        balance.addMpcTokenBalance(stakeAmount);
        balance.stakeTokens(stakeAmount);

        long delegationAmount = 20;
        long acceptAmount = 5;
        balance = delegateAndAccept(balance, delegationAmount, acceptAmount, firstAddress);
        checkDelegationInfo(
            balance, firstAddress, null, delegationAmount - acceptAmount, acceptAmount);

        balance.setStakingGoal(stakeAmount + 1);

        checkDelegationInfo(balance, firstAddress, null, delegationAmount - 1, 1);
      }

      @Test
      @DisplayName(
          "To not be reached by the accounts own staked tokens, auto accepts delegated stakes until"
              + " staking goal is reached, whereafter delegated stakes becomes pending.")
      void delegatingStakesToAccountWithUnfulfilledStakingGoalAutoAcceptsDelegatedStakes() {
        BalanceMutable accountStateBalance = Balance.create().asMutable();
        long stakingGoal = 50;
        accountStateBalance.addMpcTokenBalance(1000);
        accountStateBalance.setStakingGoal(stakingGoal);
        assertThat(getSumOfAcceptedStakes(accountStateBalance)).isEqualTo(0);

        AccountState<AccountStateLocal, Balance> accountState =
            new AccountState<>(AccountStateLocal.initial(), accountStateBalance.asImmutable());
        long delegatedAmount = 60;
        byte[] initiateReceiveDelegatedStakes =
            AccountInvocationRpc.initiateReceiveDelegatedStakes(
                EMPTY, delegatedAmount, firstAddress);
        AccountState<AccountStateLocal, Balance> recipientAfterCommit =
            successfullyDelegateStakes(initiateReceiveDelegatedStakes, accountState);

        BalanceMutable balanceAfterCommit = recipientAfterCommit.account.asMutable();
        long pendingDelegatedStakeFromDelegator =
            balanceAfterCommit
                .asImmutable()
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes;
        assertThat(getSumOfAcceptedStakes(balanceAfterCommit)).isEqualTo(stakingGoal);
        assertThat(pendingDelegatedStakeFromDelegator).isEqualTo(delegatedAmount - stakingGoal);
      }
    }

    @Test
    @DisplayName("To a non zero amount, makes an account unable to manually accept delegation.")
    void acceptDelegatedStakesWithSetStakingGoalThrowsException() {
      BalanceMutable balance = Balance.create().asMutable();
      balance.setStakingGoal(100);

      assertThatThrownBy(() -> delegateAndAccept(balance, 10, 10, firstAddress))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Cannot manually accept delegated stakes if a staking goal has been set");
    }

    @Nested
    @DisplayName("Setting the expiration timestamp for delegated stakes.")
    final class ExpiringDelegatedStakesTest {

      @Test
      @DisplayName(
          "To the same value for multiple delegators, the last delegator to set their expiration"
              + " timestamp, gets de-prioritized.")
      void redistributionOnlyMakesChangesIfNecessary() {
        BalanceMutable mutable = Balance.create().asMutable();

        mutable = delegateAndAccept(mutable, 10, 10, firstAddress, 10L);
        mutable = delegateAndAccept(mutable, 10, 5, secondAddress, 7L);
        mutable = delegateAndAccept(mutable, 10, 5, thirdAddress, 5L);

        mutable.setStakingGoal(13);

        checkDelegationInfo(mutable, firstAddress, 10L, 0, 10);
        checkDelegationInfo(mutable, secondAddress, 7L, 7, 3);
        checkDelegationInfo(mutable, thirdAddress, 5L, 10, 0);

        long expirationTimestamp = 100L;
        mutable = setDelegationExpirationTime(mutable, expirationTimestamp, firstAddress);
        mutable = setDelegationExpirationTime(mutable, expirationTimestamp, thirdAddress);
        mutable = setDelegationExpirationTime(mutable, expirationTimestamp, secondAddress);

        checkDelegationInfo(mutable, firstAddress, expirationTimestamp, 0, 10);
        checkDelegationInfo(mutable, secondAddress, expirationTimestamp, 10, 0);
        checkDelegationInfo(mutable, thirdAddress, expirationTimestamp, 7, 3);
      }

      @Test
      @DisplayName(
          "Earlier than the current delegation, through delegation invocation,does not change"
              + " the expiration date, only the amount delegated.")
      void delegatingStakesWithNonExpiringTimestampReturnsPreviousDelegation() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 10;
        balance = delegateAndAccept(balance, delegationAmount, 0, firstAddress);
        assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).expirationTimestamp)
            .isNull();
        assertThat(
                balance.delegatedStakesFromOthers().getValue(firstAddress).pendingDelegatedStakes)
            .isEqualTo(delegationAmount);

        Long earlyTimestamp = 10L;
        balance = delegateAndAccept(balance, delegationAmount, 0, firstAddress, earlyTimestamp);

        assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).expirationTimestamp)
            .isNull();
        assertThat(
                balance.delegatedStakesFromOthers().getValue(firstAddress).pendingDelegatedStakes)
            .isEqualTo(delegationAmount * 2);
      }

      @Test
      @DisplayName("To an earlier timestamp, is possible if none of them are accepted.")
      void setEarlierExpirationTimestampForDelegatedStakesNotAccepted() {
        BalanceMutable balance = Balance.create().asMutable();
        long delegationAmount = 100;
        BalanceMutable balanceAfterDelegation =
            delegateAndAccept(balance, delegationAmount, 0, firstAddress);
        assertThat(
                balanceAfterDelegation
                    .delegatedStakesFromOthers()
                    .getValue(firstAddress)
                    .expirationTimestamp)
            .isNull();

        long newExpirationTimestamp = 100;
        BalanceMutable afterLaterExpiration =
            setDelegationExpirationTime(
                balanceAfterDelegation, newExpirationTimestamp, firstAddress);

        newExpirationTimestamp = Math.floorDiv(newExpirationTimestamp, 2);
        BalanceMutable afterEarlierTimestamp =
            setDelegationExpirationTime(afterLaterExpiration, newExpirationTimestamp, firstAddress);
        assertThat(
                afterEarlierTimestamp
                    .delegatedStakesFromOthers()
                    .getValue(firstAddress)
                    .expirationTimestamp)
            .isEqualTo(newExpirationTimestamp);
      }

      @Test
      @DisplayName("To an earlier timestamp, can be done if they are accepted and retractable.")
      void setEarlierExpirationTimestampForAcceptedAndRetractableDelegatedStakes() {
        BalanceMutable balance = Balance.create().asMutable();
        long delegationAmount = 100;
        BalanceMutable balanceAfterAccept =
            delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
        assertThat(
                balanceAfterAccept
                    .delegatedStakesFromOthers()
                    .getValue(firstAddress)
                    .expirationTimestamp)
            .isNull();

        long newExpirationTimestamp = 100;
        BalanceMutable afterLaterExpiration =
            setDelegationExpirationTime(balanceAfterAccept, newExpirationTimestamp, firstAddress);

        newExpirationTimestamp = Math.floorDiv(newExpirationTimestamp, 2);
        BalanceMutable afterEarlierExpiration =
            setDelegationExpirationTime(afterLaterExpiration, newExpirationTimestamp, firstAddress);
        assertThat(
                afterEarlierExpiration
                    .delegatedStakesFromOthers()
                    .getValue(firstAddress)
                    .expirationTimestamp)
            .isEqualTo(newExpirationTimestamp);
      }

      @Test
      @DisplayName(
          "Later than the current expiration"
              + " timestamp for delegator, updates expiration timestamp.")
      void committingDelegatedStakesWithLaterExpirationUpdatesExpiration() {
        BalanceMutable balance = Balance.create().asMutable();
        long earliestExpiration = 10L;
        long latestExpiration = 20L;
        balance = delegateAndAccept(balance, 20, 20, firstAddress, earliestExpiration);
        balance = delegateAndAccept(balance, 20, 20, firstAddress, latestExpiration);
        assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).expirationTimestamp)
            .isEqualTo(latestExpiration);
      }

      @Test
      @DisplayName(
          "To the an earlier timestamp as the current expiration timestamp while the"
              + " delegation are not retractable, throws exception.")
      void setSameExpirationTimestampForNonRetractableDelegatedStakes() {
        BalanceMutable balance = Balance.create().asMutable();
        long delegationAmount = 100;
        BalanceMutable balanceAfterAccept =
            delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
        assertThat(
                balanceAfterAccept
                    .delegatedStakesFromOthers()
                    .getValue(firstAddress)
                    .expirationTimestamp)
            .isNull();

        long newExpirationTimestamp = 100;
        BalanceMutable afterLaterExpiration =
            setDelegationExpirationTime(balanceAfterAccept, newExpirationTimestamp, firstAddress);

        long associationAmount = 1;
        BalanceMutable balanceAfterAssociate =
            associateWithTimestamp(
                afterLaterExpiration, associationAmount, newExpirationTimestamp, Contracts.ONE);

        assertThat(balanceAfterAssociate).isNotNull();
        assertThat(balanceAfterAssociate.stakedToContract().getValue(Contracts.ONE).amount)
            .isEqualTo(associationAmount);

        long reducedTimestamp = Math.floorDiv(newExpirationTimestamp, 2);
        assertThatThrownBy(
                () ->
                    setDelegationExpirationTime(
                        balanceAfterAssociate, reducedTimestamp, firstAddress))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Associations cannot be covered");
      }

      @Test
      @DisplayName(
          "Lower than the current expiration, through delegation invocation"
              + " does not update expiration timestamp.")
      void committingDelegatedStakesWithEarlierTimestampDoesNothing() {
        BalanceMutable balance = Balance.create().asMutable();
        long latestExpiration = 20L;
        long earliestExpiration = 10L;
        balance = delegateAndAccept(balance, 20, 20, firstAddress, latestExpiration);
        balance = delegateAndAccept(balance, 20, 20, firstAddress, earliestExpiration);

        assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).expirationTimestamp)
            .isEqualTo(latestExpiration);
      }

      @Test
      @DisplayName(
          "Through delegation invocation, redistributes accepted"
              + " tokens to delegators with the latest expiration timestamp.")
      void delegatedStakesWithEarlierExpirationTimestampHasLessPriority() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 20;
        long earliestExpiration = 10L;
        long latestExpiration = 20L;
        balance =
            delegateAndAccept(
                balance, delegationAmount, delegationAmount, firstAddress, earliestExpiration);
        balance =
            delegateAndAccept(
                balance, delegationAmount, delegationAmount, secondAddress, latestExpiration);
        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress);
        balance.setStakingGoal(50);

        checkDelegationInfo(balance, firstAddress, earliestExpiration, 10, 10);
        checkDelegationInfo(balance, secondAddress, latestExpiration, 0, delegationAmount);
        checkDelegationInfo(balance, thirdAddress, null, 0, delegationAmount);
      }

      @Test
      @DisplayName("That does not expire and is not retractable, throws exception.")
      void setExpirationTimestampForNonExpiringNonRetractableDelegatedStakes() {
        BalanceMutable balance = Balance.create().asMutable();
        long delegationAmount = 100;
        BalanceMutable balanceAfterAccept =
            delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
        assertThat(
                balanceAfterAccept
                    .delegatedStakesFromOthers()
                    .getValue(firstAddress)
                    .expirationTimestamp)
            .isNull();

        BalanceMutable afterLaterExpiration =
            setDelegationExpirationTime(balanceAfterAccept, null, firstAddress);

        long associationAmount = 1;
        byte[] associateRpc = AccountInvocationRpc.associate(associationAmount, Contracts.ONE);
        AccountState<AccountStateLocal, Balance> associateOne =
            invokeAndCheckState(
                    pluginContextDefault,
                    global,
                    new AccountState<>(
                        AccountStateLocal.initial(), afterLaterExpiration.asImmutable()),
                    associateRpc)
                .updatedState();
        assertThat(associateOne.account).isNotNull();
        assertThat(associateOne.account.getAmountAssociationToContract(Contracts.ONE))
            .isEqualTo(associationAmount);

        long newExpirationTimestamp = 100;
        assertThatThrownBy(
                () ->
                    setDelegationExpirationTime(
                        associateOne.account.asMutable(), newExpirationTimestamp, firstAddress))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Associations cannot be covered");
      }

      @Test
      @DisplayName("Through delegation with expiration to your own account, throws exception.")
      public void delegatingStakesWithExpirationTimestampToSelfGivesError() {
        byte[] initiateDelegateStakes =
            AccountInvocationRpc.initiateDelegateStakes(EMPTY, 5, firstAddress);
        byte[] initiateReceiveDelegatedStakes =
            AccountInvocationRpc.initiateReceiveDelegatedStakesWithExpiration(
                EMPTY, 5, firstAddress, 10L);

        AccountState<AccountStateLocal, Balance> account = tokenAccount(50);

        AccountState<AccountStateLocal, Balance> accountAfterDeposit =
            invokeAndCheckState(
                    pluginContextDefault, global, account, initiateReceiveDelegatedStakes)
                .updatedState();
        assertThatThrownBy(
                () ->
                    invokeAndCheckState(
                        pluginContextDefault, global, accountAfterDeposit, initiateDelegateStakes))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Cannot perform any delegation of stakes between the same account");

        AccountState<AccountStateLocal, Balance> accountAfterWithdraw =
            invokeAndCheckState(pluginContextDefault, global, account, initiateDelegateStakes)
                .updatedState();
        assertThatThrownBy(
                () ->
                    invokeAndCheckState(
                        pluginContextDefault,
                        global,
                        accountAfterWithdraw,
                        initiateReceiveDelegatedStakes))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Cannot perform any delegation of stakes between the same account");
      }

      @Test
      @DisplayName("As a non-custodian, when a custodian has been appointed, throws an exception.")
      void invokeSetExpirationTimestampForStakedTokensAsIncorrectCustodian() {
        long initialBalance = 10;
        Balance appointCustodian =
            appointAndAcceptCustodian(tokenAccount(initialBalance), secondAddress).account;

        byte[] illegalCustodianInvocation =
            AccountInvocationRpc.setExpirationTimestampForDelegatedStakes(
                firstAddress, firstAddress, 10L);
        assertErrorThrownInInvoke(
            illegalCustodianInvocation,
            appointCustodian,
            "Only the custodian of the account is allowed to send this invocation");
      }

      @Test
      @DisplayName("To null, results in the delegation never expiring.")
      void setDelegationExpirationTimestampToNeverExpire() {
        BalanceMutable balance = Balance.create().asMutable();

        long expirationTimestamp = 100L;
        balance = delegateAndAccept(balance, 10, 10, firstAddress, expirationTimestamp);
        assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).expirationTimestamp)
            .isEqualTo(expirationTimestamp);

        byte[] setExpirationRpc =
            AccountInvocationRpc.setExpirationTimestampForDelegatedStakes(null, firstAddress, null);
        Balance accountAfterInvocation = invokeAccount(balance.asImmutable(), setExpirationRpc);
        assertThat(
                accountAfterInvocation
                    .delegatedStakesFromOthers()
                    .getValue(firstAddress)
                    .expirationTimestamp)
            .isNull();
      }

      @Test
      @DisplayName(
          "To null, when the delegation is used for association, results"
              + " in the delegation never expiring.")
      void setDelegationExpirationTimestampToNeverExpireForAssociatedDelegatedStakes() {
        BalanceMutable balance = Balance.create().asMutable();

        long expirationTimestamp = 100L;
        balance = delegateAndAccept(balance, 10, 10, firstAddress, expirationTimestamp);
        assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).expirationTimestamp)
            .isEqualTo(expirationTimestamp);

        balance = associateWithTimestamp(balance, 10, 100L, Contracts.ONE);

        byte[] setExpirationRpc =
            AccountInvocationRpc.setExpirationTimestampForDelegatedStakes(null, firstAddress, null);
        Balance accountAfterInvocation = invokeAccount(balance.asImmutable(), setExpirationRpc);
        assertThat(
                accountAfterInvocation
                    .delegatedStakesFromOthers()
                    .getValue(firstAddress)
                    .expirationTimestamp)
            .isNull();
      }

      @Test
      @DisplayName("For an account that has not been delegated to, throws exception.")
      void setDelegationExpirationTimestampForAccountThatHasNotBeenDelegatedToFails() {
        BalanceMutable balance = Balance.create().asMutable();

        byte[] setExpirationRpc =
            AccountInvocationRpc.setExpirationTimestampForDelegatedStakes(null, firstAddress, null);
        assertThatThrownBy(() -> invokeAccount(balance.asImmutable(), setExpirationRpc))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage(
                "Cannot update delegation expiration timestamp for account that has not been"
                    + " delegated to");
      }

      @Test
      @DisplayName("Later than other delegators, gets your delegation accepted instead of theirs.")
      void extendingExpirationTimestampGetsYourDelegatedStakesPrioritized() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 20;
        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress);
        balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress);

        long earliestExpiration = 10L;
        long latestExpiration = 20L;
        balance = setDelegationExpirationTime(balance, latestExpiration, secondAddress);
        balance = setDelegationExpirationTime(balance, earliestExpiration, firstAddress);
        balance.setStakingGoal(50);

        checkDelegationInfo(balance, firstAddress, earliestExpiration, 10, 10);
        checkDelegationInfo(balance, secondAddress, latestExpiration, 0, delegationAmount);
        checkDelegationInfo(balance, thirdAddress, null, 0, delegationAmount);

        long newLatestExpiration = 30L;
        balance = setDelegationExpirationTime(balance, newLatestExpiration, firstAddress);

        checkDelegationInfo(balance, firstAddress, newLatestExpiration, 0, delegationAmount);
        checkDelegationInfo(balance, secondAddress, latestExpiration, 10, 10);
        checkDelegationInfo(balance, thirdAddress, null, 0, delegationAmount);
      }

      @Test
      @DisplayName(
          "To a later timestamp, through delegation invocation, redistributes and accepts the new"
              + " delegation.")
      void prioritizeBetterDelegationAtCommit() {
        BalanceMutable balance = Balance.create().asMutable();
        long delegationAmount = 20;
        balance.setStakingGoal(delegationAmount);

        long earlyExpiration = 20L;
        balance = delegateAndAccept(balance, delegationAmount, 0L, firstAddress, earlyExpiration);
        assertThat(
                balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
            .isEqualTo(delegationAmount);

        long lateExpiration = 30L;
        balance = delegateAndAccept(balance, delegationAmount, 0L, secondAddress, lateExpiration);
        assertThat(
                balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
            .isEqualTo(0);
        assertThat(
                balance.delegatedStakesFromOthers().getValue(secondAddress).acceptedDelegatedStakes)
            .isEqualTo(delegationAmount);
      }

      @Test
      @DisplayName("Earlier than current expiration timestamp redistributes delegated stakes.")
      void delegatedStakesGetsRedistributedWhenExpirationTimestampGetsUpdated() {
        BalanceMutable balance = Balance.create().asMutable();

        long delegationAmount = 20;
        balance = delegateAndAccept(balance, delegationAmount, 5, firstAddress);
        balance = delegateAndAccept(balance, delegationAmount, 1, secondAddress);
        balance = delegateAndAccept(balance, delegationAmount, 9, thirdAddress);
        checkDelegationInfo(balance, firstAddress, null, delegationAmount - 5, 5);
        checkDelegationInfo(balance, secondAddress, null, delegationAmount - 1, 1);
        checkDelegationInfo(balance, thirdAddress, null, delegationAmount - 9, 9);

        long earliestTimestamp = 10L;
        balance = setDelegationExpirationTime(balance, earliestTimestamp, thirdAddress);
        balance.setStakingGoal(39);

        checkDelegationInfo(balance, firstAddress, null, 0, delegationAmount);
        checkDelegationInfo(balance, secondAddress, null, 1, delegationAmount - 1);
        checkDelegationInfo(balance, thirdAddress, earliestTimestamp, delegationAmount, 0);
      }

      @Test
      @DisplayName("To a non-positive value, through delegation invocation, throws exception.")
      void delegationWithExpirationTimestampMustHavePositiveAmount() {
        BalanceMutable balance = Balance.create().asMutable();
        assertThatThrownBy(() -> delegateAndAccept(balance, 0, 20, firstAddress, 10L))
            .isInstanceOf(RuntimeException.class)
            .hasMessage(
                "Cannot initiate the receive part of a stake delegation with a non-positive amount"
                    + " of tokens");

        assertThatThrownBy(() -> delegateAndAccept(balance, -1, 20, firstAddress, 10L))
            .isInstanceOf(RuntimeException.class)
            .hasMessage(
                "Cannot initiate the receive part of a stake delegation with a non-positive amount"
                    + " of tokens");
      }

      @Test
      @DisplayName("Can be done by the appointed custodian.")
      public void invokeSetExpirationTimestampForStakedTokensAsCustodian() {
        long initialStakedBalance = 10;
        Balance appointCustodian =
            appointAndAcceptCustodian(stakeAccount(initialStakedBalance), secondAddress).account;

        byte[] initiateReceiveDelegatedStakes =
            AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 100, firstAddress);
        AccountState<AccountStateLocal, Balance> recipientAfterCommit =
            successfullyDelegateStakes(
                initiateReceiveDelegatedStakes,
                new AccountState<>(AccountStateLocal.initial(), appointCustodian));

        long timestamp = 7;
        byte[] setExpirationRpc =
            AccountInvocationRpc.setExpirationTimestampForDelegatedStakes(
                secondAddress, firstAddress, timestamp);

        Balance accountAfterInvocation =
            invokeAccount(recipientAfterCommit.account, setExpirationRpc);
        assertThat(
                accountAfterInvocation
                    .asMutable()
                    .delegatedStakesFromOthers()
                    .getValue(firstAddress)
                    .expirationTimestamp)
            .isEqualTo(timestamp);
      }

      @Test
      @DisplayName(
          "Through delegation with expiration as custodian, with no custodian appointed,"
              + " throws exception.")
      void initiateDelegateStakesWithExpirationAsCustodianWhenNoCustodianAppointed() {
        byte[] illegalCustodianInvocation =
            AccountInvocationRpc.initiateDelegateStakes(firstAddress, EMPTY, 10, secondAddress);
        assertErrorThrownInInvoke(
            illegalCustodianInvocation,
            tokenAccount(100),
            "Only the owner of the account is allowed to send this invocation");
      }
    }

    @Test
    @DisplayName(
        "Setting a later expiration timestamp for association to contract with enough delegated"
            + " stakes at new expiration timestamp, updates expiration timestamp for association.")
    void setLaterExpirationForAssociationWithSufficientDelegatedStakes() {
      BalanceMutable balance = Balance.create().asMutable();
      long delegationAmount = 20;
      long delegationExpirationTimestamp = 100L;
      balance =
          delegateAndAccept(
              balance,
              delegationAmount,
              delegationAmount,
              firstAddress,
              delegationExpirationTimestamp);
      balance =
          delegateAndAccept(
              balance,
              delegationAmount,
              delegationAmount,
              secondAddress,
              delegationExpirationTimestamp);

      long earlyAssociationExpiration = 20L;
      balance =
          associateWithTimestamp(
              balance, delegationAmount * 2, earlyAssociationExpiration, Contracts.ONE);
      assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
          .isEqualTo(earlyAssociationExpiration);

      long lateAssociationExpiration = 40L;
      balance = setAssociationExpirationTime(balance, lateAssociationExpiration, Contracts.ONE);
      assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
          .isEqualTo(lateAssociationExpiration);
    }

    @Test
    @DisplayName(
        "Using delegated stakes with later expiration than the expiration of the association,"
            + " successfully associates to contracts.")
    public void associateWithExpirationTimestampEarlierThanAllDelegatedStakes() {
      BalanceMutable balance = Balance.create().asMutable();
      long delegationAmount = 20;
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress);

      long laterExpiration = 20L;
      balance = setDelegationExpirationTime(balance, laterExpiration, secondAddress);
      balance = setDelegationExpirationTime(balance, laterExpiration, firstAddress);

      balance.setStakingGoal(50);

      checkDelegationInfo(balance, firstAddress, laterExpiration, 0, delegationAmount);
      checkDelegationInfo(balance, secondAddress, laterExpiration, 0, delegationAmount);

      assertThat(unusedStakes(balance)).isEqualTo(delegationAmount * 2);

      long earlierExpiration = 10L;
      balance = associateWithTimestamp(balance, delegationAmount, earlierExpiration, Contracts.ONE);
      balance = associateWithTimestamp(balance, delegationAmount, earlierExpiration, Contracts.TWO);

      assertThat(unusedStakes(balance)).isEqualTo(0);
      assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
          .isEqualTo(earlierExpiration);
      assertThat(balance.stakedToContract().getValue(Contracts.TWO).expirationTimestamp)
          .isEqualTo(earlierExpiration);
    }

    @Test
    @DisplayName(
        "Associating with multiple different expiration timestamps that matches the expiration"
            + " timestamp of the delegations is successful.")
    public void associateMultipleTimesWithDifferentTimestamp() {
      BalanceMutable balance = Balance.create().asMutable();

      long delegationAmount = 20;
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress);
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress);

      balance = setDelegationExpirationTime(balance, 10L, secondAddress);
      balance = setDelegationExpirationTime(balance, 20L, firstAddress);
      balance = setDelegationExpirationTime(balance, 30L, thirdAddress);

      balance.setStakingGoal(100);

      checkDelegationInfo(balance, firstAddress, 20L, 0, delegationAmount);
      checkDelegationInfo(balance, secondAddress, 10L, 0, delegationAmount);
      checkDelegationInfo(balance, thirdAddress, 30L, 0, delegationAmount);

      assertThat(unusedStakes(balance)).isEqualTo(delegationAmount * 3);

      balance = associateWithTimestamp(balance, delegationAmount, 30L, Contracts.THREE);
      balance = associateWithTimestamp(balance, delegationAmount, 20L, Contracts.ONE);
      balance = associateWithTimestamp(balance, delegationAmount, 10L, Contracts.TWO);

      assertThat(unusedStakes(balance)).isEqualTo(0);
    }

    @Test
    @DisplayName(
        "Non-expiring delegated stakes can be used for both non-expiring association and"
            + " associations that expire.")
    public void associateMultipleTimesWithNonExpiringTimestamp() {
      BalanceMutable balance = Balance.create().asMutable();

      long delegationAmount = 20;
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress);
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress);

      balance.setStakingGoal(100);

      checkDelegationInfo(balance, firstAddress, null, 0, delegationAmount);
      checkDelegationInfo(balance, secondAddress, null, 0, delegationAmount);
      checkDelegationInfo(balance, thirdAddress, null, 0, delegationAmount);

      assertThat(unusedStakes(balance)).isEqualTo(delegationAmount * 3);

      balance = associate(balance, delegationAmount, Contracts.THREE);
      balance = associate(balance, delegationAmount, Contracts.ONE);
      balance = associateWithTimestamp(balance, delegationAmount, 10L, Contracts.TWO);

      assertThat(unusedStakes(balance)).isEqualTo(0);
    }

    /**
     * Associating an amount lower than the unused stakes, but the amount cannot be covered at the
     * new expiration, throws exception.
     */
    @Test
    public void validTotalAmountInvalidAtExpiration() {
      BalanceMutable balance = Balance.create().asMutable();

      long delegationAmount = 20;
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress, 50L);
      balance = delegateAndAccept(balance, 1, 1, secondAddress, 100L);

      BalanceMutable afterAssociation =
          associateWithTimestamp(balance, delegationAmount - 1, 50L, Contracts.THREE);

      assertThatThrownBy(() -> associateWithTimestamp(afterAssociation, 1, 100L, Contracts.THREE))
          .isInstanceOf(RuntimeException.class)
          .hasMessage("Associations cannot be covered");
    }

    @Test
    @DisplayName(
        "Successfully delegating stakes with no expiration, while having a delegation with an"
            + " expiring timestamp, only updates the amount, not the expiration")
    void delegateStakeWithoutExpirationWhileHavingDelegationWithExpiration() {
      BalanceMutable balance = Balance.create().asMutable();
      long expiringTimestamp = 20L;
      long delegationAmount = 20;
      balance =
          delegateAndAccept(
              balance, delegationAmount, delegationAmount, firstAddress, expiringTimestamp);

      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).expirationTimestamp)
          .isEqualTo(expiringTimestamp);
      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
          .isEqualTo(delegationAmount);

      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).expirationTimestamp)
          .isEqualTo(expiringTimestamp);
      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
          .isEqualTo(delegationAmount * 2);
    }

    @Test
    @DisplayName(
        "Returning stakes that reduces the stake for jobs below the staking goal, accepts as many"
            + " pending delegated stakes as possible from other accounts until the staking goal is"
            + " reached.")
    public void returnDelegatedStakesTriggerAutoAcceptOfOtherPendingStakes() {
      BalanceMutable balance = Balance.create().asMutable();
      long delegateAmount = 10;
      long initialAcceptAmount = 5;
      balance.addMpcTokenBalance(delegateAmount);
      balance.commitReceiveDelegatedStakes(firstAddress, delegateAmount, null);
      balance.acceptPendingDelegatedStakes(firstAddress, initialAcceptAmount);
      balance.commitReceiveDelegatedStakes(secondAddress, delegateAmount, null);
      balance.acceptPendingDelegatedStakes(secondAddress, initialAcceptAmount);

      long stakingGoal = 10;
      balance.setStakingGoal(stakingGoal);
      AccountState<AccountStateLocal, Balance> accountState =
          new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());

      assertThat(getSumOfAcceptedStakes(accountState.account.asMutable()))
          .isEqualTo(initialAcceptAmount * 2);
      long returnAmount = 6;
      AccountState<AccountStateLocal, Balance> beforeCommit =
          invokeAndCheckState(
                  () -> 0,
                  global,
                  accountState,
                  AccountInvocationRpc.initiateReturnDelegatedStakes(
                      EMPTY, returnAmount, firstAddress))
              .updatedState();

      AvlTree<BlockchainAddress, StakesFromOthers> stakesFromOthers =
          beforeCommit.account.asMutable().delegatedStakesFromOthers();

      assertThat(getSumOfAcceptedStakes(beforeCommit.account.asMutable())).isEqualTo(stakingGoal);
      assertThat(stakesFromOthers.getValue(secondAddress).acceptedDelegatedStakes)
          .isEqualTo(returnAmount);

      AccountState<AccountStateLocal, Balance> afterCommit =
          invokeAndCheckState(
                  () -> 0, global, beforeCommit, AccountInvocationRpc.commitDelegateStakes(EMPTY))
              .updatedState();

      stakesFromOthers = afterCommit.account.asMutable().delegatedStakesFromOthers();
      assertThat(getSumOfAcceptedStakes(afterCommit.account.asMutable())).isEqualTo(delegateAmount);
      assertThat(stakesFromOthers.getValue(firstAddress).pendingDelegatedStakes).isEqualTo(0);
      assertThat(stakesFromOthers.getValue(firstAddress).acceptedDelegatedStakes)
          .isEqualTo(stakingGoal - returnAmount);
      assertThat(stakesFromOthers.getValue(secondAddress).pendingDelegatedStakes)
          .isEqualTo(delegateAmount - returnAmount);
      assertThat(stakesFromOthers.getValue(secondAddress).acceptedDelegatedStakes)
          .isEqualTo(returnAmount);
    }

    @Test
    @DisplayName(
        "Successfully delegating stakes with timestamp, updates the amount of stakes delegated to"
            + " account.")
    void successfullyDelegatingStakesWithTimestampUpdatesDelegatorsAccount() {
      long delegationAmount = 10;
      byte[] initiateDelegateStakes =
          AccountInvocationRpc.initiateDelegateStakes(EMPTY, delegationAmount, secondAddress);

      AccountState<AccountStateLocal, Balance> receiveDelegatedStakes =
          successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(15));

      assertThat(receiveDelegatedStakes.account.delegatedStakesToOthers().getValue(secondAddress))
          .isEqualTo(delegationAmount);
    }

    @Test
    @DisplayName(
        "When multiple delegators has the same expiration timestamp on delegated stakes, the order"
            + " of the set expiration timestamp invocation is the tiebreaker.")
    void orderAsTiebreakerForDelegatedStakes() {
      BalanceMutable balance = Balance.create().asMutable();

      long delegationAmount = 20;
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress);
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress);

      long expirationTimestamp = 20L;
      balance = setDelegationExpirationTime(balance, expirationTimestamp, secondAddress);
      balance = setDelegationExpirationTime(balance, expirationTimestamp, firstAddress);
      balance.setStakingGoal(50);

      checkDelegationInfo(balance, firstAddress, expirationTimestamp, 10, 10);
      checkDelegationInfo(balance, secondAddress, expirationTimestamp, 0, delegationAmount);
      checkDelegationInfo(balance, thirdAddress, null, 0, delegationAmount);
    }

    @Test
    @DisplayName(
        "When multiple delegators has the same expiration timestamp, the address is the"
            + " tiebreaker.")
    void blockchainAddressAsTiebreakerForDelegatedStakes() {
      BalanceMutable balance = Balance.create().asMutable();

      long delegationAmount = 20;
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress);
      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress);

      balance.setStakingGoal(50);

      checkDelegationInfo(balance, thirdAddress, null, 0, delegationAmount);
      checkDelegationInfo(balance, firstAddress, null, 10, 10);
      checkDelegationInfo(balance, secondAddress, null, 0, delegationAmount);
    }

    @Test
    @DisplayName(
        "When the delegator with the latest expiration timestamp cannot accept more delegated"
            + " stakes, the delegator with the second-latest expiration timestamp gets"
            + " prioritized.")
    void noneExpiringDelegatorHasNoPendingToAcceptFrom() {
      BalanceMutable balance = Balance.create().asMutable();

      balance = delegateAndAccept(balance, 15, 15, firstAddress);
      balance = delegateAndAccept(balance, 10, 5, secondAddress);
      balance = delegateAndAccept(balance, 10, 10, thirdAddress);

      long earliestExpiration = 10L;
      long latestExpiration = 20L;
      balance = setDelegationExpirationTime(balance, latestExpiration, secondAddress);
      balance = setDelegationExpirationTime(balance, earliestExpiration, thirdAddress);
      balance.setStakingGoal(25);

      checkDelegationInfo(balance, firstAddress, null, 0, 15);
      checkDelegationInfo(balance, secondAddress, latestExpiration, 0, 10);
      checkDelegationInfo(balance, thirdAddress, earliestExpiration, 10, 0);
    }

    @Test
    @DisplayName(
        "Staking more tokens while having a positive staking goal, results in less accepted"
            + " delegated stakes.")
    void stakeTokensRedistributesDelegatedStakes() {
      BalanceMutable balance = Balance.create().asMutable();
      long balanceAmount = 100;
      long delegationAmount = 50;
      balance.addMpcTokenBalance(balanceAmount);
      balance.stakeTokens(delegationAmount);

      balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);

      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
          .isEqualTo(delegationAmount);
      assertThat(balance.stakedTokens()).isEqualTo(delegationAmount);

      balance.setStakingGoal(balanceAmount);

      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
          .isEqualTo(delegationAmount);

      long additionalStakingAmount = 25;
      balance.stakeTokens(additionalStakingAmount);

      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
          .isEqualTo(delegationAmount - additionalStakingAmount);
      assertThat(balance.stakedTokens()).isEqualTo(delegationAmount + additionalStakingAmount);
    }

    /**
     * Account with a positive staking goal automatically accepts pending delegated after unstaking
     * tokens.
     */
    @Test
    void redistributeAfterUnstake() {
      BalanceMutable balance = Balance.create().asMutable();
      balance.addMpcTokenBalance(100L);
      balance.stakeTokens(100L);
      balance.setStakingGoal(100L);

      balance = delegateAndAccept(balance, 75L, 0L, firstAddress);

      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
          .isEqualTo(0);
      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).pendingDelegatedStakes)
          .isEqualTo(75);

      long unstakeAmount = 50;
      balance.unstakeTokens(unstakeAmount, 0L);

      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
          .isEqualTo(unstakeAmount);
      assertThat(balance.delegatedStakesFromOthers().getValue(firstAddress).pendingDelegatedStakes)
          .isEqualTo(25);
    }

    @Test
    @DisplayName(
        "It is only possible to associate tokens to a contract with an expiration timestamp if"
            + " there are enough stakes for jobs available in the entire time period until the"
            + " expiration time.")
    public void cannotAssociateWhenTooFewStakesAtIntermediateTime() {
      BalanceMutable balance = Balance.create().asMutable();

      balance = delegateAndAccept(balance, 50_000, 50_000, firstAddress);
      balance = delegateAndAccept(balance, 25_000, 25_000, secondAddress, 100L);

      balance = associate(balance, 25_000, Contracts.ONE);
      balance = associateWithTimestamp(balance, 25_000, 200L, Contracts.TWO);

      BalanceMutable finalBalance = balance;
      assertThatThrownBy(() -> associateWithTimestamp(finalBalance, 25_000, 300L, Contracts.THREE))
          .hasMessage("Associations cannot be covered");
    }

    @Test
    @DisplayName(
        "Trying to unstake staked tokens while not enough unused tokens available at expiration,"
            + " throws exception.")
    void unstakeWithEnoughDelegatedStakesAvailableButWrongExpiration() {
      BalanceMutable balance = Balance.create().asMutable();
      long stakeAmount = 10;
      balance.addMpcTokenBalance(stakeAmount);
      balance.stakeTokens(stakeAmount);

      long expirationTimestamp = 123;
      balance =
          delegateAndAccept(balance, stakeAmount, stakeAmount, firstAddress, expirationTimestamp);
      BalanceMutable balanceAfterAssociation = associate(balance, stakeAmount, Contracts.ONE);

      assertThatThrownBy(() -> balanceAfterAssociation.unstakeTokens(stakeAmount, 100L))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Associations cannot be covered");
    }

    @Test
    @DisplayName(
        "Trying to unstake staked tokens while enough unused tokens available at expiration,"
            + " succeeds.")
    void unstakeWithEnoughDelegatedStakesAvailableWithValidExpiration() {
      BalanceMutable balance = Balance.create().asMutable();
      long stakeAmount = 10;
      balance.addMpcTokenBalance(stakeAmount);
      balance.stakeTokens(stakeAmount);

      balance = delegateAndAccept(balance, stakeAmount, stakeAmount, firstAddress);
      balance = associate(balance, stakeAmount, Contracts.ONE);

      assertThat(balance.pendingUnstakes().size()).isEqualTo(0);
      balance.unstakeTokens(stakeAmount, 100L);

      assertThat(balance.pendingUnstakes().size()).isEqualTo(1);
    }
  }

  @Test
  @DisplayName("Stakes are burned proportionally to the amount of other associations they cover.")
  void cannotBurnFromDelegatorsWithLaterAssociatedTokens() {
    final long selfStaked = 2000L;
    final long delegationAmount = 4000L;
    final long earlyTimestamp = 100L;
    final long lateTimestamp = 200L;

    BalanceMutable balance = Balance.create().asMutable();
    balance.addMpcTokenBalance(selfStaked);
    balance.stakeTokens(selfStaked);

    long burnAmount = delegationAmount + selfStaked;
    balance.commitReceiveDelegatedStakes(firstAddress, delegationAmount, earlyTimestamp);
    balance.acceptPendingDelegatedStakes(firstAddress, delegationAmount);
    balance.associateWithExpiration(Contracts.ONE, burnAmount, earlyTimestamp);

    balance.commitReceiveDelegatedStakes(secondAddress, delegationAmount, lateTimestamp);
    balance.acceptPendingDelegatedStakes(secondAddress, delegationAmount);
    balance.associateWithExpiration(Contracts.TWO, delegationAmount, lateTimestamp);

    BalanceMutable accountStateAfter = burnTokens(burnAmount, balance, Contracts.ONE);

    StakesFromOthers earlyStakes =
        accountStateAfter.delegatedStakesFromOthers().getValue(firstAddress);
    assertThat(earlyStakes).isNull();
    StakesFromOthers lateStakes =
        accountStateAfter.delegatedStakesFromOthers().getValue(secondAddress);
    assertThat(lateStakes.acceptedDelegatedStakes).isNotEqualTo(delegationAmount);
    assertThat(accountStateAfter.stakedTokens()).isNotEqualTo(selfStaked);
  }

  @Test
  @DisplayName(
      "Only some tokens gets burned for delegators, that has their delegated stakes used as"
          + " associations at later expirations than the expiration of the burned contract.")
  void dontUseTokensThatAreNeededForCollateral() {
    final long associationAmount = 2000L;
    final long delegationAmount = 4000L;
    final long earlyTimestamp = 100L;

    BalanceMutable balance = Balance.create().asMutable();

    balance.commitReceiveDelegatedStakes(firstAddress, delegationAmount, earlyTimestamp);
    balance.acceptPendingDelegatedStakes(firstAddress, delegationAmount);
    balance.associateWithExpiration(Contracts.ONE, associationAmount, earlyTimestamp);

    balance.commitReceiveDelegatedStakes(secondAddress, delegationAmount, null);
    balance.acceptPendingDelegatedStakes(secondAddress, delegationAmount);
    balance.associateWithExpiration(Contracts.TWO, delegationAmount, null);

    BalanceMutable accountStateAfter = burnTokens(associationAmount, balance, Contracts.ONE);

    StakesFromOthers earlyStakes =
        accountStateAfter.delegatedStakesFromOthers().getValue(firstAddress);
    assertThat(earlyStakes.acceptedDelegatedStakes).isEqualTo(associationAmount);
    StakesFromOthers lateStakes =
        accountStateAfter.delegatedStakesFromOthers().getValue(secondAddress);
    assertThat(lateStakes.acceptedDelegatedStakes).isEqualTo(delegationAmount);
  }

  @Test
  @DisplayName(
      "Tokens does not get burned for delegators, that has all their delegated stakes used as"
          + " associations.")
  void dontGetBurnedIfImportantCollateral() {
    final long amount = 4000L;
    final long earlyTimestamp = 100L;

    BalanceMutable balance = Balance.create().asMutable();

    balance.commitReceiveDelegatedStakes(secondAddress, amount, earlyTimestamp);
    balance.acceptPendingDelegatedStakes(secondAddress, amount);
    balance.associateWithExpiration(Contracts.TWO, amount, earlyTimestamp);

    balance.commitReceiveDelegatedStakes(firstAddress, amount, null);
    balance.acceptPendingDelegatedStakes(firstAddress, amount);
    balance.associateWithExpiration(Contracts.ONE, amount, null);

    BalanceMutable accountStateAfter = burnTokens(amount, balance, Contracts.TWO);

    StakesFromOthers earlyStakes =
        accountStateAfter.delegatedStakesFromOthers().getValue(secondAddress);
    assertThat(earlyStakes).isNull();
    StakesFromOthers lateStakes =
        accountStateAfter.delegatedStakesFromOthers().getValue(firstAddress);
    assertThat(lateStakes.acceptedDelegatedStakes).isEqualTo(amount);
  }

  @Test
  @DisplayName(
      "Delegations will only get burned such that the remaining delegations can still cover"
          + " collateral for all remaining expirations.")
  void sameAmountOfAssociationsAsDelegations() {
    BalanceMutable balance = Balance.create().asMutable();

    balance = delegateAndAccept(balance, 80, 80, firstAddress, 30L);
    balance = delegateAndAccept(balance, 40, 40, secondAddress, 20L);
    balance = delegateAndAccept(balance, 30, 30, thirdAddress, 10L);

    long associationAmount = 50;
    balance = associateWithTimestamp(balance, associationAmount, 30L, Contracts.ONE);
    balance = associateWithTimestamp(balance, associationAmount, 20L, Contracts.TWO);
    balance = associateWithTimestamp(balance, associationAmount, 10L, Contracts.THREE);

    balance = burnTokens(associationAmount, balance, Contracts.THREE);

    long bestDelegationAmount =
        balance.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes;
    long secondBestDelegationAmount =
        balance.delegatedStakesFromOthers().getValue(secondAddress).acceptedDelegatedStakes;
    long collateralForRemainingAssociation = bestDelegationAmount + secondBestDelegationAmount;
    long neededCollateralAfterBurn = associationAmount * 2;
    assertThat(neededCollateralAfterBurn).isLessThanOrEqualTo(collateralForRemainingAssociation);
  }

  @Test
  @DisplayName(
      "Burning tokens from contract, will not remove more delegated stakes than the amount that was"
          + " associated.")
  void dontBurnMoreThanThanAssociationToContract() {
    BalanceMutable balance = Balance.create().asMutable();

    long delegationAmount = 60;
    final long totalDelegationBeforeBurn = delegationAmount * 3;
    balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
    balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress, 20L);
    balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress, 10L);

    int associationAmount = 50;
    balance = associateWithTimestamp(balance, associationAmount, 30L, Contracts.ONE);
    balance = associateWithTimestamp(balance, associationAmount, 20L, Contracts.TWO);
    balance = associateWithTimestamp(balance, associationAmount, 10L, Contracts.THREE);

    balance = burnTokens(associationAmount, balance, Contracts.THREE);

    long totalDelegatedStakes = 0;
    for (StakesFromOthers value : balance.delegatedStakesFromOthers().values()) {
      totalDelegatedStakes += value.acceptedDelegatedStakes;
    }

    assertThat(totalDelegatedStakes).isEqualTo(totalDelegationBeforeBurn - associationAmount);
  }

  @Test
  @DisplayName(
      "Burning tokens from contract with the same expiration as another association, will not"
          + " remove more delegated stakes than the amount that was associated.")
  void associationsAndDelegationsWithSameExpiration() {
    BalanceMutable balance = Balance.create().asMutable();

    long delegationAmount = 60;
    final long totalDelegationBeforeBurn = delegationAmount * 3;
    balance = delegateAndAccept(balance, delegationAmount, delegationAmount, firstAddress);
    balance = delegateAndAccept(balance, delegationAmount, delegationAmount, secondAddress);
    balance = delegateAndAccept(balance, delegationAmount, delegationAmount, thirdAddress, 10L);

    int associationAmount = 50;
    balance = associate(balance, associationAmount, Contracts.ONE);
    balance = associate(balance, associationAmount, Contracts.TWO);
    balance = associateWithTimestamp(balance, associationAmount, 10L, Contracts.THREE);

    balance = burnTokens(associationAmount, balance, Contracts.THREE);

    long totalDelegatedStakes = 0;
    for (StakesFromOthers value : balance.delegatedStakesFromOthers().values()) {
      totalDelegatedStakes += value.acceptedDelegatedStakes;
    }

    assertThat(totalDelegatedStakes).isEqualTo(totalDelegationBeforeBurn - associationAmount);
  }

  @Test
  @DisplayName("Only delegated stakes not used for associations gets burned.")
  void cannotBurnAllTokensFromDelegatorsWithLaterAssociatedTokens() {
    final long selfStaked = 2000L;
    final long delegationAmount = 3000L;
    final long earlyTimestamp = 122L;
    final long lateTimestamp = 333L;

    BalanceMutable balance = Balance.create().asMutable();
    balance.addMpcTokenBalance(selfStaked);
    balance.stakeTokens(selfStaked);

    long burnAmount = delegationAmount + selfStaked;
    balance.commitReceiveDelegatedStakes(firstAddress, delegationAmount, earlyTimestamp);
    balance.acceptPendingDelegatedStakes(firstAddress, delegationAmount);
    balance.associateWithExpiration(Contracts.ONE, burnAmount, earlyTimestamp);

    balance.commitReceiveDelegatedStakes(secondAddress, delegationAmount, lateTimestamp);
    balance.acceptPendingDelegatedStakes(secondAddress, delegationAmount);
    long notAssociated = 100L;
    balance.associateWithExpiration(Contracts.TWO, delegationAmount - notAssociated, lateTimestamp);

    BalanceMutable accountStateAfter = burnTokens(burnAmount, balance, Contracts.ONE);

    StakesFromOthers earlyStakes =
        accountStateAfter.delegatedStakesFromOthers().getValue(firstAddress);
    assertThat(earlyStakes.acceptedDelegatedStakes).isNotEqualTo(delegationAmount);
    StakesFromOthers lateStakes =
        accountStateAfter.delegatedStakesFromOthers().getValue(secondAddress);
    assertThat(lateStakes.acceptedDelegatedStakes).isNotEqualTo(delegationAmount);
  }

  @Test
  @DisplayName("Only delegated stakes not used for associations gets burned.")
  void evenlySplitBurn() {
    final long amount = 2000L;
    final long earlyTimestamp = 122L;
    final long lateTimestamp = 333L;

    BalanceMutable balance = Balance.create().asMutable();
    balance.addMpcTokenBalance(amount);
    balance.stakeTokens(amount);

    balance.commitReceiveDelegatedStakes(firstAddress, amount, lateTimestamp);
    balance.acceptPendingDelegatedStakes(firstAddress, amount);
    balance.associateWithExpiration(Contracts.TWO, amount, lateTimestamp);
    balance.associateWithExpiration(Contracts.ONE, amount, earlyTimestamp);

    BalanceMutable accountStateAfter = burnTokens(amount, balance, Contracts.ONE);

    StakesFromOthers earlyStakes =
        accountStateAfter.delegatedStakesFromOthers().getValue(firstAddress);
    assertThat(earlyStakes.acceptedDelegatedStakes).isEqualTo(1000L);
    assertThat(accountStateAfter.stakedTokens()).isEqualTo(1000L);
  }

  private BalanceMutable burnTokens(
      long amount, BalanceMutable balance, BlockchainAddress contractAddress) {
    byte[] rpc = AccountInvocationRpc.burnMpcTokens(amount, contractAddress);
    AccountState<AccountStateLocal, Balance> accountStateAfter =
        invokeAndCheckState(
                pluginContextDefault,
                global,
                new AccountState<>(AccountStateLocal.initial(), balance.asImmutable()),
                rpc)
            .updatedState();
    return accountStateAfter.account.asMutable();
  }

  @Test
  public void restakeUnstakedTokens() {
    BalanceMutable accountStateBalance = Balance.create().asMutable();
    accountStateBalance.addMpcTokenBalance(20);
    accountStateBalance.stakeTokens(15);
    AccountState<AccountStateLocal, Balance> accountState =
        new AccountState<>(AccountStateLocal.initial(), accountStateBalance.asImmutable());

    byte[] unstakeRpc = AccountInvocationRpc.unstakeTokens(10);
    long unstakeTime = 1;
    AccountState<AccountStateLocal, Balance> invokeUnstakeState =
        invokeAndCheckState(() -> unstakeTime, global, accountState, unstakeRpc).updatedState();

    byte[] unstakeRpc2 = AccountInvocationRpc.unstakeTokens(5);
    AccountState<AccountStateLocal, Balance> invokeUnstakeState2 =
        invokeAndCheckState(() -> unstakeTime + 1, global, invokeUnstakeState, unstakeRpc2)
            .updatedState();

    assertThat(invokeUnstakeState2.account.pendingUnstakes().size()).isEqualTo(2);

    int restakeAmount = 11;
    byte[] restakeRpc = AccountInvocationRpc.stakeTokens(restakeAmount);
    AccountState<AccountStateLocal, Balance> restakeState =
        invokeAndCheckState(pluginContextDefault, global, invokeUnstakeState2, restakeRpc)
            .updatedState();

    assertThat(restakeState.account.stakedTokens()).isEqualTo(restakeAmount);
    // Checks that it takes the newest unstaked tokens.
    assertThat(restakeState.account.pendingUnstakes().size()).isEqualTo(1);
    assertThat(
            restakeState
                .account
                .pendingUnstakes()
                .getValue(unstakeTime + Balance.UNSTAKE_DURATION_MILLIS))
        .isEqualTo(4);
    assertThat(restakeState.account.mpcTokens()).isEqualTo(5);

    // exact amount
    restakeAmount = 15;
    byte[] restakeEqualRpc = AccountInvocationRpc.stakeTokens(restakeAmount);
    AccountState<AccountStateLocal, Balance> restakeEqualState =
        invokeAndCheckState(pluginContextDefault, global, invokeUnstakeState2, restakeEqualRpc)
            .updatedState();
    assertThat(restakeEqualState.account.mpcTokens()).isEqualTo(5);
    assertThat(restakeEqualState.account.pendingUnstakes().size()).isEqualTo(0);

    // Only a portion of unstakes is enough
    restakeAmount = 17;
    byte[] restakeOverRpc = AccountInvocationRpc.stakeTokens(restakeAmount);
    AccountState<AccountStateLocal, Balance> restakeOverState =
        invokeAndCheckState(pluginContextDefault, global, invokeUnstakeState2, restakeOverRpc)
            .updatedState();
    assertThat(restakeOverState.account.pendingUnstakes().size()).isEqualTo(0);
    assertThat(restakeOverState.account.mpcTokens()).isEqualTo(3);
  }

  @Test
  public void cannotStakeNonPositiveAmount() {
    assertThatThrownBy(
            () -> {
              byte[] rpcBytes = AccountInvocationRpc.stakeTokens(-5);
              invokeAndCheckState(pluginContextDefault, global, tokenAccount(2), rpcBytes);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot stake a non-positive amount of tokens");
  }

  @Test
  public void invokeStake_vestedTokens() {
    long mpcTokens = 10;

    BalanceMutable initialBalance = Balance.create().asMutable();
    initialBalance.addMpcTokenBalance(mpcTokens);
    int vestingAmount = 36;
    initialBalance.createVestingAccount(
        vestingAmount, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0);
    AccountState<AccountStateLocal, Balance> initial =
        new AccountState<>(AccountStateLocal.initial(), initialBalance.asImmutable());
    long toStake = 20;
    byte[] stakeMoreThanMpcTokenBalance = AccountInvocationRpc.stakeTokens(toStake);
    AccountState<AccountStateLocal, Balance> stakedVestedTokens =
        invokeAndCheckState(pluginContextDefault, global, initial, stakeMoreThanMpcTokenBalance)
            .updatedState();
    long negativeMpcTokenBalance = mpcTokens - toStake;
    assertThat(stakedVestedTokens.account.mpcTokens()).isEqualTo(negativeMpcTokenBalance);
    assertThat(stakedVestedTokens.account.stakedTokens()).isEqualTo(toStake);

    long release = vestingAmount / 2;
    BalanceMutable balance = stakedVestedTokens.account.asMutable();
    balance.checkVestedTokens(TOKEN_GENERATION_EVENT_0 + DURATION_0 / 2);
    AccountState<AccountStateLocal, Balance> releasedVestedTokens =
        new AccountState<>(stakedVestedTokens.local, balance.asImmutable());
    long mpcTokenBalanceAfterSecondRelease = releasedVestedTokens.account.mpcTokens();
    assertThat(mpcTokenBalanceAfterSecondRelease).isEqualTo(negativeMpcTokenBalance + release);
    int tierOfSale = 0;
    long releasedTokensAfterSecondRelease =
        releasedVestedTokens.account.releasedVestedTokens(tierOfSale);
    assertThat(releasedTokensAfterSecondRelease).isEqualTo(release);

    long vestedTokensLeftToRelease =
        releasedVestedTokens.account.vestingAccounts().get(tierOfSale).tokens
            - releasedTokensAfterSecondRelease;
    long everyPossibleTokenLeftToStake =
        mpcTokenBalanceAfterSecondRelease + vestedTokensLeftToRelease;
    byte[] stakeEverythingRpc = AccountInvocationRpc.stakeTokens(everyPossibleTokenLeftToStake);
    AccountState<AccountStateLocal, Balance> stakeEverything =
        invokeAndCheckState(pluginContextDefault, global, releasedVestedTokens, stakeEverythingRpc)
            .updatedState();
    assertThat(stakeEverything.account.stakedTokens())
        .isEqualTo(toStake + everyPossibleTokenLeftToStake);
    assertThat(stakeEverything.account.mpcTokens())
        .isEqualTo(mpcTokenBalanceAfterSecondRelease - everyPossibleTokenLeftToStake);

    long stakeAmount = 1;
    long mpcTokenBalance = stakeEverything.account.mpcTokens();
    long leftToRelease =
        stakeEverything.account.asMutable().calculateTotalNonReleasedVestedTokens();
    ThrowableAssert.ThrowingCallable stakeMoreThanLeftToRelease =
        () ->
            invokeAndCheckState(
                pluginContextDefault,
                global,
                stakeEverything,
                AccountInvocationRpc.stakeTokens(stakeAmount));
    assertThatThrownBy(stakeMoreThanLeftToRelease)
        .isInstanceOf(RuntimeException.class)
        .hasMessage(stakeTooMuchErrorMessage(stakeAmount, mpcTokenBalance, leftToRelease, 0));

    BlockchainAddress contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_SYSTEM,
            Hash.create(h -> h.writeString("BurnContract")));
    byte[] burnRpc =
        AccountInvocationRpc.burnMpcTokens(stakeEverything.account.stakedTokens(), contractAddress);

    BalanceMutable associatedTokensBalance = stakeEverything.account.asMutable();
    associatedTokensBalance.associateWithExpiration(
        contractAddress, stakeEverything.account.stakedTokens(), null);
    AccountState<AccountStateLocal, Balance> associatedTokens =
        new AccountState<>(stakeEverything.local, associatedTokensBalance.asImmutable());
    AccountState<AccountStateLocal, Balance> burnedAllStakedTokens =
        invokeAndCheckState(pluginContextDefault, global, associatedTokens, burnRpc).updatedState();
    assertThat(burnedAllStakedTokens.account.stakedTokens()).isEqualTo(0);
  }

  @Test
  public void invokeStake_vestedTokens_multipleVestingSchedules() {
    int vestedTokens = 50;
    BalanceMutable initialBalance = Balance.create().asMutable();
    initialBalance.createVestingAccount(
        vestedTokens, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0);
    initialBalance.createVestingAccount(
        vestedTokens, TOKEN_GENERATION_EVENT_1, DURATION_1, INTERVAL_1);
    AccountState<AccountStateLocal, Balance> initial =
        new AccountState<>(AccountStateLocal.initial(), initialBalance.asImmutable());
    long toStake = 100;
    byte[] stakeAllNonReleasedTokens = AccountInvocationRpc.stakeTokens(toStake);
    AccountState<AccountStateLocal, Balance> afterStake =
        invokeAndCheckState(pluginContextDefault, global, initial, stakeAllNonReleasedTokens)
            .updatedState();
    assertThat(afterStake.account.stakedTokens()).isEqualTo(100);

    byte[] stakeMoreThanNonReleasedTokens = AccountInvocationRpc.stakeTokens(toStake + 1);
    ThrowableAssert.ThrowingCallable stakeMoreThanLeftToRelease =
        () ->
            invokeAndCheckState(
                pluginContextDefault, global, initial, stakeMoreThanNonReleasedTokens);
    assertThatThrownBy(stakeMoreThanLeftToRelease)
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            stakeTooMuchErrorMessage(
                toStake + 1,
                initial.account.mpcTokens(),
                initial.account.asMutable().calculateTotalNonReleasedVestedTokens(),
                0));
  }

  @Test
  public void invokeAssociateAndDisassociate() {
    byte[] associateRpc = AccountInvocationRpc.associate(7, Contracts.ONE);
    AccountState<AccountStateLocal, Balance> accountState = stakeAccount(10);
    AccountState<AccountStateLocal, Balance> update =
        invokeAndCheckState(pluginContextDefault, global, accountState, associateRpc)
            .updatedState();
    assertThat(update.account).isNotNull();
    assertThat(update.account.getAmountAssociationToContract(Contracts.ONE)).isEqualTo(7);

    byte[] associateRemainingRpc = AccountInvocationRpc.associate(3, Contracts.ONE);
    AccountState<AccountStateLocal, Balance> allStakedAssociated =
        invokeAndCheckState(pluginContextDefault, global, update, associateRemainingRpc)
            .updatedState();
    assertThat(allStakedAssociated.account).isNotNull();
    assertThat(allStakedAssociated.account.getAmountAssociationToContract(Contracts.ONE))
        .isEqualTo(10);

    byte[] associateRpc2 = AccountInvocationRpc.associate(2, Contracts.TWO);
    AccountState<AccountStateLocal, Balance> update2 =
        invokeAndCheckState(pluginContextDefault, global, update, associateRpc2).updatedState();
    assertThat(update2.account).isNotNull();
    assertThat(update2.account.getAmountAssociationToContract(Contracts.TWO)).isEqualTo(2);

    byte[] associateRpc3 = AccountInvocationRpc.associate(1, Contracts.ONE);
    AccountState<AccountStateLocal, Balance> update3 =
        invokeAndCheckState(pluginContextDefault, global, update2, associateRpc3).updatedState();
    assertThat(update3.account).isNotNull();
    assertThat(update3.account.getAmountAssociationToContract(Contracts.ONE)).isEqualTo(8);

    byte[] disassociateRpc = AccountInvocationRpc.disassociate(8, Contracts.ONE);
    AccountState<AccountStateLocal, Balance> update4 =
        invokeAndCheckState(pluginContextDefault, global, update3, disassociateRpc).updatedState();
    assertThat(update4.account).isNotNull();
    assertThat(update4.account.stakedToContract().containsKey(Contracts.ONE)).isFalse();

    byte[] disassociatePartialAmount = AccountInvocationRpc.disassociate(4, Contracts.ONE);
    AccountState<AccountStateLocal, Balance> disassociate =
        invokeAndCheckState(pluginContextDefault, global, update, disassociatePartialAmount)
            .updatedState();
    assertThat(disassociate.account).isNotNull();
    assertThat(disassociate.account.getAmountAssociationToContract(Contracts.ONE)).isEqualTo(3);

    long invalidAmount = 3;

    assertThatThrownBy(
            () -> {
              byte[] associateTooMany =
                  AccountInvocationRpc.associate(invalidAmount, Contracts.ONE);
              invokeAndCheckState(
                  pluginContextDefault, global, allStakedAssociated, associateTooMany);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Associations cannot be covered");

    assertThatThrownBy(
            () -> {
              byte[] associateZero = AccountInvocationRpc.associate(0, Contracts.ONE);
              invokeAndCheckState(pluginContextDefault, global, allStakedAssociated, associateZero);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot associate non-positive amount");

    assertThatThrownBy(
            () -> {
              byte[] associateNegative = AccountInvocationRpc.disassociate(-1, Contracts.ONE);
              invokeAndCheckState(pluginContextDefault, global, update4, associateNegative);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot disassociate non-positive amount");

    assertThatThrownBy(
            () -> {
              byte[] disassociateTooMany = AccountInvocationRpc.disassociate(1, Contracts.ONE);
              invokeAndCheckState(pluginContextDefault, global, update4, disassociateTooMany);
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Amount cannot make contract association negative");
  }

  @Test
  @DisplayName("Burning tokens from a contract that has not been associated to, throws exception.")
  void burnFromNonExistingContract() {
    AccountState<AccountStateLocal, Balance> accountStateBefore =
        new AccountState<>(tokenAccount(10).local, Balance.create());

    assertThatThrownBy(
            () -> {
              byte[] disassociateTooMany = AccountInvocationRpc.burnMpcTokens(10, Contracts.ONE);
              invokeAndCheckState(
                  pluginContextDefault, global, accountStateBefore, disassociateTooMany);
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Amount cannot make contract association negative");
  }

  @Test
  public void invokeDisassociateAllTokensForContract() {
    byte[] associateLegal = AccountInvocationRpc.associate(8, Contracts.ONE);
    AccountState<AccountStateLocal, Balance> accountState = stakeAccount(10);
    AccountState<AccountStateLocal, Balance> update =
        invokeAndCheckState(pluginContextDefault, global, accountState, associateLegal)
            .updatedState();
    assertThat(update.account.getAmountAssociationToContract(Contracts.ONE)).isEqualTo(8);

    byte[] disassociateTokens =
        AccountInvocationRpc.disassociateAllTokensForContract(Contracts.ONE);
    update =
        invokeAndCheckState(pluginContextDefault, global, update, disassociateTokens)
            .updatedState();
    assertThat(update.account.stakedToContract().getValue(Contracts.ONE)).isNull();
  }

  @Test
  public void invokeUnstakeTokens() {
    byte[] unstakeLegal = AccountInvocationRpc.unstakeTokens(8);
    AccountState<AccountStateLocal, Balance> accountState = stakeAccount(10);
    AccountState<AccountStateLocal, Balance> update =
        invokeAndCheckState(pluginContextDefault, global, accountState, unstakeLegal)
            .updatedState();
    assertThat(update.account.stakedTokens()).isEqualTo(2);

    byte[] timeCheck = AccountInvocationRpc.unstakeTokens(4);
    long unstakeRequestTime = 1;

    BalanceMutable accountStateTimeBalance = Balance.create().asMutable();
    accountStateTimeBalance.addMpcTokenBalance(8);
    accountStateTimeBalance.stakeTokens(6);
    AccountState<AccountStateLocal, Balance> accountStateTime =
        new AccountState<>(AccountStateLocal.initial(), accountStateTimeBalance.asImmutable());
    AccountState<AccountStateLocal, Balance> update2 =
        invokeAndCheckState(() -> unstakeRequestTime, global, accountStateTime, timeCheck)
            .updatedState();

    assertThat(update2.account.pendingUnstakes().size()).isEqualTo(1);
    assertThat(
            update2
                .account
                .pendingUnstakes()
                .getValue(unstakeRequestTime + Balance.UNSTAKE_DURATION_MILLIS))
        .isEqualTo(4);

    assertThatThrownBy(
            () -> {
              byte[] unstakeIllegal = AccountInvocationRpc.unstakeTokens(11);
              invokeAndCheckState(pluginContextDefault, global, accountState, unstakeIllegal);
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot unstake more than the amount of staked tokens on the account");

    assertThatThrownBy(
            () -> {
              byte[] unstakeIllegalZero = AccountInvocationRpc.unstakeTokens(0);
              invokeAndCheckState(pluginContextDefault, global, accountState, unstakeIllegalZero);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot unstake non-positive amount of tokens");

    assertThatThrownBy(
            () -> {
              byte[] unstakeWithNoStakedTokens = AccountInvocationRpc.unstakeTokens(10);
              invokeAndCheckState(
                  pluginContextDefault, global, tokenAccount(10), unstakeWithNoStakedTokens);
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot unstake more than the amount of staked tokens on the account");
  }

  @Test
  public void invokeUnstake_vestedTokens() {
    long vestedTokens = 10;

    BalanceMutable initialBalance = Balance.create().asMutable();
    initialBalance.createVestingAccount(
        vestedTokens, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0);
    AccountState<AccountStateLocal, Balance> initial =
        new AccountState<>(AccountStateLocal.initial(), initialBalance.asImmutable());
    long toStake = 10;
    byte[] stakeVestedTokens = AccountInvocationRpc.stakeTokens(toStake);
    AccountState<AccountStateLocal, Balance> stakedVestedTokens =
        invokeAndCheckState(pluginContextDefault, global, initial, stakeVestedTokens)
            .updatedState();
    assertThat(stakedVestedTokens.account.mpcTokens()).isEqualTo(-toStake);
    assertThat(stakedVestedTokens.account.stakedTokens()).isEqualTo(toStake);

    long unstakeTime = 1;
    byte[] unstakeVestedTokens = AccountInvocationRpc.unstakeTokens(toStake);
    AccountState<AccountStateLocal, Balance> pendingUnstakeVestedTokens =
        invokeAndCheckState(() -> unstakeTime, global, stakedVestedTokens, unstakeVestedTokens)
            .updatedState();
    assertThat(pendingUnstakeVestedTokens.account.pendingUnstakes().size()).isEqualTo(1);

    long readyToUnstakeTime = unstakeTime + Balance.UNSTAKE_DURATION_MILLIS;

    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>> invokeResultPending =
        invokeAndCheckState(
            () -> readyToUnstakeTime,
            global,
            pendingUnstakeVestedTokens,
            AccountInvocationRpc.checkPendingUnstakes());
    AccountState<AccountStateLocal, Balance> checkPendingUnstakes =
        invokeResultPending.updatedState();
    assertThat(checkPendingUnstakes.account.pendingUnstakes().size()).isEqualTo(0);
    assertThat(checkPendingUnstakes.account.mpcTokens()).isEqualTo(0);
  }

  @Test
  public void invokeGetStakedTokens() {
    AccountState<AccountStateLocal, Balance> accountState = stakeAccount(10);
    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>> invokeResult =
        invokeAndCheckState(
            () -> 1, global, accountState, AccountInvocationRpc.getStakedTokenBalance());
    AccountState<AccountStateLocal, Balance> unstakedVestedTokens = invokeResult.updatedState();

    assertThat(unstakedVestedTokens.account.stakedTokens()).isEqualTo(10);

    SafeDataInputStream returnValueInputStream =
        SafeDataInputStream.createFromBytes(invokeResult.result());
    long returnedBalance = returnValueInputStream.readLong();
    assertThat(returnedBalance).isEqualTo(10);
  }

  @Test
  public void transferByocAccountUnsigned256() {
    // Withdraw
    Hash transactionId = Hash.create(s -> s.writeString("TxID"));
    Unsigned256 amount = Unsigned256.create(123);
    byte[] senderRpc =
        AccountInvocationRpc.initiateByocTransferSender(transactionId, amount, SYMBOL_2);
    AccountState<AccountStateLocal, Balance> account = setCoinAccount(123);
    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>> updated =
        invokeAndCheckState(pluginContextDefault, global, account, senderRpc);

    TransferInformation transfer =
        updated.updatedState().account.storedPendingTransfers().getValue(transactionId);
    assertThat(transfer).isNotNull();
    assertThat(transfer.getAmount()).isEqualTo(amount);
    assertThat(transfer.isWithdraw()).isTrue();

    // Deposit
    AccountState<AccountStateLocal, Balance> receiver = setCoinAccount(0);
    byte[] receiverRpc =
        AccountInvocationRpc.initiateByocTransferRecipient(transactionId, amount, SYMBOL_2);
    updated = invokeAndCheckState(pluginContextDefault, global, receiver, receiverRpc);

    transfer = updated.updatedState().account.storedPendingTransfers().getValue(transactionId);
    assertThat(transfer).isNotNull();
    assertThat(transfer.getAmount()).isEqualTo(amount);
    assertThat(transfer.isDeposit()).isTrue();
  }

  @Test
  public void transferByocContractUnsigned256() {
    // Withdraw
    Hash transactionId = Hash.create(s -> s.writeString("TxID"));
    Unsigned256 amount = Unsigned256.create(444);
    byte[] withdraw =
        ContractInvocationRpc.initiateByocTransferSender(transactionId, amount, SYMBOL_2);
    ContractStorage state = createContractWithByoc(444, 2);
    BlockchainPlugin.InvokeResult<ContractState<AccountStateLocal, ContractStorage>> result =
        plugin.invokeContract(
            pluginContextDefault,
            global,
            new ContractState<>(null, state),
            Contracts.ONE,
            withdraw);
    ContractStorage updated = result.updatedState().contract;
    TransferInformation transfer = updated.getStoredPendingTransfers().getValue(transactionId);
    assertThat(transfer).isNotNull();
    assertThat(transfer.getAmount()).isEqualTo(amount);
    assertThat(transfer.isWithdraw()).isTrue();

    byte[] deposit =
        ContractInvocationRpc.initiateByocTransferRecipient(transactionId, amount, SYMBOL_2);
    state = createContractWithByoc(0, 2);
    result =
        plugin.invokeContract(
            pluginContextDefault, global, new ContractState<>(null, state), Contracts.ONE, deposit);
    updated = result.updatedState().contract;
    transfer = updated.getStoredPendingTransfers().getValue(transactionId);
    assertThat(transfer).isNotNull();
    assertThat(transfer.getAmount()).isEqualTo(amount);
    assertThat(transfer.isDeposit()).isTrue();
  }

  @Test
  public void transferByocContractNegativeAmounts() {
    byte[] twoPhaseCommitRpcDepositByoc =
        AccountInvocationRpc.initiateByocTransferRecipient(EMPTY, Unsigned256.create(0), SYMBOL_2);
    assertThatThrownBy(
            () ->
                plugin.invokeContract(
                    pluginContextDefault,
                    global,
                    new ContractState<>(null, null),
                    Contracts.ONE,
                    twoPhaseCommitRpcDepositByoc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot deposit a non-positive amount of BYOC");

    byte[] twoPhaseCommitRpcWithdrawByoc =
        ContractInvocationRpc.initiateByocTransferSender(EMPTY, Unsigned256.ZERO, SYMBOL_2);
    assertThatThrownBy(
            () ->
                plugin.invokeContract(
                    pluginContextDefault,
                    global,
                    new ContractState<>(null, null),
                    Contracts.ONE,
                    twoPhaseCommitRpcWithdrawByoc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot withdraw a non-positive amount of BYOC");

    byte[] twoPhaseCommitRpcDepositByocUnsigned256 =
        ContractInvocationRpc.initiateByocTransferRecipient(EMPTY, Unsigned256.ZERO, SYMBOL_2);
    assertThatThrownBy(
            () ->
                plugin.invokeContract(
                    pluginContextDefault,
                    global,
                    new ContractState<>(null, null),
                    Contracts.ONE,
                    twoPhaseCommitRpcDepositByocUnsigned256))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot deposit a non-positive amount of BYOC");

    Unsigned256 unsignedAmount = Unsigned256.create(0);
    byte[] twoPhaseCommitRpcWithdrawByocUnsigned256 =
        ContractInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount, SYMBOL_2);
    assertThatThrownBy(
            () ->
                plugin.invokeContract(
                    pluginContextDefault,
                    global,
                    new ContractState<>(null, null),
                    Contracts.ONE,
                    twoPhaseCommitRpcWithdrawByocUnsigned256))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot withdraw a non-positive amount of BYOC");
  }

  @Test
  public void transferContractByocAccountToContract() {
    byte[] deposit =
        ContractInvocationRpc.initiateByocTransferRecipient(
            EMPTY, Unsigned256.create(123), SYMBOL_2);
    ContractStorage state = ContractStorage.create(0L);
    ContractState<AccountStateLocal, ContractStorage> resultDeposit =
        invokeContract(new ContractState<>(null, state), Contracts.ONE, deposit);

    ContractStorage updated = resultDeposit.contract;
    assertThat(updated.getStoredPendingTransfers().size()).isEqualTo(1);
    TransferInformation transfer = updated.getStoredPendingTransfers().getValue(EMPTY);
    assertThat(transfer).isNotNull();
    assertThat(transfer.isDeposit()).isTrue();
    assertThat(transfer.getAmount()).isEqualTo(Unsigned256.create(123));

    Unsigned256 unsignedAmount = Unsigned256.create(123);
    byte[] withdraw =
        AccountInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount, SYMBOL_2);
    AccountState<AccountStateLocal, Balance> account = setCoinAccount(123);

    AccountState<AccountStateLocal, Balance> resultWithdraw = invokeAccount(account, withdraw);

    byte[] commit = ContractInvocationRpc.commitTransfer(EMPTY);

    AccountState<AccountStateLocal, Balance> resultAccount = invokeAccount(resultWithdraw, commit);
    ContractState<AccountStateLocal, ContractStorage> resultContract =
        invokeContract(resultDeposit, Contracts.ONE, commit);

    assertThat(resultAccount.account.coinBalance(2)).isEqualTo(Unsigned256.ZERO);
    assertThat(resultContract.contract.getCoinBalance(2)).isEqualTo(Unsigned256.create(123));
  }

  @Test
  public void transferContractByocContractToAccount() {
    Unsigned256 unsignedAmount = Unsigned256.create(450);
    byte[] withdraw =
        ContractInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount, SYMBOL_2);
    ContractStorage contract = createContractWithByoc(500, 2);
    assertThat(contract.getCoinBalance(2)).isEqualTo(Unsigned256.create(500));
    ContractState<AccountStateLocal, ContractStorage> resultWithdraw =
        invokeContract(new ContractState<>(null, contract), Contracts.ONE, withdraw);

    ContractStorage updated = resultWithdraw.contract;
    assertThat(updated.getCoinBalance(2)).isEqualTo(Unsigned256.create(50));

    byte[] deposit =
        AccountInvocationRpc.initiateByocTransferRecipient(
            EMPTY, Unsigned256.create(450), SYMBOL_2);
    AccountState<AccountStateLocal, Balance> resultDeposit =
        invokeAccount(new AccountState<>(null, null), deposit);

    byte[] commit = ContractInvocationRpc.commitTransfer(EMPTY);
    AccountState<AccountStateLocal, Balance> resultAccount = invokeAccount(resultDeposit, commit);
    invokeAndCheckState(pluginContextDefault, global, resultDeposit, commit);
    ContractState<AccountStateLocal, ContractStorage> resultContract =
        invokeContract(resultWithdraw, Contracts.ONE, commit);

    assertThat(resultContract.contract.getCoinBalance(2)).isEqualTo(Unsigned256.create(50));
    assertThat(resultAccount.account.coinBalance(2)).isEqualTo(Unsigned256.create(450));
  }

  @Test
  public void transferContractByocContractToContract() {
    Unsigned256 unsignedAmount = Unsigned256.create(200);
    byte[] withdraw =
        ContractInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount, SYMBOL_2);
    ContractStorage contract = createContractWithByoc(200, 2);

    ContractState<AccountStateLocal, ContractStorage> resultWithdraw =
        invokeContract(new ContractState<>(null, contract), Contracts.ONE, withdraw);

    byte[] deposit =
        ContractInvocationRpc.initiateByocTransferRecipient(
            EMPTY, Unsigned256.create(200), SYMBOL_2);
    ContractState<AccountStateLocal, ContractStorage> resultDeposit =
        invokeContract(
            new ContractState<>(null, ContractStorage.create(0L)), Contracts.TWO, deposit);

    byte[] commit = ContractInvocationRpc.commitTransfer(EMPTY);
    ContractState<AccountStateLocal, ContractStorage> withdrawCommit =
        invokeContract(resultWithdraw, Contracts.ONE, commit);
    ContractState<AccountStateLocal, ContractStorage> depositCommit =
        invokeContract(resultDeposit, Contracts.TWO, commit);

    assertThat(withdrawCommit.contract.getCoinBalance(2)).isEqualTo(Unsigned256.ZERO);
    assertThat(depositCommit.contract.getCoinBalance(2)).isEqualTo(Unsigned256.create(200));
  }

  @Test
  public void contractByocFailsTransferBetweenSameContract() {
    byte[] deposit =
        ContractInvocationRpc.initiateByocTransferRecipient(
            EMPTY, Unsigned256.create(123), SYMBOL_2);
    Unsigned256 amount = Unsigned256.create(123);
    byte[] withdraw = ContractInvocationRpc.initiateByocTransferSender(EMPTY, amount, SYMBOL_2);

    ContractState<AccountStateLocal, ContractStorage> afterDeposit =
        invokeContract(
            new ContractState<>(null, ContractStorage.create(0L)), Contracts.ONE, deposit);
    assertThatThrownBy(() -> invokeContract(afterDeposit, Contracts.ONE, withdraw))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot initiate transfer between the same contract");

    ContractStorage state = createContractWithByoc(123, 2);
    ContractState<AccountStateLocal, ContractStorage> afterWithdraw =
        invokeContract(new ContractState<>(null, state), Contracts.TWO, withdraw);
    assertThatThrownBy(() -> invokeContract(afterWithdraw, Contracts.TWO, deposit))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot initiate transfer between the same contract");
  }

  @Test
  public void contractByocTransferIdempotent() {
    byte[] deposit =
        ContractInvocationRpc.initiateByocTransferRecipient(
            EMPTY, Unsigned256.create(200), SYMBOL_2);
    ContractState<AccountStateLocal, ContractStorage> deposit1 =
        invokeContract(
            new ContractState<>(null, ContractStorage.create(0L)), Contracts.ONE, deposit);
    assertThat(deposit1.contract.getStoredPendingTransfers().size()).isEqualTo(1);
    ContractState<AccountStateLocal, ContractStorage> deposit2 =
        invokeContract(deposit1, Contracts.ONE, deposit);
    assertThat(deposit2.contract.getStoredPendingTransfers().size()).isEqualTo(1);

    ContractStorage contract = createContractWithByoc(200, 2);
    Unsigned256 unsignedAmount = Unsigned256.create(200);
    byte[] withdraw =
        ContractInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount, SYMBOL_2);
    ContractState<AccountStateLocal, ContractStorage> withdraw1 =
        invokeContract(new ContractState<>(null, contract), Contracts.ONE, withdraw);
    assertThat(withdraw1.contract.getStoredPendingTransfers().size()).isEqualTo(1);
    assertThat(withdraw1.contract.getCoinBalance(2)).isEqualTo(Unsigned256.ZERO);

    ContractState<AccountStateLocal, ContractStorage> withdraw2 =
        invokeContract(withdraw1, Contracts.ONE, withdraw);

    assertThat(withdraw2.contract.getStoredPendingTransfers().size()).isEqualTo(1);
    assertThat(withdraw2.contract.getCoinBalance(2)).isEqualTo(Unsigned256.ZERO);
  }

  @Test
  public void contractByocWithdrawAbort() {
    ContractStorage contract =
        createContractWithByoc(200, 2)
            .addPendingTransfer(EMPTY, TransferInformation.createForWithdraw(200, 2));

    byte[] abort = ContractInvocationRpc.abortTransfer(EMPTY);
    ContractState<AccountStateLocal, ContractStorage> updated =
        invokeContract(new ContractState<>(null, contract), Contracts.ONE, abort);

    assertThat(updated.contract.getCoinBalance(2)).isEqualTo(Unsigned256.create(400));
    assertThat(updated.contract.getStoredPendingTransfers().size()).isEqualTo(0);
  }

  @Test
  public void contractByocDepositAbort() {
    byte[] abort = ContractInvocationRpc.abortTransfer(EMPTY);
    ContractStorage contract =
        ContractStorage.create(0L)
            .addPendingTransfer(EMPTY, TransferInformation.createForDeposit(200, 2));
    ContractState<AccountStateLocal, ContractStorage> updated =
        invokeContract(new ContractState<>(null, contract), Contracts.ONE, abort);

    assertThat(updated.contract.getCoinBalance(2)).isEqualTo(Unsigned256.create(0));
    assertThat(updated.contract.getStoredPendingTransfers().size()).isEqualTo(0);
  }

  @Test
  public void contractByocCommitNonExistingTransfer() {
    ContractStorage contract =
        ContractStorage.create(0L)
            .addPendingTransfer(EMPTY, TransferInformation.createForDeposit(123, 2));
    Hash transactionId = Hash.create(s -> s.writeString("not empty"));
    byte[] commit = ContractInvocationRpc.commitTransfer(transactionId);
    ContractState<AccountStateLocal, ContractStorage> updated =
        invokeContract(new ContractState<>(null, contract), Contracts.ONE, commit);
    assertThat(updated.contract.getStoredPendingTransfers().size()).isEqualTo(1);
  }

  @Test
  public void contractByocAbortNonExistingTransfer() {
    ContractStorage contract =
        ContractStorage.create(0L)
            .addPendingTransfer(EMPTY, TransferInformation.createForDeposit(123, 2));
    Hash transactionId = Hash.create(s -> s.writeString("not empty"));
    byte[] abort = ContractInvocationRpc.abortTransfer(transactionId);
    ContractState<AccountStateLocal, ContractStorage> updated =
        invokeContract(new ContractState<>(null, contract), Contracts.TWO, abort);
    assertThat(updated.contract.getStoredPendingTransfers().size()).isEqualTo(1);
  }

  @Test
  public void contractByocCollectedAsBlockchainUsageOnDelete() {
    ContractStorage contract = createContractWithByoc(200, 1);
    contract = contract.addBalance(Unsigned256.create(100));
    ContractState<AccountStateLocal, ContractStorage> state =
        new ContractState<>(AccountStateLocal.initial(), contract);

    assertThat(state.local.convertedExternalCoins()).isEmpty();
    AccountStateLocal removed = plugin.removeContract(pluginContextDefault, global, state);
    Unsigned256 usage =
        removed.getBlockchainUsage().getValue(pluginContextDefault.blockProductionTime());
    // 100 gas + 200 BYOC with a conversion rate of 3/1 == 700 gas total.
    assertThat(usage).isEqualTo(Unsigned256.create(100 + 200 * 3));
    // The size here is two, because the coin index of the BYOC that the contract has is 1; because
    // coins are stored in a fixed list, this list will have a "0" entry at position 0.
    assertThat(removed.convertedExternalCoins()).hasSize(2);
    assertThat(removed.convertedExternalCoins().get(1).getConvertedGasSum())
        .isEqualTo(Unsigned256.create(600));
  }

  @Test
  void moveMpcTokensOnContractRemoval() {
    long mpcTokens = 1000;
    ContractStorage contractWithMpc = createContractWithMpc(mpcTokens).contract;
    ContractState<AccountStateLocal, ContractStorage> state =
        new ContractState<>(AccountStateLocal.initial(), contractWithMpc);
    assertThat(state.local.getRemovedContractsMpcTokens()).isEqualTo(0);
    AccountStateLocal removed = plugin.removeContract(pluginContextDefault, global, state);
    assertThat(removed.getRemovedContractsMpcTokens()).isEqualTo(mpcTokens);
  }

  @Test
  public void invokeCollectBlockchainUsage() {
    long epoch = 0;
    long usage = 10;
    long serviceFee = 100;
    Unsigned256 addedConvertedCoins = Unsigned256.create(10);
    Unsigned256 correspondingGas = Unsigned256.create(1000);
    AccountStateLocal before =
        AccountStateLocal.initial()
            .addBlockchainUsage(epoch, Unsigned256.create(usage))
            .addBlockchainUsage(epoch + 1, Unsigned256.create(999))
            .addServiceFee(Accounts.ONE, serviceFee)
            .updateCollectedCoins(0, addedConvertedCoins, correspondingGas);

    byte[] rpc = ContextFreeInvocationRpc.collectServiceAndInfrastructureFeesForDistribution(epoch);
    assertThat(
            ContextFreeInvocation.COLLECT_SERVICE_AND_INFRASTRUCTURE_FEES_FOR_DISTRIBUTION
                .getInvocationByte())
        .isEqualTo((byte) 20);
    long secondEpoch = (long) Math.pow(2, 21);
    BlockchainPlugin.InvokeResult<AccountStateLocal> invokeResult =
        plugin.invokeContextFree(() -> secondEpoch, global, before, rpc);
    AccountStateLocal updated = invokeResult.updatedState();

    SafeDataInputStream returnValueInputStream =
        SafeDataInputStream.createFromBytes(invokeResult.result());

    long noConvertedCoins = returnValueInputStream.readLong();
    Unsigned256 convertedCoins = Unsigned256.read(returnValueInputStream);
    Unsigned256 convertedGasSum = Unsigned256.read(returnValueInputStream);
    assertThat(noConvertedCoins).isEqualTo(1);
    assertThat(convertedCoins).isEqualTo(addedConvertedCoins);
    assertThat(convertedGasSum).isEqualTo(correspondingGas);

    long blockchainUsageSize = returnValueInputStream.readLong();
    assertThat(blockchainUsageSize).isEqualTo(1);
    long bpUsageEpoch = returnValueInputStream.readLong();
    Unsigned256 bpUsageAmount = Unsigned256.read(returnValueInputStream);
    assertThat(bpUsageEpoch).isEqualTo(epoch);
    assertThat(bpUsageAmount).isEqualTo(Unsigned256.create(usage));

    long noServiceFees = returnValueInputStream.readLong();
    BlockchainAddress producer = BlockchainAddress.read(returnValueInputStream);
    Unsigned256 fees = Unsigned256.read(returnValueInputStream);
    assertThat(noServiceFees).isEqualTo(1);
    assertThat(producer).isEqualTo(Accounts.ONE);
    assertThat(fees).isEqualTo(Unsigned256.create(serviceFee));

    assertThat(updated.convertedExternalCoins().size()).isEqualTo(0);
    assertThat(updated.getServiceFees().size()).isEqualTo(0);
    assertThat(updated.getBlockchainUsage().size()).isEqualTo(1);
  }

  @Test
  public void invokeCollectBlockchainUsage_epochNotEnded() {
    AccountStateLocal initial = AccountStateLocal.initial();
    byte[] rpc = ContextFreeInvocationRpc.collectServiceAndInfrastructureFeesForDistribution(1);
    assertThatThrownBy(() -> plugin.invokeContextFree(() -> 0, global, initial, rpc))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot distribute fees for epochs that has not ended");
    long epochOne = (long) Math.pow(2, 21);
    assertThatThrownBy(() -> plugin.invokeContextFree(() -> epochOne, global, initial, rpc))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot distribute fees for epochs that has not ended");
  }

  @Test
  public void invokeInitiateTwoPhaseCommitByoc() {
    Unsigned256 unsignedAmount = Unsigned256.create(10);
    byte[] twoPhaseRpcWithdraw =
        AccountInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount, SYMBOL_2);
    byte[] twoPhaseRpcDeposit =
        AccountInvocationRpc.initiateByocTransferRecipient(EMPTY, Unsigned256.create(10), SYMBOL_2);
    AccountState<AccountStateLocal, Balance> initialSender = setCoinAccount(10);
    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, initialSender, twoPhaseRpcWithdraw).updatedState();

    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(() -> 0, global, setCoinAccount(10), twoPhaseRpcDeposit).updatedState();

    int coinIndex = GasAndCoinAccountPlugin.getCoinIndex(global, SYMBOL_2);
    assertThat(updatedSender.account.coinBalance(coinIndex).longValueExact()).isEqualTo(0);
    assertThat(updatedRecipient.account.coinBalance(coinIndex).longValueExact()).isEqualTo(10);

    assertThat(updatedSender.account.storedPendingTransfers().containsKey(EMPTY)).isTrue();
    assertThat(updatedRecipient.account.storedPendingTransfers().containsKey(EMPTY)).isTrue();

    AccountState<AccountStateLocal, Balance> retryMarkedAsFailedRecipient =
        invokeAndCheckState(() -> 0, global, updatedRecipient, twoPhaseRpcDeposit).updatedState();

    assertThat(
            invokeAndCheckState(() -> 0, global, updatedSender, twoPhaseRpcWithdraw)
                .updatedState()
                .account
                .storedPendingTransfers()
                .size())
        .isEqualTo(1);
    assertThat(retryMarkedAsFailedRecipient.account.storedPendingTransfers().size()).isEqualTo(1);
  }

  @Test
  public void invokeInitiateTwoPhaseCommit() {
    byte[] twoPhaseRpcWithdraw = AccountInvocationRpc.initiateWithdraw(EMPTY, 5);
    byte[] twoPhaseRpcDeposit = AccountInvocationRpc.initiateDeposit(EMPTY, 5);

    AccountState<AccountStateLocal, Balance> initialSender = tokenAccount(10);
    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, initialSender, twoPhaseRpcWithdraw).updatedState();

    AccountState<AccountStateLocal, Balance> initialRecipient = tokenAccount(5);
    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(() -> 0, global, initialRecipient, twoPhaseRpcDeposit).updatedState();

    assertThat(updatedSender.account.mpcTokens()).isEqualTo(5);
    assertThat(updatedRecipient.account.mpcTokens()).isEqualTo(5);

    assertThat(updatedSender.account.storedPendingTransfers().containsKey(EMPTY)).isTrue();
    assertThat(updatedRecipient.account.storedPendingTransfers().containsKey(EMPTY)).isTrue();

    // Retry transaction that is 'marked as failed' should not overwrite the current transaction
    byte[] twoPhaseRpcWithdrawChanged = AccountInvocationRpc.initiateWithdraw(EMPTY, 10);
    AccountState<AccountStateLocal, Balance> retryMarkedAsFailedSender =
        invokeAndCheckState(() -> 0, global, updatedSender, twoPhaseRpcWithdrawChanged)
            .updatedState();

    byte[] twoPhaseRpcDepositChanged = AccountInvocationRpc.initiateDeposit(EMPTY, 10);
    AccountState<AccountStateLocal, Balance> retryMarkedAsFailedRecipient =
        invokeAndCheckState(() -> 0, global, updatedRecipient, twoPhaseRpcDepositChanged)
            .updatedState();

    assertThat(retryMarkedAsFailedSender.account.storedPendingTransfers().getValue(EMPTY).amount())
        .isEqualTo(Unsigned256.create(5));
    assertThat(
            retryMarkedAsFailedRecipient.account.storedPendingTransfers().getValue(EMPTY).amount())
        .isEqualTo(Unsigned256.create(5));
  }

  @Test
  public void invokeCommitForTwoPhaseCommitByoc() {
    int coinIndex = GasAndCoinAccountPlugin.getCoinIndex(global, SYMBOL_2);
    TransferInformation transactionForSender = TransferInformation.createForWithdraw(20, coinIndex);

    BalanceMutable accountSender = Balance.create().asMutable();
    accountSender.addCoins(coinIndex, Unsigned256.create(5));
    accountSender.addPendingTransfer(EMPTY, transactionForSender);
    AccountState<AccountStateLocal, Balance> senderBeforeCommit =
        new AccountState<>(AccountStateLocal.initial(), accountSender.asImmutable());

    TransferInformation transactionForRecipient =
        TransferInformation.createForDeposit(20, coinIndex);
    BalanceMutable accountRecipient = Balance.create().asMutable();
    accountRecipient.addCoins(coinIndex, Unsigned256.create(5));
    accountRecipient.addPendingTransfer(EMPTY, transactionForRecipient);
    AccountState<AccountStateLocal, Balance> recipientBeforeCommit =
        new AccountState<>(AccountStateLocal.initial(), accountRecipient.asImmutable());

    assertThat(senderBeforeCommit.account.storedPendingTransfers().size()).isEqualTo(1);
    assertThat(recipientBeforeCommit.account.storedPendingTransfers().size()).isEqualTo(1);

    byte[] commitRpc = AccountInvocationRpc.commitTransfer(EMPTY);
    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        invokeAccount(recipientBeforeCommit, commitRpc);
    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        invokeAccount(senderBeforeCommit, commitRpc);

    assertThat(senderAfterCommit.account.storedPendingTransfers().size()).isEqualTo(0);
    assertThat(recipientAfterCommit.account.storedPendingTransfers().size()).isEqualTo(0);

    assertThat(getAccountCoinBalance(SYMBOL_2, senderAfterCommit)).isEqualTo(5);
    assertThat(getAccountCoinBalance(SYMBOL_2, recipientAfterCommit)).isEqualTo(25);
  }

  @Test
  public void retryTransferingWithDifferentAmount() {
    Unsigned256 unsignedAmount1 = Unsigned256.create(5);
    byte[] initiateSender =
        AccountInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount1, SYMBOL_2);
    byte[] initiateRecipient =
        AccountInvocationRpc.initiateByocTransferRecipient(EMPTY, Unsigned256.create(5), SYMBOL_2);

    AccountState<AccountStateLocal, Balance> initialSender = setCoinAccount(10);
    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAccount(initialSender, initiateSender);

    AccountState<AccountStateLocal, Balance> initialRecipient = setCoinAccount(5);
    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAccount(initialRecipient, initiateRecipient);

    assertThat(getAccountCoinBalance(SYMBOL_2, updatedSender)).isEqualTo(5);
    assertThat(getAccountCoinBalance(SYMBOL_2, updatedRecipient)).isEqualTo(5);

    assertThat(updatedSender.account.storedPendingTransfers().containsKey(EMPTY)).isTrue();
    assertThat(updatedRecipient.account.storedPendingTransfers().containsKey(EMPTY)).isTrue();

    Unsigned256 unsignedAmount = Unsigned256.create(10);
    byte[] initiateByocTransferSenderChanged =
        AccountInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount, SYMBOL_2);
    AccountState<AccountStateLocal, Balance> retryMarkedAsFailedSender =
        invokeAccount(updatedSender, initiateByocTransferSenderChanged);

    byte[] initiateByocTransferRecipientChanged =
        AccountInvocationRpc.initiateByocTransferRecipient(EMPTY, Unsigned256.create(10), SYMBOL_2);

    AccountState<AccountStateLocal, Balance> retryMarkedAsFailedRecipient =
        invokeAccount(updatedRecipient, initiateByocTransferRecipientChanged);

    assertThat(retryMarkedAsFailedSender.account.storedPendingTransfers().getValue(EMPTY).amount())
        .isEqualTo(Unsigned256.create(5));
    assertThat(
            retryMarkedAsFailedRecipient.account.storedPendingTransfers().getValue(EMPTY).amount())
        .isEqualTo(Unsigned256.create(5));
  }

  @Test
  public void invokeCommitForTwoPhaseCommit() {
    TransferInformation transactionForSender = TransferInformation.createForWithdraw(5, -1);

    BalanceMutable accountSender = Balance.create().asMutable();
    accountSender.addMpcTokenBalance(5);
    accountSender.addPendingTransfer(EMPTY, transactionForSender);
    AccountState<AccountStateLocal, Balance> senderBeforeCommit =
        new AccountState<>(AccountStateLocal.initial(), accountSender.asImmutable());

    TransferInformation transactionForRecipient = TransferInformation.createForDeposit(5, -1);
    BalanceMutable accountRecipient = Balance.create().asMutable();
    accountRecipient.addMpcTokenBalance(5);
    accountRecipient.addPendingTransfer(EMPTY, transactionForRecipient);
    AccountState<AccountStateLocal, Balance> recipientBeforeCommit =
        new AccountState<>(AccountStateLocal.initial(), accountRecipient.asImmutable());

    assertThat(senderBeforeCommit.account.storedPendingTransfers().size()).isEqualTo(1);
    assertThat(recipientBeforeCommit.account.storedPendingTransfers().size()).isEqualTo(1);

    byte[] commitRpc = AccountInvocationRpc.commitTransfer(EMPTY);
    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        invokeAndCheckState(() -> 0, global, senderBeforeCommit, commitRpc).updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        invokeAndCheckState(() -> 0, global, recipientBeforeCommit, commitRpc).updatedState();

    assertThat(senderAfterCommit.account.storedPendingTransfers().size()).isEqualTo(0);
    assertThat(recipientAfterCommit.account.storedPendingTransfers().size()).isEqualTo(0);

    assertThat(senderAfterCommit.account.mpcTokens()).isEqualTo(5);
    assertThat(recipientAfterCommit.account.mpcTokens()).isEqualTo(10);
  }

  @Test
  public void invokeAbortForTwoPhaseCommitByoc() {
    int coinIndex = GasAndCoinAccountPlugin.getCoinIndex(global, SYMBOL_2);
    TransferInformation transactionForSender = TransferInformation.createForWithdraw(10, coinIndex);

    BalanceMutable accountSender = Balance.create().asMutable();
    accountSender.addCoins(coinIndex, Unsigned256.create(5));
    accountSender.addPendingTransfer(EMPTY, transactionForSender);
    TransferInformation transactionForRecipient =
        TransferInformation.createForDeposit(10, coinIndex);
    BalanceMutable accountRecipient = Balance.create().asMutable();
    accountRecipient.addCoins(coinIndex, Unsigned256.create(5));
    accountRecipient.addPendingTransfer(EMPTY, transactionForRecipient);

    AccountState<AccountStateLocal, Balance> senderBeforeAbort =
        new AccountState<>(AccountStateLocal.initial(), accountSender.asImmutable());
    AccountState<AccountStateLocal, Balance> recipientBeforeAbort =
        new AccountState<>(AccountStateLocal.initial(), accountRecipient.asImmutable());

    assertThat(senderBeforeAbort.account.storedPendingTransfers().size()).isEqualTo(1);
    assertThat(recipientBeforeAbort.account.storedPendingTransfers().size()).isEqualTo(1);

    byte[] abortRpc = AccountInvocationRpc.abortTransfer(EMPTY);
    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAccount(senderBeforeAbort, abortRpc);
    AccountState<AccountStateLocal, Balance> recipientAfterAbort =
        invokeAccount(recipientBeforeAbort, abortRpc);

    assertThat(senderAfterAbort.account.storedPendingTransfers().size()).isEqualTo(0);
    assertThat(recipientAfterAbort.account.storedPendingTransfers().size()).isEqualTo(0);

    assertThat(getAccountCoinBalance(SYMBOL_2, senderAfterAbort)).isEqualTo(15);
    assertThat(getAccountCoinBalance(SYMBOL_2, recipientAfterAbort)).isEqualTo(5);
  }

  @Test
  public void invokeAbortForTwoPhaseCommit() {
    TransferInformation transactionForSender = TransferInformation.createForWithdraw(5, -1);

    BalanceMutable accountSender = Balance.create().asMutable();
    accountSender.addMpcTokenBalance(5);
    accountSender.addPendingTransfer(EMPTY, transactionForSender);
    AccountState<AccountStateLocal, Balance> senderBeforeAbort =
        new AccountState<>(AccountStateLocal.initial(), accountSender.asImmutable());

    TransferInformation transactionForRecipient = TransferInformation.createForDeposit(5, -1);
    BalanceMutable accountRecipient = Balance.create().asMutable();
    accountRecipient.addMpcTokenBalance(5);
    accountRecipient.addPendingTransfer(EMPTY, transactionForRecipient);
    AccountState<AccountStateLocal, Balance> recipientBeforeAbort =
        new AccountState<>(AccountStateLocal.initial(), accountRecipient.asImmutable());

    assertThat(senderBeforeAbort.account.storedPendingTransfers().size()).isEqualTo(1);
    assertThat(recipientBeforeAbort.account.storedPendingTransfers().size()).isEqualTo(1);

    byte[] abortRpc = AccountInvocationRpc.abortTransfer(EMPTY);
    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAndCheckState(() -> 0, global, senderBeforeAbort, abortRpc).updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterAbort =
        invokeAndCheckState(() -> 0, global, recipientBeforeAbort, abortRpc).updatedState();

    assertThat(senderAfterAbort.account.storedPendingTransfers().size()).isEqualTo(0);
    assertThat(recipientAfterAbort.account.storedPendingTransfers().size()).isEqualTo(0);

    assertThat(senderAfterAbort.account.mpcTokens()).isEqualTo(10);
    assertThat(recipientAfterAbort.account.mpcTokens()).isEqualTo(5);
  }

  @Test
  public void invokeEntireTwoPhaseCommitSuccess() {

    byte[] twoPhaseRpcWithdraw = AccountInvocationRpc.initiateWithdraw(EMPTY, 10);
    byte[] twoPhaseRpcDeposit = AccountInvocationRpc.initiateDeposit(EMPTY, 10);
    byte[] twoPhaseRpcCommit = AccountInvocationRpc.commitTransfer(EMPTY);

    AccountState<AccountStateLocal, Balance> initialSender = tokenAccount(50);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, initialSender, twoPhaseRpcWithdraw).updatedState();

    AccountState<AccountStateLocal, Balance> initialRecipient = tokenAccount(15);
    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(() -> 0, global, initialRecipient, twoPhaseRpcDeposit).updatedState();

    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        invokeAndCheckState(() -> 0, global, updatedSender, twoPhaseRpcCommit).updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        invokeAndCheckState(() -> 0, global, updatedRecipient, twoPhaseRpcCommit).updatedState();

    assertThat(senderAfterCommit.account.storedPendingTransfers().size()).isEqualTo(0);
    assertThat(recipientAfterCommit.account.storedPendingTransfers().size()).isEqualTo(0);

    assertThat(senderAfterCommit.account.mpcTokens()).isEqualTo(40);
    assertThat(recipientAfterCommit.account.mpcTokens()).isEqualTo(25);
  }

  @Test
  public void invokeEntireTwoPhaseCommitSuccessByoc() {
    Unsigned256 amount = Unsigned256.create("200000000000000000000"); // 200 with 18 decimals
    byte[] twoPhaseRpcWithdraw =
        AccountInvocationRpc.initiateByocTransferSender(EMPTY, amount, SYMBOL_2);
    byte[] twoPhaseRpcDeposit =
        AccountInvocationRpc.initiateByocTransferRecipient(EMPTY, amount, SYMBOL_2);
    byte[] twoPhaseRpcCommit = AccountInvocationRpc.commitTransfer(EMPTY);

    AccountState<AccountStateLocal, Balance> initialSender = setCoinAccount(amount);
    AccountState<AccountStateLocal, Balance> initialRecipient = setCoinAccount(0);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAccount(initialSender, twoPhaseRpcWithdraw);
    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAccount(initialRecipient, twoPhaseRpcDeposit);

    assertThat(getAccountCoinBalance(SYMBOL_2, updatedSender)).isEqualTo(0);
    assertThat(getAccountCoinBalance(SYMBOL_2, updatedRecipient)).isEqualTo(0);

    assertThat(updatedSender.account.storedPendingTransfers().size()).isEqualTo(1);
    assertThat(updatedRecipient.account.storedPendingTransfers().size()).isEqualTo(1);

    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        invokeAccount(updatedSender, twoPhaseRpcCommit);
    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        invokeAccount(updatedRecipient, twoPhaseRpcCommit);

    assertThat(senderAfterCommit.account.storedPendingTransfers().size()).isEqualTo(0);
    assertThat(recipientAfterCommit.account.storedPendingTransfers().size()).isEqualTo(0);

    assertThat(getAccountCoinBalance(SYMBOL_2, senderAfterCommit)).isEqualTo(0);
    assertThat(getAccountCoinBalanceAsUnsigned256(SYMBOL_2, recipientAfterCommit))
        .isEqualTo(amount);
  }

  @Test
  public void invokeEntireTwoPhaseCommitAbort() {
    byte[] twoPhaseRpcWithdraw = AccountInvocationRpc.initiateWithdraw(EMPTY, 7);
    byte[] twoPhaseRpcDeposit = AccountInvocationRpc.initiateDeposit(EMPTY, 7);
    byte[] twoPhaseRpcAbort = AccountInvocationRpc.abortTransfer(EMPTY);

    AccountState<AccountStateLocal, Balance> initialSender = tokenAccount(33);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, initialSender, twoPhaseRpcWithdraw).updatedState();

    assertThat(updatedSender.account.mpcTokens()).isEqualTo(33 - 7);

    AccountState<AccountStateLocal, Balance> initialRecipient = tokenAccount(22);
    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(() -> 0, global, initialRecipient, twoPhaseRpcDeposit).updatedState();

    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAndCheckState(() -> 0, global, updatedSender, twoPhaseRpcAbort).updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterAbort =
        invokeAndCheckState(() -> 0, global, updatedRecipient, twoPhaseRpcAbort).updatedState();

    assertThat(senderAfterAbort.account.storedPendingTransfers().size()).isEqualTo(0);
    assertThat(recipientAfterAbort.account.storedPendingTransfers().size()).isEqualTo(0);

    assertThat(senderAfterAbort.account.mpcTokens()).isEqualTo(33);
    assertThat(recipientAfterAbort.account.mpcTokens()).isEqualTo(22);
  }

  @Test
  public void invokeEntireTwoPhaseCommitAbortByoc() {
    Unsigned256 unsignedAmount = Unsigned256.create(10);
    byte[] twoPhaseRpcWithdraw =
        AccountInvocationRpc.initiateByocTransferSender(EMPTY, unsignedAmount, SYMBOL_2);
    byte[] twoPhaseRpcDeposit =
        AccountInvocationRpc.initiateByocTransferRecipient(EMPTY, Unsigned256.create(10), SYMBOL_2);
    byte[] twoPhaseRpcAbort = AccountInvocationRpc.abortTransfer(EMPTY);

    AccountState<AccountStateLocal, Balance> initialSender = setCoinAccount(59);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAccount(initialSender, twoPhaseRpcWithdraw);

    assertThat(getAccountCoinBalance(SYMBOL_2, updatedSender)).isEqualTo(59 - 10);

    AccountState<AccountStateLocal, Balance> initialRecipient = setCoinAccount(60);

    AccountState<AccountStateLocal, Balance> updatedSenderAfterAbort =
        invokeAccount(updatedSender, twoPhaseRpcAbort);
    assertThat(updatedSenderAfterAbort.account.storedPendingTransfers().size()).isEqualTo(0);

    AccountState<AccountStateLocal, Balance> updateRecipient =
        invokeAccount(initialRecipient, twoPhaseRpcDeposit);

    AccountState<AccountStateLocal, Balance> updatedRecipientAfterAbort =
        invokeAccount(updateRecipient, twoPhaseRpcAbort);
    assertThat(updatedRecipientAfterAbort.account.storedPendingTransfers().size()).isEqualTo(0);

    assertThat(getAccountCoinBalance(SYMBOL_2, updatedSenderAfterAbort)).isEqualTo(59);
    assertThat(updatedRecipientAfterAbort.account.coinBalance(2).longValueExact()).isEqualTo(60);
  }

  @Test
  public void twoPhaseCommitOnEmptyTransactionDoesNothing() {
    byte[] twoPhaseRpcCommit = AccountInvocationRpc.commitTransfer(EMPTY);
    int balance = 12;

    AccountState<AccountStateLocal, Balance> sender = tokenAccount(balance);
    AccountState<AccountStateLocal, Balance> senderByoc = setCoinAccount(balance);

    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        invokeAccount(sender, twoPhaseRpcCommit);
    AccountState<AccountStateLocal, Balance> senderAfterCommitByoc =
        invokeAccount(senderByoc, twoPhaseRpcCommit);

    assertThat(senderAfterCommit.account.mpcTokens()).isEqualTo(balance);
    assertThat(getAccountCoinBalance(SYMBOL_2, senderAfterCommitByoc)).isEqualTo(balance);
  }

  @Test
  public void twoPhaseAbortOnEmptyTransactionDoesNothing() {
    byte[] twoPhaseRpcAbort = AccountInvocationRpc.abortTransfer(EMPTY);
    int balance = 12;

    AccountState<AccountStateLocal, Balance> sender = tokenAccount(balance);
    AccountState<AccountStateLocal, Balance> senderByoc = setCoinAccount(balance);

    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAccount(sender, twoPhaseRpcAbort);
    AccountState<AccountStateLocal, Balance> senderAfterAbortByoc =
        invokeAccount(senderByoc, twoPhaseRpcAbort);

    assertThat(senderAfterAbort.account.mpcTokens()).isEqualTo(balance);
    assertThat(getAccountCoinBalance(SYMBOL_2, senderAfterAbortByoc)).isEqualTo(balance);
  }

  @Test
  public void emptyCommitForOtherDoesNothing() {
    byte[] twoPhaseRpcCommit = AccountInvocationRpc.commitDelegateStakes(EMPTY);

    AccountState<AccountStateLocal, Balance> sender = tokenAccount(12);

    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        invokeAndCheckState(() -> 0, global, sender, twoPhaseRpcCommit).updatedState();

    assertThat(senderAfterCommit.account.mpcTokens()).isEqualTo(12);
  }

  @Test
  public void emptyAbortForOtherDoesNothing() {
    byte[] twoPhaseRpcAbort = AccountInvocationRpc.abortDelegateStakes(EMPTY);

    AccountState<AccountStateLocal, Balance> sender = tokenAccount(12);

    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAndCheckState(() -> 0, global, sender, twoPhaseRpcAbort).updatedState();

    assertThat(senderAfterAbort.account.mpcTokens()).isEqualTo(12);
  }

  @Test
  public void emptyDelegateStakesDoesNothing() {
    byte[] twoPhaseRpcAbort = AccountInvocationRpc.abortDelegateStakes(EMPTY);

    AccountState<AccountStateLocal, Balance> sender = tokenAccount(12);

    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAndCheckState(() -> 0, global, sender, twoPhaseRpcAbort).updatedState();

    assertThat(senderAfterAbort.account.mpcTokens()).isEqualTo(12);
  }

  @Test
  public void emptyreturnDelegatedStakesDoesNothing() {
    byte[] twoPhaseRpcAbort = AccountInvocationRpc.abortDelegateStakes(EMPTY);

    AccountState<AccountStateLocal, Balance> sender = tokenAccount(12);

    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAndCheckState(() -> 0, global, sender, twoPhaseRpcAbort).updatedState();

    assertThat(senderAfterAbort.account.mpcTokens()).isEqualTo(12);
  }

  @Test
  public void twoPhaseTransferWithNotEnoughBalance() {
    long toDeduct = 5;
    long balance = 4;
    byte[] initiateWithdrawRpc = AccountInvocationRpc.initiateWithdraw(EMPTY, toDeduct);
    AccountState<AccountStateLocal, Balance> account = tokenAccount(balance);

    assertThatThrownBy(
            () -> invokeAndCheckState(pluginContextDefault, global, account, initiateWithdrawRpc))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Cannot deduct %s MPC tokens with a balance of %s".formatted(toDeduct, balance));
  }

  @Test
  public void twoPhaseTransferToSelfGivesError() {
    byte[] initiateWithdrawRpc = AccountInvocationRpc.initiateWithdraw(EMPTY, 5);
    byte[] initiateDepositRpc = AccountInvocationRpc.initiateDeposit(EMPTY, 5);

    AccountState<AccountStateLocal, Balance> account = tokenAccount(50);

    AccountState<AccountStateLocal, Balance> accountAfterWithdraw =
        invokeAndCheckState(pluginContextDefault, global, account, initiateWithdrawRpc)
            .updatedState();
    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                    pluginContextDefault, global, accountAfterWithdraw, initiateDepositRpc))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot initiate transfer between the same account");

    AccountState<AccountStateLocal, Balance> accountAfterDeposit =
        invokeAndCheckState(pluginContextDefault, global, account, initiateDepositRpc)
            .updatedState();
    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                    pluginContextDefault, global, accountAfterDeposit, initiateWithdrawRpc))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot initiate transfer between the same account");
  }

  @Test
  public void delegateStakesToSelfGivesError() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 5, firstAddress);
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 5, firstAddress);

    AccountState<AccountStateLocal, Balance> account = tokenAccount(50);

    AccountState<AccountStateLocal, Balance> accountAfterWithdraw =
        invokeAndCheckState(pluginContextDefault, global, account, initiateDelegateStakes)
            .updatedState();
    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                    pluginContextDefault,
                    global,
                    accountAfterWithdraw,
                    initiateReceiveDelegatedStakes))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot perform any delegation of stakes between the same account");

    AccountState<AccountStateLocal, Balance> accountAfterDeposit =
        invokeAndCheckState(pluginContextDefault, global, account, initiateReceiveDelegatedStakes)
            .updatedState();
    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                    pluginContextDefault, global, accountAfterDeposit, initiateDelegateStakes))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot perform any delegation of stakes between the same account");
  }

  @Test
  public void invokeCollectServiceAndInfrastructureFeesForDistribution() {
    byte[] rpc = ContextFreeInvocationRpc.collectServiceAndInfrastructureFeesForDistribution(1);

    AccountStateLocal initial = AccountStateLocal.initial();
    initial = initial.addInfrastructureFees(Unsigned256.create(5));
    long secondEpoch = (long) Math.pow(3, 21);
    BlockchainPlugin.InvokeResult<AccountStateLocal> invokeResult =
        plugin.invokeContextFree(() -> secondEpoch, global, initial, rpc);
    SafeDataInputStream infrastructureFeeResult =
        SafeDataInputStream.createFromBytes(invokeResult.result());
    AccountStateLocal updatedAccountState = invokeResult.updatedState();
    assertThat(updatedAccountState.getInfrastructureFeesSum())
        .isEqualTo(Unsigned256.read(infrastructureFeeResult));
  }

  @Test
  public void returnDelegatedStakesToSelfGivesError() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 10, firstAddress);
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 10, firstAddress);
    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));
    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(15));

    byte[] initiatereturnDelegatedStakesRpc =
        AccountInvocationRpc.initiateRetractDelegatedStakes(EMPTY, 5, firstAddress);
    byte[] initiateReturnDelegatedStakes =
        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, 5, firstAddress);

    AccountState<AccountStateLocal, Balance> accountAfterWithdraw =
        invokeAndCheckState(
                pluginContextDefault, global, senderAfterCommit, initiatereturnDelegatedStakesRpc)
            .updatedState();
    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                    pluginContextDefault,
                    global,
                    accountAfterWithdraw,
                    initiateReturnDelegatedStakes))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot perform any delegation of stakes between the same account");

    AccountState<AccountStateLocal, Balance> accountAfterDeposit =
        invokeAndCheckState(
                pluginContextDefault, global, recipientAfterCommit, initiateReturnDelegatedStakes)
            .updatedState();
    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                    pluginContextDefault,
                    global,
                    accountAfterDeposit,
                    initiatereturnDelegatedStakesRpc))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot perform any delegation of stakes between the same account");
  }

  @Test
  public void canStoreSameTransactionTwice() {
    byte[] initiateWithdrawRpc = AccountInvocationRpc.initiateWithdraw(EMPTY, 5);
    AccountState<AccountStateLocal, Balance> account = tokenAccount(25);

    AccountState<AccountStateLocal, Balance> accountAfterInitiate =
        invokeAndCheckState(pluginContextDefault, global, account, initiateWithdrawRpc)
            .updatedState();

    AccountState<AccountStateLocal, Balance> accountStateAfterSecondInitiate =
        invokeAndCheckState(pluginContextDefault, global, accountAfterInitiate, initiateWithdrawRpc)
            .updatedState();

    assertThat(accountStateAfterSecondInitiate.account.storedPendingTransfers().containsKey(EMPTY))
        .isTrue();
  }

  @Test
  public void retryDoesNotDeductTokensTwiceIfCallbackOutOfGas() {
    byte[] twoPhaseRpcWithdraw = AccountInvocationRpc.initiateWithdraw(EMPTY, 10);
    byte[] twoPhaseRpcDeposit = AccountInvocationRpc.initiateDeposit(EMPTY, 10);
    byte[] twoPhaseRpcCommit = AccountInvocationRpc.commitTransfer(EMPTY);

    AccountState<AccountStateLocal, Balance> accountSender = tokenAccount(35);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, accountSender, twoPhaseRpcWithdraw).updatedState();

    assertThat(updatedSender.account.mpcTokens()).isEqualTo(35 - 10);

    AccountState<AccountStateLocal, Balance> accountRecipient = tokenAccount(20);
    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(() -> 0, global, accountRecipient, twoPhaseRpcDeposit).updatedState();

    assertThat(updatedRecipient.account.mpcTokens()).isEqualTo(20);

    assertThat(updatedSender.account.storedPendingTransfers().size()).isEqualTo(1);
    assertThat(updatedRecipient.account.storedPendingTransfers().size()).isEqualTo(1);

    AccountState<AccountStateLocal, Balance> retrySender =
        invokeAndCheckState(() -> 0, global, updatedSender, twoPhaseRpcWithdraw).updatedState();

    assertThat(retrySender.account.mpcTokens()).isEqualTo(35 - 10);

    AccountState<AccountStateLocal, Balance> retryRecipient =
        invokeAndCheckState(() -> 0, global, updatedRecipient, twoPhaseRpcDeposit).updatedState();

    assertThat(retryRecipient.account.mpcTokens()).isEqualTo(20);

    AccountState<AccountStateLocal, Balance> commitSender =
        invokeAndCheckState(() -> 0, global, updatedSender, twoPhaseRpcCommit).updatedState();

    AccountState<AccountStateLocal, Balance> commitRecipient =
        invokeAndCheckState(() -> 0, global, updatedRecipient, twoPhaseRpcCommit).updatedState();

    assertThat(commitSender.account.storedPendingTransfers().size()).isEqualTo(0);
    assertThat(commitRecipient.account.storedPendingTransfers().size()).isEqualTo(0);

    assertThat(commitSender.account.mpcTokens()).isEqualTo(35 - 10);
    assertThat(commitRecipient.account.mpcTokens()).isEqualTo(20 + 10);
  }

  @Test
  public void invokeInitiateDelegateStakes() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 5, secondAddress);
    byte[] twoPhaseRpcGetStaked =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 5, firstAddress);

    AccountState<AccountStateLocal, Balance> initialSender = tokenAccount(10);
    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, initialSender, initiateDelegateStakes).updatedState();

    AccountState<AccountStateLocal, Balance> initialRecipient = tokenAccount(5);
    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(() -> 0, global, initialRecipient, twoPhaseRpcGetStaked).updatedState();

    assertThat(updatedSender.account.mpcTokens()).isEqualTo(5);
    assertThat(updatedRecipient.account.mpcTokens()).isEqualTo(5);

    assertThat(updatedSender.account.storedPendingStakeDelegations().containsKey(EMPTY)).isTrue();
    assertThat(updatedRecipient.account.storedPendingStakeDelegations().containsKey(EMPTY))
        .isTrue();

    // Retry stake for other that is 'marked as failed' should not overwrite the current
    // StakeDelegation
    byte[] initiateDelegateStakesChanged =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 10, secondAddress);
    AccountState<AccountStateLocal, Balance> retryMarkedAsFailedSender =
        invokeAndCheckState(() -> 0, global, updatedSender, initiateDelegateStakesChanged)
            .updatedState();

    byte[] twoPhaseRpcGetStakedChanged =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 10, firstAddress);
    AccountState<AccountStateLocal, Balance> retryMarkedAsFailedRecipient =
        invokeAndCheckState(() -> 0, global, updatedRecipient, twoPhaseRpcGetStakedChanged)
            .updatedState();

    assertThat(
            retryMarkedAsFailedSender
                .account
                .storedPendingStakeDelegations()
                .getValue(EMPTY)
                .amount)
        .isEqualTo(5);
    assertThat(
            retryMarkedAsFailedRecipient
                .account
                .storedPendingStakeDelegations()
                .getValue(EMPTY)
                .amount)
        .isEqualTo(5);
  }

  @Test
  public void invokeCommitDelegateStakes() {
    StakeDelegation delegateStakes = StakeDelegation.createDelegation(secondAddress, 5, 0L);

    BalanceMutable accountSender = Balance.create().asMutable();
    accountSender.addMpcTokenBalance(5);
    accountSender.addPendingStakeDelegation(EMPTY, delegateStakes);
    AccountState<AccountStateLocal, Balance> senderBeforeCommit =
        new AccountState<>(AccountStateLocal.initial(), accountSender.asImmutable());

    StakeDelegation receiveDelegatedStakes =
        StakeDelegation.createReceiveDelegation(firstAddress, 5, null, null);
    BalanceMutable accountRecipient = Balance.create().asMutable();
    accountRecipient.addMpcTokenBalance(5);
    accountRecipient.addPendingStakeDelegation(EMPTY, receiveDelegatedStakes);
    AccountState<AccountStateLocal, Balance> recipientBeforeCommit =
        new AccountState<>(AccountStateLocal.initial(), accountRecipient.asImmutable());

    assertThat(senderBeforeCommit.account.storedPendingStakeDelegations().size()).isEqualTo(1);
    assertThat(recipientBeforeCommit.account.storedPendingStakeDelegations().size()).isEqualTo(1);

    byte[] twoPhaseCommitRpcSender = AccountInvocationRpc.commitDelegateStakes(EMPTY);
    byte[] twoPhaseCommitRpcRecipient = AccountInvocationRpc.commitDelegateStakes(EMPTY);
    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        invokeAndCheckState(() -> 0, global, senderBeforeCommit, twoPhaseCommitRpcSender)
            .updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        invokeAndCheckState(() -> 0, global, recipientBeforeCommit, twoPhaseCommitRpcRecipient)
            .updatedState();

    assertThat(senderAfterCommit.account.storedPendingStakeDelegations().size()).isEqualTo(0);
    assertThat(recipientAfterCommit.account.storedPendingStakeDelegations().size()).isEqualTo(0);

    assertThat(senderAfterCommit.account.mpcTokens()).isEqualTo(5);
    assertThat(senderAfterCommit.account.stakedTokens()).isEqualTo(0);
    assertThat(
            recipientAfterCommit
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes)
        .isEqualTo(5);
  }

  @Test
  public void invokeAbortDelegateStakes() {
    StakeDelegation delegateStakes = StakeDelegation.createDelegation(secondAddress, 5, 10L);

    BalanceMutable accountSender = Balance.create().asMutable();
    accountSender.addMpcTokenBalance(5);
    accountSender.addPendingStakeDelegation(EMPTY, delegateStakes);
    accountSender.delegateStakes(5);
    AccountState<AccountStateLocal, Balance> senderBeforeAbort =
        new AccountState<>(AccountStateLocal.initial(), accountSender.asImmutable());

    StakeDelegation receiveDelegatedStakes =
        StakeDelegation.createReceiveDelegation(firstAddress, 5, null, null);
    BalanceMutable accountRecipient = Balance.create().asMutable();
    accountRecipient.addMpcTokenBalance(5);
    accountRecipient.addPendingStakeDelegation(EMPTY, receiveDelegatedStakes);
    AccountState<AccountStateLocal, Balance> recipientBeforeAbort =
        new AccountState<>(AccountStateLocal.initial(), accountRecipient.asImmutable());

    assertThat(senderBeforeAbort.account.storedPendingStakeDelegations().size()).isEqualTo(1);
    assertThat(recipientBeforeAbort.account.storedPendingStakeDelegations().size()).isEqualTo(1);

    byte[] twoPhaseAbortRpcSender = AccountInvocationRpc.abortDelegateStakes(EMPTY);
    byte[] twoPhaseAbortRpcRecipient = AccountInvocationRpc.abortDelegateStakes(EMPTY);
    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAndCheckState(() -> 0, global, senderBeforeAbort, twoPhaseAbortRpcSender)
            .updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterAbort =
        invokeAndCheckState(() -> 0, global, recipientBeforeAbort, twoPhaseAbortRpcRecipient)
            .updatedState();

    assertThat(senderAfterAbort.account.storedPendingStakeDelegations().size()).isEqualTo(0);
    assertThat(recipientAfterAbort.account.storedPendingStakeDelegations().size()).isEqualTo(0);

    assertThat(senderAfterAbort.account.mpcTokens()).isEqualTo(5);
  }

  @Test
  public void invokeEntireDelegateStakesSuccess() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 10, secondAddress);
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 10, firstAddress);

    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(15));

    assertThat(senderAfterCommit.account.storedPendingStakeDelegations().size()).isEqualTo(0);
    assertThat(recipientAfterCommit.account.storedPendingStakeDelegations().size()).isEqualTo(0);

    assertThat(senderAfterCommit.account.mpcTokens()).isEqualTo(40);
    assertThat(senderAfterCommit.account.stakedTokens()).isEqualTo(0);
    assertThat(recipientAfterCommit.account.mpcTokens()).isEqualTo(15);
    assertThat(
            recipientAfterCommit
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes)
        .isEqualTo(10);
  }

  @Test
  public void invokeEntireDelegateStakesAbort() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 7, secondAddress);
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 7, firstAddress);
    byte[] twoPhaseAbortRpcSender = AccountInvocationRpc.abortDelegateStakes(EMPTY);
    byte[] twoPhaseAbortRpcRecipient = AccountInvocationRpc.abortDelegateStakes(EMPTY);

    AccountState<AccountStateLocal, Balance> initialSender = tokenAccount(33);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, initialSender, initiateDelegateStakes).updatedState();

    assertThat(updatedSender.account.mpcTokens()).isEqualTo(33 - 7);

    AccountState<AccountStateLocal, Balance> initialRecipient = tokenAccount(22);
    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(() -> 0, global, initialRecipient, initiateReceiveDelegatedStakes)
            .updatedState();

    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAndCheckState(() -> 0, global, updatedSender, twoPhaseAbortRpcSender).updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterAbort =
        invokeAndCheckState(() -> 0, global, updatedRecipient, twoPhaseAbortRpcRecipient)
            .updatedState();

    assertThat(senderAfterAbort.account.storedPendingStakeDelegations().size()).isEqualTo(0);
    assertThat(recipientAfterAbort.account.storedPendingStakeDelegations().size()).isEqualTo(0);

    assertThat(senderAfterAbort.account.mpcTokens()).isEqualTo(33);
    assertThat(recipientAfterAbort.account.mpcTokens()).isEqualTo(22);
    assertThat(recipientAfterAbort.account.delegatedStakesFromOthers().getValue(firstAddress))
        .isNull();
  }

  @Test
  public void addToPendingRetractedDelegatedStakesTwice() {
    BalanceMutable balanceMutable = tokenAccount(50).account.asMutable();
    balanceMutable.addPendingRetractedDelegatedStakes(20, 10);
    balanceMutable.addPendingRetractedDelegatedStakes(10, 20);
    assertThat(balanceMutable.pendingRetractedDelegatedStakes().size()).isEqualTo(2);
  }

  @Test
  public void abortRetractDelegatedStakesUpdatesCorrectAmount() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 30, secondAddress);
    Balance delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50)).account;
    BalanceMutable retractDelegatedStakes = delegateStakes.asMutable();
    retractDelegatedStakes.retractDelegatedStakes(secondAddress, 20);
    retractDelegatedStakes.abortRetractDelegatedStakes(secondAddress, 20);
    Balance abortRetractDelegatedStakes = retractDelegatedStakes.asImmutable();
    assertThat(abortRetractDelegatedStakes.delegatedStakesToOthers().getValue(secondAddress))
        .isEqualTo(30);
  }

  @Test
  public void abortRetractDelegatedRetractingAll() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 50, secondAddress);
    Balance delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50)).account;
    BalanceMutable retractDelegatedStakes = delegateStakes.asMutable();
    retractDelegatedStakes.retractDelegatedStakes(secondAddress, 50);
    retractDelegatedStakes.abortRetractDelegatedStakes(secondAddress, 50);
    Balance abortRetractDelegatedStakes = retractDelegatedStakes.asImmutable();
    assertThat(abortRetractDelegatedStakes.delegatedStakesToOthers().getValue(secondAddress))
        .isEqualTo(50);
  }

  @Test
  public void addToPendingRetractedDelegatedStakes_edgeCases() {
    BalanceMutable addToPendingRetracted = tokenAccount(50).account.asMutable();
    addToPendingRetracted.addPendingRetractedDelegatedStakes(10, 10);
    assertThat(
            addToPendingRetracted
                .pendingRetractedDelegatedStakes()
                .getValue(10 + Balance.UNSTAKE_DURATION_MILLIS))
        .isEqualTo(10);
    addToPendingRetracted.addPendingRetractedDelegatedStakes(45, 20);
    assertThat(
            addToPendingRetracted
                .pendingRetractedDelegatedStakes()
                .getValue(20 + Balance.UNSTAKE_DURATION_MILLIS))
        .isEqualTo(45);
  }

  @Test
  public void getStakedForTwiceForSameAccount() {
    BalanceMutable stakedForTwice = tokenAccount(100).account.asMutable();
    stakedForTwice.commitReceiveDelegatedStakes(firstAddress, 10, null);
    stakedForTwice.commitReceiveDelegatedStakes(firstAddress, 20, null);
    assertThat(
            stakedForTwice
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes)
        .isEqualTo(30);
  }

  @Test
  public void invokeInitiateReturnDelegatedStakes() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 10, secondAddress);
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 10, firstAddress);

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));

    AccountState<AccountStateLocal, Balance> receiveDelegatedStakes =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(15));

    byte[] twoPhaseRpcRetractDelegatedStakes =
        AccountInvocationRpc.initiateRetractDelegatedStakes(EMPTY, 5, secondAddress);
    byte[] twoPhaseRpcReturnDelegatedStakes =
        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, 5, firstAddress);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, delegateStakes, twoPhaseRpcRetractDelegatedStakes)
            .updatedState();

    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(
                () -> 0, global, receiveDelegatedStakes, twoPhaseRpcReturnDelegatedStakes)
            .updatedState();

    assertThat(updatedSender.account.storedPendingStakeDelegations().containsKey(EMPTY)).isTrue();
    assertThat(updatedRecipient.account.storedPendingStakeDelegations().containsKey(EMPTY))
        .isTrue();

    // Retry return delegated stakes that is 'marked as failed' should not overwrite the current
    // StakeDelegation
    byte[] twoPhaseRpcRetractDelegatedStakesChanged =
        AccountInvocationRpc.initiateRetractDelegatedStakes(EMPTY, 10, secondAddress);
    AccountState<AccountStateLocal, Balance> retryMarkedAsFailedSender =
        invokeAndCheckState(
                () -> 0, global, updatedSender, twoPhaseRpcRetractDelegatedStakesChanged)
            .updatedState();

    byte[] twoPhaseRpcReturnDelegatedStakesChanged =
        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, 10, firstAddress);
    AccountState<AccountStateLocal, Balance> retryMarkedAsFailedRecipient =
        invokeAndCheckState(
                () -> 0, global, updatedRecipient, twoPhaseRpcReturnDelegatedStakesChanged)
            .updatedState();

    assertThat(
            retryMarkedAsFailedSender
                .account
                .storedPendingStakeDelegations()
                .getValue(EMPTY)
                .amount)
        .isEqualTo(5);
    assertThat(
            retryMarkedAsFailedRecipient
                .account
                .storedPendingStakeDelegations()
                .getValue(EMPTY)
                .amount)
        .isEqualTo(5);
  }

  @Test
  public void invokeCommitReturnDelegatedStakes() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 15, secondAddress);
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 15, firstAddress);

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));

    AccountState<AccountStateLocal, Balance> receiveDelegatedStakes =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(15));

    StakeDelegation retractDelegatedStakes =
        StakeDelegation.createRetractDelegation(secondAddress, 5);

    StakeDelegation returnDelegatedStakes = StakeDelegation.createReturnDelegation(firstAddress, 5);

    BalanceMutable accountSender = delegateStakes.account.asMutable();
    accountSender.addPendingStakeDelegation(EMPTY, retractDelegatedStakes);
    AccountState<AccountStateLocal, Balance> senderBeforeCommit =
        new AccountState<>(AccountStateLocal.initial(), accountSender.asImmutable());

    BalanceMutable accountRecipient = receiveDelegatedStakes.account.asMutable();
    accountRecipient.addPendingStakeDelegation(EMPTY, returnDelegatedStakes);
    AccountState<AccountStateLocal, Balance> recipientBeforeCommit =
        new AccountState<>(AccountStateLocal.initial(), accountRecipient.asImmutable());

    assertThat(senderBeforeCommit.account.storedPendingStakeDelegations().size()).isEqualTo(1);
    assertThat(recipientBeforeCommit.account.storedPendingStakeDelegations().size()).isEqualTo(1);

    byte[] commitRpcSender = AccountInvocationRpc.commitDelegateStakes(EMPTY);
    byte[] commitRpcRecipient = AccountInvocationRpc.commitDelegateStakes(EMPTY);
    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        invokeAndCheckState(() -> 0, global, senderBeforeCommit, commitRpcSender).updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        invokeAndCheckState(() -> 0, global, recipientBeforeCommit, commitRpcRecipient)
            .updatedState();

    assertThat(senderAfterCommit.account.storedPendingStakeDelegations().size()).isEqualTo(0);
    assertThat(recipientAfterCommit.account.storedPendingStakeDelegations().size()).isEqualTo(0);

    assertThat(senderAfterCommit.account.mpcTokens()).isEqualTo(35);
    assertThat(senderAfterCommit.account.pendingRetractedDelegatedStakes().size()).isEqualTo(1);
    assertThat(
            recipientAfterCommit
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes)
        .isEqualTo(15);
  }

  @Test
  public void invokeAbortReturnDelegatedStakes() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 25, secondAddress);
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 25, firstAddress);

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));

    AccountState<AccountStateLocal, Balance> receiveDelegatedStakes =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(15));

    StakeDelegation returnDelegatedStakes = StakeDelegation.createReturnDelegation(firstAddress, 5);

    StakeDelegation retractDelegatedStakes =
        StakeDelegation.createRetractDelegation(secondAddress, 5);

    BalanceMutable accountSender = delegateStakes.account.asMutable();
    accountSender.addPendingStakeDelegation(EMPTY, retractDelegatedStakes);
    accountSender.retractDelegatedStakes(secondAddress, 5);
    AccountState<AccountStateLocal, Balance> senderBeforeAbort =
        new AccountState<>(AccountStateLocal.initial(), accountSender.asImmutable());

    BalanceMutable accountRecipient = receiveDelegatedStakes.account.asMutable();
    accountRecipient.addPendingStakeDelegation(EMPTY, returnDelegatedStakes);
    accountRecipient.returnDelegatedStakes(firstAddress, 5);
    AccountState<AccountStateLocal, Balance> recipientBeforeAbort =
        new AccountState<>(AccountStateLocal.initial(), accountRecipient.asImmutable());

    assertThat(senderBeforeAbort.account.storedPendingStakeDelegations().size()).isEqualTo(1);
    assertThat(recipientBeforeAbort.account.storedPendingStakeDelegations().size()).isEqualTo(1);

    byte[] abortRpcSender = AccountInvocationRpc.abortDelegateStakes(EMPTY);
    byte[] abortRpcRecipient = AccountInvocationRpc.abortDelegateStakes(EMPTY);
    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAndCheckState(() -> 0, global, senderBeforeAbort, abortRpcSender).updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterAbort =
        invokeAndCheckState(() -> 0, global, recipientBeforeAbort, abortRpcRecipient)
            .updatedState();

    assertThat(senderAfterAbort.account.storedPendingStakeDelegations().size()).isEqualTo(0);
    assertThat(senderAfterAbort.account.delegatedStakesToOthers().getValue(secondAddress))
        .isEqualTo(25);
    assertThat(recipientAfterAbort.account.storedPendingStakeDelegations().size()).isEqualTo(0);
    assertThat(
            recipientAfterAbort
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes)
        .isEqualTo(25);
  }

  @Test
  public void invokeEntireReturnDelegatedStakesSuccess() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 15, secondAddress);
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 15, firstAddress);

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));

    byte[] twoPhaseRpcRetractDelegatedStakes =
        AccountInvocationRpc.initiateRetractDelegatedStakes(EMPTY, 10, secondAddress);
    byte[] initiateReturnDelegatedStakes =
        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, 10, firstAddress);
    byte[] twoPhaseRpcCommitSender = AccountInvocationRpc.commitDelegateStakes(EMPTY);
    byte[] twoPhaseRpcCommitRecipient = AccountInvocationRpc.commitDelegateStakes(EMPTY);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, delegateStakes, twoPhaseRpcRetractDelegatedStakes)
            .updatedState();

    AccountState<AccountStateLocal, Balance> receiveDelegatedStakes =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(15));
    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(() -> 0, global, receiveDelegatedStakes, initiateReturnDelegatedStakes)
            .updatedState();

    AccountState<AccountStateLocal, Balance> senderAfterCommit =
        invokeAndCheckState(() -> 0, global, updatedSender, twoPhaseRpcCommitSender).updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        invokeAndCheckState(() -> 0, global, updatedRecipient, twoPhaseRpcCommitRecipient)
            .updatedState();

    assertThat(senderAfterCommit.account.storedPendingStakeDelegations().size()).isEqualTo(0);
    assertThat(recipientAfterCommit.account.storedPendingStakeDelegations().size()).isEqualTo(0);

    assertThat(senderAfterCommit.account.mpcTokens()).isEqualTo(35);
    assertThat(recipientAfterCommit.account.mpcTokens()).isEqualTo(15);
    assertThat(
            recipientAfterCommit
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes)
        .isEqualTo(5);
  }

  @Test
  public void initiateRetractDelegatedStakesShouldRetractStakes() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 33, secondAddress);
    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(66));

    byte[] initiateRetractDelegatedStakes =
        AccountInvocationRpc.initiateRetractDelegatedStakes(EMPTY, 7, secondAddress);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, delegateStakes, initiateRetractDelegatedStakes)
            .updatedState();
    assertThat(updatedSender.account.delegatedStakesToOthers().containsKey(secondAddress)).isTrue();
    assertThat(updatedSender.account.delegatedStakesToOthers().getValue(secondAddress))
        .isEqualTo(26);
  }

  @Test
  public void invokeEntireReturnDelegatedStakesAbort() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 33, secondAddress);
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 22, firstAddress);

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(66));

    AccountState<AccountStateLocal, Balance> receiveDelegatedStakes =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(27));

    byte[] initiateRetractDelegatedStakes =
        AccountInvocationRpc.initiateRetractDelegatedStakes(EMPTY, 7, secondAddress);
    byte[] initiateReturnDelegatedStakes =
        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, 7, firstAddress);
    byte[] abortDelegateStakes = AccountInvocationRpc.abortDelegateStakes(EMPTY);

    AccountState<AccountStateLocal, Balance> updatedSender =
        invokeAndCheckState(() -> 0, global, delegateStakes, initiateRetractDelegatedStakes)
            .updatedState();

    AccountState<AccountStateLocal, Balance> updatedRecipient =
        invokeAndCheckState(() -> 0, global, receiveDelegatedStakes, initiateReturnDelegatedStakes)
            .updatedState();

    AccountState<AccountStateLocal, Balance> senderAfterAbort =
        invokeAndCheckState(() -> 0, global, updatedSender, abortDelegateStakes).updatedState();

    AccountState<AccountStateLocal, Balance> recipientAfterAbort =
        invokeAndCheckState(() -> 0, global, updatedRecipient, abortDelegateStakes).updatedState();

    assertThat(senderAfterAbort.account.storedPendingStakeDelegations().size()).isEqualTo(0);
    assertThat(recipientAfterAbort.account.storedPendingStakeDelegations().size()).isEqualTo(0);

    assertThat(senderAfterAbort.account.mpcTokens()).isEqualTo(33);
    assertThat(recipientAfterAbort.account.mpcTokens()).isEqualTo(27);
    assertThat(
            recipientAfterAbort
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .acceptedDelegatedStakes)
        .isEqualTo(0);
    assertThat(
            recipientAfterAbort
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes)
        .isEqualTo(22);
  }

  @Test
  public void returnDelegatedStakes_errorState() {
    assertThatThrownBy(() -> Balance.create().asMutable().retractDelegatedStakes(firstAddress, 10))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot retract more delegated stakes than has been delegated to the account");
  }

  @Test
  public void pendingRetractDelegatedStakes_correctAmount() {
    BalanceMutable delegateStakes = Balance.create().asMutable();
    delegateStakes.addMpcTokenBalance(100);
    delegateStakes.commitDelegateStakes(firstAddress, 43);
    assertThat(delegateStakes.delegatedStakesToOthers().getValue(firstAddress)).isEqualTo(43);
  }

  @Test
  public void delegateStakesTwice_correctUpdate() {
    BalanceMutable delegateStakes = Balance.create().asMutable();
    delegateStakes.addMpcTokenBalance(100);
    delegateStakes.commitDelegateStakes(secondAddress, 25);
    delegateStakes.commitDelegateStakes(secondAddress, 27);
    assertThat(delegateStakes.delegatedStakesToOthers().getValue(secondAddress)).isEqualTo(25 + 27);
  }

  @Test
  public void delegateStakes_tooFewTokens() {
    BalanceMutable balance = Balance.create().asMutable();
    balance.addMpcTokenBalance(100);
    assertThatThrownBy(() -> balance.delegateStakes(101))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void returnDelegatedStakes_retractTooMany() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 15, secondAddress);

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));

    assertThatThrownBy(
            () -> delegateStakes.account.asMutable().retractDelegatedStakes(secondAddress, 16))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot retract more delegated stakes than has been delegated to the account");
  }

  @Test
  public void testGetNewPendingAmount() {
    long newPendingAmount = Balance.create().asMutable().getNewPendingAmount(10, 10, 10);
    assertThat(newPendingAmount).isEqualTo(10);
    newPendingAmount = Balance.create().asMutable().getNewPendingAmount(10, 10, 11);
    assertThat(newPendingAmount).isEqualTo(1);
  }

  @Test
  public void checkBoundariesWhenReturningDelegatedStakes() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 20, secondAddress);

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));
    BalanceMutable acceptSomeStakes = delegateStakes.account.asMutable();
    acceptSomeStakes.commitReceiveDelegatedStakes(secondAddress, 20, null);
    acceptSomeStakes.acceptPendingDelegatedStakes(secondAddress, 5);
    acceptSomeStakes.returnDelegatedStakes(secondAddress, 15);
    assertThat(
            acceptSomeStakes
                .delegatedStakesFromOthers()
                .getValue(secondAddress)
                .pendingDelegatedStakes)
        .isEqualTo(0);
    assertThat(
            acceptSomeStakes
                .delegatedStakesFromOthers()
                .getValue(secondAddress)
                .acceptedDelegatedStakes)
        .isEqualTo(5);
  }

  @Test
  public void invokeReduceDelegatedStakes() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 20, secondAddress);

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));
    BalanceMutable acceptSomeStakes = delegateStakes.account.asMutable();
    acceptSomeStakes.commitReceiveDelegatedStakes(secondAddress, 20, null);
    acceptSomeStakes.acceptPendingDelegatedStakes(secondAddress, 5);

    AccountState<AccountStateLocal, Balance> accountStateBefore =
        new AccountState<>(tokenAccount(3).local, acceptSomeStakes.asImmutable());

    byte[] acceptDelegatedStakes = AccountInvocationRpc.reduceDelegatedStakes(secondAddress, 5);
    AccountState<AccountStateLocal, Balance> reduceAcceptedAmount =
        invokeAndCheckState(() -> 0, global, accountStateBefore, acceptDelegatedStakes)
            .updatedState();

    StakesFromOthers stakesFromOthers =
        reduceAcceptedAmount.account.delegatedStakesFromOthers().getValue(secondAddress);
    assertThat(stakesFromOthers.acceptedDelegatedStakes).isEqualTo(0);
    assertThat(stakesFromOthers.pendingDelegatedStakes).isEqualTo(20);
  }

  @Test
  public void invokeReduceDelegatedStakes_failure() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 20, secondAddress);
    AccountState<AccountStateLocal, Balance> noSenderState =
        new AccountState<>(tokenAccount(3).local, tokenAccount(3).account);

    byte[] acceptDelegatedStakes = AccountInvocationRpc.reduceDelegatedStakes(secondAddress, 4);
    assertThatThrownBy(
            () ->
                invokeAndCheckState(() -> 0, global, noSenderState, acceptDelegatedStakes)
                    .updatedState())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("You cannot reduce your accepted delegated stakes below 0");

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));
    BalanceMutable acceptSomeStakes = delegateStakes.account.asMutable();
    acceptSomeStakes.commitReceiveDelegatedStakes(secondAddress, 20, null);
    acceptSomeStakes.acceptPendingDelegatedStakes(secondAddress, 5);

    AccountState<AccountStateLocal, Balance> tooFewDelegatedStakes =
        new AccountState<>(tokenAccount(3).local, acceptSomeStakes.asImmutable());

    byte[] acceptTooMany = AccountInvocationRpc.reduceDelegatedStakes(secondAddress, 6);
    assertThatThrownBy(
            () ->
                invokeAndCheckState(() -> 0, global, tooFewDelegatedStakes, acceptTooMany)
                    .updatedState())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("You cannot reduce your accepted delegated stakes below 0");
  }

  @Test
  public void returnDelegatedStakes_removeIfZero() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 15, secondAddress);

    AccountState<AccountStateLocal, Balance> delegateStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(50));

    BalanceMutable retractAllDelegatedTokens = delegateStakes.account.asMutable();
    retractAllDelegatedTokens.retractDelegatedStakes(secondAddress, 15);
    assertThat(retractAllDelegatedTokens.delegatedStakesToOthers().size()).isEqualTo(0);
  }

  @Test
  public void returnDelegatedStakes() {
    AccountState<AccountStateLocal, Balance> stakeAccount = stakeAccount(30);
    BalanceMutable delegateStakes = stakeAccount.account.asMutable();
    delegateStakes.commitReceiveDelegatedStakes(firstAddress, 70, null);
    delegateStakes.acceptPendingDelegatedStakes(firstAddress, 70);

    assertThatThrownBy(() -> delegateStakes.returnDelegatedStakes(firstAddress, 71))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot return more delegated stakes than you have been delegated");

    BalanceMutable returnDelegatedStakes = stakeAccount.account.asMutable();

    returnDelegatedStakes.commitReceiveDelegatedStakes(firstAddress, 70, null);
    returnDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 70);
    returnDelegatedStakes.returnDelegatedStakes(firstAddress, 70);
    assertThat(returnDelegatedStakes.delegatedStakesFromOthers().containsKey(firstAddress))
        .isFalse();

    BalanceMutable stakedToContract = stakeAccount.account.asMutable();
    stakedToContract.commitReceiveDelegatedStakes(firstAddress, 70, null);
    stakedToContract.acceptPendingDelegatedStakes(firstAddress, 70);
    stakedToContract.associateWithExpiration(Contracts.ONE, 50, null);

    assertThatThrownBy(() -> stakedToContract.returnDelegatedStakes(firstAddress, 51))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Associations cannot be covered");

    stakedToContract.returnDelegatedStakes(firstAddress, 50);
    assertThat(
            stakedToContract
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .acceptedDelegatedStakes)
        .isEqualTo(70 - 50);
  }

  @Test
  public void retractDelegatedStakesExactAmount() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 15, secondAddress);
    BalanceMutable balance =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(15)).account.asMutable();
    balance.retractDelegatedStakes(secondAddress, 5);
    assertThat(balance.delegatedStakesToOthers().getValue(secondAddress)).isEqualTo(10);
    balance.retractDelegatedStakes(secondAddress, 5);
    assertThat(balance.delegatedStakesToOthers().getValue(secondAddress)).isEqualTo(5);
  }

  @Test
  public void returnDelegatedStakes_removeIfNoStakesLeft() {
    BalanceMutable balance = stakeAccount(100).account.asMutable();
    balance.commitReceiveDelegatedStakes(secondAddress, 30, null);
    balance.acceptPendingDelegatedStakes(secondAddress, 10);
    balance.returnDelegatedStakes(secondAddress, 10);
    assertThat(balance.delegatedStakesFromOthers().containsKey(secondAddress)).isTrue();
  }

  @Test
  public void acceptPendingDelegateStakes_exceptions() {
    byte[] initiateDelegateStakes =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, 15, secondAddress);
    BalanceMutable noPendingDelegatedStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(15)).account.asMutable();
    noPendingDelegatedStakes.commitReceiveDelegatedStakes(secondAddress, 0, null);
    assertThatThrownBy(
            () -> noPendingDelegatedStakes.acceptPendingDelegatedStakes(secondAddress, 2))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            2
                + " exceeds the amount of pending delegated stakes available from "
                + secondAddress
                + ", which is: "
                + 0);

    BalanceMutable tooFewPendingDelegatedStakes =
        successfullyDelegateStakes(initiateDelegateStakes, tokenAccount(15)).account.asMutable();
    tooFewPendingDelegatedStakes.commitReceiveDelegatedStakes(secondAddress, 15, null);

    long amount = 16;
    assertThatThrownBy(
            () -> tooFewPendingDelegatedStakes.acceptPendingDelegatedStakes(secondAddress, 16))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            amount
                + " exceeds the amount of pending delegated stakes available from "
                + secondAddress
                + ", which is: "
                + 15);
  }

  @Test
  public void acceptAndReduceDelegatedStakes_nonZero() {
    assertThatThrownBy(
            () -> Balance.create().asMutable().reducePendingDelegatedStakes(firstAddress, 0))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Amount must be positive");

    assertThatThrownBy(
            () -> Balance.create().asMutable().acceptPendingDelegatedStakes(firstAddress, 0))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Amount must be positive");
  }

  @Test
  public void invokeGlobal() {
    byte[] rpc = GlobalInteractionRpc.setGasPriceFor1000Network(777);
    AccountStateGlobal updated =
        plugin.invokeGlobal(pluginContextDefault, global, rpc).updatedState();
    assertThat(updated.getGasPriceFor1000Network()).isEqualTo(777);
  }

  @Test
  public void burnMpcTokens() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BalanceMutable balance = tokenAccount(3).account.asMutable();
    balance.stakeTokens(3);
    balance.associateWithExpiration(contractAddress, 2, null);
    AccountState<AccountStateLocal, Balance> accountStateBefore =
        new AccountState<>(tokenAccount(3).local, balance.asImmutable());
    long mpcTokensBefore = accountStateBefore.account.mpcTokens();
    long stakedTokensBefore = accountStateBefore.account.stakedTokens();

    long amount = 2L;
    byte[] rpc = AccountInvocationRpc.burnMpcTokens(amount, contractAddress);
    AccountState<AccountStateLocal, Balance> accountState =
        invokeAndCheckState(pluginContextDefault, global, accountStateBefore, rpc).updatedState();

    assertThat(accountState.account.mpcTokens()).isEqualTo(mpcTokensBefore);
    assertThat(accountState.account.stakedTokens()).isEqualTo(stakedTokensBefore - amount);
    assertThat(accountState.account.stakedToContract().containsKey(contractAddress)).isFalse();
  }

  @Test
  public void burnStakedTokensWithDelegatedStakes() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 10, firstAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(15));

    BalanceMutable acceptDelegatedStakes = recipientAfterCommit.account.asMutable();
    acceptDelegatedStakes.commitReceiveDelegatedStakes(firstAddress, 10, null);
    acceptDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 10);

    long burnAmount = 2L;
    AccountState<AccountStateLocal, Balance> accountStateAfter =
        successfullyBurnTokens(
            contractAddress, acceptDelegatedStakes.asImmutable(), 10, burnAmount);

    Balance balanceAfter = accountStateAfter.account;
    assertThat(balanceAfter.stakedTokens()).isEqualTo(9);
    assertThat(
            balanceAfter.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
        .isEqualTo(9);
    assertThat(balanceAfter.delegatedStakesFromOthers().size()).isEqualTo(1);
  }

  @Test
  public void burnStakedTokensWithDelegatedStakes_proportionateBurning() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 50, firstAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(50));

    BalanceMutable acceptDelegatedStakes = recipientAfterCommit.account.asMutable();
    acceptDelegatedStakes.commitReceiveDelegatedStakes(firstAddress, 50, null);
    acceptDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 50);

    long burnAmount = 10L;
    AccountState<AccountStateLocal, Balance> accountStateAfter =
        successfullyBurnTokens(
            contractAddress, acceptDelegatedStakes.asImmutable(), 10, burnAmount);

    Balance balanceAfter = accountStateAfter.account;
    assertThat(balanceAfter.stakedTokens()).isEqualTo(8);
    assertThat(
            balanceAfter.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
        .isEqualTo(42);
    assertThat(balanceAfter.delegatedStakesFromOthers().size()).isEqualTo(1);
  }

  @Test
  public void burnStakedTokensWithDelegatedStakes_unevenBurning() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 100, firstAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(100));

    BalanceMutable acceptDelegatedStakes = recipientAfterCommit.account.asMutable();
    acceptDelegatedStakes.commitReceiveDelegatedStakes(firstAddress, 100, null);
    acceptDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 100);

    long burnAmount = 3L;
    AccountState<AccountStateLocal, Balance> accountStateAfter =
        successfullyBurnTokens(
            contractAddress, acceptDelegatedStakes.asImmutable(), 100, burnAmount);

    Balance balanceAfter = accountStateAfter.account;
    assertThat(balanceAfter.stakedTokens()).isEqualTo(98);
    assertThat(
            balanceAfter.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
        .isEqualTo(99);
    assertThat(balanceAfter.delegatedStakesFromOthers().size()).isEqualTo(1);
  }

  @Test
  public void dontBurnForAccountIfNothingStaked() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 100, firstAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(100));

    BalanceMutable acceptDelegatedStakes = recipientAfterCommit.account.asMutable();
    acceptDelegatedStakes.commitReceiveDelegatedStakes(firstAddress, 100, null);
    acceptDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 100);

    long burnAmount = 3L;
    AccountState<AccountStateLocal, Balance> accountStateAfter =
        successfullyBurnTokens(contractAddress, acceptDelegatedStakes.asImmutable(), 0, burnAmount);

    Balance balanceAfter = accountStateAfter.account;
    assertThat(balanceAfter.stakedTokens()).isEqualTo(0);
    assertThat(
            balanceAfter.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
        .isEqualTo(97);
    assertThat(balanceAfter.delegatedStakesFromOthers().size()).isEqualTo(1);
  }

  @Test
  public void burnProportionateAmountOfTokens() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 1, firstAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(10000));

    BalanceMutable acceptDelegatedStakes = recipientAfterCommit.account.asMutable();
    acceptDelegatedStakes.commitReceiveDelegatedStakes(firstAddress, 1, null);
    acceptDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 1);

    long burnAmount = 2L;
    AccountState<AccountStateLocal, Balance> accountStateAfter =
        successfullyBurnTokens(
            contractAddress, acceptDelegatedStakes.asImmutable(), 10000, burnAmount);

    Balance balanceAfter = accountStateAfter.account;
    assertThat(balanceAfter.stakedTokens()).isEqualTo(9998);
    assertThat(
            balanceAfter.delegatedStakesFromOthers().getValue(firstAddress).acceptedDelegatedStakes)
        .isEqualTo(1);
    assertThat(balanceAfter.delegatedStakesFromOthers().size()).isEqualTo(1);
  }

  @Test
  public void testMoreStakersThanBurnedTokens() {
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 1, firstAddress);
    byte[] initiateReceiveDelegatedStakes2 =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 1, secondAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(1));

    AccountState<AccountStateLocal, Balance> initiateDelegateStaking =
        invokeAndCheckState(() -> 0, global, recipientAfterCommit, initiateReceiveDelegatedStakes2)
            .updatedState();

    byte[] commitDelegateStakes = AccountInvocationRpc.commitDelegateStakes(EMPTY);

    AccountState<AccountStateLocal, Balance> delegatedStakesFromTwoOtherAccounts =
        invokeAndCheckState(() -> 0, global, initiateDelegateStaking, commitDelegateStakes)
            .updatedState();

    assertThat(delegatedStakesFromTwoOtherAccounts.account.delegatedStakesFromOthers().size())
        .isEqualTo(2);

    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    long burnAmount = 2L;
    BalanceMutable acceptDelegatedStakes = delegatedStakesFromTwoOtherAccounts.account.asMutable();
    acceptDelegatedStakes.commitReceiveDelegatedStakes(firstAddress, 1, null);
    acceptDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 1);
    acceptDelegatedStakes.commitReceiveDelegatedStakes(secondAddress, 1, null);
    acceptDelegatedStakes.acceptPendingDelegatedStakes(secondAddress, 1);

    AccountState<AccountStateLocal, Balance> accountStateAfterBurning =
        successfullyBurnTokens(contractAddress, acceptDelegatedStakes.asImmutable(), 1, burnAmount);

    Balance balanceAfter = accountStateAfterBurning.account;
    long sumOfStakedTokens = balanceAfter.stakedTokens();
    for (StakesFromOthers stakesFromOther : balanceAfter.delegatedStakesFromOthers().values()) {
      sumOfStakedTokens += stakesFromOther.acceptedDelegatedStakes;
    }
    assertThat(sumOfStakedTokens).isEqualTo(3 - burnAmount);
    assertThat(balanceAfter.stakedTokens()).isEqualTo(0);
    assertThat(balanceAfter.delegatedStakesFromOthers().size()).isEqualTo(2);
  }

  @Test
  public void testRemovalIfNoDelegatedStakesAfterBurning() {
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 1, firstAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(1));

    assertThat(recipientAfterCommit.account.delegatedStakesFromOthers().size()).isEqualTo(1);

    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    long burnAmount = 2L;
    BalanceMutable acceptDelegatedStakes = recipientAfterCommit.account.asMutable();
    acceptDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 1);

    AccountState<AccountStateLocal, Balance> accountStateAfterBurning =
        successfullyBurnTokens(contractAddress, acceptDelegatedStakes.asImmutable(), 1, burnAmount);
    assertThat(accountStateAfterBurning.account.delegatedStakesFromOthers().size()).isEqualTo(0);
  }

  @Test
  public void invokeAcceptDelegatedStakes() {
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 10, firstAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(50));

    assertThat(
            recipientAfterCommit
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes)
        .isEqualTo(10);
    assertThat(
            recipientAfterCommit
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .acceptedDelegatedStakes)
        .isEqualTo(0);

    byte[] acceptDelegatedStakes = AccountInvocationRpc.acceptDelegatedStakes(6, firstAddress);
    AccountState<AccountStateLocal, Balance> acceptedDelegatedStakes =
        invokeAndCheckState(() -> 0, global, recipientAfterCommit, acceptDelegatedStakes)
            .updatedState();

    assertThat(
            acceptedDelegatedStakes
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .pendingDelegatedStakes)
        .isEqualTo(4);
    assertThat(
            acceptedDelegatedStakes
                .account
                .delegatedStakesFromOthers()
                .getValue(firstAddress)
                .acceptedDelegatedStakes)
        .isEqualTo(6);
  }

  @Test
  public void takeFromPendingDelegatedStakesFirstWhenReturning() {
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 10, firstAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(50));

    BalanceMutable acceptSomeDelegatedStakes = recipientAfterCommit.account.asMutable();
    acceptSomeDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 7);

    StakesFromOthers stakesFromOther =
        acceptSomeDelegatedStakes.delegatedStakesFromOthers().getValue(firstAddress);
    assertThat(stakesFromOther.acceptedDelegatedStakes).isEqualTo(7);
    assertThat(stakesFromOther.pendingDelegatedStakes).isEqualTo(3);

    byte[] initiateReturnDelegatedStakes =
        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, 5, firstAddress);

    AccountState<AccountStateLocal, Balance> acceptSome =
        new AccountState<>(recipientAfterCommit.local, acceptSomeDelegatedStakes.asImmutable());
    AccountState<AccountStateLocal, Balance> retractFromRecipient =
        invokeAndCheckState(() -> 0, global, acceptSome, initiateReturnDelegatedStakes)
            .updatedState();

    stakesFromOther =
        retractFromRecipient.account.delegatedStakesFromOthers().getValue(firstAddress);
    assertThat(stakesFromOther.pendingDelegatedStakes).isEqualTo(0);
    assertThat(stakesFromOther.acceptedDelegatedStakes).isEqualTo(5);
  }

  @Test
  public void cannotReturnDelegatedStakesIfTooManyAssociatedToContract() {
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, 10, firstAddress);

    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(initiateReceiveDelegatedStakes, tokenAccount(50));

    BalanceMutable acceptSomeDelegatedStakes = recipientAfterCommit.account.asMutable();
    acceptSomeDelegatedStakes.acceptPendingDelegatedStakes(firstAddress, 7);
    StakesFromOthers stakesFromOther =
        acceptSomeDelegatedStakes.delegatedStakesFromOthers().getValue(firstAddress);
    assertThat(stakesFromOther.acceptedDelegatedStakes).isEqualTo(7);
    assertThat(stakesFromOther.pendingDelegatedStakes).isEqualTo(3);

    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    acceptSomeDelegatedStakes.associateWithExpiration(contractAddress, 5, null);
    AccountState<AccountStateLocal, Balance> updatedState =
        new AccountState<>(recipientAfterCommit.local, acceptSomeDelegatedStakes.asImmutable());

    long retractAmount = 6;
    byte[] initiateReturnDelegatedStakes =
        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, retractAmount, firstAddress);

    assertThatThrownBy(
            () ->
                invokeAndCheckState(() -> 0, global, updatedState, initiateReturnDelegatedStakes)
                    .updatedState())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Associations cannot be covered");
  }

  @Test
  public void returnDelegatedStakesWithOtherStakers() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("010000000000000000000000000000000000000001");
    BalanceMutable balance = Balance.create().asMutable();
    balance.addMpcTokenBalance(10);
    balance.stakeTokens(10);
    balance.commitReceiveDelegatedStakes(firstAddress, 10, null);
    balance.acceptPendingDelegatedStakes(firstAddress, 5);
    balance.commitReceiveDelegatedStakes(secondAddress, 10, null);
    balance.acceptPendingDelegatedStakes(secondAddress, 5);
    balance.associateWithExpiration(contractAddress, 17, null);

    AccountState<AccountStateLocal, Balance> updatedState =
        new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());

    AccountState<AccountStateLocal, Balance> updated =
        invokeAndCheckState(
                () -> 0,
                global,
                updatedState,
                AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, 8, firstAddress))
            .updatedState();
    assertThat(unusedStakes(updated.account.asMutable())).isEqualTo(0);

    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                        () -> 0,
                        global,
                        updatedState,
                        AccountInvocationRpc.initiateReturnDelegatedStakes(EMPTY, 9, firstAddress))
                    .updatedState())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Associations cannot be covered");
  }

  @Test
  public void unstakeWithDelegatedTokens() {
    BalanceMutable balance = Balance.create().asMutable();
    balance.addMpcTokenBalance(10);
    balance.stakeTokens(10);
    balance.commitReceiveDelegatedStakes(firstAddress, 10, null);
    balance.acceptPendingDelegatedStakes(firstAddress, 5);
    balance.associateWithExpiration(
        BlockchainAddress.fromString("010000000000000000000000000000000000000001"), 6, null);

    AccountState<AccountStateLocal, Balance> updatedState =
        new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());

    AccountState<AccountStateLocal, Balance> updated =
        invokeAndCheckState(() -> 0, global, updatedState, AccountInvocationRpc.unstakeTokens(9))
            .updatedState();
    assertThat(unusedStakes(updated.account.asMutable())).isEqualTo(0);

    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                        () -> 0, global, updatedState, AccountInvocationRpc.unstakeTokens(10))
                    .updatedState())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Associations cannot be covered");
  }

  /**
   * Changing both the amount of expiration of an association with stakes that can cover both,
   * succeeds.
   */
  @Test
  void canMoveAssociationExpirationAndAmountAtTheSameTime() {
    BalanceMutable balance = Balance.create().asMutable();
    int selfStaked = 100;
    balance.addMpcTokenBalance(selfStaked);
    balance.stakeTokens(selfStaked);

    long expirationTimestamp = 100L;
    balance = delegateAndAccept(balance, 1, 1, firstAddress, expirationTimestamp);
    balance = associateWithTimestamp(balance, selfStaked, expirationTimestamp + 1, Contracts.ONE);
    balance = associateWithTimestamp(balance, 1, expirationTimestamp, Contracts.ONE);

    assertThat(balance.stakedToContract().getValue(Contracts.ONE).amount).isEqualTo(selfStaked + 1);
    assertThat(balance.stakedToContract().getValue(Contracts.ONE).expirationTimestamp)
        .isEqualTo(expirationTimestamp);
  }

  @Test
  public void reduceAcceptedStakesWithOtherStakers() {
    BalanceMutable balance = Balance.create().asMutable();
    balance.addMpcTokenBalance(10);
    balance.stakeTokens(10);
    balance.commitReceiveDelegatedStakes(firstAddress, 10, null);
    balance.acceptPendingDelegatedStakes(firstAddress, 5);
    balance.commitReceiveDelegatedStakes(secondAddress, 10, null);
    balance.acceptPendingDelegatedStakes(secondAddress, 5);
    balance.associateWithExpiration(
        BlockchainAddress.fromString("010000000000000000000000000000000000000001"), 17, null);

    AccountState<AccountStateLocal, Balance> updatedState =
        new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());

    AccountState<AccountStateLocal, Balance> updated =
        invokeAndCheckState(
                () -> 0,
                global,
                updatedState,
                AccountInvocationRpc.reduceDelegatedStakes(firstAddress, 3))
            .updatedState();
    assertThat(unusedStakes(updated.account.asMutable())).isEqualTo(0);

    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                        () -> 0,
                        global,
                        updatedState,
                        AccountInvocationRpc.reduceDelegatedStakes(firstAddress, 4))
                    .updatedState())
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Associations cannot be covered");
  }

  @Test
  public void invokeMakeAccountNonStakeable() {
    AccountState<AccountStateLocal, Balance> account = tokenAccount(25);
    AccountState<AccountStateLocal, Balance> account2 = tokenAccount(25);
    byte[] makeAccountNonStakeableRpc = AccountInvocationRpc.makeAccountNonStakeable();
    AccountState<AccountStateLocal, Balance> nonStakeable =
        invokeAndCheckState(pluginContextDefault, global, account, makeAccountNonStakeableRpc)
            .updatedState();

    byte[] stakeRpc = AccountInvocationRpc.stakeTokens(10);
    assertThatThrownBy(
            () -> invokeAndCheckState(pluginContextDefault, global, nonStakeable, stakeRpc))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("This account cannot stake");

    AccountState<AccountStateLocal, Balance> stakedTokensState =
        invokeAndCheckState(pluginContextDefault, global, account2, stakeRpc).updatedState();

    assertThat(stakedTokensState.account.stakedTokens()).isEqualTo(10);
    assertThat(stakedTokensState.account.mpcTokens()).isEqualTo(15);
    assertThat(nonStakeable.account.mpcTokens()).isEqualTo(25);

    // Cannot make account non-stakeable if they have staked tokens
    assertThatThrownBy(
            () ->
                invokeAndCheckState(
                    pluginContextDefault, global, stakedTokensState, makeAccountNonStakeableRpc))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Cannot make account non-stakeable if it has staked tokens");
  }

  @Test
  public void iceStakeGood() {
    AccountState<AccountStateLocal, Balance> accountState = initiateIceStakeAccount(10);
    assertThat(accountState.account.pendingIceStake(EMPTY)).isNotNull();
    assertThat(accountState.account.pendingIceStake(EMPTY).type())
        .isEqualTo(IceStake.IceStakeType.ASSOCIATE);
    assertThat(accountState.account.pendingIceStake(EMPTY).amount()).isEqualTo(10);
    assertThat(accountState.account.pendingIceStake(EMPTY).address()).isEqualTo(contractAddress);

    ContractState<AccountStateLocal, ContractStorage> contractState = initiateIceStakeContract(10);
    assertThat(contractState.contract.getPendingIceStake(EMPTY)).isNotNull();
    assertThat(contractState.contract.getPendingIceStake(EMPTY).type())
        .isEqualTo(IceStake.IceStakeType.ASSOCIATE);
    assertThat(contractState.contract.getPendingIceStake(EMPTY).amount()).isEqualTo(10);
    assertThat(contractState.contract.getPendingIceStake(EMPTY).address()).isEqualTo(firstAddress);

    byte[] commitRpc = AccountInvocationRpc.commitIce(EMPTY);
    AccountState<AccountStateLocal, Balance> commitState =
        invokeAndCheckState(pluginContextDefault, global, accountState, commitRpc).updatedState();
    assertThat(commitState.account.pendingIceStake(EMPTY)).isNull();
    assertThat(commitState.account.stakedToContract().getValue(contractAddress).amount)
        .isEqualTo(10);

    ContractState<AccountStateLocal, ContractStorage> commitContractState =
        plugin
            .invokeContract(pluginContextDefault, global, contractState, contractAddress, commitRpc)
            .updatedState();
    assertThat(commitContractState.contract.getPendingIceStake(EMPTY)).isNull();
    assertThat(commitContractState.contract.getIceStakeBy(firstAddress)).isEqualTo(10);
  }

  @Test
  public void iceStakeRetractGood() {
    AccountState<AccountStateLocal, Balance> account = initiateIceStakeAccount(20);
    ContractState<AccountStateLocal, ContractStorage> contract = initiateIceStakeContract(20);

    byte[] rpc = AccountInvocationRpc.commitIce(EMPTY);
    account = invokeAndCheckState(pluginContextDefault, global, account, rpc).updatedState();
    assertThat(account.account.stakedToContract().getValue(contractAddress).amount).isEqualTo(20);
    byte[] rpc1 = AccountInvocationRpc.commitIce(EMPTY);
    contract =
        plugin
            .invokeContract(pluginContextDefault, global, contract, firstAddress, rpc1)
            .updatedState();
    assertThat(contract.contract.getIceStakeBy(firstAddress)).isEqualTo(20);

    byte[] retractAccount =
        AccountInvocationRpc.initiateIceDisassociate(10, contractAddress, EMPTY);
    account =
        invokeAndCheckState(pluginContextDefault, global, account, retractAccount).updatedState();
    assertThat(account.account.pendingIceStake(EMPTY)).isNotNull();
    assertThat(account.account.pendingIceStake(EMPTY).type())
        .isEqualTo(IceStake.IceStakeType.DISASSOCIATE);

    byte[] retractContract = ContractInvocationRpc.initiateIceDisassociate(EMPTY, 10, firstAddress);
    contract =
        plugin
            .invokeContract(
                pluginContextDefault, global, contract, contractAddress, retractContract)
            .updatedState();
    assertThat(contract.contract.getPendingIceStake(EMPTY)).isNotNull();
    assertThat(contract.contract.getPendingIceStake(EMPTY).type())
        .isEqualTo(IceStake.IceStakeType.DISASSOCIATE);

    byte[] commitRpc = AccountInvocationRpc.commitIce(EMPTY);
    account = invokeAndCheckState(pluginContextDefault, global, account, commitRpc).updatedState();
    assertThat(account.account.pendingIceStake(EMPTY)).isNull();
    assertThat(account.account.stakedToContract().getValue(contractAddress).amount).isEqualTo(10);

    contract =
        plugin
            .invokeContract(pluginContextDefault, global, contract, contractAddress, commitRpc)
            .updatedState();
    assertThat(contract.contract.getPendingIceStake(EMPTY)).isNull();
    assertThat(contract.contract.getIceStakeBy(firstAddress)).isEqualTo(10);
  }

  @Test
  public void iceStakeBad() {
    AccountState<AccountStateLocal, Balance> account = initiateIceStakeAccount(10);
    ContractState<AccountStateLocal, ContractStorage> contract = initiateIceStakeContract(10);

    assertThat(account.account.stakedToContract().getValue(contractAddress).amount).isEqualTo(10);

    byte[] abortRpc = AccountInvocationRpc.abortIce(EMPTY);
    AccountState<AccountStateLocal, Balance> accountAbort =
        invokeAndCheckState(pluginContextDefault, global, account, abortRpc).updatedState();
    assertThat(accountAbort.account.pendingIceStake(EMPTY)).isNull();

    assertThat(accountAbort.account.stakedToContract().getValue(contractAddress)).isNull();

    ContractState<AccountStateLocal, ContractStorage> contractAbort =
        plugin
            .invokeContract(pluginContextDefault, global, contract, contractAddress, abortRpc)
            .updatedState();
    assertThat(contractAbort.contract.getPendingIceStake(EMPTY)).isNull();
  }

  @Test
  public void iceStakeRetractBad() {
    AccountState<AccountStateLocal, Balance> account = initiateIceStakeAccount(20);
    ContractState<AccountStateLocal, ContractStorage> contract = initiateIceStakeContract(20);

    byte[] rpc1 = AccountInvocationRpc.commitIce(EMPTY);
    account = invokeAndCheckState(pluginContextDefault, global, account, rpc1).updatedState();
    byte[] rpc2 = AccountInvocationRpc.commitIce(EMPTY);
    contract =
        plugin
            .invokeContract(pluginContextDefault, global, contract, firstAddress, rpc2)
            .updatedState();

    byte[] accountRetract =
        AccountInvocationRpc.initiateIceDisassociate(10, contractAddress, EMPTY);
    byte[] contractRetract = ContractInvocationRpc.initiateIceDisassociate(EMPTY, 10, firstAddress);

    account =
        invokeAndCheckState(pluginContextDefault, global, account, accountRetract).updatedState();
    assertThat(account.account.pendingIceStake(EMPTY)).isNotNull();
    contract =
        plugin
            .invokeContract(
                pluginContextDefault, global, contract, contractAddress, contractRetract)
            .updatedState();
    assertThat(contract.contract.getPendingIceStake(EMPTY)).isNotNull();
    assertThat(contract.contract.getIceStakeBy(firstAddress)).isEqualTo(10);

    byte[] rpc = AccountInvocationRpc.abortIce(EMPTY);
    account = invokeAndCheckState(pluginContextDefault, global, account, rpc).updatedState();
    assertThat(account.account.pendingIceStake(EMPTY)).isNull();
    assertThat(account.account.stakedToContract().getValue(contractAddress).amount).isEqualTo(20);

    contract =
        plugin
            .invokeContract(pluginContextDefault, global, contract, contractAddress, rpc)
            .updatedState();
    assertThat(contract.contract.getIceStakeBy(firstAddress)).isEqualTo(20);
  }

  @Test
  public void cannotInitiateIceStakeWithNonPositive() {
    byte[] accountRpc = AccountInvocationRpc.initiateIceAssociate(0, contractAddress, EMPTY);
    AccountState<AccountStateLocal, Balance> account = tokenAccount(25);
    assertThatThrownBy(() -> invokeAndCheckState(pluginContextDefault, global, account, accountRpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot ice stake non-positive amount");

    byte[] contractRpc = ContractInvocationRpc.initiateIceAssociate(EMPTY, 0, contractAddress);

    ContractState<AccountStateLocal, ContractStorage> contract = createContract();
    assertThatThrownBy(
            () ->
                plugin.invokeContract(
                    pluginContextDefault, global, contract, contractAddress, contractRpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot stake non-positive amount of ice stakes");

    byte[] retractRpc = AccountInvocationRpc.initiateIceDisassociate(0, contractAddress, EMPTY);
    assertThatThrownBy(() -> invokeAndCheckState(pluginContextDefault, global, account, retractRpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot retract a non-positive amount of ice stakes");

    byte[] contractRetractRpc =
        ContractInvocationRpc.initiateIceDisassociate(EMPTY, 0, contractAddress);

    assertThatThrownBy(
            () ->
                plugin.invokeContract(
                    pluginContextDefault, global, contract, contractAddress, contractRetractRpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot retract non-positive amount of ice stakes");
  }

  @Test
  public void cannotIceStakeAgainstNonContract() {
    byte[] rpc = AccountInvocationRpc.initiateIceAssociate(10, firstAddress, EMPTY);
    AccountState<AccountStateLocal, Balance> account = tokenAccount(25);

    assertThatThrownBy(() -> invokeAndCheckState(pluginContextDefault, global, account, rpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Can only ice stake against contracts");
  }

  @Test
  public void initiateIceStakeSameTxId() {
    // account
    AccountState<AccountStateLocal, Balance> account = initiateIceStakeAccount(10);
    byte[] accountRpc = AccountInvocationRpc.initiateIceAssociate(20, contractAddress, EMPTY);
    assertThat(account.account.pendingIceStake(EMPTY).amount()).isEqualTo(10);

    account = invokeAndCheckState(pluginContextDefault, global, account, accountRpc).updatedState();
    assertThat(account.account.pendingIceStake(EMPTY).amount()).isEqualTo(10);

    byte[] rpcRetract = AccountInvocationRpc.initiateIceDisassociate(20, contractAddress, EMPTY);
    account = invokeAndCheckState(pluginContextDefault, global, account, rpcRetract).updatedState();
    assertThat(account.account.pendingIceStake(EMPTY).amount()).isEqualTo(10);

    // contract
    ContractState<AccountStateLocal, ContractStorage> contract = initiateIceStakeContract(10);
    assertThat(contract.contract.getPendingIceStake(EMPTY).amount()).isEqualTo(10);

    byte[] contractRpc = ContractInvocationRpc.initiateIceAssociate(EMPTY, 20, contractAddress);

    contract =
        plugin
            .invokeContract(pluginContextDefault, global, contract, contractAddress, contractRpc)
            .updatedState();
    assertThat(contract.contract.getPendingIceStake(EMPTY).amount()).isEqualTo(10);

    byte[] contractRetractRpc =
        ContractInvocationRpc.initiateIceDisassociate(EMPTY, 20, contractAddress);

    contract =
        plugin
            .invokeContract(
                pluginContextDefault, global, contract, contractAddress, contractRetractRpc)
            .updatedState();
    assertThat(contract.contract.getPendingIceStake(EMPTY).amount()).isEqualTo(10);
  }

  @Test
  public void commitIceStakeWithDifferenTxHash() {
    // account
    AccountState<AccountStateLocal, Balance> state = initiateIceStakeAccount(10);
    Hash anotherTx = Hash.create(s -> s.writeString("tx"));
    byte[] commitRpc = AccountInvocationRpc.commitIce(anotherTx);
    assertThat(state.account.pendingIceStake(EMPTY)).isNotNull();
    state = invokeAndCheckState(pluginContextDefault, global, state, commitRpc).updatedState();
    assertThat(state.account.pendingIceStake(EMPTY)).isNotNull();

    // contract
    ContractState<AccountStateLocal, ContractStorage> contract = initiateIceStakeContract(10);
    assertThat(contract.contract.getPendingIceStake(EMPTY)).isNotNull();
    contract =
        plugin
            .invokeContract(pluginContextDefault, global, contract, firstAddress, commitRpc)
            .updatedState();
    assertThat(contract.contract.getPendingIceStake(EMPTY)).isNotNull();
  }

  @Test
  public void abortIceStakeWithDifferentTxHash() {
    // account
    AccountState<AccountStateLocal, Balance> state = initiateIceStakeAccount(10);
    Hash anotherTx = Hash.create(s -> s.writeString("tx"));
    byte[] abortRpc = AccountInvocationRpc.abortIce(anotherTx);
    assertThat(state.account.pendingIceStake(EMPTY)).isNotNull();
    state = invokeAndCheckState(pluginContextDefault, global, state, abortRpc).updatedState();
    assertThat(state.account.pendingIceStake(EMPTY)).isNotNull();

    // contract
    ContractState<AccountStateLocal, ContractStorage> contract = initiateIceStakeContract(10);
    assertThat(contract.contract.getPendingIceStake(EMPTY)).isNotNull();
    contract =
        plugin
            .invokeContract(pluginContextDefault, global, contract, firstAddress, abortRpc)
            .updatedState();
    assertThat(contract.contract.getPendingIceStake(EMPTY)).isNotNull();
  }

  @Test
  public void contractPaysStorageFees() {
    AccountStateLocal initial = AccountStateLocal.initial();
    BlockchainAccountPlugin.IcedContractState<AccountStateLocal, ContractStorage> state =
        plugin.updateActiveContractIced(
            () -> GasAndCoinAccountPlugin.MILLIS_PER_YEAR,
            global,
            new ContractState<>(
                initial, ContractStorage.create(0L).addBalance(Unsigned256.create(5000))),
            Contracts.ONE,
            100);
    assertThat(state.iced()).isFalse();
    ContractState<AccountStateLocal, ContractStorage> contract = state.contractState();
    assertThat(contract).isNotNull();
    assertThat(contract.contract.latestStorageFeeTime())
        .isEqualTo(GasAndCoinAccountPlugin.MILLIS_PER_YEAR);
    assertThat(contract.contract.getBalance().isPositive()).isTrue();
    assertThat(contract.contract.getBalance().getAbsoluteValue())
        .isEqualTo(Unsigned256.create(4900));
    long epoch =
        GasAndCoinAccountPlugin.convertBlockProductionTimeToEpoch(
            GasAndCoinAccountPlugin.MILLIS_PER_YEAR);
    assertThat(contract.local.getBlockchainUsageForEpoch(epoch)).isEqualTo(Unsigned256.create(100));
  }

  @Test
  public void contractPaysStorageFeesAndGoesIntoDebt() {
    AccountStateLocal initial = AccountStateLocal.initial();
    var state =
        plugin.updateActiveContractIced(
            () -> GasAndCoinAccountPlugin.MILLIS_PER_YEAR,
            global,
            new ContractState<>(
                initial, ContractStorage.create(0L).addBalance(Unsigned256.create(99))),
            Contracts.PUB_CONTRACT,
            100);
    assertThat(state.iced()).isTrue();
    ContractState<AccountStateLocal, ContractStorage> contract = state.contractState();
    assertThat(contract).isNotNull();
    assertThat(contract.contract.getBalance().isPositive()).isFalse();
    assertThat(contract.contract.getBalance().getAbsoluteValue()).isEqualTo(Unsigned256.ONE);
    long epoch =
        GasAndCoinAccountPlugin.convertBlockProductionTimeToEpoch(
            GasAndCoinAccountPlugin.MILLIS_PER_YEAR);
    assertThat(contract.local.getBlockchainUsageForEpoch(epoch)).isEqualTo(Unsigned256.create(99));
  }

  @Test
  public void zkContractsAreNotIced() {
    AccountStateLocal initial = AccountStateLocal.initial();

    var state =
        plugin.updateActiveContractIced(
            () -> GasAndCoinAccountPlugin.MILLIS_PER_WEEK + 1L,
            global,
            new ContractState<>(initial, ContractStorage.create(0L)),
            Contracts.ZK,
            100);

    assertThat(state.iced()).isFalse();
    assertThat(state.contractState()).isNull();
  }

  @Test
  public void icedContractLivesOneWeek() {
    AccountStateLocal initial = AccountStateLocal.initial();

    var state =
        plugin.updateActiveContractIced(
            () -> GasAndCoinAccountPlugin.MILLIS_PER_WEEK + 1L,
            global,
            new ContractState<>(
                initial, ContractStorage.create(0L).deductBalance(Unsigned256.ONE, 1L)),
            Contracts.PUB_CONTRACT,
            100);

    assertThat(state.iced()).isTrue();
    assertThat(state.contractState()).isNotNull();

    state =
        plugin.updateActiveContractIced(
            () -> GasAndCoinAccountPlugin.MILLIS_PER_WEEK + 2L,
            global,
            state.contractState(),
            Contracts.PUB_CONTRACT,
            100);

    assertThat(state.iced()).isFalse();
    assertThat(state.contractState()).isNull();
  }

  @Test
  public void icedContractLivesOneMonthOnAnyAmountOfStakes() {
    AccountStateLocal initial = AccountStateLocal.initial();

    var state =
        plugin.updateActiveContractIced(
            () -> MILLIS_PER_MONTH + 1L,
            global,
            new ContractState<>(
                initial,
                ContractStorage.create(0L)
                    .deductBalance(Unsigned256.ONE, 1L)
                    .addIceStakes(firstAddress, 1)),
            Contracts.PUB_CONTRACT,
            100);

    assertThat(state.contractState()).isNotNull();
    assertThat(state.iced()).isTrue();

    state =
        plugin.updateActiveContractIced(
            () -> MILLIS_PER_MONTH + 2L,
            global,
            new ContractState<>(initial, state.contractState().contract),
            Contracts.PUB_CONTRACT,
            100);

    assertThat(state.iced()).isFalse();
    assertThat(state.contractState()).isNull();
  }

  @Test
  public void icedContractLivesLongerThanOneMonthOnSufficientStakes() {
    AccountStateLocal initial = AccountStateLocal.initial();

    var state =
        plugin.updateActiveContractIced(
            () -> GasAndCoinAccountPlugin.MILLIS_PER_YEAR,
            global,
            new ContractState<>(
                initial,
                ContractStorage.create(GasAndCoinAccountPlugin.MILLIS_PER_YEAR)
                    .deductBalance(
                        Unsigned256.create(400),
                        GasAndCoinAccountPlugin.MILLIS_PER_YEAR - MILLIS_PER_MONTH - 1)
                    .addIceStakes(firstAddress, 50)
                    .addIceStakes(secondAddress, 50)),
            Contracts.PUB_CONTRACT,
            100);

    assertThat(state.iced()).isTrue();
    assertThat(state.contractState()).isNotNull();

    state =
        plugin.updateActiveContractIced(
            () -> GasAndCoinAccountPlugin.MILLIS_PER_YEAR + 1,
            global,
            state.contractState(),
            Contracts.PUB_CONTRACT,
            100);

    assertThat(state.iced()).isFalse();
    assertThat(state.contractState()).isNull();
  }

  @Test
  public void icedContractBecomesActiveAfterPayment() {
    AccountStateLocal initial = AccountStateLocal.initial();

    ContractStorage storage = ContractStorage.create(0L).deductBalance(Unsigned256.TEN, 1L);

    var state =
        plugin.updateContractGasBalance(
            () -> 2L, global, new ContractState<>(initial, storage), Contracts.ONE, 20);

    storage = state.contract;
    assertThat(storage.getIcedTime()).isNull();
    assertThat(storage.getBalance().isPositive()).isTrue();
    assertThat(storage.getBalance().getAbsoluteValue()).isEqualTo(Unsigned256.TEN);
    assertThat(state.local.getBlockchainUsageForEpoch(0L)).isEqualTo(Unsigned256.TEN);
  }

  @Test
  public void payByocFeesEmptyList() {
    AccountStateLocal local =
        plugin.payByocFees(
            pluginContextDefault,
            global,
            setCoinAccount(0).local,
            Unsigned256.create(5),
            SYMBOL_0,
            FixedList.create());
    long epoch =
        GasAndCoinAccountPlugin.convertBlockProductionTimeToEpoch(
            pluginContextDefault.blockProductionTime());
    assertThat(local.getBlockchainUsageForEpoch(epoch)).isEqualTo(Unsigned256.create(10));
  }

  @Test
  public void payByocFees() {
    FixedList<BlockchainAddress> nodes =
        FixedList.create(List.of(Accounts.ONE, Accounts.TWO, Accounts.THREE));
    AccountStateLocal local =
        plugin.payByocFees(
            pluginContextDefault,
            global,
            setCoinAccount(0).local,
            Unsigned256.create(5),
            SYMBOL_0,
            nodes);
    assertThat(local.getServiceFeesForAddress(Accounts.ONE)).isEqualTo(Unsigned256.create(3));
    assertThat(local.getServiceFeesForAddress(Accounts.TWO)).isEqualTo(Unsigned256.create(3));
    assertThat(local.getServiceFeesForAddress(Accounts.THREE)).isEqualTo(Unsigned256.create(3));
    long epoch =
        GasAndCoinAccountPlugin.convertBlockProductionTimeToEpoch(
            pluginContextDefault.blockProductionTime());
    assertThat(local.getBlockchainUsageForEpoch(epoch)).isEqualTo(Unsigned256.create(1));
  }

  @Test
  public void appointCustodian_byOwnerAndWithNoAppointedCustodian_custodianIsAppointed() {
    Balance noAppointedCustodian = Balance.create();
    Balance appointedCustodian =
        invokeAccount(noAppointedCustodian, AccountInvocationRpc.appointCustodian(firstAddress));
    assertThat(appointedCustodian.hasAppointedCustodian()).isTrue();
    assertThat(appointedCustodian.isAppointedCustodian(firstAddress)).isTrue();
    assertThat(appointedCustodian.hasCustodian()).isFalse();
  }

  @Test
  public void appointCustodian_byOwnerAndWithAppointedCustodian_appointedCustodianIsOverwritten() {
    Balance appointedCustodian = Balance.create().mutate(m -> m.appointCustodian(firstAddress));
    Balance afterAppointCustodian =
        invokeAccount(appointedCustodian, AccountInvocationRpc.appointCustodian(secondAddress));
    assertThat(afterAppointCustodian.hasAppointedCustodian()).isTrue();
    assertThat(afterAppointCustodian.isAppointedCustodian(secondAddress)).isTrue();
    assertThat(afterAppointCustodian.hasCustodian()).isFalse();
  }

  @Test
  public void appointCustodian_byOwnerAndWithAcceptedCustodian_fail() {
    Balance acceptedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                });
    assertThatThrownBy(
            () ->
                invokeAccount(
                    acceptedCustodian, AccountInvocationRpc.appointCustodian(secondAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void appointCustodian_byOtherAndWithAcceptedCustodian_fail() {
    Balance acceptedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                });
    assertThatThrownBy(
            () ->
                invokeAccount(
                    acceptedCustodian,
                    AccountInvocationRpc.appointCustodian(secondAddress, secondAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void appointCustodian_byOtherAndWithNoAcceptedCustodian_fail() {
    Balance noAcceptedCustodian = Balance.create();
    assertThatThrownBy(
            () ->
                invokeAccount(
                    noAcceptedCustodian,
                    AccountInvocationRpc.appointCustodian(firstAddress, secondAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the owner of the account is allowed to send this invocation");
  }

  @Test
  public void appointCustodian_byCustodianAndWithAcceptedCustodian_newCustodianIsAppointed() {
    Balance acceptedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                });
    Balance newCustodianAppointed =
        invokeAccount(
            acceptedCustodian, AccountInvocationRpc.appointCustodian(firstAddress, secondAddress));
    assertThat(newCustodianAppointed.isAppointedCustodian(secondAddress)).isTrue();
    assertThat(newCustodianAppointed.isCustodian(firstAddress)).isTrue();
  }

  @Test
  public void cancelAppointedCustodian_withNoAppointedCustodian_fail() {
    Balance noAppointedCustodian = Balance.create();
    assertThatThrownBy(
            () ->
                invokeAccount(
                    noAppointedCustodian, AccountInvocationRpc.cancelAppointedCustodian(null)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No custodian is currently appointed");
  }

  @Test
  public void cancelAppointedCustodian_byOwnerAndWithAppointedCustodian_cancelAppointedCustodian() {
    Balance withAppointedCustodian = Balance.create().mutate(m -> m.appointCustodian(firstAddress));
    Balance cancelledAppointedCustodian =
        invokeAccount(withAppointedCustodian, AccountInvocationRpc.cancelAppointedCustodian(null));
    assertThat(cancelledAppointedCustodian.hasAppointedCustodian()).isFalse();
  }

  @Test
  public void cancelAppointedCustodian_byOwnerAndWithAcceptedCustodian_fail() {
    Balance acceptedAndAppointedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                  m.appointCustodian(secondAddress);
                });
    assertThatThrownBy(
            () ->
                invokeAccount(
                    acceptedAndAppointedCustodian,
                    AccountInvocationRpc.cancelAppointedCustodian(null)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void cancelAppointedCustodian_byOtherThanAcceptedCustodian_fail() {
    BlockchainAddress thirdAddress =
        BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeInt(3)));
    Balance acceptedAndAppointedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(secondAddress);
                  m.acceptCustodian();
                  m.appointCustodian(thirdAddress);
                });
    assertThatThrownBy(
            () ->
                invokeAccount(
                    acceptedAndAppointedCustodian,
                    AccountInvocationRpc.cancelAppointedCustodian(firstAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void
      cancelAppointedCustodian_byCustodianAndWithAppointedCustodian_cancelAppointedCustodian() {
    Balance acceptedAndAppointedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                  m.appointCustodian(secondAddress);
                });
    Balance cancelledAppointedCustodian =
        invokeAccount(
            acceptedAndAppointedCustodian,
            AccountInvocationRpc.cancelAppointedCustodian(firstAddress));
    assertThat(cancelledAppointedCustodian.hasCustodian()).isTrue();
    assertThat(cancelledAppointedCustodian.isCustodian(firstAddress)).isTrue();
    assertThat(cancelledAppointedCustodian.hasAppointedCustodian()).isFalse();
  }

  @Test
  public void resetCustodian_withNoCustodian_fail() {
    Balance noCustodian = Balance.create();
    assertThatThrownBy(
            () -> invokeAccount(noCustodian, AccountInvocationRpc.resetCustodian(firstAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No custodian has been accepted for the account");
  }

  @Test
  public void resetCustodian_byOtherThanCustodian_fail() {
    Balance acceptedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                });
    assertThatThrownBy(
            () ->
                invokeAccount(
                    acceptedCustodian, AccountInvocationRpc.resetCustodian(secondAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the custodian can reset the custodian");
  }

  @Test
  public void
      resetCustodian_byCustodianAndWithAppointedCustodian_resetBothAcceptedAndAppointedCustodian() {
    Balance acceptedAndAppointedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                  m.appointCustodian(secondAddress);
                });
    Balance afterResetCustodian =
        invokeAccount(
            acceptedAndAppointedCustodian, AccountInvocationRpc.resetCustodian(firstAddress));
    assertThat(afterResetCustodian.hasCustodian()).isFalse();
    assertThat(afterResetCustodian.hasAppointedCustodian()).isFalse();
  }

  @Test
  public void resetCustodian_byAppointedCustodian_fail() {
    Balance acceptedAndAppointedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                  m.appointCustodian(secondAddress);
                });
    assertThatThrownBy(
            () ->
                invokeAccount(
                    acceptedAndAppointedCustodian,
                    AccountInvocationRpc.resetCustodian(secondAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the custodian can reset the custodian");
  }

  @Test
  public void acceptCustodianship_byAppointedCustodian_acceptCustodian() {
    Balance appointedCustodian = Balance.create().mutate(m -> m.appointCustodian(firstAddress));
    Balance afterAcceptCustodianship =
        invokeAccount(appointedCustodian, AccountInvocationRpc.acceptCustodianship(firstAddress));
    assertThat(afterAcceptCustodianship.hasAppointedCustodian()).isFalse();
    assertThat(afterAcceptCustodianship.hasCustodian()).isTrue();
    assertThat(afterAcceptCustodianship.isCustodian(firstAddress)).isTrue();
  }

  @Test
  public void acceptCustodianship_byOtherThanAppointedCustodian_fail() {
    Balance appointedCustodian = Balance.create().mutate(m -> m.appointCustodian(firstAddress));
    assertThatThrownBy(
            () ->
                invokeAccount(
                    appointedCustodian, AccountInvocationRpc.acceptCustodianship(secondAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the appointed custodian can accept custodianship");
  }

  @Test
  public void acceptCustodianship_byAcceptedCustodianAndWithAppointedCustodian_fail() {
    Balance acceptedAndAppointedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                  m.appointCustodian(secondAddress);
                });
    assertThatThrownBy(
            () ->
                invokeAccount(
                    acceptedAndAppointedCustodian,
                    AccountInvocationRpc.acceptCustodianship(firstAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the appointed custodian can accept custodianship");
  }

  @Test
  public void
      acceptCustodianship_byAppointedCustodianAndWithAcceptedCustodian_acceptAppointedCustodian() {
    Balance acceptedAndAppointedCustodian =
        Balance.create()
            .mutate(
                m -> {
                  m.appointCustodian(firstAddress);
                  m.acceptCustodian();
                  m.appointCustodian(secondAddress);
                });
    Balance afterAcceptedCustodianship =
        invokeAccount(
            acceptedAndAppointedCustodian, AccountInvocationRpc.acceptCustodianship(secondAddress));
    assertThat(afterAcceptedCustodianship.hasAppointedCustodian()).isFalse();
    assertThat(afterAcceptedCustodianship.hasCustodian()).isTrue();
    assertThat(afterAcceptedCustodianship.isCustodian(secondAddress)).isTrue();
  }

  @Test
  public void acceptCustodianship_withNoAppointedCustodian_fail() {
    Balance noAppointedCustodian = Balance.create();
    assertThatThrownBy(
            () ->
                invokeAccount(
                    noAppointedCustodian, AccountInvocationRpc.acceptCustodianship(firstAddress)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No custodian has been appointed for the account");
  }

  @Test
  public void invokeStakeTokensAsCustodian() {
    long initialTokenBalance = 10;
    Balance accountBefore =
        appointAndAcceptCustodian(tokenAccount(initialTokenBalance), secondAddress).account;

    long stakeAmount = 7;
    byte[] stakeRpc = AccountInvocationRpc.stakeTokens(secondAddress, stakeAmount);

    Balance accountAfter = invokeAccount(accountBefore, stakeRpc);

    assertThat(accountAfter.stakedTokens()).isEqualTo(stakeAmount);
    assertThat(accountAfter.mpcTokens()).isEqualTo(initialTokenBalance - stakeAmount);

    byte[] illegalCustodianInvocation = AccountInvocationRpc.stakeTokens(firstAddress, stakeAmount);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        accountBefore,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void invokeUnstakeTokensAsCustodian() {

    long initialStakedBalance = 10;
    Balance accountBefore =
        appointAndAcceptCustodian(stakeAccount(initialStakedBalance), secondAddress).account;

    long unstakeAmount = 7;
    byte[] unstakeRpc = AccountInvocationRpc.unstakeTokens(secondAddress, unstakeAmount);

    Balance accountAfter = invokeAccount(accountBefore, unstakeRpc);

    assertThat(accountAfter.stakedTokens()).isEqualTo(initialStakedBalance - unstakeAmount);

    byte[] illegalCustodianInvocation =
        AccountInvocationRpc.unstakeTokens(firstAddress, unstakeAmount);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        accountBefore,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void invokeTwoPhaseWithdrawalAsCustodian() {
    long initialTokenBalance = 1000;
    Balance accountBefore =
        appointAndAcceptCustodian(tokenAccount(initialTokenBalance), secondAddress).account;

    long withdrawalAmount = 10;
    byte[] withdrawRpc =
        AccountInvocationRpc.initiateWithdraw(secondAddress, EMPTY, withdrawalAmount);
    Balance accountAfter = invokeAccount(accountBefore, withdrawRpc);

    assertThat(accountAfter.mpcTokens()).isEqualTo(initialTokenBalance - withdrawalAmount);

    byte[] illegalCustodianInvocation =
        AccountInvocationRpc.initiateWithdraw(firstAddress, EMPTY, withdrawalAmount);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        accountBefore,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void invokeCheckVestedTokensAsCustodian() {
    long amount = 2000;

    AccountState<AccountStateLocal, Balance> account =
        appointAndAcceptCustodian(
            createVestingAccount(
                amount, TOKEN_GENERATION_EVENT_0, DURATION_0, INTERVAL_0, Balance.create()),
            secondAddress);

    byte[] checkVestedTokensRpc = AccountInvocationRpc.checkVestedTokens(secondAddress);

    Balance result =
        invokeAndCheckState(() -> 13 * MILLIS_PER_MONTH, global, account, checkVestedTokensRpc)
            .updatedState()
            .account;

    assertThat(result.mpcTokens())
        .isEqualTo(
            outputAmountForVestedTokenFor3MonthsInterval(
                TOKEN_GENERATION_EVENT_0, 13 * MILLIS_PER_MONTH, 36 * MILLIS_PER_MONTH, amount));

    assertThat(result.releasedVestedTokens(0))
        .isEqualTo(
            outputAmountForVestedTokenFor3MonthsInterval(
                TOKEN_GENERATION_EVENT_0, 13 * MILLIS_PER_MONTH, 36 * MILLIS_PER_MONTH, amount));

    byte[] illegalCustodianInvocation = AccountInvocationRpc.checkVestedTokens(firstAddress);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        account,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void invokeCheckPendingUnstakesAsCustodian() {
    long initialStakeAmount = 1000;
    Balance account =
        appointAndAcceptCustodian(stakeAccount(initialStakeAmount), secondAddress).account;

    byte[] checkPendingUnstakesRpc = AccountInvocationRpc.checkPendingUnstakes(secondAddress);

    Balance result = invokeAccount(account, checkPendingUnstakesRpc);

    assertThat(result.pendingUnstakes().size()).isEqualTo(0);

    byte[] illegalCustodianInvocation = AccountInvocationRpc.checkPendingUnstakes(firstAddress);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        account,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void invokeInitiateDelegateStakesAsCustodian() {
    long senderInitialTokenBalance = 10;
    long delegateAmount = 5;
    long recipientInitialTokenBalance = 5;

    Balance senderBefore =
        appointAndAcceptCustodian(tokenAccount(senderInitialTokenBalance), secondAddress).account;
    Balance recipientBefore = tokenAccount(recipientInitialTokenBalance).account;

    byte[] initiateDelegateStakesRpc =
        AccountInvocationRpc.initiateDelegateStakes(
            secondAddress, EMPTY, delegateAmount, secondAddress);
    byte[] initiateReceiveDelegatedStakesRpc =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, delegateAmount, firstAddress);

    Balance senderAfter = invokeAccount(senderBefore, initiateDelegateStakesRpc);
    Balance recipientAfter = invokeAccount(recipientBefore, initiateReceiveDelegatedStakesRpc);

    assertThat(senderAfter.mpcTokens()).isEqualTo(senderInitialTokenBalance - delegateAmount);
    assertThat(recipientAfter.mpcTokens()).isEqualTo(recipientInitialTokenBalance);

    byte[] illegalCustodianInvocation =
        AccountInvocationRpc.initiateDelegateStakes(
            firstAddress, EMPTY, delegateAmount, secondAddress);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        senderBefore,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void invokeRetractDelegatedStakesAsCustodian() {
    long delegateAmount = 15;
    long retractAmount = 5;

    byte[] initiateDelegateStakesRpc =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, delegateAmount, secondAddress);

    Balance account =
        appointAndAcceptCustodian(
                successfullyDelegateStakes(initiateDelegateStakesRpc, tokenAccount(delegateAmount)),
                secondAddress)
            .account;

    byte[] retractDelegatedStakesRpc =
        AccountInvocationRpc.initiateRetractDelegatedStakes(
            secondAddress, EMPTY, retractAmount, secondAddress);

    Balance result = invokeAccount(account, retractDelegatedStakesRpc);

    assertThat(result.delegatedStakesToOthers().getValue(secondAddress))
        .isEqualTo(delegateAmount - retractAmount);

    byte[] illegalCustodianInvocation =
        AccountInvocationRpc.initiateRetractDelegatedStakes(
            firstAddress, EMPTY, retractAmount, secondAddress);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        account,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void invokeAcceptDelegatedStakesAsCustodian() {

    long pendingAmount = 10;
    int initialTokenBalance = 50;
    long acceptAmount = 6;

    byte[] initiateReceiveDelegatedStakesRpc =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, pendingAmount, secondAddress);

    Balance accountBefore =
        appointAndAcceptCustodian(
                successfullyDelegateStakes(
                    initiateReceiveDelegatedStakesRpc, tokenAccount(initialTokenBalance)),
                secondAddress)
            .account;

    assertThat(
            accountBefore
                .delegatedStakesFromOthers()
                .getValue(secondAddress)
                .pendingDelegatedStakes)
        .isEqualTo(pendingAmount);
    assertThat(
            accountBefore
                .delegatedStakesFromOthers()
                .getValue(secondAddress)
                .acceptedDelegatedStakes)
        .isEqualTo(0);

    byte[] acceptDelegatedStakesRpc =
        AccountInvocationRpc.acceptDelegatedStakes(secondAddress, acceptAmount, secondAddress);

    Balance accountAfter = invokeAccount(accountBefore, acceptDelegatedStakesRpc);

    assertThat(
            accountAfter.delegatedStakesFromOthers().getValue(secondAddress).pendingDelegatedStakes)
        .isEqualTo(pendingAmount - acceptAmount);
    assertThat(
            accountAfter
                .delegatedStakesFromOthers()
                .getValue(secondAddress)
                .acceptedDelegatedStakes)
        .isEqualTo(acceptAmount);

    byte[] illegalCustodianInvocation =
        AccountInvocationRpc.acceptDelegatedStakes(firstAddress, acceptAmount, secondAddress);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        accountBefore,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void invokeReduceDelegatedStakesAsCustodian() {

    long delegateAmount = 20;
    int initialTokenBalance = 50;
    long acceptAmount = 10;
    long reduceAmount = 2;

    byte[] initiateDelegateStakesRpc =
        AccountInvocationRpc.initiateDelegateStakes(EMPTY, delegateAmount, secondAddress);

    Balance accountBefore =
        appointAndAcceptCustodian(
                successfullyDelegateStakes(
                    initiateDelegateStakesRpc, tokenAccount(initialTokenBalance)),
                secondAddress)
            .account
            .mutate(
                m -> {
                  m.commitReceiveDelegatedStakes(secondAddress, delegateAmount, null);
                  m.acceptPendingDelegatedStakes(secondAddress, acceptAmount);
                });

    byte[] reduceDelegatedStakesRpc =
        AccountInvocationRpc.reduceDelegatedStakes(secondAddress, secondAddress, reduceAmount);

    Balance accountAfter = invokeAccount(accountBefore, reduceDelegatedStakesRpc);

    assertThat(
            accountAfter
                .delegatedStakesFromOthers()
                .getValue(secondAddress)
                .acceptedDelegatedStakes)
        .isEqualTo(acceptAmount - reduceAmount);
    assertThat(
            accountAfter.delegatedStakesFromOthers().getValue(secondAddress).pendingDelegatedStakes)
        .isEqualTo(acceptAmount + reduceAmount);

    byte[] illegalCustodianInvocation =
        AccountInvocationRpc.reduceDelegatedStakes(firstAddress, secondAddress, reduceAmount);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        accountBefore,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  public void invokeInitiateTwoPhaseByocCommitAsCustodian() {

    long initialSenderBalance = 20;
    long initialRecipientBalance = 13;
    Unsigned256 transferAmount = Unsigned256.create(7L);

    Balance senderBefore =
        appointAndAcceptCustodian(setCoinAccount(initialSenderBalance), secondAddress).account;
    Balance recipientBefore = setCoinAccount(initialRecipientBalance).account;

    byte[] initiateByocTransferSenderRpc =
        AccountInvocationRpc.initiateByocTransferSender(
            secondAddress, EMPTY, transferAmount, SYMBOL_2);
    byte[] initiateDepositRpc =
        AccountInvocationRpc.initiateDeposit(EMPTY, transferAmount.longValueExact());

    Balance senderAfter = invokeAccount(senderBefore, initiateByocTransferSenderRpc);
    Balance recipientAfter = invokeAccount(recipientBefore, initiateDepositRpc);

    int coinIndex = GasAndCoinAccountPlugin.getCoinIndex(global, SYMBOL_2);
    assertThat(senderAfter.storedPendingTransfers().containsKey(EMPTY)).isTrue();
    assertThat(recipientAfter.storedPendingTransfers().containsKey(EMPTY)).isTrue();

    assertThat(senderAfter.coinBalance(coinIndex).longValueExact())
        .isEqualTo(initialSenderBalance - transferAmount.longValueExact());
    assertThat(recipientAfter.coinBalance(coinIndex).longValueExact())
        .isEqualTo(initialRecipientBalance);

    byte[] illegalCustodianInvocation =
        AccountInvocationRpc.initiateByocTransferSender(
            firstAddress, EMPTY, transferAmount, SYMBOL_2);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        senderBefore,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  void associateIceAsCustodian() {
    long initialStakeAmount = 25;
    long associateAmount = 10;

    Balance accountBefore =
        appointAndAcceptCustodian(stakeAccount(initialStakeAmount), secondAddress).account;

    byte[] initiateIceAssociateRpc =
        AccountInvocationRpc.initiateIceAssociate(
            secondAddress, associateAmount, contractAddress, EMPTY);

    Balance accountAfter = invokeAccount(accountBefore, initiateIceAssociateRpc);

    assertThat(accountAfter.pendingIceStake(EMPTY)).isNotNull();
    assertThat(accountAfter.pendingIceStake(EMPTY).type())
        .isEqualTo(IceStake.IceStakeType.ASSOCIATE);
    assertThat(accountAfter.pendingIceStake(EMPTY).amount()).isEqualTo(associateAmount);
    assertThat(accountAfter.pendingIceStake(EMPTY).address()).isEqualTo(contractAddress);

    byte[] illegalCustodianInvocation =
        AccountInvocationRpc.initiateIceAssociate(
            firstAddress, associateAmount, contractAddress, EMPTY);
    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        accountBefore,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  void disassociateIceAsCustodian() {
    long initialAssociateAmount = 20;
    long disassociateAmount = 7;

    Balance accountBefore =
        appointAndAcceptCustodian(initiateIceStakeAccount(initialAssociateAmount), secondAddress)
            .account
            .mutate(m -> m.commitIce(EMPTY));

    byte[] initiateIceDisassociateRpc =
        AccountInvocationRpc.initiateIceDisassociate(
            secondAddress, disassociateAmount, contractAddress, EMPTY);

    Balance accountAfter = invokeAccount(accountBefore, initiateIceDisassociateRpc);

    assertThat(accountAfter.pendingIceStake(EMPTY)).isNotNull();
    assertThat(accountAfter.pendingIceStake(EMPTY).type())
        .isEqualTo(IceStake.IceStakeType.DISASSOCIATE);

    byte[] illegalCustodianInvocation =
        AccountInvocationRpc.initiateIceDisassociate(
            firstAddress, disassociateAmount, contractAddress, EMPTY);

    assertErrorThrownInInvoke(
        illegalCustodianInvocation,
        accountBefore,
        "Only the custodian of the account is allowed to send this invocation");
  }

  @Test
  void byocWithdrawalFailsForAccountsWithAcceptedCustodian() {
    Balance accountBefore = appointAndAcceptCustodian(setCoinAccount(5000), secondAddress).account;
    byte[] deductCoinsRpc = AccountInvocationRpc.deductCoinBalance(SYMBOL_2, Unsigned256.TEN);
    assertErrorThrownInInvoke(
        deductCoinsRpc, accountBefore, "Disabled for accounts with a custodian");
  }

  @Test
  void transferMpcFromAccountToContract() {
    Unsigned256 transferAmount = Unsigned256.create(80);
    byte[] accountWithdrawalRpc =
        AccountInvocationRpc.initiateWithdraw(EMPTY, transferAmount.longValueExact());
    byte[] contractDepositRpc =
        ContractInvocationRpc.initiateMpcTokenDeposit(EMPTY, transferAmount);
    long accountBalance = 100;
    Balance account = invokeAccount(tokenAccount(accountBalance), accountWithdrawalRpc).account;
    ContractState<AccountStateLocal, ContractStorage> contract =
        invokeContract(createContract(), contractAddress, contractDepositRpc);
    byte[] commitRpc = AccountInvocationRpc.commitTransfer(EMPTY);
    account = invokeAccount(account, commitRpc);
    contract = invokeContract(contract, contractAddress, commitRpc);
    assertThat(contract.contract.getMpcTokens()).isEqualTo(transferAmount.longValueExact());
    assertThat(account.mpcTokens()).isEqualTo(accountBalance - transferAmount.longValueExact());
  }

  @Test
  void transferMpcFromContractToAccount() {
    Unsigned256 transferAmount = Unsigned256.create(80);
    byte[] contractWithdrawalRpc =
        ContractInvocationRpc.initiateMpcTokenWithdrawal(EMPTY, transferAmount);
    byte[] accountDepositRpc =
        AccountInvocationRpc.initiateDeposit(EMPTY, transferAmount.longValueExact());
    long accountBalance = 5;
    Balance account = invokeAccount(tokenAccount(accountBalance), accountDepositRpc).account;
    long contractBalance = 100;
    ContractState<AccountStateLocal, ContractStorage> contract =
        invokeContract(
            createContractWithMpc(contractBalance), contractAddress, contractWithdrawalRpc);
    byte[] commitRpc = AccountInvocationRpc.commitTransfer(EMPTY);
    account = invokeAccount(account, commitRpc);
    contract = invokeContract(contract, contractAddress, commitRpc);
    assertThat(contract.contract.getMpcTokens())
        .isEqualTo(contractBalance - transferAmount.longValueExact());
    assertThat(account.mpcTokens()).isEqualTo(accountBalance + transferAmount.longValueExact());
  }

  @Test
  void transferMpcFromContractToContract() {
    final BlockchainAddress contractAddress2 =
        BlockchainAddress.fromString("020000000000000000000000000000000000000007");
    Unsigned256 transferAmount = Unsigned256.create(80);
    byte[] contractWithdrawalRpc =
        ContractInvocationRpc.initiateMpcTokenWithdrawal(EMPTY, transferAmount);
    byte[] contractDepositRpc =
        ContractInvocationRpc.initiateMpcTokenDeposit(EMPTY, transferAmount);
    ContractState<AccountStateLocal, ContractStorage> receiverContract =
        invokeContract(createContract(), contractAddress, contractDepositRpc);
    long contractBalance = 100;
    ContractState<AccountStateLocal, ContractStorage> senderContract =
        invokeContract(
            createContractWithMpc(contractBalance), contractAddress2, contractWithdrawalRpc);

    byte[] commitRpc = ContractInvocationRpc.commitTransfer(EMPTY);
    receiverContract = invokeContract(receiverContract, contractAddress, commitRpc);
    senderContract = invokeContract(senderContract, contractAddress2, commitRpc);
    assertThat(receiverContract.contract.getMpcTokens()).isEqualTo(transferAmount.longValueExact());
    assertThat(senderContract.contract.getMpcTokens())
        .isEqualTo(contractBalance - transferAmount.longValueExact());
  }

  /**
   * Custodians are not supported on contracts, so it should not be possible to transfer with a
   * custodian from a contract.
   */
  @Test
  void transferMpcFromContractAsCustodianFails() {
    final BlockchainAddress contractAddress2 =
        BlockchainAddress.fromString("020000000000000000000000000000000000000007");
    Unsigned256 transferAmount = Unsigned256.create(80);
    byte[] contractWithdrawalRpc =
        AccountInvocationRpc.initiateWithdraw(firstAddress, EMPTY, transferAmount.longValueExact());
    assertThatThrownBy(
            () ->
                invokeContract(createContractWithMpc(100), contractAddress2, contractWithdrawalRpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Custodians are not supported for contracts");
  }

  /**
   * Aborting a transfer should restore the MPC tokens to the withdrawing contract, that are
   * (temporarily) removed.
   */
  @Test
  void abortContractTransfer() {
    final BlockchainAddress contractAddress2 =
        BlockchainAddress.fromString("020000000000000000000000000000000000000007");
    Unsigned256 transferAmount = Unsigned256.create(80);
    byte[] contractWithdrawalRpc =
        ContractInvocationRpc.initiateMpcTokenWithdrawal(EMPTY, transferAmount);
    byte[] contractDepositRpc =
        ContractInvocationRpc.initiateMpcTokenDeposit(EMPTY, transferAmount);
    ContractState<AccountStateLocal, ContractStorage> receiverContract =
        invokeContract(createContract(), contractAddress, contractDepositRpc);
    long contractBalance = 100;
    ContractState<AccountStateLocal, ContractStorage> senderContract =
        invokeContract(
            createContractWithMpc(contractBalance), contractAddress2, contractWithdrawalRpc);
    assertThat(senderContract.contract.getMpcTokens())
        .isEqualTo(contractBalance - transferAmount.longValueExact());

    byte[] abortRpc = ContractInvocationRpc.abortTransfer(EMPTY);
    receiverContract = invokeContract(receiverContract, contractAddress, abortRpc);
    senderContract = invokeContract(senderContract, contractAddress2, abortRpc);
    assertThat(receiverContract.contract.getMpcTokens()).isEqualTo(0);
    assertThat(senderContract.contract.getMpcTokens()).isEqualTo(contractBalance);
  }

  @Test
  void cannotTransfer0Tokens() {
    final BlockchainAddress contractAddress2 =
        BlockchainAddress.fromString("020000000000000000000000000000000000000007");
    Unsigned256 transferAmount = Unsigned256.create(0);
    byte[] contractWithdrawalRpc =
        ContractInvocationRpc.initiateMpcTokenWithdrawal(EMPTY, transferAmount);
    byte[] contractDepositRpc =
        ContractInvocationRpc.initiateMpcTokenDeposit(EMPTY, transferAmount);
    assertThatThrownBy(() -> invokeContract(createContract(), contractAddress, contractDepositRpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Can not deposit a non-positive amount of MPC token");
    assertThatThrownBy(
            () ->
                invokeContract(createContractWithMpc(100), contractAddress2, contractWithdrawalRpc))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Can not withdraw a non-positive amount of MPC token");
  }

  /** Adding MPC tokens to a contract increases the MPC token balance in the contract state. */
  @Test
  void addMpcTokenstoContract() {
    long amountToAdd = 12345;
    ContractState<AccountStateLocal, ContractStorage> receiverContract =
        invokeContract(
            createContract(), contractAddress, ContractInvocationRpc.addMpcTokens(amountToAdd));
    assertThat(receiverContract.contract.getMpcTokens()).isEqualTo(amountToAdd);
  }

  /** Adding MPC tokens to a contract must be with a positive amount. */
  @Test
  void addNonPositiveMpcTokensToContract() {
    long zeroAmount = 0;
    assertThatThrownBy(
            () ->
                invokeContract(
                    createContract(),
                    contractAddress,
                    ContractInvocationRpc.addMpcTokens(zeroAmount)))
        .hasMessageContaining("Can not add a non-positive amount of MPC tokens");

    // Amount can not be negative either
    long negativeAmount = -1;
    assertThatThrownBy(
            () ->
                invokeContract(
                    createContract(),
                    contractAddress,
                    ContractInvocationRpc.addMpcTokens(negativeAmount)))
        .hasMessageContaining("Can not add a non-positive amount of MPC tokens");
  }

  /** Deducting MPC tokens from a contract decreases the MPC token balance in the contract state. */
  @Test
  void deductMpcTokensFromContract() {
    long amountToDeduct = 123;
    long initialBalance = 200;

    ContractState<AccountStateLocal, ContractStorage> receiverContract =
        invokeContract(
            createContractWithMpc(initialBalance),
            contractAddress,
            ContractInvocationRpc.deductMpcTokens(amountToDeduct));
    assertThat(receiverContract.contract.getMpcTokens()).isEqualTo(initialBalance - amountToDeduct);
  }

  /** Deducting MPC tokens from a contract must be with a positive amount. */
  @Test
  void deductNonPositiveMpcTokensFromContract() {
    long zeroAmount = 0;
    assertThatThrownBy(
            () ->
                invokeContract(
                    createContract(),
                    contractAddress,
                    ContractInvocationRpc.deductMpcTokens(zeroAmount)))
        .hasMessageContaining("Can not deduct a non-positive amount of MPC tokens");

    // Amount can not be negative either
    long negativeAmount = -1;
    assertThatThrownBy(
            () ->
                invokeContract(
                    createContract(),
                    contractAddress,
                    ContractInvocationRpc.deductMpcTokens(negativeAmount)))
        .hasMessageContaining("Can not deduct a non-positive amount of MPC tokens");
  }

  /** It is not possible to deduct more MPC tokens than available on the contract. */
  @Test
  void deductMoreTokensThanAvailable() {
    long tokenBalance = 99;
    long amountToDeduct = 100;
    assertThatThrownBy(
            () ->
                invokeContract(
                    createContractWithMpc(tokenBalance),
                    contractAddress,
                    ContractInvocationRpc.deductMpcTokens(amountToDeduct)))
        .hasMessageContaining("MPC token balance can not be negative");
  }

  /** Adding BYOC to a contract increases that BYOC's balance in the contract state. */
  @Test
  void addByocToContract() {
    long amount = 100;
    ContractStorage contract =
        invokeContract(
                createContract(),
                contractAddress,
                ContractInvocationRpc.addByoc(SYMBOL_0, Unsigned256.create(amount)))
            .contract;
    assertThat(
            contract
                .accountCoins
                .get(GasAndCoinAccountPlugin.getCoinIndex(global, SYMBOL_0))
                .balance
                .longValueExact())
        .isEqualTo(amount);
  }

  /** It is not possible to add a non-positive amount of BYOC. */
  @Test
  void addNonPositiveByocToContract() {
    assertThatThrownBy(
            () ->
                invokeContract(
                    createContract(),
                    contractAddress,
                    ContractInvocationRpc.addByoc(SYMBOL_0, Unsigned256.ZERO)))
        .hasMessageContaining("Can not add a non-positive amount of %s".formatted(SYMBOL_0));
  }

  /** Deducting BYOC from a contract decreases that BYOC's balance in the contract state. */
  @Test
  void deductByocFromContract() {
    long balance = 100;
    long toDeduct = 75;
    ContractStorage contract =
        invokeContract(
                new ContractState<>(
                    null,
                    createContractWithByoc(
                        balance, GasAndCoinAccountPlugin.getCoinIndex(global, SYMBOL_0))),
                contractAddress,
                ContractInvocationRpc.deductByoc(SYMBOL_0, Unsigned256.create(toDeduct)))
            .contract;
    assertThat(
            contract
                .accountCoins
                .get(GasAndCoinAccountPlugin.getCoinIndex(global, SYMBOL_0))
                .balance
                .longValueExact())
        .isEqualTo(balance - toDeduct);
  }

  /** It is not possible to deduct a non-positive amount of BYOC. */
  @Test
  void deductNonPositiveByocFromContract() {
    long balance = 100;
    assertThatThrownBy(
            () ->
                invokeContract(
                    new ContractState<>(
                        null,
                        createContractWithByoc(
                            balance, GasAndCoinAccountPlugin.getCoinIndex(global, SYMBOL_0))),
                    contractAddress,
                    ContractInvocationRpc.deductByoc(SYMBOL_0, Unsigned256.ZERO)))
        .hasMessageContaining("Can not deduct a non-positive amount of %s".formatted(SYMBOL_0));
  }

  /** It is not possible to deduct more BYOC than available on the contract. */
  @Test
  void deductMoreByocThanAvailableOnContract() {
    long balance = 100;
    long toDeduct = 101;
    assertThatThrownBy(
            () ->
                invokeContract(
                    new ContractState<>(
                        null,
                        createContractWithByoc(
                            balance, GasAndCoinAccountPlugin.getCoinIndex(global, SYMBOL_0))),
                    contractAddress,
                    ContractInvocationRpc.deductByoc(SYMBOL_0, Unsigned256.create(toDeduct))))
        .hasMessageContaining(
            "Can not deduct %s %s with a balance of %s".formatted(toDeduct, SYMBOL_0, balance));
  }

  private void assertErrorThrownInInvoke(
      byte[] invocation, AccountState<AccountStateLocal, Balance> sender, String errorMessage) {
    assertThatThrownBy(() -> invokeAndCheckState(() -> 0, global, sender, invocation))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(errorMessage);
  }

  private void assertErrorThrownInInvoke(byte[] invocation, Balance sender, String errorMessage) {
    assertThatThrownBy(() -> invokeAccount(sender, invocation))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(errorMessage);
  }

  private void testFee(AccountState<AccountStateLocal, Balance> state, long toPay) {
    testFee(state, toPay, 0, 0, 0, 0, 0, 0, 0);
  }

  private void testFee(
      AccountState<AccountStateLocal, Balance> state,
      long symbolThreeFees,
      long symbolThreeGasSum) {
    long toPay = 25;
    long expectedBlockchainUsage = 12;
    long symbolThreeBalanceAfter = 0;
    SignedTransaction signedTransaction = mockSignedTransaction(toPay, Accounts.THREE, 0);
    BlockchainAccountPlugin.PayCostResult<AccountStateLocal, Balance> after =
        plugin.payCost(pluginContextDefault, global, state, signedTransaction);
    long expected = toPay - 12;
    assertThat(after.remainingGas()).isEqualTo(expected);

    Balance account = after.accountState().account;
    assertThat(account.coinBalance(3)).isEqualTo(Unsigned256.create(symbolThreeBalanceAfter));
    AccountStateLocal fees = after.accountState().local;
    FixedList<ConvertedExternalCoin> accountCoins = fees.convertedExternalCoins();
    assertThat(coinBalance(accountCoins, 3)).isEqualTo(Unsigned256.create(symbolThreeFees));
    assertThat(convertedGasSum(accountCoins, 3)).isEqualTo(Unsigned256.create(symbolThreeGasSum));
    assertThat(
            after
                .accountState()
                .local
                .getBlockchainUsageForEpoch(pluginContextDefault.blockProductionTime() >>> 21))
        .isEqualTo(Unsigned256.create(expectedBlockchainUsage));
  }

  private void testFee(
      AccountState<AccountStateLocal, Balance> state,
      long toPay,
      long symbolZeroBalanceAfter,
      long symbolOneBalanceAfter,
      long symbolZeroFees,
      long symbolOneFees,
      long symbolZeroGasSum,
      long symbolOneGasSum,
      long blockchainUsage) {
    SignedTransaction signedTransaction = mockSignedTransaction(toPay, Accounts.THREE, 0);
    BlockchainAccountPlugin.PayCostResult<AccountStateLocal, Balance> after =
        plugin.payCost(pluginContextDefault, global, state, signedTransaction);
    long expected = 0;
    if (toPay != 0) {
      expected = toPay - 12;
      blockchainUsage += 12;
    }
    assertThat(after.remainingGas()).isEqualTo(expected);

    Balance account = after.accountState().account;
    assertThat(account.coinBalance(0)).isEqualTo(Unsigned256.create(symbolZeroBalanceAfter));
    assertThat(account.coinBalance(1)).isEqualTo(Unsigned256.create(symbolOneBalanceAfter));
    AccountStateLocal fees = after.accountState().local;
    FixedList<ConvertedExternalCoin> accountCoins = fees.convertedExternalCoins();
    assertThat(coinBalance(accountCoins, 0)).isEqualTo(Unsigned256.create(symbolZeroFees));
    assertThat(coinBalance(accountCoins, 1)).isEqualTo(Unsigned256.create(symbolOneFees));
    assertThat(convertedGasSum(accountCoins, 0)).isEqualTo(Unsigned256.create(symbolZeroGasSum));
    assertThat(convertedGasSum(accountCoins, 1)).isEqualTo(Unsigned256.create(symbolOneGasSum));
    assertThat(
            after
                .accountState()
                .local
                .getBlockchainUsageForEpoch(pluginContextDefault.blockProductionTime() >>> 21))
        .isEqualTo(Unsigned256.create(blockchainUsage));
  }

  private Unsigned256 coinBalance(FixedList<ConvertedExternalCoin> accountCoins, int index) {
    if (accountCoins.size() > index) {
      return accountCoins.get(index).getConvertedCoins();
    } else {
      return Unsigned256.ZERO;
    }
  }

  private Unsigned256 convertedGasSum(FixedList<ConvertedExternalCoin> accountCoins, int index) {
    if (accountCoins.size() > index) {
      return accountCoins.get(index).getConvertedGasSum();
    } else {
      return Unsigned256.ZERO;
    }
  }

  private void testFeeInsufficientFunds(
      AccountState<AccountStateLocal, Balance> state, long toPay) {
    assertThatThrownBy(
            () ->
                plugin.payCost(
                    pluginContextDefault,
                    global,
                    state,
                    mockSignedTransaction(toPay, Accounts.THREE, 0)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Calls to this function should be guarded with canCoverFee");

    assertThat(testCanCoverFee(state, toPay, 0, 0)).isFalse();
  }

  private boolean testCanCoverFee(
      AccountState<AccountStateLocal, Balance> initial,
      long cost,
      long epoch,
      int additionalNetworkFee) {
    return plugin.canCoverCost(
        () -> epoch,
        global,
        initial,
        mockSignedTransaction(cost, Contracts.GOV_CONTRACT, additionalNetworkFee));
  }

  private SignedTransaction mockSignedTransaction(
      long cost, BlockchainAddress contractAddress, int additionalNetworkFee) {
    CoreTransactionPart core = new CoreTransactionPart(0, 1, cost);
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contractAddress, new byte[additionalNetworkFee]);
    KeyPair keyPair = new KeyPair(BigInteger.ONE);
    return SignedTransaction.create(core, interact).sign(keyPair, "ChainId");
  }

  private AccountState<AccountStateLocal, Balance> setCoinAccount(long coinBalance) {
    BalanceMutable balance = Balance.create().asMutable();
    balance.addCoins(2, Unsigned256.create(coinBalance));
    return new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());
  }

  private AccountState<AccountStateLocal, Balance> setCoinAccount(Unsigned256 coinBalance) {
    BalanceMutable balance = Balance.create().asMutable();
    balance.addCoins(2, coinBalance);
    return new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());
  }

  private AccountState<AccountStateLocal, Balance> tokenAccount(long tokenBalance) {
    BalanceMutable balance = Balance.create().asMutable();
    balance.addMpcTokenBalance(tokenBalance);
    return new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());
  }

  private AccountState<AccountStateLocal, Balance> stakeAccount(long stakeBalance) {
    BalanceMutable balance = Balance.create().asMutable();
    balance.addMpcTokenBalance(stakeBalance);
    balance.stakeTokens(stakeBalance);
    return new AccountState<>(AccountStateLocal.initial(), balance.asImmutable());
  }

  /**
   * Get the sum of all accepted delegated stakes for account.
   *
   * @return sum of all accepted delegated stakes for account
   */
  public long getSumOfAcceptedStakes(BalanceMutable balance) {
    long totalAmountOfAcceptedDelegatedStakes = 0;
    for (StakesFromOthers stakesFromOthers : balance.delegatedStakesFromOthers().values()) {
      totalAmountOfAcceptedDelegatedStakes += stakesFromOthers.acceptedDelegatedStakes;
    }
    return totalAmountOfAcceptedDelegatedStakes;
  }

  private AccountState<AccountStateLocal, Balance> createVestingAccount(
      long tokens,
      long tokenGenerationEvent,
      long vestingDuration,
      long vestingInterval,
      Balance balance) {
    BalanceMutable balanceMutable = balance.asMutable();
    balanceMutable.createVestingAccount(
        tokens, tokenGenerationEvent, vestingDuration, vestingInterval);
    return new AccountState<>(AccountStateLocal.initial(), balanceMutable.asImmutable());
  }

  private AccountState<AccountStateLocal, Balance> appointAndAcceptCustodian(
      AccountState<AccountStateLocal, Balance> accountState, BlockchainAddress custodianAddress) {
    BalanceMutable balance = accountState.account.asMutable();
    balance.appointCustodian(custodianAddress);
    balance.acceptCustodian();
    return new AccountState<>(accountState.local, balance.asImmutable());
  }

  private long outputAmountForVestedTokenFor3MonthsInterval(
      long start, long stop, long duration, long tokens) {
    double threeMonths = (double) 3 * MILLIS_PER_MONTH;
    double amountOfTimes = Math.floor((stop - start) / Math.floor(threeMonths));
    double amount =
        Math.min(
            amountOfTimes * Math.floor(tokens / (duration / Math.floor(threeMonths))),
            (double) tokens);
    return (long) amount;
  }

  private void checkDelegationInfo(
      BalanceMutable mutable,
      BlockchainAddress delegator,
      Long timestamp,
      long pendingAmount,
      long acceptedAmount) {
    assertThat(mutable.delegatedStakesFromOthers().getValue(delegator).expirationTimestamp)
        .isEqualTo(timestamp);
    assertThat(mutable.delegatedStakesFromOthers().getValue(delegator).pendingDelegatedStakes)
        .isEqualTo(pendingAmount);
    assertThat(mutable.delegatedStakesFromOthers().getValue(delegator).acceptedDelegatedStakes)
        .isEqualTo(acceptedAmount);
  }

  private BalanceMutable delegateAndAccept(
      BalanceMutable accountStateBalance,
      long delegationAmount,
      long acceptAmount,
      BlockchainAddress delegator) {
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakes(EMPTY, delegationAmount, delegator);
    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(
            initiateReceiveDelegatedStakes,
            new AccountState<>(AccountStateLocal.initial(), accountStateBalance.asImmutable()));
    BalanceMutable balance = recipientAfterCommit.account.asMutable();

    if (acceptAmount > 0) {
      balance.acceptPendingDelegatedStakes(delegator, acceptAmount);
    }

    return balance;
  }

  private BalanceMutable delegateAndAccept(
      BalanceMutable accountStateBalance,
      long delegationAmount,
      long acceptAmount,
      BlockchainAddress delegator,
      Long expirationTimestamp) {
    byte[] initiateReceiveDelegatedStakes =
        AccountInvocationRpc.initiateReceiveDelegatedStakesWithExpiration(
            EMPTY, delegationAmount, delegator, expirationTimestamp);
    AccountState<AccountStateLocal, Balance> recipientAfterCommit =
        successfullyDelegateStakes(
            initiateReceiveDelegatedStakes,
            new AccountState<>(AccountStateLocal.initial(), accountStateBalance.asImmutable()));
    BalanceMutable balance = recipientAfterCommit.account.asMutable();

    if (acceptAmount > 0) {
      balance.acceptPendingDelegatedStakes(delegator, acceptAmount);
    }

    return balance;
  }

  private BalanceMutable setDelegationExpirationTime(
      BalanceMutable accountStateBalance, Long expirationTimestamp, BlockchainAddress delegator) {
    byte[] setExpirationTimestampRpc =
        AccountInvocationRpc.setExpirationTimestampForDelegatedStakes(
            null, delegator, expirationTimestamp);
    AccountState<AccountStateLocal, Balance> updatedAccountState =
        invokeAndCheckState(
                () -> 0L,
                global,
                new AccountState<>(AccountStateLocal.initial(), accountStateBalance.asImmutable()),
                setExpirationTimestampRpc)
            .updatedState();
    return updatedAccountState.account.asMutable();
  }

  private BalanceMutable setAssociationExpirationTime(
      BalanceMutable accountStateBalance,
      Long expirationTimestamp,
      BlockchainAddress contractAddress) {
    byte[] setExpirationTimestampRpc =
        AccountInvocationRpc.setExpirationTimestampForAssociation(
            contractAddress, expirationTimestamp);
    PluginContext pluginContext = () -> 0L;
    AccountStateGlobal global1 = global;
    AccountState<AccountStateLocal, Balance> accountState =
        new AccountState<>(AccountStateLocal.initial(), accountStateBalance.asImmutable());
    AccountState<AccountStateLocal, Balance> updatedAccountState =
        invokeAndCheckState(pluginContext, global1, accountState, setExpirationTimestampRpc)
            .updatedState();
    return updatedAccountState.account.asMutable();
  }

  private BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>>
      invokeAndCheckState(
          PluginContext pluginContext,
          AccountStateGlobal global,
          AccountState<AccountStateLocal, Balance> accountState,
          byte[] rpc) {

    BlockchainPlugin.InvokeResult<AccountState<AccountStateLocal, Balance>>
        accountStateInvokeResult = plugin.invokeAccount(pluginContext, global, accountState, rpc);
    Balance account = accountStateInvokeResult.updatedState().account;
    BalanceMutable mutable = account.asMutable();
    long availableForJobs = mutable.stakeForJobs();
    long inUseForJobs = account.stakedToContract().values().stream().mapToLong(s -> s.amount).sum();
    assertThat(availableForJobs).isGreaterThanOrEqualTo(inUseForJobs);

    long pending =
        mutable.delegatedStakesFromOthers().values().stream()
            .mapToLong(s -> s.pendingDelegatedStakes)
            .sum();
    long accepted =
        mutable.delegatedStakesFromOthers().values().stream()
            .mapToLong(s -> s.acceptedDelegatedStakes)
            .sum();
    long stakingGoal = mutable.stakingGoal();
    if (stakingGoal > 0) {
      assertThat(accepted).isLessThanOrEqualTo(stakingGoal);
      if (pending > 0 && mutable.stakedTokens() < stakingGoal) {
        assertThat(accepted + mutable.stakedTokens()).isEqualTo(stakingGoal);
      }
    }
    return accountStateInvokeResult;
  }

  private BalanceMutable associateWithTimestamp(
      BalanceMutable balance,
      long amount,
      Long expirationTimestamp,
      BlockchainAddress contractAddress) {
    byte[] associateWithExpiration =
        AccountInvocationRpc.associateWithExpiration(amount, contractAddress, expirationTimestamp);
    AccountState<AccountStateLocal, Balance> afterAssociation =
        invokeAndCheckState(
                pluginContextDefault,
                global,
                new AccountState<>(AccountStateLocal.initial(), balance.asImmutable()),
                associateWithExpiration)
            .updatedState();

    return afterAssociation.account.asMutable();
  }

  private BalanceMutable associate(
      BalanceMutable balance, long amount, BlockchainAddress contractAddress) {
    byte[] associateWithExpiration = AccountInvocationRpc.associate(amount, contractAddress);
    AccountState<AccountStateLocal, Balance> afterAssociation =
        invokeAndCheckState(
                pluginContextDefault,
                global,
                new AccountState<>(AccountStateLocal.initial(), balance.asImmutable()),
                associateWithExpiration)
            .updatedState();

    return afterAssociation.account.asMutable();
  }

  private String stakeTooMuchErrorMessage(
      long stakeAmount, long mpcTokenBalance, long leftToRelease, long pendingUnstakes) {
    return "Cannot stake "
        + stakeAmount
        + " tokens with a balance of "
        + mpcTokenBalance
        + " and "
        + leftToRelease
        + " vested tokens left to release"
        + " and "
        + pendingUnstakes
        + " pending unstakes.";
  }

  private ContractState<AccountStateLocal, ContractStorage> invokeContract(
      ContractState<AccountStateLocal, ContractStorage> before,
      BlockchainAddress contractAddress,
      byte[] rpc) {
    return plugin.invokeContract(() -> 0, global, before, contractAddress, rpc).updatedState();
  }

  private ContractStorage createContractWithByoc(long amount, int coinIndex) {
    var coins =
        AccountCoin.COINS_LIST.withUpdatedEntry(
            FixedList.create(),
            coinIndex,
            accountCoin -> accountCoin.add(Unsigned256.create(amount)));
    return new ContractStorage(
        0L, Signed256.create(), 0L, coins, AvlTree.create(), AvlTree.create(), AvlTree.create(), 0);
  }

  private AccountState<AccountStateLocal, Balance> successfullyDelegateStakes(
      byte[] delegateStakesRpc, AccountState<AccountStateLocal, Balance> accountState) {
    byte[] commitDelegateStakes = AccountInvocationRpc.commitDelegateStakes(EMPTY);

    AccountState<AccountStateLocal, Balance> updatedAccountState =
        invokeAndCheckState(() -> 0L, global, accountState, delegateStakesRpc).updatedState();

    return invokeAndCheckState(() -> 0L, global, updatedAccountState, commitDelegateStakes)
        .updatedState();
  }

  long unusedStakes(BalanceMutable balance) {
    return balance.stakeForJobs() - associatedTokens(balance);
  }

  private long associatedTokens(BalanceMutable balance) {
    long sumOfAssociated = 0;
    for (BlockchainAddress address : balance.stakedToContract().keySet()) {
      sumOfAssociated += balance.stakedToContract().getValue(address).amount;
    }
    return sumOfAssociated;
  }

  private AccountState<AccountStateLocal, Balance> successfullyBurnTokens(
      BlockchainAddress contractAddress, Balance balance, long stakeAmount, long burnAmount) {
    BalanceMutable balanceMutable = balance.asMutable();
    if (stakeAmount > 0) {
      balanceMutable.stakeTokens(stakeAmount);
    }
    balanceMutable.associateWithExpiration(contractAddress, burnAmount, null);

    AccountState<AccountStateLocal, Balance> accountStateBefore =
        new AccountState<>(tokenAccount(3).local, balanceMutable.asImmutable());

    byte[] rpc = AccountInvocationRpc.burnMpcTokens(burnAmount, contractAddress);
    return invokeAndCheckState(pluginContextDefault, global, accountStateBefore, rpc)
        .updatedState();
  }

  private ContractState<AccountStateLocal, ContractStorage> createContract() {
    return createContractWithMpc(0);
  }

  private ContractState<AccountStateLocal, ContractStorage> createContractWithMpc(long mpcTokens) {
    return new ContractState<>(
        AccountStateLocal.initial(),
        new ContractStorage(
            0L,
            Signed256.create(),
            0L,
            FixedList.create(),
            AvlTree.create(),
            AvlTree.create(),
            AvlTree.create(),
            mpcTokens));
  }

  private AccountState<AccountStateLocal, Balance> initiateIceStakeAccount(long amount) {
    // stake tokens
    AccountState<AccountStateLocal, Balance> account = tokenAccount(25);
    byte[] stakeRpc = AccountInvocationRpc.stakeTokens(amount);
    account = invokeAndCheckState(pluginContextDefault, global, account, stakeRpc).updatedState();
    // associate to contract
    byte[] accountRpc = AccountInvocationRpc.initiateIceAssociate(amount, contractAddress, EMPTY);
    return invokeAndCheckState(pluginContextDefault, global, account, accountRpc).updatedState();
  }

  private ContractState<AccountStateLocal, ContractStorage> initiateIceStakeContract(long amount) {
    byte[] contractRpc = ContractInvocationRpc.initiateIceAssociate(EMPTY, amount, firstAddress);
    ContractState<AccountStateLocal, ContractStorage> contract = createContract();
    return plugin
        .invokeContract(pluginContextDefault, global, contract, contractAddress, contractRpc)
        .updatedState();
  }

  private long getAccountCoinBalance(
      String symbol, AccountState<AccountStateLocal, Balance> accountState) {
    int coinIndex = GasAndCoinAccountPlugin.getCoinIndex(global, symbol);
    return accountState.account.coinBalance(coinIndex).longValueExact();
  }

  private Unsigned256 getAccountCoinBalanceAsUnsigned256(
      String symbol, AccountState<AccountStateLocal, Balance> accountState) {
    int coinIndex = GasAndCoinAccountPlugin.getCoinIndex(global, symbol);
    return accountState.account.coinBalance(coinIndex);
  }

  private AccountState<AccountStateLocal, Balance> invokeAccount(
      AccountState<AccountStateLocal, Balance> before, byte[] rpc) {
    return invokeAndCheckState(pluginContextDefault, global, before, rpc).updatedState();
  }

  private Balance invokeAccount(Balance before, byte[] rpc) {
    return invokeAndCheckState(
            pluginContextDefault,
            global,
            new AccountState<>(AccountStateLocal.initial(), before),
            rpc)
        .updatedState()
        .account;
  }

  private record OldAccountStateLocal(
      FixedList<ConvertedExternalCoin> convertedExternalCoins,
      AvlTree<BlockchainAddress, Unsigned256> serviceFees,
      AvlTree<Long, Unsigned256> blockchainUsage,
      Unsigned256 infrastructureFeesSum)
      implements StateSerializable {}

  private static final class TestBlockContext implements BlockContext<ContractStorage> {

    private final List<BlockchainAddress> registeredUpdates = new ArrayList<>();
    private final long blockTime;
    private final long blockProductionTime;

    @Override
    public Block currentBlock() {
      return new Block(blockProductionTime, blockTime, 10, EMPTY, EMPTY, List.of(), List.of());
    }

    @Override
    public void registerContractUpdate(BlockchainAddress blockchainAddress) {
      registeredUpdates.add(blockchainAddress);
    }

    @Override
    public AvlTree<BlockchainAddress, ContractStorage> contracts() {
      AvlTree<BlockchainAddress, ContractStorage> contracts = AvlTree.create();
      return contracts
          .set(
              Contracts.ONE,
              new ContractStorage(
                  0L,
                  Signed256.create(),
                  0L,
                  FixedList.create(),
                  AvlTree.create(),
                  AvlTree.create(),
                  AvlTree.create(),
                  0))
          .set(
              Contracts.TWO,
              new ContractStorage(
                  0L,
                  Signed256.create(),
                  0L,
                  FixedList.create(),
                  AvlTree.create(),
                  AvlTree.create(),
                  AvlTree.create(),
                  0))
          .set(
              Contracts.THREE,
              new ContractStorage(
                  0L,
                  Signed256.create(),
                  0L,
                  FixedList.create(),
                  AvlTree.create(),
                  AvlTree.create(),
                  AvlTree.create(),
                  0));
    }

    TestBlockContext(long firstTime, long blockProductionTime) {
      this.blockTime = firstTime;
      this.blockProductionTime = blockProductionTime;
    }
  }
}
