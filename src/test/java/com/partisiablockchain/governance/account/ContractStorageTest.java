package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.MemoryStateStorage;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractStorageTest {

  private static final BlockchainAddress address =
      BlockchainAddress.fromString("000000000000000000000000000000000000000008");

  private static final Hash EMPTY = Hash.create(FunctionUtility.noOpConsumer());

  private final GasAndCoinAccountPlugin plugin = new GasAndCoinAccountPlugin();

  @Test
  public void verifySerialization() {
    Hash pendingIceStakeHash = Hash.create(s -> s.writeString("pending ice"));
    IceStake iceStake = new IceStake(address, 10, IceStake.IceStakeType.ASSOCIATE);
    ContractStorage withEverything =
        new ContractStorage(
            86654L,
            Signed256.create(Unsigned256.create(123)),
            1234L,
            FixedList.create(List.of(AccountCoin.create().add(Unsigned256.TEN))),
            AvlTree.create(Map.of(EMPTY, TransferInformation.createForDeposit(123, 3))),
            AvlTree.create(Map.of(address, 20L)),
            AvlTree.create(Map.of(pendingIceStakeHash, iceStake)),
            123);

    StateSerializer serializer = new StateSerializer(new MemoryStateStorage(), true);
    SerializationResult result = serializer.write(withEverything);
    ContractStorage read = serializer.read(result.hash(), ContractStorage.class);

    StateSerializableEquality.assertStatesEqual(read, withEverything);
  }

  @Test
  public void migrateContractStorage() {
    ContractStorage contractStorage =
        new ContractStorage(
            null,
            Signed256.create(),
            123L,
            FixedList.create(List.of(AccountCoin.create().add(Unsigned256.TEN))),
            AvlTree.create(),
            AvlTree.create(),
            AvlTree.create(),
            0);
    StateAccessor contractStorageAccessor = StateAccessor.create(contractStorage);
    StateSerializableEquality.assertStatesEqual(
        plugin.migrateContract(Contracts.ONE, contractStorageAccessor),
        ContractStorage.createFromStateAccessor(contractStorageAccessor));

    contractStorage = contractStorage.addBalance(Unsigned256.TEN);
    contractStorage = contractStorage.updateLatestStorageFeeTime(100L);
    StateAccessor contractStorageAccessorUpdated = StateAccessor.create(contractStorage);
    ContractStorage contractStorageUpdated =
        plugin.migrateContract(Contracts.ONE, contractStorageAccessorUpdated);
    StateSerializableEquality.assertStatesEqual(contractStorage, contractStorageUpdated);
  }

  @Test
  void updateIceStake() {
    ContractStorage contract = new ContractStorage().addIceStakes(address, 100);
    Assertions.assertThat(contract.getIceStakeBy(address)).isEqualTo(100);

    contract = contract.addIceStakes(address, 10);
    Assertions.assertThat(contract.getIceStakeBy(address)).isEqualTo(110);

    contract = contract.subtractIceStakes(address, 90);
    Assertions.assertThat(contract.getIceStakeBy(address)).isEqualTo(20);

    final var contractFinal = contract;
    Assertions.assertThatThrownBy(() -> contractFinal.subtractIceStakes(address, 21))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Ice stake amount cannot be negative");

    contract = contract.subtractIceStakes(address, 20);
    Assertions.assertThat(contract.getIceStakes().getValue(address)).isNull();
  }

  @Test
  void addBalanceResetsIcedTime() {
    ContractStorage storage = ContractStorage.create(0L).deductBalance(Unsigned256.create(2), 123L);
    Assertions.assertThat(storage.getIcedTime()).isNotNull();
    Assertions.assertThat(storage.getIcedTime()).isEqualTo(123);
    storage = storage.addBalance(Unsigned256.ONE);
    Assertions.assertThat(storage.getIcedTime()).isNotNull();
    Assertions.assertThat(storage.getIcedTime()).isEqualTo(123);
    storage = storage.addBalance(Unsigned256.ONE);
    Assertions.assertThat(storage.getIcedTime()).isNull();
  }

  /** Getting the balance of a coin not in the list of balances yields 0. */
  @Test
  void getCoinBalanceHandlesIndexOutOfBounds() {
    ContractStorage storage = ContractStorage.create(0L);
    Assertions.assertThat(storage.getCoinBalance(0)).isEqualTo(Unsigned256.ZERO);
    Assertions.assertThat(storage.getCoinBalance(1)).isEqualTo(Unsigned256.ZERO);
    Assertions.assertThat(storage.getCoinBalance(Integer.MAX_VALUE)).isEqualTo(Unsigned256.ZERO);
  }

  @Test
  void addAndDeductMpcTokens() {
    ContractStorage storage = ContractStorage.create(0L);
    storage = storage.addMpcToken(7);
    Assertions.assertThat(storage.getMpcTokens()).isEqualTo(7);
    storage = storage.deductMpcToken(3);
    Assertions.assertThat(storage.getMpcTokens()).isEqualTo(4);
    storage = storage.addMpcToken(2);
    Assertions.assertThat(storage.getMpcTokens()).isEqualTo(6);
  }

  @Test
  void mpcTokensCanNotBeNegative() {
    ContractStorage storageWithOneToken = ContractStorage.create(0L).addMpcToken(1);
    ContractStorage storageWith0Tokens = storageWithOneToken.deductMpcToken(1);
    Assertions.assertThat(storageWith0Tokens.getMpcTokens()).isEqualTo(0);
    Assertions.assertThatThrownBy(() -> storageWith0Tokens.deductMpcToken(1))
        .hasMessageContaining("MPC token balance can not be negative");
  }
}
