package com.partisiablockchain.governance.account;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.MemoryStateStorage;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AccountStateGlobalTest {

  private static final String SYMBOL_0 = "sym0";
  private static final String SYMBOL_1 = "sym1";
  private static final byte INVALID_INTERACTION_BYTE = 100;
  private AccountStateGlobal initial;

  /** Setup plugin. */
  @BeforeEach
  public void setUp() {
    initial =
        new AccountStateGlobal()
            .setGasPriceFor1000NetworkInternal(1000)
            .setGasPriceFor1000StorageInternal(1000)
            .setCoinInternal(SYMBOL_0, new Fraction(2, 1))
            .setCoinInternal(SYMBOL_1, new Fraction(3, 1));
  }

  @Test
  public void verifySerialization() {
    AccountStateGlobal withEverything = initial;
    StateSerializer serializer = new StateSerializer(new MemoryStateStorage(), true);
    SerializationResult result = serializer.write(withEverything);
    AccountStateGlobal read = serializer.read(result.hash(), AccountStateGlobal.class);

    Assertions.assertThat(read).isNotNull();
    Assertions.assertThat(read.getGasPriceFor1000Network()).isEqualTo(1000);
    Assertions.assertThat(read.getGasPriceFor1000Storage()).isEqualTo(1000);

    Assertions.assertThat(read.coinSymbols()).containsExactly(SYMBOL_0, SYMBOL_1);
    Assertions.assertThat(read.coinConversionRate(SYMBOL_0)).isEqualTo(new Fraction(2, 1));
    Assertions.assertThat(read.coinConversionRate(SYMBOL_1)).isEqualTo(new Fraction(3, 1));
  }

  @Test
  public void setCoin() {
    Assertions.assertThat(initial.coinSymbols()).containsExactly(SYMBOL_0, SYMBOL_1);

    String nextSymbol = "NewSymbol";
    byte[] setCoinRpc = GlobalInteractionRpc.setCoin(nextSymbol, 55, 1);
    AccountStateGlobal withNew = initial.invoke(SafeDataInputStream.createFromBytes(setCoinRpc));
    Assertions.assertThat(withNew.coinSymbols()).containsExactly(SYMBOL_0, SYMBOL_1, nextSymbol);
    Assertions.assertThat(withNew.coinConversionRate(nextSymbol)).isEqualTo(new Fraction(55, 1));

    AccountStateGlobal overwrite = invoke(withNew, GlobalInteractionRpc.setCoin(nextSymbol, 0, 1));
    Assertions.assertThat(overwrite.coinSymbols()).containsExactly(SYMBOL_0, SYMBOL_1, nextSymbol);
    Assertions.assertThat(overwrite.coinConversionRate("NewSymbol")).isEqualTo(new Fraction(0, 1));

    Assertions.assertThatThrownBy(
            () -> invoke(initial, GlobalInteractionRpc.setCoin("negativeCoin", -1, 1)))
        .hasMessage("Conversion rate for coins has to be non-negative")
        .isInstanceOf(RuntimeException.class);
    Assertions.assertThatThrownBy(
            () -> invoke(initial, GlobalInteractionRpc.setCoin("negativeCoin", 1, -1)))
        .hasMessage("Conversion rate for coins has to be non-negative")
        .isInstanceOf(RuntimeException.class);
    Assertions.assertThatThrownBy(
            () -> invoke(initial, GlobalInteractionRpc.setCoin("zeroDivide", 1, 0)))
        .hasMessage("Conversion rate for coins has to be non-negative")
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void coinNotFound() {
    String symbol = "non-existing";
    Assertions.assertThatThrownBy(() -> initial.coinConversionRate(symbol))
        .hasMessageContaining("Coin not found: ")
        .hasMessageContaining(symbol);
  }

  @Test
  public void setGasPriceFor1000Network() {
    AccountStateGlobal withUpdatedPrice =
        invoke(initial, GlobalInteractionRpc.setGasPriceFor1000Network(7000));
    Assertions.assertThat(withUpdatedPrice.getGasPriceFor1000Network()).isEqualTo(7000);

    AccountStateGlobal withZeroUpdatePrice =
        invoke(initial, GlobalInteractionRpc.setGasPriceFor1000Network(0));
    Assertions.assertThat(withZeroUpdatePrice.getGasPriceFor1000Network()).isEqualTo(0);

    Assertions.assertThatThrownBy(
            () -> invoke(initial, GlobalInteractionRpc.setGasPriceFor1000Network(-1)))
        .hasMessage("Gas price has to be non-negative")
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void setGasPriceFor1000Storage() {
    AccountStateGlobal withUpdatedPrice =
        invoke(initial, GlobalInteractionRpc.setGasPriceFor1000Storage(7000));
    Assertions.assertThat(withUpdatedPrice.getGasPriceFor1000Storage()).isEqualTo(7000);

    AccountStateGlobal withZeroUpdatePrice =
        invoke(initial, GlobalInteractionRpc.setGasPriceFor1000Storage(0));
    Assertions.assertThat(withZeroUpdatePrice.getGasPriceFor1000Storage()).isEqualTo(0);
    Assertions.assertThatThrownBy(
            () -> invoke(initial, GlobalInteractionRpc.setGasPriceFor1000Storage(-1)))
        .hasMessage("Gas price has to be non-negative")
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void setMpcTokensForIcing() {
    AccountStateGlobal withUpdatedPrice =
        invoke(initial, GlobalInteractionRpc.setGasPriceFor1000IceStakes(7000));
    Assertions.assertThat(withUpdatedPrice.getGasPriceFor1000IceStakes()).isEqualTo(7000);

    AccountStateGlobal withZeroUpdatePrice =
        invoke(initial, GlobalInteractionRpc.setGasPriceFor1000IceStakes(0));
    Assertions.assertThat(withZeroUpdatePrice.getGasPriceFor1000IceStakes()).isEqualTo(0);
    Assertions.assertThatThrownBy(
            () -> invoke(initial, GlobalInteractionRpc.setGasPriceFor1000IceStakes(-1)))
        .hasMessage("Number of ice stakes has to be non-negative")
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void invokeInvalidGlobalInteraction() {
    Assertions.assertThatThrownBy(
            () ->
                invoke(
                    initial,
                    GlobalPluginStateUpdate.create(
                            SafeDataOutputStream.serialize(
                                stream -> stream.writeByte(INVALID_INTERACTION_BYTE)))
                        .getRpc()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Interaction does not exist");
  }

  private AccountStateGlobal invoke(AccountStateGlobal stateGlobal, byte[] rpc) {
    return stateGlobal.invoke(SafeDataInputStream.createFromBytes(rpc));
  }
}
